<?php

declare(strict_types=1);

namespace Tests;

use Database\Seeders\TestingDatabaseSeeder;
use Illuminate\Console\BufferedConsoleOutput;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Database\DatabaseManager;
use Illuminate\Foundation\Application;

use const PHP_EOL;

class AppBootstrap
{
    use CreatesApplication;
    private readonly Application $application;
    private readonly DatabaseManager $databaseManager;
    private readonly Kernel $kernel;

    public function __construct(bool $shouldPrepareDatabase = true)
    {
        $this->application = $this->createApplication();
        $this->databaseManager = $this->application->make(DatabaseManager::class);
        $this->kernel = $this->application->make(Kernel::class);

        if ($shouldPrepareDatabase) {
            $this->prepareDatabase();
        }
    }

    private function prepareDatabase(): void
    {
        echo 'Import mysql dump with migrations & directories seeders... ';

        $this->clearDatabase();
        $this->migrateDatabase();
        $this->seedDatabase();

        echo 'success!' . PHP_EOL;
    }

    private function clearDatabase(): void
    {
        $builder = $this->databaseManager->getSchemaBuilder();

        $builder->dropAllTables();
        $builder->dropAllViews();

        $statements = $this->databaseManager->select(
            "SELECT CONCAT('DROP ', ROUTINE_TYPE, ' `', ROUTINE_NAME, '`;') as stmt FROM information_schema.ROUTINES where ROUTINE_SCHEMA = ?",
            [$this->databaseManager->getDatabaseName()],
        );

        foreach ($statements as $statement) {
            $this->databaseManager->unprepared($statement->stmt);
        }
    }

    private function migrateDatabase(): void
    {
        $output = $this->application->make(BufferedConsoleOutput::class);

        $this->kernel->call('migrate', [], $output);

        echo $output->fetch();
    }

    private function seedDatabase(): void
    {
        $this
            ->application
            ->make(TestingDatabaseSeeder::class)
            ->run();
    }
}
