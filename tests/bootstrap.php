<?php

declare(strict_types=1);

use Tests\AppBootstrap;

if (env('LARAVEL_PARALLEL_TESTING') === '1') {
    return;
}

$localOverride = __DIR__ . \DIRECTORY_SEPARATOR . 'bootstrap.override.php';

if (file_exists($localOverride)) {
    require $localOverride;
} else {
    new AppBootstrap(true);
}
