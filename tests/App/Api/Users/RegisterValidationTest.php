<?php

declare(strict_types=1);

namespace Tests\App\Api\Users;

use Domain\GuestHouses\Models\GuestHouse;
use Tests\TestCase;

/**
 * @internal
 *
 * @medium
 *
 * @coversNothing
 */
class RegisterValidationTest extends TestCase
{
    public function testValidationTouristOk(): void
    {
        $this->seed();

        $guestHouse = GuestHouse::query()->first();

        $response = $this->post('/api/v1/auth/validation/tourist', [
            'full_name' => $this->faker->name,
            'phone' => '+79000000000',
            'guest_house_id' => $guestHouse->id,
            'agreement' => true,
        ]);

        $response->assertOk();
    }
}
