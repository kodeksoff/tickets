<?php

declare(strict_types=1);

namespace Tests\App\Api\Confirmations;

use Domain\Confirmations\Models\Confirmation;
use Tests\TestCase;
use Throwable;

/**
 * @internal
 *
 * @medium
 *
 * @coversNothing
 */
class ConfirmationTest extends TestCase
{
    /** @throws Throwable */
    public function testSendPhoneConfirmationCodeOk(): void
    {
        $response = $this->post('/api/v1/confirmations/phones/send', [
            'phone' => '+79111111111',
        ]);

        $response->assertOk();

        $this->assertDatabaseHas(Confirmation::table(), [
            'id' => $response->decodeResponseJson()['payload']['id'],
        ]);
    }

    public function testSendPhoneConfirmationCodeUnprocessable(): void
    {
        $testResponse = $this->post('/api/v1/confirmations/phones/send', [
            'phone' => 'badPhone',
        ]);

        $testResponse->assertUnprocessable();
    }

    /** @throws Throwable */
    public function testPhoneConfirmationApproveOk(): void
    {
        $sendResponse = $this->post('/api/v1/confirmations/phones/send', [
            'phone' => '+79111111111',
        ]);

        $sendResponse->assertOk();

        $this->assertDatabaseHas(Confirmation::table(), [
            'id' => $sendResponse->decodeResponseJson()['payload']['id'],
        ]);

        $response = $this->post('/api/v1/confirmations/phones/approve', [
            'id' => $sendResponse->decodeResponseJson()['payload']['id'],
            'token' => $sendResponse->decodeResponseJson()['payload']['token'],
        ]);

        $this->assertNotNull(
            Confirmation::query()
                ->where('id', $sendResponse->decodeResponseJson()['payload']['id'])
                ->where('token', $sendResponse->decodeResponseJson()['payload']['token'])
                ->first()
                ->executed_at,
        );

        $response->assertOk();
    }

    public function testPhoneConfirmationApproveUnprocessable(): void
    {
        $testResponse = $this->post('/api/v1/confirmations/phones/approve', [
            'id' => '9939cfea-64f7-49f2-892b-3faf4b507baf',
            'token' => '1111',
        ]);

        $testResponse->assertUnprocessable();
    }
}
