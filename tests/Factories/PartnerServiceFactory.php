<?php

declare(strict_types=1);

namespace Tests\Factories;

use Domain\PartnerServices\Models\PartnerService;
use Domain\Users\Enums\UserRole;
use Domain\Users\Models\User;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Collection;
use Spatie\Permission\Models\Role;
use Support\FakerProvider;
use Support\Model;

/**
 * @property Faker|FakerProvider $faker
 *
 * @method Collection<int, PartnerService>|PartnerService make($attributes = [], ?Model $parent = null)
 * @method Collection<int, PartnerService>|PartnerService create($attributes = [], ?Model $parent = null)
 *
 * @extends Factory<PartnerService>
 */
class PartnerServiceFactory extends Factory
{
    protected $model = PartnerService::class;

    /** @return array{user_id: UserFactory} */
    public function definition(): array
    {
        $user = User::factory()->create();

        $user->assignRole(
            Role::query()
                ->where('name', UserRole::PARTNER_SERVICE())
                ->first(),
        );

        return [
            'user_id' => $user,
            'full_name' => fake()->name(),
            'inn' => fake()->unique()->numerify('##########'),
        ];
    }
}
