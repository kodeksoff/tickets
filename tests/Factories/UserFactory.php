<?php

declare(strict_types=1);

namespace Tests\Factories;

use Carbon\CarbonImmutable;
use Domain\Users\Models\User;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Support\FakerProvider;
use Support\Model;

/**
 * @property Faker|FakerProvider $faker
 *
 * @method Collection<int, User>|User make($attributes = [], ?Model $parent = null)
 * @method Collection<int, User>|User create($attributes = [], ?Model $parent = null)
 *
 * @extends Factory<User>
 */
class UserFactory extends Factory
{
    protected $model = User::class;

    /** @return array{name: string, email: string, email_verified_at: CarbonImmutable, password: string, remember_token: string} */
    public function definition(): array
    {
        return [
            'phone' => fake()->unique()->numerify('+790########'),
            'email' => fake()->unique()->safeEmail(),
            'email_verified_at' => now()->toImmutable(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ];
    }
}
