<?php

declare(strict_types=1);

namespace Tests\Factories;

use Domain\Settings\Models\Setting;
use Illuminate\Database\Eloquent\Factories\Factory;

class SettingFactory extends Factory
{
    /** @var string */
    protected $model = Setting::class;

    /** @return array */
    public function definition(): array
    {
        return [
            'environment' => 'production',
            'contacts' => [
                'phone' => '+7 900 000 00 00',
                'telegram_link' => 'https://google.ru',
            ],
        ];
    }
}
