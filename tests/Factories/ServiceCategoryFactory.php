<?php

declare(strict_types=1);

namespace Tests\Factories;

use Domain\ServiceCategories\Models\ServiceCategory;
use Illuminate\Database\Eloquent\Factories\Factory;

class ServiceCategoryFactory extends Factory
{
    protected $model = ServiceCategory::class;

    /** @return array{parent_id: null, title: string, is_published: true, description: string} */
    public function definition(): array
    {
        return [
            'parent_id' => null,
            'title' => fake()->text(20),
            'is_published' => true,
            'description' => fake()->text(),
        ];
    }
}
