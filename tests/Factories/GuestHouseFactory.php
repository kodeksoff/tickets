<?php

declare(strict_types=1);

namespace Tests\Factories;

use Domain\GuestHouses\Models\GuestHouse;
use Domain\PartnerHouses\Models\PartnerHouse;
use Exception;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;
use Support\FakerProvider;
use Support\Model;

/**
 * @property Faker|FakerProvider $faker
 *
 * @method Collection<int, GuestHouse>|GuestHouse make($attributes = [], ?Model $parent = null)
 * @method Collection<int, GuestHouse>|GuestHouse create($attributes = [], ?Model $parent = null)
 *
 * @extends Factory<GuestHouse>
 */
class GuestHouseFactory extends Factory
{
    protected $model = GuestHouse::class;

    /**
     * @return array{partner_house_id: PartnerHouseFactory}
     * @throws Exception
     */
    public function definition(): array
    {
        return [
            'title' => substr(strstr(fake()->company(), ' '), 1),
            'address' => fake()->address(),
            'short_address' => fake()->streetAddress(),
            'partner_house_id' => PartnerHouse::factory(),
            'code' => fake()
                ->unique()
                ->numberBetween(11111, 99999),
        ];
    }
}
