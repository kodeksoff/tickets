<?php

declare(strict_types=1);

namespace Tests\Factories;

use Domain\GuestHouses\Models\GuestHouse;
use Domain\Tourists\Models\Tourist;
use Domain\Users\Enums\UserRole;
use Domain\Users\Models\User;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;
use Spatie\Permission\Models\Role;
use Support\FakerProvider;
use Support\Model;

/**
 * @property Faker|FakerProvider $faker
 *
 * @method Collection<int, Tourist>|Tourist make($attributes = [], ?Model $parent = null)
 * @method Collection<int, Tourist>|Tourist create($attributes = [], ?Model $parent = null)
 *
 * @extends Factory<Tourist>
 */
class TouristFactory extends Factory
{
    protected $model = Tourist::class;

    /** @return array{user_id: UserFactory, guest_house_id: GuestHouseFactory} */
    public function definition(): array
    {
        $user = User::factory()->create();

        $user->assignRole(
            Role::query()
                ->where('name', UserRole::TOURIST())
                ->first(),
        );

        return [
            'user_id' => $user,
            'guest_house_id' => GuestHouse::factory(),
            'full_name' => fake()->name(),
        ];
    }
}
