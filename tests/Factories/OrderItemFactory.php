<?php

declare(strict_types=1);

namespace Tests\Factories;

use Domain\Orders\Models\OrderItem;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;
use Support\FakerProvider;
use Support\Utils\MoneyFactory;

/**
 * @property Faker|FakerProvider $faker
 *
 * @extends Factory<OrderItem>
 */
class OrderItemFactory extends Factory
{
    protected $model = OrderItem::class;

    public function definition(): array
    {
        return [
            'count' => 1,
            'price' => resolve(MoneyFactory::class)->ofMinor(
                fake()->numerify('####00'),
            ),
            'full_price' => resolve(MoneyFactory::class)->ofMinor(
                fake()->numerify('####00'),
            ),
            'booking_price' => resolve(MoneyFactory::class)->ofMinor(
                fake()->numerify('####00'),
            ),
        ];
    }
}
