<?php

declare(strict_types=1);

namespace Tests\Factories;

use Domain\Orders\Enums\OrderStatus;
use Domain\Orders\Models\Order;
use Domain\Users\Enums\UserRole;
use Domain\Users\Models\User;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;
use Spatie\Permission\Models\Role;
use Support\FakerProvider;
use Support\Utils\MoneyFactory;

/**
 * @property Faker|FakerProvider $faker
 *
 * @extends Factory<Order>
 */
class OrderFactory extends Factory
{
    protected $model = Order::class;

    public function definition(): array
    {
        $user = User::factory()->create();

        $user->assignRole(
            Role::query()
                ->where('name', UserRole::TOURIST())
                ->first(),
        );

        return [
            'user_id' => $user,
            'full_price' => resolve(MoneyFactory::class)->ofMinor(
                fake()->numerify('####00'),
            ),
            'booking_price' => resolve(MoneyFactory::class)->ofMinor(
                fake()->numerify('####00'),
            ),
            'status' => OrderStatus::ACTIVE->value,
        ];
    }
}
