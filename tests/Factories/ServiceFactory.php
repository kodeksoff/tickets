<?php

declare(strict_types=1);

namespace Tests\Factories;

use Domain\PartnerServices\Models\PartnerService;
use Domain\Services\Enums\ServiceStatus;
use Domain\Services\Models\Service;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;
use Support\FakerProvider;
use Support\Model;

/**
 * @property Faker|FakerProvider $faker
 *
 * @method Collection<int, Service>|Service make($attributes = [], ?Model $parent = null)
 * @method Collection<int, Service>|Service create($attributes = [], ?Model $parent = null)
 *
 * @extends Factory<Service>
 */
class ServiceFactory extends Factory
{
    /** @var string */
    protected $model = Service::class;

    /** @return array */
    public function definition(): array
    {
        $serviceSeasonFrom = null;
        $serviceSeasonTo = null;

        if (!$isAllYear = fake()->boolean) {
            $serviceSeasonFrom = fake()->numberBetween(1, 4);
            $serviceSeasonTo = fake()->numberBetween(6, 12);
        }

        return [
            'title' => fake()->text(50),
            'description' => fake()->text(),
            'about_included' => fake()->text(),
            'about_additional' => fake()->text(),
            'status' => ServiceStatus::PUBLISHED,
            'partner_service_id' => PartnerService::factory(),
            'service_season_from' => $serviceSeasonFrom,
            'service_season_to' => $serviceSeasonTo,
            'service_all_year' => $isAllYear,
            'fee' => 10,
        ];
    }
}
