<?php

declare(strict_types=1);

namespace Tests\Factories;

use Database\Factories\Collection;
use Domain\PartnerHouses\Models\PartnerHouse;
use Domain\Users\Enums\UserRole;
use Domain\Users\Models\User;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;
use Spatie\Permission\Models\Role;
use Support\FakerProvider;
use Support\Model;

/**
 * @property Faker|FakerProvider $faker
 *
 * @method Collection<int, PartnerHouse>|PartnerHouse make($attributes = [], ?Model $parent = null)
 * @method Collection<int, PartnerHouse>|PartnerHouse create($attributes = [], ?Model $parent = null)
 *
 * @extends Factory<PartnerHouse>
 */
class PartnerHouseFactory extends Factory
{
    protected $model = PartnerHouse::class;

    /** @return array{user_id: UserFactory} */
    public function definition(): array
    {
        $user = User::factory()->create();

        $user->assignRole(
            Role::query()
                ->where('name', UserRole::PARTNER_HOUSE())
                ->first(),
        );

        return [
            'user_id' => $user,
            'full_name' => fake()->name(),
            'inn' => fake()->unique()->numerify('##########'),
        ];
    }
}
