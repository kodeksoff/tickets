<?php

declare(strict_types=1);

namespace Tests\Factories;

use Brick\Math\Exception\NumberFormatException;
use Brick\Math\Exception\RoundingNecessaryException;
use Brick\Money\Exception\UnknownCurrencyException;
use Domain\ServiceBooking\Models\ServiceBooking;
use Illuminate\Database\Eloquent\Factories\Factory;
use Support\Utils\MoneyFactory;

class ServiceBookingFactory extends Factory
{
    /** @var string */
    protected $model = ServiceBooking::class;

    /**
     * @return array
     * @throws NumberFormatException
     * @throws RoundingNecessaryException
     * @throws UnknownCurrencyException
     */
    public function definition(): array
    {
        return [
            'service_id' => 1,
            'title' => fake()->text(50),
            'full_price' => resolve(MoneyFactory::class)->ofMinor(
                fake()->numerify('####00'),
            ),
            'discount_price' => resolve(MoneyFactory::class)->ofMinor(
                fake()->numerify('##00'),
            ),
            'is_default' => false,
            'service_discount_price' => resolve(MoneyFactory::class)->ofMinor(
                fake()->numerify('#00'),
            ),
        ];
    }
}
