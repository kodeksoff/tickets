<?php

declare(strict_types=1);

namespace Tests\Factories;

use Domain\Orders\Models\OrderPayment;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;
use Support\FakerProvider;

/**
 * @property Faker|FakerProvider $faker
 *
 * @extends Factory<OrderPayment>
 */
class OrderPaymentFactory extends Factory
{
    protected $model = OrderPayment::class;

    public function definition(): array
    {
        $statuses = ['new', 'pending', 'succeeded'];

        return [
            'is_paid' => (bool)rand(0, 1),
            'status' => $statuses[array_rand($statuses)],
            'return_url' => 'https://master.gosha.dc.itgro.dev/',
            'acquirer' => 'yookassa',
            'acquirer_payment_id' => fake()->uuid(),
            'confirmation_type' => 'redirect',
        ];
    }
}
