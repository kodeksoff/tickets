<?php

declare(strict_types=1);

namespace Tests\Factories;

use Domain\Tickets\Models\Ticket;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;
use Support\FakerProvider;

/**
 * @property Faker|FakerProvider $faker
 *
 * @extends Factory<Ticket>
 */
class TicketFactory extends Factory
{
    protected $model = Ticket::class;

    public function definition(): array
    {
        return [
            'status' => 'active',
            'number' => fake()
                ->unique()
                ->numberBetween(11111, 99999),
            'code' => fake()
                ->unique()
                ->numberBetween(11111, 99999),
            'closed_at' => now()->addDays(10),
        ];
    }
}
