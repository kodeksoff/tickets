<?php

declare(strict_types=1);

namespace Tests\Factories;

use Domain\AskedQuestions\Models\AskedQuestion;
use Illuminate\Database\Eloquent\Factories\Factory;

class AskedQuestionFactory extends Factory
{
    /** @var string */
    protected $model = AskedQuestion::class;

    /** @return array */
    public function definition(): array
    {
        return [
            'type' => 'default',
            'questions' => [
                [
                    'question' => fake()->text(20),
                    'answer' => fake()->text(250),
                ],
                [
                    'question' => fake()->text(30),
                    'answer' => fake()->text(150),
                ],
                [
                    'question' => fake()->text(20),
                    'answer' => fake()->text(300),
                ],
                [
                    'question' => fake()->text(10),
                    'answer' => fake()->text(200),
                ],
            ],
        ];
    }
}
