#!/usr/bin/env bash

RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[1;34m'
NC='\033[0m'

printf "\n${BLUE}Running composer validate...${NC}\n"
time composer validate
COMPOSER_VALIDATE_EXIT_CODE=$?

printf "\n${BLUE}Running composer install...${NC}\n"
time composer install
COMPOSER_INSTALL_EXIT_CODE=$?

printf "\n${BLUE}Running pint...${NC}\n"
time composer pint
PINT_EXIT_CODE=$?

printf "\n${BLUE}Running phpstan...${NC}\n"
time composer phpstan
PHPSTAN_EXIT_CODE=$?

printf "\n${BLUE}Running tests...${NC}\n"
time composer test:parallel
TESTS_EXIT_CODE=$?

if 'true' &&
    [[ ${COMPOSER_VALIDATE_EXIT_CODE} -eq 0 ]] &&
    [[ ${COMPOSER_INSTALL_EXIT_CODE} -eq 0 ]] &&
    [[ ${RECTOR_EXIT_CODE} -eq 0 ]] &&
    [[ ${PINT_EXIT_CODE} -eq 0 ]] &&
    [[ ${PHPSTAN_EXIT_CODE} -eq 0 ]] &&
    [[ ${TESTS_EXIT_CODE} -eq 0 ]] &&
    'true'; then
    printf "\n${GREEN}THE CODE IS HEALTHY.${NC}\n"

    exit 0
else
    printf "\n${RED}SOMETHING WRONG WITH THE CODE.${NC}\n"

    printf "composer validate exit code: ${BLUE}${COMPOSER_VALIDATE_EXIT_CODE}${NC}\n"
    printf "composer install exit code: ${BLUE}${COMPOSER_INSTALL_EXIT_CODE}${NC}\n"
    printf "rector exit code: ${BLUE}${RECTOR_EXIT_CODE}${NC}\n"
    printf "pint exit code: ${BLUE}${PINT_EXIT_CODE}${NC}\n"
    printf "phpstan exit code: ${BLUE}${PHPSTAN_EXIT_CODE}${NC}\n"
    printf "tests exit code: ${BLUE}${TESTS_EXIT_CODE}${NC}\n"

    exit 1
fi
