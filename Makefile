test:
	./_infrastructure/composer test:parallel

config_cache:
	./_infrastructure/artisan config:cache

cache_clear:
	./_infrastructure/artisan cache:clear

check:
	./_infrastructure/composer check

stan:
	./_infrastructure/composer phpstan

rector:
	./_infrastructure/composer rector

migrate_fresh_seed:
	./_infrastructure/artisan migrate:fresh --seed


