#!/bin/bash

#
# Запуск development-среды
#

set -o errexit
set -o pipefail
set -o nounset

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

if [[ ! -f "${__dir}/.env" ]]; then
    cp "${__dir}/.env.development" "${__dir}/.env"
fi

source "${__dir}/.env"

"${__dir}/ensure-timezone.sh"
"${__dir}/ensure-user-and-group.sh"
"${__dir}/ensure-network.sh"
"${__dir}/ensure-directories.sh"
"${__dir}/ensure-docker-registry-auth.sh"

if [[ ! -f "${__dir}/docker-compose.override.yml" ]]; then
    cp "${__dir}/docker-compose.override.yml.example" "${__dir}/docker-compose.override.yml"
fi

docker-compose up -d --build --no-deps php-base
docker-compose up -d --build --remove-orphans

# Даём службам в контейнерах время запуститься
tm=9
while [ ${tm} -ge 0 ]; do
  echo -ne "${tm}\r"
  tm=$((tm - 1))
  sleep 1
done

if [[ ! -f "${__dir}/../.env" ]]; then
    cp "${__dir}/../.env.example" "${__dir}/../.env"
fi

"${__dir}/php-cli" composer install

"${__dir}/php-cli" artisan key:generate

"${__dir}/php-cli" artisan migrate --force

source "${__dir}/.env"
printf "\nAccess site at http://%s.80\n" "${SUBNET}"
