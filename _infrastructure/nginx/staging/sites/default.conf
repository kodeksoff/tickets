server {
    listen 80 default_server;
    listen 443 ssl http2 default_server;

    ssl_certificate     /etc/nginx/ssl/default.crt;
    ssl_certificate_key /etc/nginx/ssl/default.key;
    ssl_protocols       TLSv1.2 TLSv1.3;
    ssl_ciphers         ECDH+AESGCM:ECDH+AES256:DH+3DES:!ADH:!AECDH:!MD5;
    ssl_session_cache   shared:ssl_session_cache:10m;

    root /var/www/public;
    index index.php index.html;

    error_log /var/log/nginx/nginx-error.log;
    access_log /var/log/nginx/nginx-access.log combined_plus;

    include /etc/nginx/includes/*.conf;
    include /etc/nginx/includes/default/*.conf;

    include /etc/nginx/static.conf;

    location / {
        try_files $uri $uri/ /index.php$is_args$args;
    }

    location ^~ /docs/ {
        try_files $uri $uri/ /index.php$is_args$args;

        proxy_cache off;
    }

    location ~ \.php$ {
        if ($request_method = 'OPTIONS') {
            add_header 'Access-Control-Allow-Origin' '*' always;
            add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS' always;
            #
            # Custom headers and headers various browsers *should* be OK with but aren't
            #
            add_header 'Access-Control-Allow-Headers' 'DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range,X-CSRF-TOKEN,X-Frisbee-Response-Type' always;
            #
            # Tell client that this pre-flight info is valid for 20 days
            #
            add_header 'Access-Control-Max-Age' 1728000;
            add_header 'Content-Type' 'text/plain charset=UTF-8';
            add_header 'Content-Length' 0;
            return 204;
        }

        if ($request_method = 'POST') {
            add_header 'Access-Control-Allow-Origin' '*' always;
            add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS' always;
            add_header 'Access-Control-Allow-Headers' 'DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range,X-CSRF-TOKEN,X-Frisbee-Response-Type' always;
            add_header 'Access-Control-Expose-Headers' 'DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range,X-CSRF-TOKEN,X-Frisbee-Response-Type' always;
        }

        if ($request_method = 'GET') {
            add_header 'Access-Control-Allow-Origin' '*' always;
            add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS' always;
            add_header 'Access-Control-Allow-Headers' 'DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range,X-CSRF-TOKEN,X-Frisbee-Response-Type' always;
            add_header 'Access-Control-Expose-Headers' 'DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range,X-CSRF-TOKEN,X-Frisbee-Response-Type' always;
        }

        try_files $uri /index.php =404;
        fastcgi_pass php-fpm:9000;
        fastcgi_index index.php;
        fastcgi_buffers 16 16k;
        fastcgi_buffer_size 32k;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        #fixes timeouts
        fastcgi_read_timeout 600;
        include fastcgi_params;
    }

    location ~ /\.ht {
        deny all;
    }

    location ~ ^/(status|ping)$ {
        allow all;

        access_log off;

        fastcgi_pass php-fpm:9000;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }
}
