# Expire rules for static content

# No default expire rule. This config mirrors that of apache as outlined in the
# html5-boilerplate .htaccess file. However, nginx applies rules by location,
# the apache rules are defined by type. A consequence of this difference is that
# if you use no file extension in the url and serve html, with apache you get an
# expire time of 0s, with nginx you'd get an expire header of one month in the
# future (if the default expire rule is 1 month). Therefore, do not use a
# default expire rule with nginx unless your site is completely static

# cache.appcache, your document html and data
location ~* \.(?:manifest|appcache|html?|xml|json)$ {
    add_header Cache-Control "max-age=0";
}

# https://laracasts.com/discuss/channels/vite/problem-with-site-in-proudcction-livewirejs-and-appjs-404?page=1&replyId=859244
location ~* \.(jpg|jpeg|gif|png|webp|svg|woff|woff2|ttf|css|js|ico|xml)(\?.*)?$ {
    try_files $uri /index.php?$query_string;
    access_log off;
    log_not_found off;
}

# Media: images, icons, video, audio, HTC
location ~* \.(?:jpg|jpeg|gif|png|ico|cur|gz|svg|mp4|ogg|ogv|webm|htc)$ {
    proxy_buffering        on;
    proxy_cache            static_files_cache;
    proxy_cache_valid      200  1d;
    proxy_cache_use_stale  error timeout invalid_header updating http_500 http_502 http_503 http_504;

    add_header Cache-Control "max-age=31536000";
}

# Media: svgz files are already compressed.
location ~* \.svgz$ {
    gzip off;

    proxy_buffering        on;
    proxy_cache            static_files_cache;
    proxy_cache_valid      200  1d;
    proxy_cache_use_stale  error timeout invalid_header updating http_500 http_502 http_503 http_504;

    add_header Cache-Control "max-age=31536000";
}

# CSS and Javascript
location ~* \.(?:css|js)$ {
    proxy_buffering        on;
    proxy_cache            static_files_cache;
    proxy_cache_valid      200  1d;
    proxy_cache_use_stale  error timeout invalid_header updating http_500 http_502 http_503 http_504;

    add_header Cache-Control "max-age=31536000";
}

# WebFonts
# If you are NOT using cross-domain-fonts.conf, uncomment the following directive
location ~* \.(?:ttf|ttc|otf|eot|woff|woff2)$ {
    proxy_buffering        on;
    proxy_cache            static_files_cache;
    proxy_cache_valid      200  1d;
    proxy_cache_use_stale  error timeout invalid_header updating http_500 http_502 http_503 http_504;

    add_header Cache-Control "max-age=31536000";
}
