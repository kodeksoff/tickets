user itgro;
worker_processes 4;
pid /run/nginx.pid;
daemon off;

events {
    worker_connections 2048;
    multi_accept on;
    use epoll;
}

http {
    resolver 8.8.8.8;
    server_tokens off;
    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;
    keepalive_timeout 15;
    types_hash_max_size 2048;
    client_max_body_size 512M;

    proxy_cache_path /var/cache/nginx levels=1:2 keys_zone=static_files_cache:10m inactive=24h max_size=1g;

    include /etc/nginx/mime.types;
    default_type application/octet-stream;

    log_format combined_plus '$remote_addr - $remote_user [$time_local]'
                             ' "$request" $status $body_bytes_sent "$http_referer"'
                             ' "$http_user_agent" $request_time $upstream_cache_status'
                             ' [$upstream_response_time]';

    access_log /dev/stdout;
    error_log /dev/stderr;

    ssl_protocols TLSv1.2 TLSv1.3;
    ssl_ciphers ECDH+AESGCM:ECDH+AES256:DH+3DES:!ADH:!AECDH:!MD5;

    open_file_cache off;
    charset UTF-8;

    map $http_upgrade $connection_upgrade {
        default upgrade;
        ''      close;
    }

    include /etc/nginx/conf.d/*.conf;
    include /etc/nginx/sites/*.conf;
}
