#!/bin/bash

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

source "${__dir}/.env"

function in_subnet() {
    local ip ip_a mask netmask sub sub_ip rval start end
    local BITMASK=0xFFFFFFFF

    IFS=/ read -r sub mask <<<"${1}"
    IFS=. read -r -a sub_ip <<<"${sub}"
    IFS=. read -r -a ip_a <<<"${2}"

    if [[ ${mask} == "" || ${mask} == "0" ]]; then
       mask=24
    fi

    netmask=$((BITMASK << $((32 - mask)) & BITMASK))

    start=0
    for o in "${sub_ip[@]}"; do
        start=$((start << 8 | o))
    done

    start=$((start & netmask))
    end=$((start | ~netmask & BITMASK))

    ip=0
    for o in "${ip_a[@]}"; do
        ip=$((ip << 8 | o))
    done

    ((ip >= start)) && ((ip <= end)) && rval=1 || rval=0

    echo "${rval}"
}

function find_free_subnet() {
    if [[ ${SUBNET} == "" ]]; then
        SUBNET="192.168.0"
    fi

    local ipcmd
    command -v ip 2>&1 1>/dev/null && ipcmd="ip a" || ipcmd="ifconfig"
    maxcdn_array=$($ipcmd | grep inet | grep -v "::" | awk '{ print $2 }')

    # check subnet from .env
    IP="${SUBNET}.0"
    SUBNET_IS_BUSY=0

    for current_subnet in $maxcdn_array; do
        if [[ $(in_subnet "${current_subnet}" "${IP}") == "1" ]]; then
            SUBNET_IS_BUSY=1
        fi
    done

    # find free subnet
    if [[ ${SUBNET_IS_BUSY} == "1" ]]; then
        i=0
        while [[ ${SUBNET_IS_BUSY} == "1" && i -lt 256 ]]; do
            IP="192.168.${i}.0"
            SUBNET_IS_BUSY=0

            for current_subnet in $maxcdn_array; do
                if [[ $(in_subnet "${current_subnet}" ${IP}) == "1" ]]; then
                    SUBNET_IS_BUSY=1
                fi
            done

            (("i+=1"))
        done
    fi

    # shellcheck disable=SC2001
    echo ${IP} | sed "s~\.0$~~"
}

function get_subnet() {

    local network_name="${1:-}"

    [ -z "${network_name}" ] && return

    local existing_network=$(docker network ls | grep -w "${network_name}" | awk '{ print $2 }')

    if [[ "${existing_network}" != "${network_name}" ]]; then
        echo "Network '${network_name}' does not exist, determining free subnet"

        SUBNET=$(find_free_subnet)

        docker network create --driver=bridge --subnet="${SUBNET}.0/24" --ip-range="${SUBNET}.0/24" --gateway="${SUBNET}.1" "${network_name}"
        echo "Network '${network_name}' created"
    else
        echo "Network '${network_name}' exists"

        # get subnet parameters for SUBNET
        SUBNET=$(
            docker network ls | grep -w "${network_name}" | awk '{ print $1 }' |
                xargs docker network inspect | grep Subnet | awk '{ print $2 }' |
                sed "s~\"~~g" | sed "s~,~~g" | sed "s~\/.*$~~" | sed "s~\.0$~~"
        )
    fi
}

get_subnet "${NETWORK_NAME:-new-project-backend-template}"

## change subnet in .env
EXISTS=$(grep "^SUBNET=.*$" "${__dir}/.env")
if [[ ${EXISTS} == "" ]]; then
    echo "SUBNET=${SUBNET}" >>"${__dir}/.env"
else
    sed -i.bk "s~^SUBNET=.*$~SUBNET=${SUBNET}~" "${__dir}/.env" \
        && rm -f "${__dir}/.env.bk"
fi

get_subnet "${FRONTEND_NETWORK:-}"
