#!/bin/bash

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

source "${__dir}/.env"

REGISTRY_URL=${REGISTRY_URL:-registry.itgro.dev}
REGISTRY_TEST_IMAGE=${REGISTRY_TEST_IMAGE:-images/php/base}

function set_registry_creds() {

    if [[ ${DOCKER_REGISTRY_USER} == "" ]]; then
        printf "\nEnter gitlab.itgro.dev login:"
        read -r DOCKER_REGISTRY_USER
        printf "\n"
    fi

    EXISTS=$(grep "^DOCKER_REGISTRY_USER=.*$" "${__dir}/.env")
    if [[ ${EXISTS} == "" ]]; then
        echo "DOCKER_REGISTRY_USER=${DOCKER_REGISTRY_USER}" >>"${__dir}/.env"
    else
        sed -i.bk "s~^DOCKER_REGISTRY_USER=.*$~DOCKER_REGISTRY_USER=${DOCKER_REGISTRY_USER}~" "${__dir}/.env" \
            && rm -f "${__dir}/.env.bk"
    fi

    if [[ ${DOCKER_REGISTRY_TOKEN} == "" ]]; then
        printf "Enter gitlab.itgro.dev token with 'read_registry rights':\n"
        printf "(you can create new token at https://gitlab.itgro.dev/-/profile/personal_access_tokens)\n"
        read -r DOCKER_REGISTRY_TOKEN
        printf "\n"
    fi

    EXISTS=$(grep "^DOCKER_REGISTRY_TOKEN=.*$" "${__dir}/.env")
    if [[ ${EXISTS} == "" ]]; then
        echo "DOCKER_REGISTRY_TOKEN=${DOCKER_REGISTRY_TOKEN}" >>"${__dir}/.env"
    else
        sed -i.bk "s~^DOCKER_REGISTRY_TOKEN=.*$~DOCKER_REGISTRY_TOKEN=${DOCKER_REGISTRY_TOKEN}~" "${__dir}/.env" \
            && rm -f "${__dir}/.env.bk"
    fi

    docker login ${REGISTRY_URL} -u "${DOCKER_REGISTRY_USER}" -p "${DOCKER_REGISTRY_TOKEN}"
}

docker pull ${REGISTRY_URL}/${REGISTRY_TEST_IMAGE} || set_registry_creds
