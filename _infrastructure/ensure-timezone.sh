#!/bin/bash

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

source "${__dir}/.env"

if [[ ${TIMEZONE} == "" ]]; then
    TIMEZONE=$(cat /etc/timezone)
fi

EXISTS=$(grep "^TIMEZONE=.*$" "${__dir}/.env")
if [[ ${EXISTS} == "" ]]; then
    echo "TIMEZONE=${TIMEZONE}" >>"${__dir}/.env"
else
    sed -i.bk "s~^TIMEZONE=.*$~TIMEZONE=${TIMEZONE}~" "${__dir}/.env" \
        && rm -f "${__dir}/.env.bk"
fi
