#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

source "${__dir}/.env"

if [[ ! -d "${DATA_DIR}" ]]; then
    mkdir -p "${DATA_DIR}"
    sudo chmod 0777 "${DATA_DIR}"
fi

if [[ ! -d "${LOGS_DIR}" ]]; then
    mkdir -p "${LOGS_DIR}"
    sudo chmod 0777 "${LOGS_DIR}"
fi

LOGS_DIRS=(
    "pgsql"
)

for d in ${LOGS_DIRS[@]}; do
    if [[ ! -d "${LOGS_DIR}/${d}" ]]; then
        mkdir -p "${LOGS_DIR}/${d}"
        chmod 0777 "${LOGS_DIR}/${d}"
    fi
done

DATA_DIRS=(
    "pgsql"
    "redis"
    "xhprof"
    "meilisearch"
)

for d in ${DATA_DIRS[@]}; do
    if [[ ! -d "${DATA_DIR}/${d}" ]]; then
        mkdir -p "${DATA_DIR}/${d}"
        chmod 0777 "${DATA_DIR}/${d}"
    fi
done


