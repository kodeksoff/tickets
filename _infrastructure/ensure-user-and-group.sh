#!/bin/bash

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

source "${__dir}/.env"


if [[ ${USER_ID} == "" ]]; then
    USER_ID=$(id -u)
fi

EXISTS=$(grep "^USER_ID=.*$" "${__dir}/.env")
if [[ ${EXISTS} == "" ]]; then
    echo "USER_ID=${USER_ID}" >>"${__dir}/.env"
else
    sed -i.bk "s~^USER_ID=.*$~USER_ID=${USER_ID}~" "${__dir}/.env" \
        && rm -f "${__dir}/.env.bk"
fi


if [[ ${GROUP_ID} == "" ]]; then
    GROUP_ID=$(id -g)
fi

EXISTS=$(grep "^GROUP_ID=.*$" "${__dir}/.env")
if [[ ${EXISTS} == "" ]]; then
    echo "GROUP_ID=${GROUP_ID}" >>"${__dir}/.env"
else
    sed -i.bk "s~^GROUP_ID=.*$~GROUP_ID=${GROUP_ID}~" "${__dir}/.env" \
        && rm -f "${__dir}/.env.bk"
fi
