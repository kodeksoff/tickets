ARG BASE_IMAGE
FROM --platform=linux/amd64 ${BASE_IMAGE}


ARG MODE=development
ENV MODE ${MODE}

ARG PHP_MODE=fpm
ENV PHP_MODE ${PHP_MODE}

###########################################################################
# Config:
###########################################################################

# Базовые настройки
COPY ./_config/php/common/* /etc/php/${PHP_VERSION}/cli/conf.d/
COPY ./_config/php/common/* /etc/php/${PHP_VERSION}/${PHP_MODE}/conf.d/

# Настройки текущей среды
COPY ./_config/php/${MODE}/* /etc/php/${PHP_VERSION}/cli/conf.d/
COPY ./_config/php/${MODE}/* /etc/php/${PHP_VERSION}/${PHP_MODE}/conf.d/

# Локальные переопределения
COPY ./_config/php/override/* /etc/php/${PHP_VERSION}/cli/conf.d/
COPY ./_config/php/override/* /etc/php/${PHP_VERSION}/${PHP_MODE}/conf.d/


###########################################################################
# Check PHP version:
###########################################################################

RUN set -xe; php -v | head -n 1 | grep -q "PHP ${PHP_VERSION}."


#####################################
# This container specifics below:
#####################################

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y --allow-downgrades --allow-remove-essential --allow-change-held-packages \
        php${PHP_VERSION}-fpm \
    && apt-get clean

# Удаляем стандартные настройки
RUN rm /etc/php/${PHP_VERSION}/${PHP_MODE}/pool.d/*.conf

# Базовые настройки
COPY ./fpm/common/* /etc/php/${PHP_VERSION}/${PHP_MODE}/pool.d/

# Настройки текущей среды
COPY ./fpm/${MODE}/* /etc/php/${PHP_VERSION}/${PHP_MODE}/pool.d/

# Локальные переопределения
COPY ./fpm/override/* /etc/php/${PHP_VERSION}/${PHP_MODE}/pool.d/


###########################################################################
# Final Touch
###########################################################################

# Clean up
RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    rm /var/log/lastlog /var/log/faillog

# Set default work directory
WORKDIR /var/www

CMD exec php-fpm${PHP_VERSION}

EXPOSE 9000
