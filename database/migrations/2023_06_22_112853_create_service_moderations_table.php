<?php

declare(strict_types=1);

use Domain\Services\Models\Service;
use Domain\Services\Models\ServiceModeration;
use Domain\Users\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /** @return void */
    public function up(): void
    {
        Schema::create(
            ServiceModeration::table(),
            function (Blueprint $table) {
                $table->id();
                $table
                    ->foreignIdFor(User::class)
                    ->constrained()
                    ->cascadeOnUpdate()
                    ->restrictOnDelete();
                $table
                    ->foreignIdFor(Service::class)
                    ->constrained()
                    ->cascadeOnUpdate()
                    ->restrictOnDelete();
                $table
                    ->string('reason')
                    ->nullable();
                $table
                    ->jsonb('data')
                    ->nullable();
                $table->timestamps();
            },
        );
    }

    /** @return void */
    public function down(): void
    {
        Schema::dropIfExists(ServiceModeration::table());
    }
};
