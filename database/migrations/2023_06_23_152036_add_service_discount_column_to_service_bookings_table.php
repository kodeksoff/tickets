<?php

declare(strict_types=1);

use Domain\ServiceBooking\Models\ServiceBooking;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::table(
            ServiceBooking::table(),
            function (Blueprint $table) {
                /* *
                * postgres не поддерживает after вставку,
                * поэтому костыльно удаляем timestamps и создаем заново
                */
                $table->dropColumn(['created_at', 'updated_at']);
            },
        );

        Schema::table(
            ServiceBooking::table(),
            function (Blueprint $table) {
                $table
                    ->jsonb('service_discount_price')
                    ->default(
                        json_encode(
                            [
                                'amount' => 0,
                                'currency' => 'RUB',
                            ],
                        ),
                    );
                $table
                    ->index('service_discount_price')
                    ->algorithm('gin');
                $table->timestamps();
            },
        );
    }

    public function down(): void
    {
        Schema::table(
            ServiceBooking::table(),
            function (Blueprint $table) {
                $table->dropColumn('service_discount_price');
            },
        );
    }
};
