<?php

declare(strict_types=1);

use Domain\Orders\Models\Order;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::table(
            Order::table(),
            function (Blueprint $table) {
                $table->dropIndex(['status', 'is_paid']);
                $table->dropColumn([
                    'external_id',
                    'acquirer',
                    'confirmation_type',
                    'is_paid',
                    'acquirer_response',
                ]);
                $table
                    ->string('status')
                    ->default('new')
                    ->change();

                $table->index(['status']);
            },
        );
    }

    public function down(): void
    {
        Schema::table(
            Order::table(),
            function (Blueprint $table) {
            },
        );
    }
};
