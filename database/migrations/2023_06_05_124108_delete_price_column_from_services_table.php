<?php

declare(strict_types=1);

use Domain\Services\Models\Service;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /** @return void */
    public function up(): void
    {
        Schema::table(
            Service::table(),
            function (Blueprint $table) {
                $table->dropColumn('price');
            },
        );
    }

    /** @return void */
    public function down(): void
    {
        Schema::table(
            Service::table(),
            function (Blueprint $table) {
                $table
                    ->jsonb('price')
                    ->nullable();
            },
        );
    }
};
