<?php

declare(strict_types=1);

use Domain\ServiceBooking\Models\ServiceBooking;
use Domain\Services\Models\Service;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create(
            ServiceBooking::table(),
            function (Blueprint $table) {
                $table->id();
                $table
                    ->foreignIdFor(Service::class)
                    ->constrained()
                    ->cascadeOnUpdate()
                    ->restrictOnDelete();
                $table
                    ->string('title')
                    ->nullable();
                $table
                    ->jsonb('booking_price')
                    ->nullable();
                $table
                    ->jsonb('full_price')
                    ->nullable();
                $table
                    ->boolean('is_default')
                    ->default(false);
                $table->timestamps();

                $table
                    ->index('booking_price')
                    ->algorithm('gin');
                $table
                    ->index('full_price')
                    ->algorithm('gin');
            },
        );
    }

    public function down(): void
    {
        Schema::dropIfExists(ServiceBooking::table());
    }
};
