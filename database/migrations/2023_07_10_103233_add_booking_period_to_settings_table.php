<?php

declare(strict_types=1);

use Domain\Settings\Models\Setting;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::table(
            Setting::table(),
            function (Blueprint $table) {
                /* *
                * postgres не поддерживает after вставку,
                * поэтому костыльно удаляем timestamps и создаем заново
                */
                $table->dropColumn(['created_at', 'updated_at']);
            },
        );

        Schema::table(
            Setting::table(),
            function (Blueprint $table) {
                $table
                    ->integer('booking_period')
                    ->nullable();
                $table->timestamps();
            },
        );
    }

    public function down(): void
    {
        Schema::table(
            Setting::table(),
            function (Blueprint $table) {
                $table->dropColumn('booking_period');
            },
        );
    }
};
