<?php

declare(strict_types=1);

use Domain\Tourists\Models\Tourist;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create(
            Tourist::table(),
            function (Blueprint $table): void {
                $table->id();
                $table
                    ->foreignId('user_id')
                    ->constrained()
                    ->cascadeOnUpdate()
                    ->restrictOnDelete();
                $table
                    ->foreignId('guest_house_id')
                    ->constrained()
                    ->cascadeOnUpdate()
                    ->restrictOnDelete();
                $table->string('full_name');
                $table->timestamps();
            },
        );
    }

    public function down(): void
    {
        Schema::dropIfExists(Tourist::table());
    }
};
