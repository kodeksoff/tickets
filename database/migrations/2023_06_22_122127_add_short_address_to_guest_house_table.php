<?php

declare(strict_types=1);

use Domain\GuestHouses\Models\GuestHouse;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::table(
            GuestHouse::table(),
            function (Blueprint $table) {
                /* *
                * postgres не поддерживает after вставку,
                * поэтому костыльно удаляем timestamps и создаем заново
                */
                $table->dropColumn(['created_at', 'updated_at']);
            },
        );

        Schema::table(
            GuestHouse::table(),
            function (Blueprint $table): void {
                $table
                    ->string('short_address')
                    ->nullable();
                $table->timestamps();
            }
        );
    }

    public function down(): void
    {
        Schema::dropColumns(GuestHouse::table(), 'short_address');
    }
};
