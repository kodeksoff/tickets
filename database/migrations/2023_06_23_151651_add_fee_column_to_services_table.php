<?php

declare(strict_types=1);

use Domain\Services\Models\Service;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::table(
            Service::table(),
            function (Blueprint $table) {
                /* *
                * postgres не поддерживает after вставку,
                * поэтому костыльно удаляем timestamps и создаем заново
                */
                $table->dropColumn(['created_at', 'updated_at']);
            },
        );

        Schema::table(
            Service::table(),
            function (Blueprint $table) {
                $table
                    ->unsignedInteger('fee')
                    ->default(0);

                $table->timestamps();
            },
        );
    }

    public function down(): void
    {
        Schema::table(
            Service::table(),
            function (Blueprint $table) {
                $table->dropColumn('fee');
            },
        );
    }
};
