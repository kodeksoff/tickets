<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create(
            'confirmations',
            function (Blueprint $table): void {
                $table
                    ->uuid('id')
                    ->primary();
                $table
                    ->string('action')
                    ->nullable();
                $table->string('class');
                $table->string('channel');
                $table->string('deliver_to');
                $table->string('token');
                $table
                    ->json('options')
                    ->nullable();
                $table
                    ->timestamp('expires_at')
                    ->nullable();
                $table
                    ->timestamp('confirmed_at')
                    ->nullable();
                $table
                    ->timestamp('executed_at')
                    ->nullable();
                $table->timestamps();
                $table->index([
                    'created_at',
                    'confirmed_at',
                    'expires_at',
                    'executed_at',
                ]);
                $table->index([
                    'token',
                    'deliver_to',
                ]);
            },
        );
    }

    public function down(): void
    {
        Schema::dropIfExists('confirmations');
    }
};
