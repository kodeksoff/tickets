<?php

declare(strict_types=1);

use Domain\Orders\Models\Order;
use Domain\Services\Models\Service;
use Domain\Users\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create(
            Order::table(),
            function (Blueprint $table) {
                $table->id();
                $table
                    ->uuid('external_id')
                    ->nullable()
                    ->index();
                $table
                    ->foreignIdFor(User::class)
                    ->nullable()
                    ->index()
                    ->constrained()
                    ->cascadeOnUpdate()
                    ->restrictOnDelete();
                $table
                    ->foreignIdFor(Service::class)
                    ->nullable()
                    ->constrained()
                    ->cascadeOnUpdate()
                    ->nullOnDelete();
                $table->string('acquirer');
                $table->string('confirmation_type');
                $table
                    ->string('status')
                    ->default('inactive');
                $table
                    ->boolean('is_paid')
                    ->default(false);
                $table
                    ->jsonb('full_price')
                    ->nullable();
                $table
                    ->jsonb('acquirer_response')
                    ->nullable();
                $table->timestamps();

                $table->index(['status', 'is_paid']);
                $table->index(['created_at']);
            },
        );
    }

    public function down(): void
    {
        Schema::dropIfExists(Order::table());
    }
};
