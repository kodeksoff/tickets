<?php

declare(strict_types=1);

use Domain\PartnerServices\Models\PartnerService;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create(
            PartnerService::table(),
            function (Blueprint $table): void {
                $table->id();
                $table
                    ->foreignId('user_id')
                    ->constrained()
                    ->cascadeOnUpdate()
                    ->restrictOnDelete();
                $table->string('full_name');
                $table
                    ->string('inn')
                    ->nullable();
                $table->timestamps();
            },
        );
    }

    public function down(): void
    {
        Schema::dropIfExists(PartnerService::table());
    }
};
