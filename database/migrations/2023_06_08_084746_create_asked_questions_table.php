<?php

declare(strict_types=1);

use Domain\AskedQuestions\Models\AskedQuestion;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /** @return void */
    public function up(): void
    {
        Schema::create(
            AskedQuestion::table(),
            function (Blueprint $table): void {
                $table->id();
                $table->jsonb('questions');
                $table
                    ->string('type')
                    ->default('default');
                $table->timestamps();
            },
        );
    }

    /** @return void */
    public function down(): void
    {
        Schema::dropIfExists('asked_questions');
    }
};
