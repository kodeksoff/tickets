<?php

declare(strict_types=1);

use Domain\Orders\Models\OrderItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::table(
            OrderItem::table(),
            function (Blueprint $table) {
                /* *
                * postgres не поддерживает after вставку,
                * поэтому костыльно удаляем timestamps и создаем заново
                */
                $table->dropColumn(['created_at', 'updated_at']);
            },
        );

        Schema::table(
            OrderItem::table(),
            function (Blueprint $table) {
                $table
                    ->jsonb('booking_price')
                    ->nullable();
                $table->timestamps();
            }
        );
    }

    public function down(): void
    {
        Schema::table(
            OrderItem::table(),
            function (Blueprint $table) {
                $table->dropColumn('booking_price');
            }
        );
    }
};
