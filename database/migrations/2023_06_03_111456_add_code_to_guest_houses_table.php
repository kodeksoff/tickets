<?php

declare(strict_types=1);

use Domain\GuestHouses\Models\GuestHouse;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::table(
            GuestHouse::table(),
            function (Blueprint $table): void {
                $table
                    ->string('title')
                    ->nullable()
                    ->change();
                $table
                    ->string('address')
                    ->nullable()
                    ->change();
                $table
                    ->integer('code')
                    ->nullable()
                    ->unique();
            }
        );
    }

    public function down(): void
    {
        Schema::dropColumns(GuestHouse::table(), 'code');
    }
};
