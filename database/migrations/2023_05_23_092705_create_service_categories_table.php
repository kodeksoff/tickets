<?php

declare(strict_types=1);

use Domain\ServiceCategories\Models\ServiceCategory;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create(
            ServiceCategory::table(),
            function (Blueprint $table): void {
                $table->id();

                $table
                    ->unsignedBigInteger('parent_id')
                    ->nullable();
                $table
                    ->integer('order')
                    ->default(0)
                    ->index();
                $table
                    ->boolean('is_published')
                    ->default(false)
                    ->index();
                $table->string('title');
                $table
                    ->string('description')
                    ->nullable();
                $table->timestamps();
                $table
                    ->foreign('parent_id')
                    ->references('id')
                    ->on('service_categories')
                    ->cascadeOnUpdate()
                    ->restrictOnDelete();
            },
        );
    }

    public function down(): void
    {
        Schema::dropIfExists('service_categories');
    }
};
