<?php

declare(strict_types=1);

use Domain\GuestHouses\Models\GuestHouse;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create(
            GuestHouse::table(),
            function (Blueprint $table): void {
                $table->id();
                $table
                    ->foreignId('partner_house_id')
                    ->constrained()
                    ->cascadeOnUpdate()
                    ->restrictOnDelete();
                $table->string('title');
                $table->string('address');
                $table->timestamps();
            },
        );
    }

    public function down(): void
    {
        Schema::dropIfExists(GuestHouse::table());
    }
};
