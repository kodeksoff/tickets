<?php

declare(strict_types=1);

use Domain\Users\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create(
            User::table(),
            function (Blueprint $table): void {
                $table->id();
                $table
                    ->string('phone')
                    ->unique()
                    ->nullable();

                $table
                    ->string('email')
                    ->unique()
                    ->nullable();

                $table
                    ->string('password')
                    ->nullable();
                $table->rememberToken();
                $table
                    ->timestamp('phone_verified_at')
                    ->nullable();
                $table
                    ->timestamp('email_verified_at')
                    ->nullable();
                $table->timestamps();

                $table->index(['phone', 'email']);
            },
        );
    }

    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
