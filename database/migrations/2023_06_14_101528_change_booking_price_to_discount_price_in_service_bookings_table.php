<?php

declare(strict_types=1);

use Domain\ServiceBooking\Models\ServiceBooking;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /** @return void */
    public function up(): void
    {
        Schema::table(
            ServiceBooking::table(),
            function (Blueprint $table) {
                $table->renameColumn('booking_price', 'discount_price');

                $table->dropIndex(['booking_price']);

                $table
                    ->index('discount_price')
                    ->algorithm('gin');
            },
        );
    }

    /** @return void */
    public function down(): void
    {
        Schema::table(
            ServiceBooking::table(),
            function (Blueprint $table) {
                $table->renameColumn('discount_price', 'booking_price');

                $table->dropIndex(['discount_price']);

                $table
                    ->index('booking_price')
                    ->algorithm('gin');
            },
        );
    }
};
