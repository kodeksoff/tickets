<?php

declare(strict_types=1);

use Domain\Tourists\Models\Tourist;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::table(
            Tourist::table(),
            function (Blueprint $table): void {
                $table
                    ->string('full_name')
                    ->nullable()
                    ->change();
                $table
                    ->foreignId('guest_house_id')
                    ->nullable()
                    ->change();
            }
        );
    }

    public function down(): void
    {
    }
};
