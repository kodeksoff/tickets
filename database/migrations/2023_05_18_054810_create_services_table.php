<?php

declare(strict_types=1);

use Domain\Services\Models\Service;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create(
            Service::table(),
            function (Blueprint $table): void {
                $table->id();
                $table
                    ->foreignId('partner_service_id')
                    ->constrained()
                    ->cascadeOnUpdate()
                    ->restrictOnDelete();
                $table
                    ->jsonb('category_ids')
                    ->nullable();
                $table
                    ->boolean('is_published')
                    ->default(false)
                    ->index();
                $table
                    ->string('title')
                    ->nullable();
                $table
                    ->string('description')
                    ->nullable();
                $table
                    ->string('about_included')
                    ->nullable();
                $table
                    ->string('about_additional')
                    ->nullable();
                $table->timestamps();

                $table
                    ->index('category_ids')
                    ->algorithm('gin');
            },
        );
    }

    public function down(): void
    {
        Schema::dropIfExists(Service::table());
    }
};
