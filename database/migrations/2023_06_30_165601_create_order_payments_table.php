<?php

declare(strict_types=1);

use Domain\Orders\Models\Order;
use Domain\Orders\Models\OrderPayment;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create(OrderPayment::table(), function (Blueprint $table) {
            $table->id();
            $table
                ->foreignIdFor(Order::class)
                ->constrained()
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            $table
                ->string('status')
                ->default('new');
            $table
                ->boolean('is_paid')
                ->default(false);
            $table
                ->string('return_url')
                ->nullable();
            $table->string('acquirer');
            $table
                ->uuid('acquirer_payment_id')
                ->nullable()
                ->index();
            $table->string('confirmation_type');
            $table
                ->jsonb('acquirer_response')
                ->nullable();
            $table->timestamps();

            $table->index(['status', 'is_paid']);
            $table->index(['created_at']);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists(OrderPayment::table());
    }
};
