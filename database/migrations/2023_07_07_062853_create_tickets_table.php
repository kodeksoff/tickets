<?php

declare(strict_types=1);

use Domain\Tickets\Enums\TicketStatuses;
use Domain\Tickets\Models\Ticket;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create(Ticket::table(), function (Blueprint $table) {
            $table->id();
            $table
                ->foreignIdFor(\Domain\Orders\Models\OrderItem::class)
                ->constrained()
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            $table
                ->foreignIdFor(\Domain\ServiceBooking\Models\ServiceBooking::class)
                ->constrained()
                ->cascadeOnUpdate()
                ->restrictOnDelete();
            $table
                ->string('status')
                ->default(TicketStatuses::ACTIVE->value);
            $table->integer('number');
            $table->integer('code');
            $table
                ->timestamp('closed_at')
                ->nullable();
            $table->timestamps();

            $table->index(['created_at']);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists(Ticket::table());
    }
};
