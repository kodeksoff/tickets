<?php

declare(strict_types=1);

use Domain\Orders\Models\Order;
use Domain\Orders\Models\OrderItem;
use Domain\ServiceBooking\Models\ServiceBooking;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create(
            OrderItem::table(),
            function (Blueprint $table) {
                $table->id();
                $table
                    ->foreignIdFor(Order::class)
                    ->constrained()
                    ->cascadeOnUpdate()
                    ->restrictOnDelete();
                $table
                    ->foreignIdFor(ServiceBooking::class)
                    ->constrained()
                    ->cascadeOnUpdate()
                    ->restrictOnDelete();
                $table
                    ->integer('count')
                    ->default(1);
                $table
                    ->jsonb('price')
                    ->nullable();
                $table
                    ->jsonb('full_price')
                    ->nullable();
                $table->timestamps();
            },
        );
    }

    public function down(): void
    {
        Schema::dropIfExists(OrderItem::table());
    }
};
