<?php

declare(strict_types=1);

use Domain\Services\Models\Service;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::table(
            Service::table(),
            function (Blueprint $table) {
                /* *
                * postgres не поддерживает after вставку,
                * поэтому костыльно удаляем timestamps и создаем заново
                */
                $table->dropColumn(['created_at', 'updated_at']);
            },
        );

        Schema::table(
            Service::table(),
            function (Blueprint $table) {
                $table
                    ->integer('service_season_from')
                    ->nullable();
                $table
                    ->integer('service_season_to')
                    ->nullable();
                $table
                    ->boolean('service_all_year')
                    ->default(false);
                $table
                    ->text('description')
                    ->change()
                    ->nullable();
                $table
                    ->text('about_included')
                    ->change()
                    ->nullable();
                $table
                    ->text('about_additional')
                    ->change()
                    ->nullable();
                $table->timestamps();

                $table->index(['service_season_from', 'service_season_to', 'service_all_year']);
            },
        );
    }

    public function down(): void
    {
        Schema::table(
            Service::table(),
            function (Blueprint $table) {
                $table->dropIndex(['service_season_from', 'service_season_to', 'service_all_year']);
                $table->dropColumn(['service_season_from', 'service_season_to', 'service_all_year']);
            },
        );
    }
};
