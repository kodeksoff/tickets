<?php

declare(strict_types=1);

namespace Database\Seeders;

use Domain\Tourists\Models\Tourist;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TouristSeeder extends Seeder
{
    use WithoutModelEvents;

    public function run(): void
    {
        Tourist::factory(10)->create();
    }
}
