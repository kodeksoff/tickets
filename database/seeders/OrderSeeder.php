<?php

declare(strict_types=1);

namespace Database\Seeders;

use Domain\Orders\Models\Order;
use Domain\Services\Models\Service;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    use WithoutModelEvents;

    public function run(): void
    {
        Service::query()
            ->get()
            ->each(function (Service $service) {
                Order::factory(1)->create(
                    [
                        'service_id' => $service->id,
                    ]
                );
            });

    }
}
