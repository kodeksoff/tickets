<?php

declare(strict_types=1);

namespace Database\Seeders;

use Domain\Orders\Models\Order;
use Domain\Orders\Models\OrderPayment;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class OrderPaymentSeeder extends Seeder
{
    use WithoutModelEvents;

    public function run(): void
    {
        Order::query()
            ->get()
            ->each(function (Order $order) {
                OrderPayment::factory(1)->create(
                    [
                        'order_id' => $order->id,
                    ]
                );
            });
    }
}
