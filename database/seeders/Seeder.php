<?php

declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder as Base;

abstract class Seeder extends Base
{
    use WithoutModelEvents;

    /** @var array<int, class-string> */
    protected array $children = [];

    public function run(): void
    {
        foreach ($this->children as $child) {
            $this->call($child);
        }
    }
}
