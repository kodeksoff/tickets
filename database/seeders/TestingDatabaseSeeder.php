<?php

declare(strict_types=1);

namespace Database\Seeders;

class TestingDatabaseSeeder extends Seeder
{
    protected array $children = [];
}
