<?php

declare(strict_types=1);

namespace Database\Seeders;

use Domain\Settings\Models\Setting;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    use WithoutModelEvents;

    /** @return void */
    public function run(): void
    {
        Setting::factory()->create();
    }
}
