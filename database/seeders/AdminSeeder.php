<?php

declare(strict_types=1);

namespace Database\Seeders;

use Domain\Users\Enums\UserRole;
use Domain\Users\Models\User;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class AdminSeeder extends Seeder
{
    use WithoutModelEvents;

    public function __construct(private readonly Hasher $hasher)
    {
    }

    public function run(): void
    {
        $model = User::query()
            ->create([
                'phone' => '9111111111',
                'email' => 'admin@itgro.ru',
                'password' => $this->hasher->make('password'),
                'phone_verified_at' => now(),
                'email_verified_at' => now(),
            ]);

        $model->assignRole(
            Role::query()
                ->where('name', UserRole::ADMIN())
                ->first(),
        );
    }
}
