<?php

declare(strict_types=1);

namespace Database\Seeders;

use Domain\ServiceCategories\Models\ServiceCategory;
use Domain\Services\Enums\ServiceStatus;
use Domain\Services\Models\Service;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class ServiceSeeder extends Seeder
{
    use WithoutModelEvents;

    public function run(): void
    {
        ServiceCategory::query()
            ->whereNull('parent_id')
            ->get()
            ->each(function (ServiceCategory $serviceCategory) {
                $categoryIds = [$serviceCategory->id, ...$serviceCategory->descendants->pluck('id')];
                Service::factory(5)->create([
                    'category_ids' => $categoryIds,
                ]);

                Service::factory(3)->create([
                    'category_ids' => $categoryIds,
                    'status' => ServiceStatus::MODERATION,
                ]);
            });

        Service::query()
            ->each(function (Service $service): void {
                $service
                    ->addMediaFromUrl(
                        fake()->imageUrl(
                            1920,
                            1080,
                            'travel',
                            true,
                            'trips',
                            true,
                            'png',
                        ),
                    )
                    ->toMediaCollection('main_image');

                $service
                    ->addMediaFromUrl(
                        fake()->imageUrl(
                            1920,
                            1080,
                            'travel',
                            true,
                            'trips',
                            true,
                            'png',
                        ),
                    )
                    ->withCustomProperties(['type' => 'first'])
                    ->toMediaCollection('additional_images');

                $service
                    ->addMediaFromUrl(
                        fake()->imageUrl(
                            1920,
                            1080,
                            'travel',
                            true,
                            'trips',
                            true,
                            'png',
                        ),
                    )
                    ->withCustomProperties(['type' => 'second'])
                    ->toMediaCollection('additional_images');

                $service
                    ->addMediaFromUrl(
                        fake()->imageUrl(
                            1920,
                            1080,
                            'travel',
                            true,
                            'trips',
                            true,
                            'png',
                        ),
                    )
                    ->setOrder(3)
                    ->withCustomProperties(['type' => 'third'])
                    ->toMediaCollection('additional_images');

                $service
                    ->addMediaFromUrl(
                        fake()->imageUrl(
                            1920,
                            1080,
                            'travel',
                            true,
                            'trips',
                            true,
                            'png',
                        ),
                    )
                    ->withCustomProperties(['type' => 'fourth'])
                    ->toMediaCollection('additional_images');
            });

        Media::cursor()
            ->each(fn (Media $media): bool => $media->update(['uuid' => Str::uuid()]));
    }
}
