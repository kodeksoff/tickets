<?php

declare(strict_types=1);

namespace Database\Seeders;

use Domain\PartnerHouses\Models\PartnerHouse;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PartnerHouseSeeder extends Seeder
{
    use WithoutModelEvents;

    public function run(): void
    {
        PartnerHouse::factory(10)->create();
    }
}
