<?php

declare(strict_types=1);

namespace Database\Seeders;

use Domain\ServiceBooking\Models\ServiceBooking;
use Domain\Services\Models\Service;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ServiceBookingSeeder extends Seeder
{
    use WithoutModelEvents;

    public function run(): void
    {
        Service::query()
            ->each(function (Service $service): void {
                ServiceBooking::factory(1)->create([
                    'service_id' => $service->id,
                    'is_default' => true,
                ]);
                ServiceBooking::factory(4)->create([
                    'service_id' => $service->id,
                ]);
            });
    }
}
