<?php

declare(strict_types=1);

namespace Database\Seeders;

class DatabaseSeeder extends Seeder
{
    /** @var array|string[] */
    protected array $children = [
        RoleSeeder::class,
        AdminSeeder::class,
        PartnerHouseSeeder::class,
        PartnerServiceSeeder::class,
        GuestHouseSeeder::class,
        ServiceCategorySeeder::class,
        ServiceSeeder::class,
        ServiceBookingSeeder::class,
        TouristSeeder::class,
        SettingSeeder::class,
        AskedQuestionSeeder::class,
        OrderSeeder::class,
        OrderPaymentSeeder::class,
        OrderItemSeeder::class,
        TicketSeeder::class,
    ];

    /** @return void */
    public function run(): void
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');

        parent::run();
    }
}
