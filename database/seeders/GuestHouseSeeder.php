<?php

declare(strict_types=1);

namespace Database\Seeders;

use Domain\GuestHouses\Models\GuestHouse;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class GuestHouseSeeder extends Seeder
{
    use WithoutModelEvents;

    public function run(): void
    {
        GuestHouse::factory(10)->create();
    }
}
