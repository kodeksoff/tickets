<?php

declare(strict_types=1);

namespace Database\Seeders;

use Domain\AskedQuestions\Models\AskedQuestion;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AskedQuestionSeeder extends Seeder
{
    use WithoutModelEvents;

    public function run(): void
    {
        AskedQuestion::factory()->create();
    }
}
