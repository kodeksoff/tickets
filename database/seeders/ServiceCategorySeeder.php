<?php

declare(strict_types=1);

namespace Database\Seeders;

use Domain\ServiceCategories\Models\ServiceCategory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ServiceCategorySeeder extends Seeder
{
    use WithoutModelEvents;

    public function run(): void
    {
        $categories = ServiceCategory::factory(5)->create();
        ServiceCategory::factory(5)->create([
            'is_published' => false,
        ]);

        $categories->each(function (ServiceCategory $serviceCategory): void {
            ServiceCategory::factory(5)->create([
                'parent_id' => $serviceCategory->id,
            ]);
        });
    }
}
