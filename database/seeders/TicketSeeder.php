<?php

declare(strict_types=1);

namespace Database\Seeders;

use BaconQrCode\Renderer\Image\SvgImageBackEnd;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;
use Domain\Orders\Enums\OrderStatus;
use Domain\Orders\Models\OrderItem;
use Domain\Tickets\Models\Ticket;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TicketSeeder extends Seeder
{
    use WithoutModelEvents;

    public function run(): void
    {
        OrderItem::query()
            ->whereHas('order', fn ($query) => $query->where('status', OrderStatus::ACTIVE->value))
            ->each(function (OrderItem $orderItem) {
                Ticket::factory(1)->create(
                    [
                        'order_item_id' => $orderItem->id,
                        'service_booking_id' => $orderItem->service_booking_id,
                    ]
                );
            });

        Ticket::query()
            ->each(function (Ticket $ticket): void {
                $renderer = new ImageRenderer(
                    new RendererStyle(400),
                    new SvgImageBackEnd()
                );
                $writer = new Writer($renderer);
                $writer->writeFile(strval($ticket->code), 'storage/qrcode.svg');

                $ticket->addMedia('storage/qrcode.svg')
                    ->toMediaCollection('qr_image');
            });
    }
}
