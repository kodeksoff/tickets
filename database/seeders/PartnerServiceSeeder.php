<?php

declare(strict_types=1);

namespace Database\Seeders;

use Domain\PartnerServices\Models\PartnerService;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PartnerServiceSeeder extends Seeder
{
    use WithoutModelEvents;

    public function run(): void
    {
        PartnerService::factory(10)->create();
    }
}
