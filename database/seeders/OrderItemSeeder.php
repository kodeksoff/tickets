<?php

declare(strict_types=1);

namespace Database\Seeders;

use Domain\Orders\Models\Order;
use Domain\Orders\Models\OrderItem;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class OrderItemSeeder extends Seeder
{
    use WithoutModelEvents;

    public function run(): void
    {
        Order::query()
            ->get()
            ->each(function (Order $order) {
                $bookingIds = $order
                    ->service
                    ->bookings
                    ->pluck('id')
                    ->toArray();

                OrderItem::factory(2)->create(
                    [
                        'order_id' => $order->id,
                        'service_booking_id' => $bookingIds[array_rand($bookingIds)],
                    ]
                );
            });
    }
}
