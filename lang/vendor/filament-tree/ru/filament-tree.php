<?php

return [
    'root' => 'Root',
    
    /*
        |--------------------------------------------------------------------------
        | Buttons
        |--------------------------------------------------------------------------
    */
    'button.save' => 'Сохранить',
    'button.expand_all' => 'Развернуть все',
    'button.collapse_all' => 'Свернуть все',

    /*
        |--------------------------------------------------------------------------
        | Form
        |--------------------------------------------------------------------------
    */
    'components.tree.buttons.select_all.label' => 'Выделить все',
    'components.tree.buttons.deselect_all.label' => 'Отменить выделение',
    'components.tree.buttons.expand_all.label' => 'Развернуть все',
    'components.tree.buttons.collapse_all.label' => 'Свернуть все',
];