<?php

declare(strict_types=1);

use App\Api\V1\Controllers\Users\EmailVerificationController;
use Domain\Orders\Models\Order;
use Illuminate\Support\Facades\Route;

Route::get('/', static fn () => ['version' => app()->version()]);

Route::get('/email/verify/{id}/{hash}', EmailVerificationController::class)
    ->middleware(['auth:api', 'signed'])
    ->name('verification.verify');

Route::get(
    '/test-webhook',
    static fn () => Order::query()
        ->select(['id', 'status'])
        ->get()
);
