<?php

declare(strict_types=1);

use App\Api\V1\Controllers\Orders\AcquirerWebhookController;
use App\Api\V1\Controllers\Orders\PartnerServiceOrderController;
use App\Api\V1\Controllers\Orders\TouristOrdersController;
use Domain\Orders\Models\Order;
use Illuminate\Support\Facades\Route;

Route::as('orders:')
    ->prefix('orders')
    ->group(function (): void {
        Route::as('acquiring:')
            ->prefix('acquiring')
            ->group(function (): void {
                Route::post('webhook/{acquirer}', AcquirerWebhookController::class)->name('webhook');
            });

        Route::as('tourist:')
            ->prefix('tourist')
            ->middleware(['auth:api', 'role:tourist'])
            ->controller(TouristOrdersController::class)
            ->group(function (): void {
                Route::get('/', 'index')->name('index');
                Route::post('/', 'store')
                    ->can('create', Order::class)
                    ->name('store');
                Route::get('/{order}', 'show')
                    ->can('viewTourist', 'order')
                    ->middleware('role:admin|tourist')
                    ->name('show');
                Route::post('/refund/{order}', 'refund')->name('refund');
            });

        Route::as('partner-service:')
            ->prefix('partner-service')
            ->middleware(['auth:api', 'role:partner-service'])
            ->controller(PartnerServiceOrderController::class)
            ->group(function (): void {
                Route::get('/{order}', 'show')
                    ->can('viewPartnerService', 'order')
                    ->name('show');
                Route::post('/tickets/{ticket}/use', 'useTicket')->name('useTicket');
            });

    });
