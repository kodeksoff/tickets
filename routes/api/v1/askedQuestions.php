<?php

declare(strict_types=1);

use App\Api\V1\Controllers\AskedQuestions\AskedQuestionController;
use Illuminate\Support\Facades\Route;

Route::as('asked-questions:')
    ->prefix('asked-questions')
    ->controller(AskedQuestionController::class)
    ->group(function (): void {
        Route::get('/', 'index')->name('index');
    });
