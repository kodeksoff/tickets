<?php

declare(strict_types=1);

use App\Api\V1\Controllers\Settings\SettingController;
use Illuminate\Support\Facades\Route;

Route::as('settings:')
    ->prefix('settings')
    ->controller(SettingController::class)
    ->group(function (): void {
        Route::get('/', 'index')->name('index');
        Route::get('/contacts', 'contacts')->name('contacts');
    });
