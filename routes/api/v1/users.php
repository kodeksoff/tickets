<?php

declare(strict_types=1);

use App\Api\V1\Controllers\Confirmations\ConfirmationController;
use App\Api\V1\Controllers\PartnerHouse\PartnerHouseProfileController;
use App\Api\V1\Controllers\PartnerServices\PartnerServiceController;
use App\Api\V1\Controllers\PartnerServices\PartnerServiceProfileController;
use App\Api\V1\Controllers\Tourists\TouristProfileController;
use App\Api\V1\Controllers\Users\AuthenticationController;
use App\Api\V1\Controllers\Users\CheckRegisterPhoneUserController;
use App\Api\V1\Controllers\Users\NotificationController;
use App\Api\V1\Controllers\Users\ProfileController;
use App\Api\V1\Controllers\Users\RecoveryPasswordController;
use App\Api\V1\Controllers\Users\RegisterUserController;
use App\Api\V1\Controllers\Users\RegisterValidationController;
use App\Api\V1\Controllers\Users\ResetPasswordController;
use Illuminate\Foundation\Http\Middleware\HandlePrecognitiveRequests;
use Illuminate\Support\Facades\Route;
use Support\Middleware\ThrottleConfirmationRequests;

Route::as('confirmations:')
    ->prefix('confirmations')
    ->controller(ConfirmationController::class)
    ->middleware(HandlePrecognitiveRequests::class)
    ->group(function (): void {
        Route::prefix('phones')->group(function (): void {
            Route::post('/send', 'phoneSend')
                ->name('phone:send')
                ->middleware(ThrottleConfirmationRequests::class);
            Route::post('/approve', 'phoneApprove')->name('phone:approve');
        });
    });

Route::as('auth:')
    ->prefix('auth')
    ->middleware(['guest', HandlePrecognitiveRequests::class])
    ->group(function (): void {
        Route::post('/register-validation/{userRole}', RegisterValidationController::class)
            ->name('register-validation');

        Route::post('/register-check/phone', CheckRegisterPhoneUserController::class)
            ->name('register-check');

        Route::post('/register/{userRole}', RegisterUserController::class)
            ->name('register');

        Route::post('/login/{authUserRole}', AuthenticationController::class)
            ->name('login');

        Route::post('/password-recovery/{authUserRole}', RecoveryPasswordController::class)
            ->middleware(ThrottleConfirmationRequests::class)
            ->name('password-recovery');

        Route::post('/password-reset', ResetPasswordController::class)
            ->name('password-reset');
    });

Route::as('profile:')
    ->prefix('profile')
    ->middleware(['auth:api'])
    ->controller(ProfileController::class)
    ->group(function (): void {
        Route::get('/', 'show')->name('show');
        Route::put('/update-phone', 'updatePhone')
            ->middleware(HandlePrecognitiveRequests::class)
            ->name('update-phone');
        Route::put('/update-password', 'updatePassword')
            ->middleware([
                'role:partner-service|partner-house',
                HandlePrecognitiveRequests::class,
            ])
            ->name('update-password');
        Route::post('/logout', 'logout')->name('logout');
    });

Route::as('notifications:')
    ->prefix('notifications')
    ->middleware(['auth:api'])
    ->controller(NotificationController::class)
    ->group(function (): void {
        Route::get('/', 'index')->name('index');
        Route::put('{notification}/read', 'read')
            ->can('update', 'notification')
            ->name('read');
        Route::delete('{notification}/delete', 'delete')
            ->can('delete', 'notification')
            ->name('delete');
    });

Route::as('cabinet:')
    ->prefix('cabinet')
    ->middleware(['auth:api'])
    ->group(function (): void {
        Route::as('partner-service:')
            ->prefix('partner-service')
            ->middleware('role:partner-service')
            ->group(function (): void {
                Route::as('profile:')
                    ->prefix('profile')
                    ->controller(PartnerServiceProfileController::class)
                    ->group(function (): void {
                        Route::get('/', 'show')->name('show');
                        Route::delete('/delete', 'delete')->name('delete');
                    });
                Route::as('services:')
                    ->prefix('services')
                    ->controller(PartnerServiceController::class)
                    ->group(function (): void {
                        Route::get('/purchased', 'purchased')->name('purchased');
                        Route::get('/published', 'published')->name('published');
                        Route::get('/hidden', 'hidden')->name('hidden');
                    });
            });

        Route::as('partner-house:')
            ->prefix('partner-house')
            ->middleware('role:partner-house')
            ->group(function (): void {
                Route::as('profile:')
                    ->prefix('profile')
                    ->controller(PartnerHouseProfileController::class)
                    ->group(function (): void {
                        Route::get('/', 'show')->name('show');
                    });
            });

        Route::as('tourist:')
            ->prefix('tourist')
            ->middleware('role:tourist')
            ->group(function (): void {
                Route::as('profile:')
                    ->prefix('profile')
                    ->controller(TouristProfileController::class)
                    ->group(function (): void {
                        Route::get('/', 'show')->name('show');
                        Route::post('/update', 'update')->name('update');
                    });
            });
    });
