<?php

declare(strict_types=1);

use App\Api\V1\Controllers\ServiceCategories\ServiceCategoriesController;
use App\Api\V1\Controllers\Services\ServiceController;
use Domain\Services\Models\Service;
use Illuminate\Foundation\Http\Middleware\HandlePrecognitiveRequests;
use Illuminate\Support\Facades\Route;

Route::as('services:')
    ->prefix('services')
    ->group(function (): void {
        Route::get('/categories', ServiceCategoriesController::class)->name('categories');

        Route::as('service:')
            ->controller(ServiceController::class)
            ->group(function (): void {
                Route::get('/', 'index')->name('index');
                Route::get('/{service}', 'show')->name('show');

                Route::middleware(['auth:api', 'role:partner-service'])
                    ->group(function (): void {
                        Route::post('/', 'store')
                            ->can('create', Service::class)
                            ->middleware(HandlePrecognitiveRequests::class)
                            ->name('store');
                        Route::get('/{service}/edit', 'edit')
                            ->can('update', 'service')
                            ->name('edit');
                        Route::post('/{service}/update', 'update')
                            ->can('update', 'service')
                            ->middleware(HandlePrecognitiveRequests::class)
                            ->name('update');
                        Route::delete('/{service}/delete', 'delete')
                            ->can('delete', 'service')
                            ->name('delete');
                        Route::post('/{service}/publish', 'publish')
                            ->can('update', 'service')
                            ->name('published');
                        Route::post('/{service}/unpublished', 'unpublished')
                            ->can('update', 'service')
                            ->name('unpublished');
                    });
            });
    });
