<?php
// @formatter:off
// phpcs:ignoreFile

/**
 * A helper file for Laravel, to provide autocomplete information to your IDE
 * Generated for Laravel 10.13.0.
 *
 * This file should not be included in your code, only analyzed by your IDE!
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 * @see https://github.com/barryvdh/laravel-ide-helper
 */

        namespace L5Swagger { 
            /**
     * 
     *
     */ 
        class L5SwaggerFacade {
                    /**
         * 
         *
         * @throws L5SwaggerException
         * @static 
         */ 
        public static function generateDocs()
        {
                        /** @var \L5Swagger\Generator $instance */
                        return $instance->generateDocs();
        }
         
    }
     
}

    namespace Intervention\Image\Facades { 
            /**
     * 
     *
     */ 
        class Image {
                    /**
         * Overrides configuration settings
         *
         * @param array $config
         * @return self 
         * @static 
         */ 
        public static function configure($config = [])
        {
                        /** @var \Intervention\Image\ImageManager $instance */
                        return $instance->configure($config);
        }
                    /**
         * Initiates an Image instance from different input types
         *
         * @param mixed $data
         * @return \Intervention\Image\Image 
         * @static 
         */ 
        public static function make($data)
        {
                        /** @var \Intervention\Image\ImageManager $instance */
                        return $instance->make($data);
        }
                    /**
         * Creates an empty image canvas
         *
         * @param int $width
         * @param int $height
         * @param mixed $background
         * @return \Intervention\Image\Image 
         * @static 
         */ 
        public static function canvas($width, $height, $background = null)
        {
                        /** @var \Intervention\Image\ImageManager $instance */
                        return $instance->canvas($width, $height, $background);
        }
                    /**
         * Create new cached image and run callback
         * (requires additional package intervention/imagecache)
         *
         * @param \Closure $callback
         * @param int $lifetime
         * @param boolean $returnObj
         * @return \Image 
         * @static 
         */ 
        public static function cache($callback, $lifetime = null, $returnObj = false)
        {
                        /** @var \Intervention\Image\ImageManager $instance */
                        return $instance->cache($callback, $lifetime, $returnObj);
        }
         
    }
     
}

    namespace Laravel\Horizon { 
            /**
     * 
     *
     */ 
        class Horizon {
         
    }
     
}

    namespace Livewire { 
            /**
     * 
     *
     * @see \Livewire\LivewireManager
     */ 
        class Livewire {
                    /**
         * 
         *
         * @static 
         */ 
        public static function component($alias, $viewClass = null)
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->component($alias, $viewClass);
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function getAlias($class, $default = null)
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->getAlias($class, $default);
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function getComponentAliases()
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->getComponentAliases();
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function resolveMissingComponent($resolver)
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->resolveMissingComponent($resolver);
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function getClass($alias)
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->getClass($alias);
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function getInstance($component, $id)
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->getInstance($component, $id);
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function mount($name, $params = [])
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->mount($name, $params);
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function dummyMount($id, $tagName)
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->dummyMount($id, $tagName);
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function test($name, $params = [])
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->test($name, $params);
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function visit($browser, $class, $queryString = '')
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->visit($browser, $class, $queryString);
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function actingAs($user, $driver = null)
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->actingAs($user, $driver);
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function addPersistentMiddleware($middleware)
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->addPersistentMiddleware($middleware);
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function setPersistentMiddleware($middleware)
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->setPersistentMiddleware($middleware);
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function getPersistentMiddleware()
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->getPersistentMiddleware();
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function styles($options = [])
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->styles($options);
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function scripts($options = [])
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->scripts($options);
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function isLivewireRequest()
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->isLivewireRequest();
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function isDefinitelyLivewireRequest()
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->isDefinitelyLivewireRequest();
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function isProbablyLivewireRequest()
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->isProbablyLivewireRequest();
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function originalUrl()
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->originalUrl();
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function originalPath()
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->originalPath();
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function originalMethod()
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->originalMethod();
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function getRootElementTagName($dom)
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->getRootElementTagName($dom);
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function dispatch($event, ...$params)
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->dispatch($event, ...$params);
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function listen($event, $callback)
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->listen($event, $callback);
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function isOnVapor()
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->isOnVapor();
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function isRunningServerless()
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->isRunningServerless();
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function withQueryParams($queryParams)
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->withQueryParams($queryParams);
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function setBackButtonCache()
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->setBackButtonCache();
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function disableBackButtonCache()
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->disableBackButtonCache();
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function enableBackButtonCache()
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->enableBackButtonCache();
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function shouldDisableBackButtonCache()
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->shouldDisableBackButtonCache();
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function flushState()
        {
                        /** @var \Livewire\LivewireManager $instance */
                        return $instance->flushState();
        }
         
    }
     
}

    namespace Opcodes\LogViewer\Facades { 
            /**
     * 
     *
     * @see \Opcodes\LogViewer\LogViewerService
     */ 
        class LogViewer {
                    /**
         * 
         *
         * @static 
         */ 
        public static function basePathForLogs()
        {
                        /** @var \Opcodes\LogViewer\LogViewerService $instance */
                        return $instance->basePathForLogs();
        }
                    /**
         * 
         *
         * @return \Opcodes\LogViewer\LogFileCollection|\Opcodes\LogViewer\LogFile[] 
         * @static 
         */ 
        public static function getFiles()
        {
                        /** @var \Opcodes\LogViewer\LogViewerService $instance */
                        return $instance->getFiles();
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function getFilesGroupedByFolder()
        {
                        /** @var \Opcodes\LogViewer\LogViewerService $instance */
                        return $instance->getFilesGroupedByFolder();
        }
                    /**
         * Find the file with the given identifier or file name.
         *
         * @static 
         */ 
        public static function getFile($fileIdentifier)
        {
                        /** @var \Opcodes\LogViewer\LogViewerService $instance */
                        return $instance->getFile($fileIdentifier);
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function getFolder($folderIdentifier)
        {
                        /** @var \Opcodes\LogViewer\LogViewerService $instance */
                        return $instance->getFolder($folderIdentifier);
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function supportsHostsFeature()
        {
                        /** @var \Opcodes\LogViewer\LogViewerService $instance */
                        return $instance->supportsHostsFeature();
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function resolveHostsUsing($callback)
        {
                        /** @var \Opcodes\LogViewer\LogViewerService $instance */
                        return $instance->resolveHostsUsing($callback);
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function getHosts()
        {
                        /** @var \Opcodes\LogViewer\LogViewerService $instance */
                        return $instance->getHosts();
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function getHost($hostIdentifier)
        {
                        /** @var \Opcodes\LogViewer\LogViewerService $instance */
                        return $instance->getHost($hostIdentifier);
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function clearFileCache()
        {
                        /** @var \Opcodes\LogViewer\LogViewerService $instance */
                        return $instance->clearFileCache();
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function getRouteDomain()
        {
                        /** @var \Opcodes\LogViewer\LogViewerService $instance */
                        return $instance->getRouteDomain();
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function getRoutePrefix()
        {
                        /** @var \Opcodes\LogViewer\LogViewerService $instance */
                        return $instance->getRoutePrefix();
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function getRouteMiddleware()
        {
                        /** @var \Opcodes\LogViewer\LogViewerService $instance */
                        return $instance->getRouteMiddleware();
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function auth($callback = null)
        {
                        /** @var \Opcodes\LogViewer\LogViewerService $instance */
                        return $instance->auth($callback);
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function lazyScanChunkSize()
        {
                        /** @var \Opcodes\LogViewer\LogViewerService $instance */
                        return $instance->lazyScanChunkSize();
        }
                    /**
         * Get the maximum number of bytes of the log that we should display.
         *
         * @static 
         */ 
        public static function maxLogSize()
        {
                        /** @var \Opcodes\LogViewer\LogViewerService $instance */
                        return $instance->maxLogSize();
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function setMaxLogSize($bytes)
        {
                        /** @var \Opcodes\LogViewer\LogViewerService $instance */
                        return $instance->setMaxLogSize($bytes);
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function laravelRegexPattern()
        {
                        /** @var \Opcodes\LogViewer\LogViewerService $instance */
                        return $instance->laravelRegexPattern();
        }
                    /**
         * 
         *
         * @static 
         */ 
        public static function logMatchPattern()
        {
                        /** @var \Opcodes\LogViewer\LogViewerService $instance */
                        return $instance->logMatchPattern();
        }
                    /**
         * Determine if Log Viewer's published assets are up-to-date.
         *
         * @throws \RuntimeException
         * @static 
         */ 
        public static function assetsAreCurrent()
        {
                        /** @var \Opcodes\LogViewer\LogViewerService $instance */
                        return $instance->assetsAreCurrent();
        }
                    /**
         * Get the current version of the Log Viewer
         *
         * @static 
         */ 
        public static function version()
        {
                        /** @var \Opcodes\LogViewer\LogViewerService $instance */
                        return $instance->version();
        }
         
    }
     
}

    namespace Saloon\Laravel\Facades { 
            /**
     * 
     *
     * @see \Saloon\Laravel\Saloon
     */ 
        class Saloon {
                    /**
         * Start mocking!
         *
         * @param array $responses
         * @return \Saloon\Laravel\Http\Faking\MockClient 
         * @throws \Saloon\Exceptions\InvalidMockResponseCaptureMethodException
         * @static 
         */ 
        public static function fake($responses)
        {
                        return \Saloon\Laravel\Saloon::fake($responses);
        }
                    /**
         * Retrieve the mock client from the container
         *
         * @return \Saloon\Laravel\Http\Faking\MockClient 
         * @static 
         */ 
        public static function mockClient()
        {
                        return \Saloon\Laravel\Saloon::mockClient();
        }
                    /**
         * Assert that a given request was sent.
         *
         * @param string|callable $value
         * @return void 
         * @throws \ReflectionException
         * @static 
         */ 
        public static function assertSent($value)
        {
                        \Saloon\Laravel\Saloon::assertSent($value);
        }
                    /**
         * Assert that a given request was not sent.
         *
         * @param string|callable $value
         * @return void 
         * @throws \ReflectionException
         * @static 
         */ 
        public static function assertNotSent($value)
        {
                        \Saloon\Laravel\Saloon::assertNotSent($value);
        }
                    /**
         * Assert JSON data was sent
         *
         * @param string $request
         * @param array $data
         * @return void 
         * @throws \ReflectionException
         * @static 
         */ 
        public static function assertSentJson($request, $data)
        {
                        \Saloon\Laravel\Saloon::assertSentJson($request, $data);
        }
                    /**
         * Assert that nothing was sent.
         *
         * @return void 
         * @static 
         */ 
        public static function assertNothingSent()
        {
                        \Saloon\Laravel\Saloon::assertNothingSent();
        }
                    /**
         * Assert a request count has been met.
         *
         * @param int $count
         * @return void 
         * @static 
         */ 
        public static function assertSentCount($count)
        {
                        \Saloon\Laravel\Saloon::assertSentCount($count);
        }
                    /**
         * Start Saloon recording responses.
         *
         * @return void 
         * @static 
         */ 
        public static function record()
        {
                        /** @var \Saloon\Laravel\Saloon $instance */
                        $instance->record();
        }
                    /**
         * Stop Saloon recording responses.
         *
         * @return void 
         * @static 
         */ 
        public static function stopRecording()
        {
                        /** @var \Saloon\Laravel\Saloon $instance */
                        $instance->stopRecording();
        }
                    /**
         * Check if Saloon is recording
         *
         * @return bool 
         * @static 
         */ 
        public static function isRecording()
        {
                        /** @var \Saloon\Laravel\Saloon $instance */
                        return $instance->isRecording();
        }
                    /**
         * Record a response.
         *
         * @param \Saloon\Contracts\Response $response
         * @return void 
         * @static 
         */ 
        public static function recordResponse($response)
        {
                        /** @var \Saloon\Laravel\Saloon $instance */
                        $instance->recordResponse($response);
        }
                    /**
         * Get all the recorded responses.
         *
         * @return array 
         * @static 
         */ 
        public static function getRecordedResponses()
        {
                        /** @var \Saloon\Laravel\Saloon $instance */
                        return $instance->getRecordedResponses();
        }
                    /**
         * Get the last response that Saloon recorded.
         *
         * @return \Saloon\Contracts\Response|null 
         * @static 
         */ 
        public static function getLastRecordedResponse()
        {
                        /** @var \Saloon\Laravel\Saloon $instance */
                        return $instance->getLastRecordedResponse();
        }
         
    }
     
}

    namespace TimeHunter\LaravelGoogleReCaptchaV3\Facades { 
            /**
     * 
     *
     * @see \TimeHunter\LaravelGoogleReCaptchaV3\GoogleReCaptchaV3
     */ 
        class GoogleReCaptchaV3 {
                    /**
         * 
         *
         * @param $mappers
         * @return array 
         * @static 
         */ 
        public static function prepareViewData($mappers)
        {
                        /** @var \TimeHunter\LaravelGoogleReCaptchaV3\GoogleReCaptchaV3 $instance */
                        return $instance->prepareViewData($mappers);
        }
                    /**
         * 
         *
         * @return array 
         * @static 
         */ 
        public static function prepareData()
        {
                        /** @var \TimeHunter\LaravelGoogleReCaptchaV3\GoogleReCaptchaV3 $instance */
                        return $instance->prepareData();
        }
                    /**
         * 
         *
         * @return array 
         * @static 
         */ 
        public static function prepareBackgroundViewData()
        {
                        /** @var \TimeHunter\LaravelGoogleReCaptchaV3\GoogleReCaptchaV3 $instance */
                        return $instance->prepareBackgroundViewData();
        }
                    /**
         * 
         *
         * @param array $params
         * @return \Illuminate\Contracts\View\View|mixed 
         * @throws \Illuminate\Contracts\Container\BindingResolutionException
         * @static 
         */ 
        public static function init($params = [])
        {
                        /** @var \TimeHunter\LaravelGoogleReCaptchaV3\GoogleReCaptchaV3 $instance */
                        return $instance->init($params);
        }
                    /**
         * 
         *
         * @param $id
         * @param $action
         * @static 
         */ 
        public static function renderOne($id, $action)
        {
                        /** @var \TimeHunter\LaravelGoogleReCaptchaV3\GoogleReCaptchaV3 $instance */
                        return $instance->renderOne($id, $action);
        }
                    /**
         * 
         *
         * @param $id
         * @param $action
         * @param string $class
         * @param string $style
         * @return \Illuminate\Contracts\View\View|mixed 
         * @static 
         */ 
        public static function renderField($id, $action, $class = '', $style = '')
        {
                        /** @var \TimeHunter\LaravelGoogleReCaptchaV3\GoogleReCaptchaV3 $instance */
                        return $instance->renderField($id, $action, $class, $style);
        }
                    /**
         * 
         *
         * @param $mappers
         * @static 
         */ 
        public static function render($mappers)
        {
                        /** @var \TimeHunter\LaravelGoogleReCaptchaV3\GoogleReCaptchaV3 $instance */
                        return $instance->render($mappers);
        }
                    /**
         * 
         *
         * @param $response
         * @param null $ip
         * @return \TimeHunter\LaravelGoogleReCaptchaV3\Core\GoogleReCaptchaV3Response 
         * @static 
         */ 
        public static function verifyResponse($response, $ip = null)
        {
                        /** @var \TimeHunter\LaravelGoogleReCaptchaV3\GoogleReCaptchaV3 $instance */
                        return $instance->verifyResponse($response, $ip);
        }
                    /**
         * 
         *
         * @return \TimeHunter\LaravelGoogleReCaptchaV3\Interfaces\ReCaptchaConfigV3Interface 
         * @static 
         */ 
        public static function getConfig()
        {
                        /** @var \TimeHunter\LaravelGoogleReCaptchaV3\GoogleReCaptchaV3 $instance */
                        return $instance->getConfig();
        }
                    /**
         * 
         *
         * @param string|null $value
         * @return \TimeHunter\LaravelGoogleReCaptchaV3\GoogleReCaptchaV3 
         * @static 
         */ 
        public static function setAction($value = null)
        {
                        /** @var \TimeHunter\LaravelGoogleReCaptchaV3\GoogleReCaptchaV3 $instance */
                        return $instance->setAction($value);
        }
                    /**
         * 
         *
         * @param string|null $value
         * @return \TimeHunter\LaravelGoogleReCaptchaV3\GoogleReCaptchaV3 
         * @static 
         */ 
        public static function setScore($value = null)
        {
                        /** @var \TimeHunter\LaravelGoogleReCaptchaV3\GoogleReCaptchaV3 $instance */
                        return $instance->setScore($value);
        }
         
    }
     
}

    namespace Illuminate\Support { 
            /**
     * 
     *
     */ 
        class Str {
                    /**
         * 
         *
         * @see \Filament\Support\SupportServiceProvider::packageBooted()
         * @param string $html
         * @return string 
         * @static 
         */ 
        public static function sanitizeHtml($html)
        {
                        return \Illuminate\Support\Str::sanitizeHtml($html);
        }
         
    }
            /**
     * 
     *
     * @template TKey of array-key
     * @template-covariant TValue
     * @implements \ArrayAccess<TKey, TValue>
     * @implements \Illuminate\Support\Enumerable<TKey, TValue>
     */ 
        class Collection {
                    /**
         * 
         *
         * @see \Spatie\LaravelRay\RayServiceProvider::registerMacros()
         * @param string $description
         * @static 
         */ 
        public static function ray($description = '')
        {
                        return \Illuminate\Support\Collection::ray($description);
        }
         
    }
            /**
     * 
     *
     */ 
        class Stringable {
                    /**
         * 
         *
         * @see \Spatie\LaravelRay\RayServiceProvider::registerMacros()
         * @param string $description
         * @static 
         */ 
        public static function ray($description = '')
        {
                        return \Illuminate\Support\Stringable::ray($description);
        }
                    /**
         * 
         *
         * @see \Filament\Support\SupportServiceProvider::packageBooted()
         * @return \Illuminate\Support\Stringable 
         * @static 
         */ 
        public static function sanitizeHtml()
        {
                        return \Illuminate\Support\Stringable::sanitizeHtml();
        }
         
    }
     
}

    namespace Illuminate\Http { 
            /**
     * 
     *
     */ 
        class Request {
                    /**
         * 
         *
         * @see \Illuminate\Foundation\Providers\FoundationServiceProvider::registerRequestValidation()
         * @param array $rules
         * @param mixed $params
         * @static 
         */ 
        public static function validate($rules, ...$params)
        {
                        return \Illuminate\Http\Request::validate($rules, ...$params);
        }
                    /**
         * 
         *
         * @see \Illuminate\Foundation\Providers\FoundationServiceProvider::registerRequestValidation()
         * @param string $errorBag
         * @param array $rules
         * @param mixed $params
         * @static 
         */ 
        public static function validateWithBag($errorBag, $rules, ...$params)
        {
                        return \Illuminate\Http\Request::validateWithBag($errorBag, $rules, ...$params);
        }
                    /**
         * 
         *
         * @see \Illuminate\Foundation\Providers\FoundationServiceProvider::registerRequestSignatureValidation()
         * @param mixed $absolute
         * @static 
         */ 
        public static function hasValidSignature($absolute = true)
        {
                        return \Illuminate\Http\Request::hasValidSignature($absolute);
        }
                    /**
         * 
         *
         * @see \Illuminate\Foundation\Providers\FoundationServiceProvider::registerRequestSignatureValidation()
         * @static 
         */ 
        public static function hasValidRelativeSignature()
        {
                        return \Illuminate\Http\Request::hasValidRelativeSignature();
        }
                    /**
         * 
         *
         * @see \Illuminate\Foundation\Providers\FoundationServiceProvider::registerRequestSignatureValidation()
         * @param mixed $ignoreQuery
         * @param mixed $absolute
         * @static 
         */ 
        public static function hasValidSignatureWhileIgnoring($ignoreQuery = [], $absolute = true)
        {
                        return \Illuminate\Http\Request::hasValidSignatureWhileIgnoring($ignoreQuery, $absolute);
        }
         
    }
     
}

    namespace Livewire\Testing { 
            /**
     * 
     *
     * @mixin \Illuminate\Testing\TestResponse
     */ 
        class TestableLivewire {
                    /**
         * 
         *
         * @see \Filament\Support\Testing\TestsActions::parseActionName()
         * @param string $name
         * @return string 
         * @static 
         */ 
        public static function parseActionName($name)
        {
                        return \Livewire\Testing\TestableLivewire::parseActionName($name);
        }
                    /**
         * 
         *
         * @see \Filament\Support\Testing\TestsActions::assertActionListInOrder()
         * @param array $names
         * @param array $actions
         * @param string $actionType
         * @param string $actionClass
         * @return self 
         * @static 
         */ 
        public static function assertActionListInOrder($names, $actions, $actionType, $actionClass)
        {
                        return \Livewire\Testing\TestableLivewire::assertActionListInOrder($names, $actions, $actionType, $actionClass);
        }
                    /**
         * 
         *
         * @see \Filament\Testing\TestsPageActions::mountPageAction()
         * @param string $name
         * @return static 
         * @static 
         */ 
        public static function mountPageAction($name)
        {
                        return \Livewire\Testing\TestableLivewire::mountPageAction($name);
        }
                    /**
         * 
         *
         * @see \Filament\Testing\TestsPageActions::setPageActionData()
         * @param array $data
         * @return static 
         * @static 
         */ 
        public static function setPageActionData($data)
        {
                        return \Livewire\Testing\TestableLivewire::setPageActionData($data);
        }
                    /**
         * 
         *
         * @see \Filament\Testing\TestsPageActions::assertPageActionDataSet()
         * @param array $data
         * @return static 
         * @static 
         */ 
        public static function assertPageActionDataSet($data)
        {
                        return \Livewire\Testing\TestableLivewire::assertPageActionDataSet($data);
        }
                    /**
         * 
         *
         * @see \Filament\Testing\TestsPageActions::callPageAction()
         * @param string $name
         * @param array $data
         * @param array $arguments
         * @return static 
         * @static 
         */ 
        public static function callPageAction($name, $data = [], $arguments = [])
        {
                        return \Livewire\Testing\TestableLivewire::callPageAction($name, $data, $arguments);
        }
                    /**
         * 
         *
         * @see \Filament\Testing\TestsPageActions::callMountedPageAction()
         * @param array $arguments
         * @return static 
         * @static 
         */ 
        public static function callMountedPageAction($arguments = [])
        {
                        return \Livewire\Testing\TestableLivewire::callMountedPageAction($arguments);
        }
                    /**
         * 
         *
         * @see \Filament\Testing\TestsPageActions::assertPageActionExists()
         * @param string $name
         * @return static 
         * @static 
         */ 
        public static function assertPageActionExists($name)
        {
                        return \Livewire\Testing\TestableLivewire::assertPageActionExists($name);
        }
                    /**
         * 
         *
         * @see \Filament\Testing\TestsPageActions::assertPageActionDoesNotExist()
         * @param string $name
         * @return static 
         * @static 
         */ 
        public static function assertPageActionDoesNotExist($name)
        {
                        return \Livewire\Testing\TestableLivewire::assertPageActionDoesNotExist($name);
        }
                    /**
         * 
         *
         * @see \Filament\Testing\TestsPageActions::assertPageActionsExistInOrder()
         * @param array $names
         * @return static 
         * @static 
         */ 
        public static function assertPageActionsExistInOrder($names)
        {
                        return \Livewire\Testing\TestableLivewire::assertPageActionsExistInOrder($names);
        }
                    /**
         * 
         *
         * @see \Filament\Testing\TestsPageActions::assertPageActionVisible()
         * @param string $name
         * @return static 
         * @static 
         */ 
        public static function assertPageActionVisible($name)
        {
                        return \Livewire\Testing\TestableLivewire::assertPageActionVisible($name);
        }
                    /**
         * 
         *
         * @see \Filament\Testing\TestsPageActions::assertPageActionHidden()
         * @param string $name
         * @return static 
         * @static 
         */ 
        public static function assertPageActionHidden($name)
        {
                        return \Livewire\Testing\TestableLivewire::assertPageActionHidden($name);
        }
                    /**
         * 
         *
         * @see \Filament\Testing\TestsPageActions::assertPageActionEnabled()
         * @param string $name
         * @return static 
         * @static 
         */ 
        public static function assertPageActionEnabled($name)
        {
                        return \Livewire\Testing\TestableLivewire::assertPageActionEnabled($name);
        }
                    /**
         * 
         *
         * @see \Filament\Testing\TestsPageActions::assertPageActionDisabled()
         * @param string $name
         * @return static 
         * @static 
         */ 
        public static function assertPageActionDisabled($name)
        {
                        return \Livewire\Testing\TestableLivewire::assertPageActionDisabled($name);
        }
                    /**
         * 
         *
         * @see \Filament\Testing\TestsPageActions::assertPageActionHasIcon()
         * @param string $name
         * @param string $icon
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertPageActionHasIcon($name, $icon, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertPageActionHasIcon($name, $icon, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Testing\TestsPageActions::assertPageActionDoesNotHaveIcon()
         * @param string $name
         * @param string $icon
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertPageActionDoesNotHaveIcon($name, $icon, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertPageActionDoesNotHaveIcon($name, $icon, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Testing\TestsPageActions::assertPageActionHasLabel()
         * @param string $name
         * @param string $label
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertPageActionHasLabel($name, $label, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertPageActionHasLabel($name, $label, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Testing\TestsPageActions::assertPageActionDoesNotHaveLabel()
         * @param string $name
         * @param string $label
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertPageActionDoesNotHaveLabel($name, $label, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertPageActionDoesNotHaveLabel($name, $label, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Testing\TestsPageActions::assertPageActionHasColor()
         * @param string $name
         * @param string $color
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertPageActionHasColor($name, $color, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertPageActionHasColor($name, $color, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Testing\TestsPageActions::assertPageActionDoesNotHaveColor()
         * @param string $name
         * @param string $color
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertPageActionDoesNotHaveColor($name, $color, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertPageActionDoesNotHaveColor($name, $color, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Testing\TestsPageActions::assertPageActionHasUrl()
         * @param string $name
         * @param string $url
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertPageActionHasUrl($name, $url, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertPageActionHasUrl($name, $url, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Testing\TestsPageActions::assertPageActionDoesNotHaveUrl()
         * @param string $name
         * @param string $url
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertPageActionDoesNotHaveUrl($name, $url, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertPageActionDoesNotHaveUrl($name, $url, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Testing\TestsPageActions::assertPageActionShouldOpenUrlInNewTab()
         * @param string $name
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertPageActionShouldOpenUrlInNewTab($name, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertPageActionShouldOpenUrlInNewTab($name, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Testing\TestsPageActions::assertPageActionShouldNotOpenUrlInNewTab()
         * @param string $name
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertPageActionShouldNotOpenUrlInNewTab($name, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertPageActionShouldNotOpenUrlInNewTab($name, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Testing\TestsPageActions::assertPageActionHalted()
         * @param string $name
         * @return static 
         * @static 
         */ 
        public static function assertPageActionHalted($name)
        {
                        return \Livewire\Testing\TestableLivewire::assertPageActionHalted($name);
        }
                    /**
         * 
         *
         * @see \Filament\Testing\TestsPageActions::assertPageActionHalted()
         * @param string $name
         * @return static 
         * @static 
         */ 
        public static function assertPageActionHeld($name)
        {
                        return \Livewire\Testing\TestableLivewire::assertPageActionHeld($name);
        }
                    /**
         * 
         *
         * @see \Filament\Testing\TestsPageActions::assertHasPageActionErrors()
         * @param array $keys
         * @return static 
         * @static 
         */ 
        public static function assertHasPageActionErrors($keys = [])
        {
                        return \Livewire\Testing\TestableLivewire::assertHasPageActionErrors($keys);
        }
                    /**
         * 
         *
         * @see \Filament\Testing\TestsPageActions::assertHasNoPageActionErrors()
         * @param array $keys
         * @return static 
         * @static 
         */ 
        public static function assertHasNoPageActionErrors($keys = [])
        {
                        return \Livewire\Testing\TestableLivewire::assertHasNoPageActionErrors($keys);
        }
                    /**
         * 
         *
         * @see \Filament\Forms\Testing\TestsForms::fillForm()
         * @param array $state
         * @param string $formName
         * @return static 
         * @static 
         */ 
        public static function fillForm($state = [], $formName = 'form')
        {
                        return \Livewire\Testing\TestableLivewire::fillForm($state, $formName);
        }
                    /**
         * 
         *
         * @see \Filament\Forms\Testing\TestsForms::assertFormSet()
         * @param array $state
         * @param string $formName
         * @return static 
         * @static 
         */ 
        public static function assertFormSet($state, $formName = 'form')
        {
                        return \Livewire\Testing\TestableLivewire::assertFormSet($state, $formName);
        }
                    /**
         * 
         *
         * @see \Filament\Forms\Testing\TestsForms::assertHasFormErrors()
         * @param array $keys
         * @param string $formName
         * @return static 
         * @static 
         */ 
        public static function assertHasFormErrors($keys = [], $formName = 'form')
        {
                        return \Livewire\Testing\TestableLivewire::assertHasFormErrors($keys, $formName);
        }
                    /**
         * 
         *
         * @see \Filament\Forms\Testing\TestsForms::assertHasNoFormErrors()
         * @param array $keys
         * @param string $formName
         * @return static 
         * @static 
         */ 
        public static function assertHasNoFormErrors($keys = [], $formName = 'form')
        {
                        return \Livewire\Testing\TestableLivewire::assertHasNoFormErrors($keys, $formName);
        }
                    /**
         * 
         *
         * @see \Filament\Forms\Testing\TestsForms::assertFormExists()
         * @param string $name
         * @return static 
         * @static 
         */ 
        public static function assertFormExists($name = 'form')
        {
                        return \Livewire\Testing\TestableLivewire::assertFormExists($name);
        }
                    /**
         * 
         *
         * @see \Filament\Forms\Testing\TestsForms::assertFormFieldExists()
         * @param string $fieldName
         * @param \Closure|string $formName
         * @param \Closure|null $callback
         * @return static 
         * @static 
         */ 
        public static function assertFormFieldExists($fieldName, $formName = 'form', $callback = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertFormFieldExists($fieldName, $formName, $callback);
        }
                    /**
         * 
         *
         * @see \Filament\Forms\Testing\TestsForms::assertFormFieldIsDisabled()
         * @param string $fieldName
         * @param string $formName
         * @return static 
         * @static 
         */ 
        public static function assertFormFieldIsDisabled($fieldName, $formName = 'form')
        {
                        return \Livewire\Testing\TestableLivewire::assertFormFieldIsDisabled($fieldName, $formName);
        }
                    /**
         * 
         *
         * @see \Filament\Forms\Testing\TestsForms::assertFormFieldIsEnabled()
         * @param string $fieldName
         * @param string $formName
         * @return static 
         * @static 
         */ 
        public static function assertFormFieldIsEnabled($fieldName, $formName = 'form')
        {
                        return \Livewire\Testing\TestableLivewire::assertFormFieldIsEnabled($fieldName, $formName);
        }
                    /**
         * 
         *
         * @see \Filament\Forms\Testing\TestsForms::assertFormFieldIsHidden()
         * @param string $fieldName
         * @param string $formName
         * @return static 
         * @static 
         */ 
        public static function assertFormFieldIsHidden($fieldName, $formName = 'form')
        {
                        return \Livewire\Testing\TestableLivewire::assertFormFieldIsHidden($fieldName, $formName);
        }
                    /**
         * 
         *
         * @see \Filament\Forms\Testing\TestsForms::assertFormFieldIsVisible()
         * @param string $fieldName
         * @param string $formName
         * @return static 
         * @static 
         */ 
        public static function assertFormFieldIsVisible($fieldName, $formName = 'form')
        {
                        return \Livewire\Testing\TestableLivewire::assertFormFieldIsVisible($fieldName, $formName);
        }
                    /**
         * 
         *
         * @see \Filament\Notifications\Testing\TestsNotifications::assertNotified()
         * @param \Filament\Notifications\Notification|string|null $notification
         * @return static 
         * @static 
         */ 
        public static function assertNotified($notification = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertNotified($notification);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsActions::mountTableAction()
         * @param string $name
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function mountTableAction($name, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::mountTableAction($name, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsActions::setTableActionData()
         * @param array $data
         * @return static 
         * @static 
         */ 
        public static function setTableActionData($data)
        {
                        return \Livewire\Testing\TestableLivewire::setTableActionData($data);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsActions::assertTableActionDataSet()
         * @param array $data
         * @return static 
         * @static 
         */ 
        public static function assertTableActionDataSet($data)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableActionDataSet($data);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsActions::callTableAction()
         * @param string $name
         * @param mixed $record
         * @param array $data
         * @param array $arguments
         * @return static 
         * @static 
         */ 
        public static function callTableAction($name, $record = null, $data = [], $arguments = [])
        {
                        return \Livewire\Testing\TestableLivewire::callTableAction($name, $record, $data, $arguments);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsActions::callMountedTableAction()
         * @param array $arguments
         * @return static 
         * @static 
         */ 
        public static function callMountedTableAction($arguments = [])
        {
                        return \Livewire\Testing\TestableLivewire::callMountedTableAction($arguments);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsActions::assertTableActionExists()
         * @param string $name
         * @return static 
         * @static 
         */ 
        public static function assertTableActionExists($name)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableActionExists($name);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsActions::assertTableActionDoesNotExist()
         * @param string $name
         * @return static 
         * @static 
         */ 
        public static function assertTableActionDoesNotExist($name)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableActionDoesNotExist($name);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsActions::assertTableActionsExistInOrder()
         * @param array $names
         * @return static 
         * @static 
         */ 
        public static function assertTableActionsExistInOrder($names)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableActionsExistInOrder($names);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsActions::assertTableHeaderActionsExistInOrder()
         * @param array $names
         * @return static 
         * @static 
         */ 
        public static function assertTableHeaderActionsExistInOrder($names)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableHeaderActionsExistInOrder($names);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsActions::assertTableEmptyStateActionsExistInOrder()
         * @param array $names
         * @return static 
         * @static 
         */ 
        public static function assertTableEmptyStateActionsExistInOrder($names)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableEmptyStateActionsExistInOrder($names);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsActions::assertTableActionVisible()
         * @param string $name
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertTableActionVisible($name, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableActionVisible($name, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsActions::assertTableActionHidden()
         * @param string $name
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertTableActionHidden($name, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableActionHidden($name, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsActions::assertTableActionEnabled()
         * @param string $name
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertTableActionEnabled($name, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableActionEnabled($name, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsActions::assertTableActionDisabled()
         * @param string $name
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertTableActionDisabled($name, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableActionDisabled($name, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsActions::assertTableActionHasIcon()
         * @param string $name
         * @param string $icon
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertTableActionHasIcon($name, $icon, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableActionHasIcon($name, $icon, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsActions::assertTableActionDoesNotHaveIcon()
         * @param string $name
         * @param string $icon
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertTableActionDoesNotHaveIcon($name, $icon, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableActionDoesNotHaveIcon($name, $icon, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsActions::assertTableActionHasLabel()
         * @param string $name
         * @param string $label
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertTableActionHasLabel($name, $label, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableActionHasLabel($name, $label, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsActions::assertTableActionDoesNotHaveLabel()
         * @param string $name
         * @param string $label
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertTableActionDoesNotHaveLabel($name, $label, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableActionDoesNotHaveLabel($name, $label, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsActions::assertTableActionHasColor()
         * @param string $name
         * @param string $color
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertTableActionHasColor($name, $color, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableActionHasColor($name, $color, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsActions::assertTableActionDoesNotHaveColor()
         * @param string $name
         * @param string $color
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertTableActionDoesNotHaveColor($name, $color, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableActionDoesNotHaveColor($name, $color, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsActions::assertTableActionHasUrl()
         * @param string $name
         * @param string $url
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertTableActionHasUrl($name, $url, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableActionHasUrl($name, $url, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsActions::assertTableActionDoesNotHaveUrl()
         * @param string $name
         * @param string $url
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertTableActionDoesNotHaveUrl($name, $url, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableActionDoesNotHaveUrl($name, $url, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsActions::assertTableActionShouldOpenUrlInNewTab()
         * @param string $name
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertTableActionShouldOpenUrlInNewTab($name, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableActionShouldOpenUrlInNewTab($name, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsActions::assertTableActionShouldNotOpenUrlInNewTab()
         * @param string $name
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertTableActionShouldNotOpenUrlInNewTab($name, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableActionShouldNotOpenUrlInNewTab($name, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsActions::assertTableActionHalted()
         * @param string $name
         * @return static 
         * @static 
         */ 
        public static function assertTableActionHalted($name)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableActionHalted($name);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsActions::assertTableActionHalted()
         * @param string $name
         * @return static 
         * @static 
         */ 
        public static function assertTableActionHeld($name)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableActionHeld($name);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsActions::assertHasTableActionErrors()
         * @param array $keys
         * @return static 
         * @static 
         */ 
        public static function assertHasTableActionErrors($keys = [])
        {
                        return \Livewire\Testing\TestableLivewire::assertHasTableActionErrors($keys);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsActions::assertHasNoTableActionErrors()
         * @param array $keys
         * @return static 
         * @static 
         */ 
        public static function assertHasNoTableActionErrors($keys = [])
        {
                        return \Livewire\Testing\TestableLivewire::assertHasNoTableActionErrors($keys);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsBulkActions::mountTableBulkAction()
         * @param string $name
         * @param \Illuminate\Support\Collection|array $records
         * @return static 
         * @static 
         */ 
        public static function mountTableBulkAction($name, $records)
        {
                        return \Livewire\Testing\TestableLivewire::mountTableBulkAction($name, $records);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsBulkActions::setTableBulkActionData()
         * @param array $data
         * @return static 
         * @static 
         */ 
        public static function setTableBulkActionData($data)
        {
                        return \Livewire\Testing\TestableLivewire::setTableBulkActionData($data);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsBulkActions::assertTableBulkActionDataSet()
         * @param array $data
         * @return static 
         * @static 
         */ 
        public static function assertTableBulkActionDataSet($data)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableBulkActionDataSet($data);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsBulkActions::callTableBulkAction()
         * @param string $name
         * @param \Illuminate\Support\Collection|array $records
         * @param array $data
         * @param array $arguments
         * @return static 
         * @static 
         */ 
        public static function callTableBulkAction($name, $records, $data = [], $arguments = [])
        {
                        return \Livewire\Testing\TestableLivewire::callTableBulkAction($name, $records, $data, $arguments);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsBulkActions::callMountedTableBulkAction()
         * @param array $arguments
         * @return static 
         * @static 
         */ 
        public static function callMountedTableBulkAction($arguments = [])
        {
                        return \Livewire\Testing\TestableLivewire::callMountedTableBulkAction($arguments);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsBulkActions::assertTableBulkActionExists()
         * @param string $name
         * @return static 
         * @static 
         */ 
        public static function assertTableBulkActionExists($name)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableBulkActionExists($name);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsBulkActions::assertTableBulkActionDoesNotExist()
         * @param string $name
         * @return static 
         * @static 
         */ 
        public static function assertTableBulkActionDoesNotExist($name)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableBulkActionDoesNotExist($name);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsBulkActions::assertTableBulkActionsExistInOrder()
         * @param array $names
         * @return static 
         * @static 
         */ 
        public static function assertTableBulkActionsExistInOrder($names)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableBulkActionsExistInOrder($names);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsBulkActions::assertTableBulkActionVisible()
         * @param string $name
         * @return static 
         * @static 
         */ 
        public static function assertTableBulkActionVisible($name)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableBulkActionVisible($name);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsBulkActions::assertTableBulkActionHidden()
         * @param string $name
         * @return static 
         * @static 
         */ 
        public static function assertTableBulkActionHidden($name)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableBulkActionHidden($name);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsBulkActions::assertTableBulkActionEnabled()
         * @param string $name
         * @return static 
         * @static 
         */ 
        public static function assertTableBulkActionEnabled($name)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableBulkActionEnabled($name);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsBulkActions::assertTableBulkActionDisabled()
         * @param string $name
         * @return static 
         * @static 
         */ 
        public static function assertTableBulkActionDisabled($name)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableBulkActionDisabled($name);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsBulkActions::assertTableBulkActionHasIcon()
         * @param string $name
         * @param string $icon
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertTableBulkActionHasIcon($name, $icon, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableBulkActionHasIcon($name, $icon, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsBulkActions::assertTableBulkActionDoesNotHaveIcon()
         * @param string $name
         * @param string $icon
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertTableBulkActionDoesNotHaveIcon($name, $icon, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableBulkActionDoesNotHaveIcon($name, $icon, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsBulkActions::assertTableBulkActionHasLabel()
         * @param string $name
         * @param string $label
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertTableBulkActionHasLabel($name, $label, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableBulkActionHasLabel($name, $label, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsBulkActions::assertTableBulkActionDoesNotHaveLabel()
         * @param string $name
         * @param string $label
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertTableBulkActionDoesNotHaveLabel($name, $label, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableBulkActionDoesNotHaveLabel($name, $label, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsBulkActions::assertTableBulkActionHasColor()
         * @param string $name
         * @param string $color
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertTableBulkActionHasColor($name, $color, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableBulkActionHasColor($name, $color, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsBulkActions::assertTableBulkActionDoesNotHaveColor()
         * @param string $name
         * @param string $color
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertTableBulkActionDoesNotHaveColor($name, $color, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableBulkActionDoesNotHaveColor($name, $color, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsBulkActions::assertTableBulkActionHalted()
         * @param string $name
         * @return static 
         * @static 
         */ 
        public static function assertTableBulkActionHalted($name)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableBulkActionHalted($name);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsBulkActions::assertTableBulkActionHalted()
         * @param string $name
         * @return static 
         * @static 
         */ 
        public static function assertTableBulkActionHeld($name)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableBulkActionHeld($name);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsBulkActions::assertHasTableBulkActionErrors()
         * @param array $keys
         * @return static 
         * @static 
         */ 
        public static function assertHasTableBulkActionErrors($keys = [])
        {
                        return \Livewire\Testing\TestableLivewire::assertHasTableBulkActionErrors($keys);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsBulkActions::assertHasNoTableBulkActionErrors()
         * @param array $keys
         * @return static 
         * @static 
         */ 
        public static function assertHasNoTableBulkActionErrors($keys = [])
        {
                        return \Livewire\Testing\TestableLivewire::assertHasNoTableBulkActionErrors($keys);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsColumns::assertCanRenderTableColumn()
         * @param string $name
         * @return static 
         * @static 
         */ 
        public static function assertCanRenderTableColumn($name)
        {
                        return \Livewire\Testing\TestableLivewire::assertCanRenderTableColumn($name);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsColumns::assertCanNotRenderTableColumn()
         * @param string $name
         * @return static 
         * @static 
         */ 
        public static function assertCanNotRenderTableColumn($name)
        {
                        return \Livewire\Testing\TestableLivewire::assertCanNotRenderTableColumn($name);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsColumns::assertTableColumnExists()
         * @param string $name
         * @return static 
         * @static 
         */ 
        public static function assertTableColumnExists($name)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableColumnExists($name);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsColumns::assertTableColumnVisible()
         * @param string $name
         * @return static 
         * @static 
         */ 
        public static function assertTableColumnVisible($name)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableColumnVisible($name);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsColumns::assertTableColumnHidden()
         * @param string $name
         * @return static 
         * @static 
         */ 
        public static function assertTableColumnHidden($name)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableColumnHidden($name);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsColumns::assertTableColumnStateSet()
         * @param string $name
         * @param mixed $value
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertTableColumnStateSet($name, $value, $record)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableColumnStateSet($name, $value, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsColumns::assertTableColumnStateNotSet()
         * @param string $name
         * @param mixed $value
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertTableColumnStateNotSet($name, $value, $record)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableColumnStateNotSet($name, $value, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsColumns::assertTableColumnFormattedStateSet()
         * @param string $name
         * @param mixed $value
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertTableColumnFormattedStateSet($name, $value, $record)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableColumnFormattedStateSet($name, $value, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsColumns::assertTableColumnFormattedStateNotSet()
         * @param string $name
         * @param mixed $value
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function assertTableColumnFormattedStateNotSet($name, $value, $record)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableColumnFormattedStateNotSet($name, $value, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsColumns::assertTableColumnHasExtraAttributes()
         * @param string $name
         * @param array $attributes
         * @param mixed $record
         * @static 
         */ 
        public static function assertTableColumnHasExtraAttributes($name, $attributes, $record)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableColumnHasExtraAttributes($name, $attributes, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsColumns::assertTableColumnDoesNotHaveExtraAttributes()
         * @param string $name
         * @param array $attributes
         * @param mixed $record
         * @static 
         */ 
        public static function assertTableColumnDoesNotHaveExtraAttributes($name, $attributes, $record)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableColumnDoesNotHaveExtraAttributes($name, $attributes, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsColumns::assertTableColumnHasDescription()
         * @param string $name
         * @param mixed $description
         * @param mixed $record
         * @param string $position
         * @static 
         */ 
        public static function assertTableColumnHasDescription($name, $description, $record, $position = 'below')
        {
                        return \Livewire\Testing\TestableLivewire::assertTableColumnHasDescription($name, $description, $record, $position);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsColumns::assertTableColumnDoesNotHaveDescription()
         * @param string $name
         * @param mixed $description
         * @param mixed $record
         * @param string $position
         * @static 
         */ 
        public static function assertTableColumnDoesNotHaveDescription($name, $description, $record, $position = 'below')
        {
                        return \Livewire\Testing\TestableLivewire::assertTableColumnDoesNotHaveDescription($name, $description, $record, $position);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsColumns::assertSelectColumnHasOptions()
         * @param string $name
         * @param array $options
         * @param mixed $record
         * @static 
         */ 
        public static function assertSelectColumnHasOptions($name, $options, $record)
        {
                        return \Livewire\Testing\TestableLivewire::assertSelectColumnHasOptions($name, $options, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsColumns::assertSelectColumnDoesNotHaveOptions()
         * @param string $name
         * @param array $options
         * @param mixed $record
         * @static 
         */ 
        public static function assertSelectColumnDoesNotHaveOptions($name, $options, $record)
        {
                        return \Livewire\Testing\TestableLivewire::assertSelectColumnDoesNotHaveOptions($name, $options, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsColumns::callTableColumnAction()
         * @param string $name
         * @param mixed $record
         * @return static 
         * @static 
         */ 
        public static function callTableColumnAction($name, $record = null)
        {
                        return \Livewire\Testing\TestableLivewire::callTableColumnAction($name, $record);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsColumns::sortTable()
         * @param string|null $name
         * @param string|null $direction
         * @return static 
         * @static 
         */ 
        public static function sortTable($name = null, $direction = null)
        {
                        return \Livewire\Testing\TestableLivewire::sortTable($name, $direction);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsColumns::searchTable()
         * @param string|null $search
         * @return static 
         * @static 
         */ 
        public static function searchTable($search = null)
        {
                        return \Livewire\Testing\TestableLivewire::searchTable($search);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsColumns::searchTableColumns()
         * @param array $searches
         * @return static 
         * @static 
         */ 
        public static function searchTableColumns($searches)
        {
                        return \Livewire\Testing\TestableLivewire::searchTableColumns($searches);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsFilters::filterTable()
         * @param string $name
         * @param mixed $data
         * @return static 
         * @static 
         */ 
        public static function filterTable($name, $data = null)
        {
                        return \Livewire\Testing\TestableLivewire::filterTable($name, $data);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsFilters::resetTableFilters()
         * @return static 
         * @static 
         */ 
        public static function resetTableFilters()
        {
                        return \Livewire\Testing\TestableLivewire::resetTableFilters();
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsFilters::removeTableFilter()
         * @param string $filter
         * @param string|null $field
         * @return static 
         * @static 
         */ 
        public static function removeTableFilter($filter, $field = null)
        {
                        return \Livewire\Testing\TestableLivewire::removeTableFilter($filter, $field);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsFilters::removeTableFilters()
         * @return static 
         * @static 
         */ 
        public static function removeTableFilters()
        {
                        return \Livewire\Testing\TestableLivewire::removeTableFilters();
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsFilters::assertTableFilterExists()
         * @param string $name
         * @return static 
         * @static 
         */ 
        public static function assertTableFilterExists($name)
        {
                        return \Livewire\Testing\TestableLivewire::assertTableFilterExists($name);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsRecords::assertCanSeeTableRecords()
         * @param \Illuminate\Support\Collection|array $records
         * @param bool $inOrder
         * @return static 
         * @static 
         */ 
        public static function assertCanSeeTableRecords($records, $inOrder = false)
        {
                        return \Livewire\Testing\TestableLivewire::assertCanSeeTableRecords($records, $inOrder);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsRecords::assertCanNotSeeTableRecords()
         * @param \Illuminate\Support\Collection|array $records
         * @return static 
         * @static 
         */ 
        public static function assertCanNotSeeTableRecords($records)
        {
                        return \Livewire\Testing\TestableLivewire::assertCanNotSeeTableRecords($records);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsRecords::assertCountTableRecords()
         * @param int $count
         * @return static 
         * @static 
         */ 
        public static function assertCountTableRecords($count)
        {
                        return \Livewire\Testing\TestableLivewire::assertCountTableRecords($count);
        }
                    /**
         * 
         *
         * @see \Filament\Tables\Testing\TestsRecords::loadTable()
         * @return static 
         * @static 
         */ 
        public static function loadTable()
        {
                        return \Livewire\Testing\TestableLivewire::loadTable();
        }
         
    }
     
}

    namespace Illuminate\Testing { 
            /**
     * 
     *
     * @mixin \Illuminate\Http\Response
     */ 
        class TestResponse {
                    /**
         * 
         *
         * @see \Livewire\LivewireServiceProvider::registerTestMacros()
         * @param mixed $component
         * @static 
         */ 
        public static function assertSeeLivewire($component)
        {
                        return \Illuminate\Testing\TestResponse::assertSeeLivewire($component);
        }
                    /**
         * 
         *
         * @see \Livewire\LivewireServiceProvider::registerTestMacros()
         * @param mixed $component
         * @static 
         */ 
        public static function assertDontSeeLivewire($component)
        {
                        return \Illuminate\Testing\TestResponse::assertDontSeeLivewire($component);
        }
                    /**
         * 
         *
         * @see \Spatie\LaravelRay\RayServiceProvider::registerMacros()
         * @static 
         */ 
        public static function ray()
        {
                        return \Illuminate\Testing\TestResponse::ray();
        }
         
    }
            /**
     * 
     *
     */ 
        class TestView {
                    /**
         * 
         *
         * @see \Livewire\LivewireServiceProvider::registerTestMacros()
         * @param mixed $component
         * @static 
         */ 
        public static function assertSeeLivewire($component)
        {
                        return \Illuminate\Testing\TestView::assertSeeLivewire($component);
        }
                    /**
         * 
         *
         * @see \Livewire\LivewireServiceProvider::registerTestMacros()
         * @param mixed $component
         * @static 
         */ 
        public static function assertDontSeeLivewire($component)
        {
                        return \Illuminate\Testing\TestView::assertDontSeeLivewire($component);
        }
         
    }
     
}

    namespace Illuminate\Validation { 
            /**
     * 
     *
     */ 
        class Rule {
                    /**
         * 
         *
         * @see \Propaganistas\LaravelPhone\PhoneServiceProvider::registerValidator()
         * @static 
         */ 
        public static function phone()
        {
                        return \Illuminate\Validation\Rule::phone();
        }
         
    }
     
}

    namespace Illuminate\Database\Query { 
            /**
     * 
     *
     */ 
        class Builder {
                    /**
         * 
         *
         * @see \Spatie\LaravelRay\RayServiceProvider::registerMacros()
         * @static 
         */ 
        public static function ray()
        {
                        return \Illuminate\Database\Query\Builder::ray();
        }
         
    }
     
}

    namespace Illuminate\Routing { 
            /**
     * 
     *
     */ 
        class Route {
                    /**
         * 
         *
         * @see \Spatie\Permission\PermissionServiceProvider::registerMacroHelpers()
         * @param mixed $roles
         * @static 
         */ 
        public static function role($roles = [])
        {
                        return \Illuminate\Routing\Route::role($roles);
        }
                    /**
         * 
         *
         * @see \Spatie\Permission\PermissionServiceProvider::registerMacroHelpers()
         * @param mixed $permissions
         * @static 
         */ 
        public static function permission($permissions = [])
        {
                        return \Illuminate\Routing\Route::permission($permissions);
        }
         
    }
     
}

    namespace Illuminate\View { 
            /**
     * 
     *
     */ 
        class ComponentAttributeBag {
                    /**
         * 
         *
         * @see \Livewire\LivewireServiceProvider::registerViewMacros()
         * @param mixed $name
         * @static 
         */ 
        public static function wire($name)
        {
                        return \Illuminate\View\ComponentAttributeBag::wire($name);
        }
         
    }
            /**
     * 
     *
     */ 
        class View {
                    /**
         * 
         *
         * @see \Livewire\Macros\ViewMacros::extends()
         * @param mixed $view
         * @param mixed $params
         * @static 
         */ 
        public static function extends($view, $params = [])
        {
                        return \Illuminate\View\View::extends($view, $params);
        }
                    /**
         * 
         *
         * @see \Livewire\Macros\ViewMacros::layout()
         * @param mixed $view
         * @param mixed $params
         * @static 
         */ 
        public static function layout($view, $params = [])
        {
                        return \Illuminate\View\View::layout($view, $params);
        }
                    /**
         * 
         *
         * @see \Livewire\Macros\ViewMacros::layoutData()
         * @param mixed $data
         * @static 
         */ 
        public static function layoutData($data = [])
        {
                        return \Illuminate\View\View::layoutData($data);
        }
                    /**
         * 
         *
         * @see \Livewire\Macros\ViewMacros::section()
         * @param mixed $section
         * @static 
         */ 
        public static function section($section)
        {
                        return \Illuminate\View\View::section($section);
        }
                    /**
         * 
         *
         * @see \Livewire\Macros\ViewMacros::slot()
         * @param mixed $slot
         * @static 
         */ 
        public static function slot($slot)
        {
                        return \Illuminate\View\View::slot($slot);
        }
         
    }
     
}

    namespace Illuminate\Database\Schema { 
            /**
     * 
     *
     */ 
        class Blueprint {
                    /**
         * 
         *
         * @see \SolutionForest\FilamentTree\Macros\BlueprintMarcos::treeColumns()
         * @param string $titleType
         * @static 
         */ 
        public static function treeColumns($titleType = 'string')
        {
                        return \Illuminate\Database\Schema\Blueprint::treeColumns($titleType);
        }
         
    }
     
}


namespace  { 
            class Eloquent extends \Illuminate\Database\Eloquent\Model {             
                /**
             * Create and return an un-saved model instance.
             *
             * @param array $attributes
             * @return \Illuminate\Database\Eloquent\Model|static 
             * @static 
             */ 
            public static function make($attributes = [])
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->make($attributes);
            }
             
                /**
             * Register a new global scope.
             *
             * @param string $identifier
             * @param \Illuminate\Database\Eloquent\Scope|\Closure $scope
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function withGlobalScope($identifier, $scope)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->withGlobalScope($identifier, $scope);
            }
             
                /**
             * Remove a registered global scope.
             *
             * @param \Illuminate\Database\Eloquent\Scope|string $scope
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function withoutGlobalScope($scope)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->withoutGlobalScope($scope);
            }
             
                /**
             * Remove all or passed registered global scopes.
             *
             * @param array|null $scopes
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function withoutGlobalScopes($scopes = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->withoutGlobalScopes($scopes);
            }
             
                /**
             * Get an array of global scopes that were removed from the query.
             *
             * @return array 
             * @static 
             */ 
            public static function removedScopes()
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->removedScopes();
            }
             
                /**
             * Add a where clause on the primary key to the query.
             *
             * @param mixed $id
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function whereKey($id)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->whereKey($id);
            }
             
                /**
             * Add a where clause on the primary key to the query.
             *
             * @param mixed $id
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function whereKeyNot($id)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->whereKeyNot($id);
            }
             
                /**
             * Add a basic where clause to the query.
             *
             * @param \Closure|string|array|\Illuminate\Contracts\Database\Query\Expression $column
             * @param mixed $operator
             * @param mixed $value
             * @param string $boolean
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function where($column, $operator = null, $value = null, $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->where($column, $operator, $value, $boolean);
            }
             
                /**
             * Add a basic where clause to the query, and return the first result.
             *
             * @param \Closure|string|array|\Illuminate\Contracts\Database\Query\Expression $column
             * @param mixed $operator
             * @param mixed $value
             * @param string $boolean
             * @return \Illuminate\Database\Eloquent\Model|static|null 
             * @static 
             */ 
            public static function firstWhere($column, $operator = null, $value = null, $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->firstWhere($column, $operator, $value, $boolean);
            }
             
                /**
             * Add an "or where" clause to the query.
             *
             * @param \Closure|array|string|\Illuminate\Contracts\Database\Query\Expression $column
             * @param mixed $operator
             * @param mixed $value
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function orWhere($column, $operator = null, $value = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->orWhere($column, $operator, $value);
            }
             
                /**
             * Add a basic "where not" clause to the query.
             *
             * @param \Closure|string|array|\Illuminate\Contracts\Database\Query\Expression $column
             * @param mixed $operator
             * @param mixed $value
             * @param string $boolean
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function whereNot($column, $operator = null, $value = null, $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->whereNot($column, $operator, $value, $boolean);
            }
             
                /**
             * Add an "or where not" clause to the query.
             *
             * @param \Closure|array|string|\Illuminate\Contracts\Database\Query\Expression $column
             * @param mixed $operator
             * @param mixed $value
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function orWhereNot($column, $operator = null, $value = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->orWhereNot($column, $operator, $value);
            }
             
                /**
             * Add an "order by" clause for a timestamp to the query.
             *
             * @param string|\Illuminate\Contracts\Database\Query\Expression $column
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function latest($column = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->latest($column);
            }
             
                /**
             * Add an "order by" clause for a timestamp to the query.
             *
             * @param string|\Illuminate\Contracts\Database\Query\Expression $column
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function oldest($column = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->oldest($column);
            }
             
                /**
             * Create a collection of models from plain arrays.
             *
             * @param array $items
             * @return \Illuminate\Database\Eloquent\Collection 
             * @static 
             */ 
            public static function hydrate($items)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->hydrate($items);
            }
             
                /**
             * Create a collection of models from a raw query.
             *
             * @param string $query
             * @param array $bindings
             * @return \Illuminate\Database\Eloquent\Collection 
             * @static 
             */ 
            public static function fromQuery($query, $bindings = [])
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->fromQuery($query, $bindings);
            }
             
                /**
             * Find a model by its primary key.
             *
             * @param mixed $id
             * @param array|string $columns
             * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|static[]|static|null 
             * @static 
             */ 
            public static function find($id, $columns = [])
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->find($id, $columns);
            }
             
                /**
             * Find multiple models by their primary keys.
             *
             * @param \Illuminate\Contracts\Support\Arrayable|array $ids
             * @param array|string $columns
             * @return \Illuminate\Database\Eloquent\Collection 
             * @static 
             */ 
            public static function findMany($ids, $columns = [])
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->findMany($ids, $columns);
            }
             
                /**
             * Find a model by its primary key or throw an exception.
             *
             * @param mixed $id
             * @param array|string $columns
             * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|static|static[] 
             * @throws \Illuminate\Database\Eloquent\ModelNotFoundException<\Illuminate\Database\Eloquent\Model>
             * @static 
             */ 
            public static function findOrFail($id, $columns = [])
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->findOrFail($id, $columns);
            }
             
                /**
             * Find a model by its primary key or return fresh model instance.
             *
             * @param mixed $id
             * @param array|string $columns
             * @return \Illuminate\Database\Eloquent\Model|static 
             * @static 
             */ 
            public static function findOrNew($id, $columns = [])
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->findOrNew($id, $columns);
            }
             
                /**
             * Find a model by its primary key or call a callback.
             *
             * @param mixed $id
             * @param \Closure|array|string $columns
             * @param \Closure|null $callback
             * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|static[]|static|mixed 
             * @static 
             */ 
            public static function findOr($id, $columns = [], $callback = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->findOr($id, $columns, $callback);
            }
             
                /**
             * Get the first record matching the attributes or instantiate it.
             *
             * @param array $attributes
             * @param array $values
             * @return \Illuminate\Database\Eloquent\Model|static 
             * @static 
             */ 
            public static function firstOrNew($attributes = [], $values = [])
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->firstOrNew($attributes, $values);
            }
             
                /**
             * Get the first record matching the attributes or create it.
             *
             * @param array $attributes
             * @param array $values
             * @return \Illuminate\Database\Eloquent\Model|static 
             * @static 
             */ 
            public static function firstOrCreate($attributes = [], $values = [])
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->firstOrCreate($attributes, $values);
            }
             
                /**
             * Create or update a record matching the attributes, and fill it with values.
             *
             * @param array $attributes
             * @param array $values
             * @return \Illuminate\Database\Eloquent\Model|static 
             * @static 
             */ 
            public static function updateOrCreate($attributes, $values = [])
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->updateOrCreate($attributes, $values);
            }
             
                /**
             * Execute the query and get the first result or throw an exception.
             *
             * @param array|string $columns
             * @return \Illuminate\Database\Eloquent\Model|static 
             * @throws \Illuminate\Database\Eloquent\ModelNotFoundException<\Illuminate\Database\Eloquent\Model>
             * @static 
             */ 
            public static function firstOrFail($columns = [])
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->firstOrFail($columns);
            }
             
                /**
             * Execute the query and get the first result or call a callback.
             *
             * @param \Closure|array|string $columns
             * @param \Closure|null $callback
             * @return \Illuminate\Database\Eloquent\Model|static|mixed 
             * @static 
             */ 
            public static function firstOr($columns = [], $callback = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->firstOr($columns, $callback);
            }
             
                /**
             * Execute the query and get the first result if it's the sole matching record.
             *
             * @param array|string $columns
             * @return \Illuminate\Database\Eloquent\Model 
             * @throws \Illuminate\Database\Eloquent\ModelNotFoundException<\Illuminate\Database\Eloquent\Model>
             * @throws \Illuminate\Database\MultipleRecordsFoundException
             * @static 
             */ 
            public static function sole($columns = [])
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->sole($columns);
            }
             
                /**
             * Get a single column's value from the first result of a query.
             *
             * @param string|\Illuminate\Contracts\Database\Query\Expression $column
             * @return mixed 
             * @static 
             */ 
            public static function value($column)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->value($column);
            }
             
                /**
             * Get a single column's value from the first result of a query if it's the sole matching record.
             *
             * @param string|\Illuminate\Contracts\Database\Query\Expression $column
             * @return mixed 
             * @throws \Illuminate\Database\Eloquent\ModelNotFoundException<\Illuminate\Database\Eloquent\Model>
             * @throws \Illuminate\Database\MultipleRecordsFoundException
             * @static 
             */ 
            public static function soleValue($column)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->soleValue($column);
            }
             
                /**
             * Get a single column's value from the first result of the query or throw an exception.
             *
             * @param string|\Illuminate\Contracts\Database\Query\Expression $column
             * @return mixed 
             * @throws \Illuminate\Database\Eloquent\ModelNotFoundException<\Illuminate\Database\Eloquent\Model>
             * @static 
             */ 
            public static function valueOrFail($column)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->valueOrFail($column);
            }
             
                /**
             * Execute the query as a "select" statement.
             *
             * @param array|string $columns
             * @return \Illuminate\Database\Eloquent\Collection|static[] 
             * @static 
             */ 
            public static function get($columns = [])
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->get($columns);
            }
             
                /**
             * Get the hydrated models without eager loading.
             *
             * @param array|string $columns
             * @return \Illuminate\Database\Eloquent\Model[]|static[] 
             * @static 
             */ 
            public static function getModels($columns = [])
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->getModels($columns);
            }
             
                /**
             * Eager load the relationships for the models.
             *
             * @param array $models
             * @return array 
             * @static 
             */ 
            public static function eagerLoadRelations($models)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->eagerLoadRelations($models);
            }
             
                /**
             * Get a lazy collection for the given query.
             *
             * @return \Illuminate\Support\LazyCollection 
             * @static 
             */ 
            public static function cursor()
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->cursor();
            }
             
                /**
             * Get a collection with the values of a given column.
             *
             * @param string|\Illuminate\Contracts\Database\Query\Expression $column
             * @param string|null $key
             * @return \Illuminate\Support\Collection 
             * @static 
             */ 
            public static function pluck($column, $key = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->pluck($column, $key);
            }
             
                /**
             * Paginate the given query.
             *
             * @param int|null|\Closure $perPage
             * @param array|string $columns
             * @param string $pageName
             * @param int|null $page
             * @param \Closure|int|null $total
             * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator 
             * @throws \InvalidArgumentException
             * @static 
             */ 
            public static function paginate($perPage = null, $columns = [], $pageName = 'page', $page = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->paginate($perPage, $columns, $pageName, $page);
            }
             
                /**
             * Paginate the given query into a simple paginator.
             *
             * @param int|null $perPage
             * @param array|string $columns
             * @param string $pageName
             * @param int|null $page
             * @return \Illuminate\Contracts\Pagination\Paginator 
             * @static 
             */ 
            public static function simplePaginate($perPage = null, $columns = [], $pageName = 'page', $page = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->simplePaginate($perPage, $columns, $pageName, $page);
            }
             
                /**
             * Paginate the given query into a cursor paginator.
             *
             * @param int|null $perPage
             * @param array|string $columns
             * @param string $cursorName
             * @param \Illuminate\Pagination\Cursor|string|null $cursor
             * @return \Illuminate\Contracts\Pagination\CursorPaginator 
             * @static 
             */ 
            public static function cursorPaginate($perPage = null, $columns = [], $cursorName = 'cursor', $cursor = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->cursorPaginate($perPage, $columns, $cursorName, $cursor);
            }
             
                /**
             * Save a new model and return the instance.
             *
             * @param array $attributes
             * @return \Illuminate\Database\Eloquent\Model|$this 
             * @static 
             */ 
            public static function create($attributes = [])
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->create($attributes);
            }
             
                /**
             * Save a new model and return the instance. Allow mass-assignment.
             *
             * @param array $attributes
             * @return \Illuminate\Database\Eloquent\Model|$this 
             * @static 
             */ 
            public static function forceCreate($attributes)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->forceCreate($attributes);
            }
             
                /**
             * Save a new model instance with mass assignment without raising model events.
             *
             * @param array $attributes
             * @return \Illuminate\Database\Eloquent\Model|$this 
             * @static 
             */ 
            public static function forceCreateQuietly($attributes = [])
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->forceCreateQuietly($attributes);
            }
             
                /**
             * Insert new records or update the existing ones.
             *
             * @param array $values
             * @param array|string $uniqueBy
             * @param array|null $update
             * @return int 
             * @static 
             */ 
            public static function upsert($values, $uniqueBy, $update = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->upsert($values, $uniqueBy, $update);
            }
             
                /**
             * Register a replacement for the default delete function.
             *
             * @param \Closure $callback
             * @return void 
             * @static 
             */ 
            public static function onDelete($callback)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                $instance->onDelete($callback);
            }
             
                /**
             * Call the given local model scopes.
             *
             * @param array|string $scopes
             * @return static|mixed 
             * @static 
             */ 
            public static function scopes($scopes)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->scopes($scopes);
            }
             
                /**
             * Apply the scopes to the Eloquent builder instance and return it.
             *
             * @return static 
             * @static 
             */ 
            public static function applyScopes()
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->applyScopes();
            }
             
                /**
             * Prevent the specified relations from being eager loaded.
             *
             * @param mixed $relations
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function without($relations)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->without($relations);
            }
             
                /**
             * Set the relationships that should be eager loaded while removing any previously added eager loading specifications.
             *
             * @param mixed $relations
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function withOnly($relations)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->withOnly($relations);
            }
             
                /**
             * Create a new instance of the model being queried.
             *
             * @param array $attributes
             * @return \Illuminate\Database\Eloquent\Model|static 
             * @static 
             */ 
            public static function newModelInstance($attributes = [])
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->newModelInstance($attributes);
            }
             
                /**
             * Apply query-time casts to the model instance.
             *
             * @param array $casts
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function withCasts($casts)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->withCasts($casts);
            }
             
                /**
             * Get the underlying query builder instance.
             *
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function getQuery()
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->getQuery();
            }
             
                /**
             * Set the underlying query builder instance.
             *
             * @param \Illuminate\Database\Query\Builder $query
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function setQuery($query)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->setQuery($query);
            }
             
                /**
             * Get a base query builder instance.
             *
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function toBase()
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->toBase();
            }
             
                /**
             * Get the relationships being eagerly loaded.
             *
             * @return array 
             * @static 
             */ 
            public static function getEagerLoads()
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->getEagerLoads();
            }
             
                /**
             * Set the relationships being eagerly loaded.
             *
             * @param array $eagerLoad
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function setEagerLoads($eagerLoad)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->setEagerLoads($eagerLoad);
            }
             
                /**
             * Indicate that the given relationships should not be eagerly loaded.
             *
             * @param array $relations
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function withoutEagerLoad($relations)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->withoutEagerLoad($relations);
            }
             
                /**
             * Flush the relationships being eagerly loaded.
             *
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function withoutEagerLoads()
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->withoutEagerLoads();
            }
             
                /**
             * Get the model instance being queried.
             *
             * @return \Illuminate\Database\Eloquent\Model|static 
             * @static 
             */ 
            public static function getModel()
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->getModel();
            }
             
                /**
             * Set a model instance for the model being queried.
             *
             * @param \Illuminate\Database\Eloquent\Model $model
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function setModel($model)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->setModel($model);
            }
             
                /**
             * Get the given macro by name.
             *
             * @param string $name
             * @return \Closure 
             * @static 
             */ 
            public static function getMacro($name)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->getMacro($name);
            }
             
                /**
             * Checks if a macro is registered.
             *
             * @param string $name
             * @return bool 
             * @static 
             */ 
            public static function hasMacro($name)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->hasMacro($name);
            }
             
                /**
             * Get the given global macro by name.
             *
             * @param string $name
             * @return \Closure 
             * @static 
             */ 
            public static function getGlobalMacro($name)
            {
                                return \Illuminate\Database\Eloquent\Builder::getGlobalMacro($name);
            }
             
                /**
             * Checks if a global macro is registered.
             *
             * @param string $name
             * @return bool 
             * @static 
             */ 
            public static function hasGlobalMacro($name)
            {
                                return \Illuminate\Database\Eloquent\Builder::hasGlobalMacro($name);
            }
             
                /**
             * Clone the Eloquent query builder.
             *
             * @return static 
             * @static 
             */ 
            public static function clone()
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->clone();
            }
             
                /**
             * Chunk the results of the query.
             *
             * @param int $count
             * @param callable $callback
             * @return bool 
             * @static 
             */ 
            public static function chunk($count, $callback)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->chunk($count, $callback);
            }
             
                /**
             * Run a map over each item while chunking.
             *
             * @param callable $callback
             * @param int $count
             * @return \Illuminate\Support\Collection 
             * @static 
             */ 
            public static function chunkMap($callback, $count = 1000)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->chunkMap($callback, $count);
            }
             
                /**
             * Execute a callback over each item while chunking.
             *
             * @param callable $callback
             * @param int $count
             * @return bool 
             * @throws \RuntimeException
             * @static 
             */ 
            public static function each($callback, $count = 1000)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->each($callback, $count);
            }
             
                /**
             * Chunk the results of a query by comparing IDs.
             *
             * @param int $count
             * @param callable $callback
             * @param string|null $column
             * @param string|null $alias
             * @return bool 
             * @static 
             */ 
            public static function chunkById($count, $callback, $column = null, $alias = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->chunkById($count, $callback, $column, $alias);
            }
             
                /**
             * Execute a callback over each item while chunking by ID.
             *
             * @param callable $callback
             * @param int $count
             * @param string|null $column
             * @param string|null $alias
             * @return bool 
             * @static 
             */ 
            public static function eachById($callback, $count = 1000, $column = null, $alias = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->eachById($callback, $count, $column, $alias);
            }
             
                /**
             * Query lazily, by chunks of the given size.
             *
             * @param int $chunkSize
             * @return \Illuminate\Support\LazyCollection 
             * @throws \InvalidArgumentException
             * @static 
             */ 
            public static function lazy($chunkSize = 1000)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->lazy($chunkSize);
            }
             
                /**
             * Query lazily, by chunking the results of a query by comparing IDs.
             *
             * @param int $chunkSize
             * @param string|null $column
             * @param string|null $alias
             * @return \Illuminate\Support\LazyCollection 
             * @throws \InvalidArgumentException
             * @static 
             */ 
            public static function lazyById($chunkSize = 1000, $column = null, $alias = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->lazyById($chunkSize, $column, $alias);
            }
             
                /**
             * Query lazily, by chunking the results of a query by comparing IDs in descending order.
             *
             * @param int $chunkSize
             * @param string|null $column
             * @param string|null $alias
             * @return \Illuminate\Support\LazyCollection 
             * @throws \InvalidArgumentException
             * @static 
             */ 
            public static function lazyByIdDesc($chunkSize = 1000, $column = null, $alias = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->lazyByIdDesc($chunkSize, $column, $alias);
            }
             
                /**
             * Execute the query and get the first result.
             *
             * @param array|string $columns
             * @return \Illuminate\Database\Eloquent\Model|object|static|null 
             * @static 
             */ 
            public static function first($columns = [])
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->first($columns);
            }
             
                /**
             * Execute the query and get the first result if it's the sole matching record.
             *
             * @param array|string $columns
             * @return \Illuminate\Database\Eloquent\Model|object|static|null 
             * @throws \Illuminate\Database\RecordsNotFoundException
             * @throws \Illuminate\Database\MultipleRecordsFoundException
             * @static 
             */ 
            public static function baseSole($columns = [])
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->baseSole($columns);
            }
             
                /**
             * Pass the query to a given callback.
             *
             * @param callable $callback
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function tap($callback)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->tap($callback);
            }
             
                /**
             * Apply the callback if the given "value" is (or resolves to) truthy.
             *
             * @template TWhenParameter
             * @template TWhenReturnType
             * @param \Illuminate\Database\Eloquent\(\Closure($this):  TWhenParameter)|TWhenParameter|null  $value
             * @param \Illuminate\Database\Eloquent\(callable($this,  TWhenParameter): TWhenReturnType)|null  $callback
             * @param \Illuminate\Database\Eloquent\(callable($this,  TWhenParameter): TWhenReturnType)|null  $default
             * @return $this|\Illuminate\Database\Eloquent\TWhenReturnType 
             * @static 
             */ 
            public static function when($value = null, $callback = null, $default = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->when($value, $callback, $default);
            }
             
                /**
             * Apply the callback if the given "value" is (or resolves to) falsy.
             *
             * @template TUnlessParameter
             * @template TUnlessReturnType
             * @param \Illuminate\Database\Eloquent\(\Closure($this):  TUnlessParameter)|TUnlessParameter|null  $value
             * @param \Illuminate\Database\Eloquent\(callable($this,  TUnlessParameter): TUnlessReturnType)|null  $callback
             * @param \Illuminate\Database\Eloquent\(callable($this,  TUnlessParameter): TUnlessReturnType)|null  $default
             * @return $this|\Illuminate\Database\Eloquent\TUnlessReturnType 
             * @static 
             */ 
            public static function unless($value = null, $callback = null, $default = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->unless($value, $callback, $default);
            }
             
                /**
             * Add a relationship count / exists condition to the query.
             *
             * @param \Illuminate\Database\Eloquent\Relations\Relation|string $relation
             * @param string $operator
             * @param int $count
             * @param string $boolean
             * @param \Closure|null $callback
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @throws \RuntimeException
             * @static 
             */ 
            public static function has($relation, $operator = '>=', $count = 1, $boolean = 'and', $callback = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->has($relation, $operator, $count, $boolean, $callback);
            }
             
                /**
             * Add a relationship count / exists condition to the query with an "or".
             *
             * @param string $relation
             * @param string $operator
             * @param int $count
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function orHas($relation, $operator = '>=', $count = 1)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->orHas($relation, $operator, $count);
            }
             
                /**
             * Add a relationship count / exists condition to the query.
             *
             * @param string $relation
             * @param string $boolean
             * @param \Closure|null $callback
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function doesntHave($relation, $boolean = 'and', $callback = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->doesntHave($relation, $boolean, $callback);
            }
             
                /**
             * Add a relationship count / exists condition to the query with an "or".
             *
             * @param string $relation
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function orDoesntHave($relation)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->orDoesntHave($relation);
            }
             
                /**
             * Add a relationship count / exists condition to the query with where clauses.
             *
             * @param string $relation
             * @param \Closure|null $callback
             * @param string $operator
             * @param int $count
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function whereHas($relation, $callback = null, $operator = '>=', $count = 1)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->whereHas($relation, $callback, $operator, $count);
            }
             
                /**
             * Add a relationship count / exists condition to the query with where clauses.
             * 
             * Also load the relationship with same condition.
             *
             * @param string $relation
             * @param \Closure|null $callback
             * @param string $operator
             * @param int $count
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function withWhereHas($relation, $callback = null, $operator = '>=', $count = 1)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->withWhereHas($relation, $callback, $operator, $count);
            }
             
                /**
             * Add a relationship count / exists condition to the query with where clauses and an "or".
             *
             * @param string $relation
             * @param \Closure|null $callback
             * @param string $operator
             * @param int $count
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function orWhereHas($relation, $callback = null, $operator = '>=', $count = 1)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->orWhereHas($relation, $callback, $operator, $count);
            }
             
                /**
             * Add a relationship count / exists condition to the query with where clauses.
             *
             * @param string $relation
             * @param \Closure|null $callback
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function whereDoesntHave($relation, $callback = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->whereDoesntHave($relation, $callback);
            }
             
                /**
             * Add a relationship count / exists condition to the query with where clauses and an "or".
             *
             * @param string $relation
             * @param \Closure|null $callback
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function orWhereDoesntHave($relation, $callback = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->orWhereDoesntHave($relation, $callback);
            }
             
                /**
             * Add a polymorphic relationship count / exists condition to the query.
             *
             * @param \Illuminate\Database\Eloquent\Relations\MorphTo|string $relation
             * @param string|array $types
             * @param string $operator
             * @param int $count
             * @param string $boolean
             * @param \Closure|null $callback
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function hasMorph($relation, $types, $operator = '>=', $count = 1, $boolean = 'and', $callback = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->hasMorph($relation, $types, $operator, $count, $boolean, $callback);
            }
             
                /**
             * Add a polymorphic relationship count / exists condition to the query with an "or".
             *
             * @param \Illuminate\Database\Eloquent\Relations\MorphTo|string $relation
             * @param string|array $types
             * @param string $operator
             * @param int $count
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function orHasMorph($relation, $types, $operator = '>=', $count = 1)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->orHasMorph($relation, $types, $operator, $count);
            }
             
                /**
             * Add a polymorphic relationship count / exists condition to the query.
             *
             * @param \Illuminate\Database\Eloquent\Relations\MorphTo|string $relation
             * @param string|array $types
             * @param string $boolean
             * @param \Closure|null $callback
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function doesntHaveMorph($relation, $types, $boolean = 'and', $callback = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->doesntHaveMorph($relation, $types, $boolean, $callback);
            }
             
                /**
             * Add a polymorphic relationship count / exists condition to the query with an "or".
             *
             * @param \Illuminate\Database\Eloquent\Relations\MorphTo|string $relation
             * @param string|array $types
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function orDoesntHaveMorph($relation, $types)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->orDoesntHaveMorph($relation, $types);
            }
             
                /**
             * Add a polymorphic relationship count / exists condition to the query with where clauses.
             *
             * @param \Illuminate\Database\Eloquent\Relations\MorphTo|string $relation
             * @param string|array $types
             * @param \Closure|null $callback
             * @param string $operator
             * @param int $count
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function whereHasMorph($relation, $types, $callback = null, $operator = '>=', $count = 1)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->whereHasMorph($relation, $types, $callback, $operator, $count);
            }
             
                /**
             * Add a polymorphic relationship count / exists condition to the query with where clauses and an "or".
             *
             * @param \Illuminate\Database\Eloquent\Relations\MorphTo|string $relation
             * @param string|array $types
             * @param \Closure|null $callback
             * @param string $operator
             * @param int $count
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function orWhereHasMorph($relation, $types, $callback = null, $operator = '>=', $count = 1)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->orWhereHasMorph($relation, $types, $callback, $operator, $count);
            }
             
                /**
             * Add a polymorphic relationship count / exists condition to the query with where clauses.
             *
             * @param \Illuminate\Database\Eloquent\Relations\MorphTo|string $relation
             * @param string|array $types
             * @param \Closure|null $callback
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function whereDoesntHaveMorph($relation, $types, $callback = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->whereDoesntHaveMorph($relation, $types, $callback);
            }
             
                /**
             * Add a polymorphic relationship count / exists condition to the query with where clauses and an "or".
             *
             * @param \Illuminate\Database\Eloquent\Relations\MorphTo|string $relation
             * @param string|array $types
             * @param \Closure|null $callback
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function orWhereDoesntHaveMorph($relation, $types, $callback = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->orWhereDoesntHaveMorph($relation, $types, $callback);
            }
             
                /**
             * Add a basic where clause to a relationship query.
             *
             * @param string $relation
             * @param \Closure|string|array|\Illuminate\Contracts\Database\Query\Expression $column
             * @param mixed $operator
             * @param mixed $value
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function whereRelation($relation, $column, $operator = null, $value = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->whereRelation($relation, $column, $operator, $value);
            }
             
                /**
             * Add an "or where" clause to a relationship query.
             *
             * @param string $relation
             * @param \Closure|string|array|\Illuminate\Contracts\Database\Query\Expression $column
             * @param mixed $operator
             * @param mixed $value
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function orWhereRelation($relation, $column, $operator = null, $value = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->orWhereRelation($relation, $column, $operator, $value);
            }
             
                /**
             * Add a polymorphic relationship condition to the query with a where clause.
             *
             * @param \Illuminate\Database\Eloquent\Relations\MorphTo|string $relation
             * @param string|array $types
             * @param \Closure|string|array|\Illuminate\Contracts\Database\Query\Expression $column
             * @param mixed $operator
             * @param mixed $value
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function whereMorphRelation($relation, $types, $column, $operator = null, $value = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->whereMorphRelation($relation, $types, $column, $operator, $value);
            }
             
                /**
             * Add a polymorphic relationship condition to the query with an "or where" clause.
             *
             * @param \Illuminate\Database\Eloquent\Relations\MorphTo|string $relation
             * @param string|array $types
             * @param \Closure|string|array|\Illuminate\Contracts\Database\Query\Expression $column
             * @param mixed $operator
             * @param mixed $value
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function orWhereMorphRelation($relation, $types, $column, $operator = null, $value = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->orWhereMorphRelation($relation, $types, $column, $operator, $value);
            }
             
                /**
             * Add a morph-to relationship condition to the query.
             *
             * @param \Illuminate\Database\Eloquent\Relations\MorphTo|string $relation
             * @param \Illuminate\Database\Eloquent\Model|string|null $model
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function whereMorphedTo($relation, $model, $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->whereMorphedTo($relation, $model, $boolean);
            }
             
                /**
             * Add a not morph-to relationship condition to the query.
             *
             * @param \Illuminate\Database\Eloquent\Relations\MorphTo|string $relation
             * @param \Illuminate\Database\Eloquent\Model|string $model
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function whereNotMorphedTo($relation, $model, $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->whereNotMorphedTo($relation, $model, $boolean);
            }
             
                /**
             * Add a morph-to relationship condition to the query with an "or where" clause.
             *
             * @param \Illuminate\Database\Eloquent\Relations\MorphTo|string $relation
             * @param \Illuminate\Database\Eloquent\Model|string|null $model
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function orWhereMorphedTo($relation, $model)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->orWhereMorphedTo($relation, $model);
            }
             
                /**
             * Add a not morph-to relationship condition to the query with an "or where" clause.
             *
             * @param \Illuminate\Database\Eloquent\Relations\MorphTo|string $relation
             * @param \Illuminate\Database\Eloquent\Model|string $model
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function orWhereNotMorphedTo($relation, $model)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->orWhereNotMorphedTo($relation, $model);
            }
             
                /**
             * Add a "belongs to" relationship where clause to the query.
             *
             * @param \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection<\Illuminate\Database\Eloquent\Model> $related
             * @param string|null $relationshipName
             * @param string $boolean
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @throws \Illuminate\Database\Eloquent\RelationNotFoundException
             * @static 
             */ 
            public static function whereBelongsTo($related, $relationshipName = null, $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->whereBelongsTo($related, $relationshipName, $boolean);
            }
             
                /**
             * Add an "BelongsTo" relationship with an "or where" clause to the query.
             *
             * @param \Illuminate\Database\Eloquent\Model $related
             * @param string|null $relationshipName
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @throws \RuntimeException
             * @static 
             */ 
            public static function orWhereBelongsTo($related, $relationshipName = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->orWhereBelongsTo($related, $relationshipName);
            }
             
                /**
             * Add subselect queries to include an aggregate value for a relationship.
             *
             * @param mixed $relations
             * @param string $column
             * @param string $function
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function withAggregate($relations, $column, $function = null)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->withAggregate($relations, $column, $function);
            }
             
                /**
             * Add subselect queries to count the relations.
             *
             * @param mixed $relations
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function withCount($relations)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->withCount($relations);
            }
             
                /**
             * Add subselect queries to include the max of the relation's column.
             *
             * @param string|array $relation
             * @param string $column
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function withMax($relation, $column)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->withMax($relation, $column);
            }
             
                /**
             * Add subselect queries to include the min of the relation's column.
             *
             * @param string|array $relation
             * @param string $column
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function withMin($relation, $column)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->withMin($relation, $column);
            }
             
                /**
             * Add subselect queries to include the sum of the relation's column.
             *
             * @param string|array $relation
             * @param string $column
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function withSum($relation, $column)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->withSum($relation, $column);
            }
             
                /**
             * Add subselect queries to include the average of the relation's column.
             *
             * @param string|array $relation
             * @param string $column
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function withAvg($relation, $column)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->withAvg($relation, $column);
            }
             
                /**
             * Add subselect queries to include the existence of related models.
             *
             * @param string|array $relation
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function withExists($relation)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->withExists($relation);
            }
             
                /**
             * Merge the where constraints from another query to the current query.
             *
             * @param \Illuminate\Database\Eloquent\Builder $from
             * @return \Illuminate\Database\Eloquent\Builder|static 
             * @static 
             */ 
            public static function mergeConstraintsFrom($from)
            {
                                /** @var \Illuminate\Database\Eloquent\Builder $instance */
                                return $instance->mergeConstraintsFrom($from);
            }
             
                /**
             * Set the columns to be selected.
             *
             * @param array|mixed $columns
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function select($columns = [])
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->select($columns);
            }
             
                /**
             * Add a subselect expression to the query.
             *
             * @param \Closure|\Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder|string $query
             * @param string $as
             * @return \Illuminate\Database\Query\Builder 
             * @throws \InvalidArgumentException
             * @static 
             */ 
            public static function selectSub($query, $as)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->selectSub($query, $as);
            }
             
                /**
             * Add a new "raw" select expression to the query.
             *
             * @param string $expression
             * @param array $bindings
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function selectRaw($expression, $bindings = [])
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->selectRaw($expression, $bindings);
            }
             
                /**
             * Makes "from" fetch from a subquery.
             *
             * @param \Closure|\Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder|string $query
             * @param string $as
             * @return \Illuminate\Database\Query\Builder 
             * @throws \InvalidArgumentException
             * @static 
             */ 
            public static function fromSub($query, $as)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->fromSub($query, $as);
            }
             
                /**
             * Add a raw from clause to the query.
             *
             * @param string $expression
             * @param mixed $bindings
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function fromRaw($expression, $bindings = [])
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->fromRaw($expression, $bindings);
            }
             
                /**
             * Add a new select column to the query.
             *
             * @param array|mixed $column
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function addSelect($column)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->addSelect($column);
            }
             
                /**
             * Force the query to only return distinct results.
             *
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function distinct()
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->distinct();
            }
             
                /**
             * Set the table which the query is targeting.
             *
             * @param \Closure|\Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder|string $table
             * @param string|null $as
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function from($table, $as = null)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->from($table, $as);
            }
             
                /**
             * Add an index hint to suggest a query index.
             *
             * @param string $index
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function useIndex($index)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->useIndex($index);
            }
             
                /**
             * Add an index hint to force a query index.
             *
             * @param string $index
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function forceIndex($index)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->forceIndex($index);
            }
             
                /**
             * Add an index hint to ignore a query index.
             *
             * @param string $index
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function ignoreIndex($index)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->ignoreIndex($index);
            }
             
                /**
             * Add a join clause to the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $table
             * @param \Closure|string $first
             * @param string|null $operator
             * @param string|null $second
             * @param string $type
             * @param bool $where
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function join($table, $first, $operator = null, $second = null, $type = 'inner', $where = false)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->join($table, $first, $operator, $second, $type, $where);
            }
             
                /**
             * Add a "join where" clause to the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $table
             * @param \Closure|string $first
             * @param string $operator
             * @param string $second
             * @param string $type
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function joinWhere($table, $first, $operator, $second, $type = 'inner')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->joinWhere($table, $first, $operator, $second, $type);
            }
             
                /**
             * Add a subquery join clause to the query.
             *
             * @param \Closure|\Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder|string $query
             * @param string $as
             * @param \Closure|string $first
             * @param string|null $operator
             * @param string|null $second
             * @param string $type
             * @param bool $where
             * @return \Illuminate\Database\Query\Builder 
             * @throws \InvalidArgumentException
             * @static 
             */ 
            public static function joinSub($query, $as, $first, $operator = null, $second = null, $type = 'inner', $where = false)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->joinSub($query, $as, $first, $operator, $second, $type, $where);
            }
             
                /**
             * Add a left join to the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $table
             * @param \Closure|string $first
             * @param string|null $operator
             * @param string|null $second
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function leftJoin($table, $first, $operator = null, $second = null)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->leftJoin($table, $first, $operator, $second);
            }
             
                /**
             * Add a "join where" clause to the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $table
             * @param \Closure|string $first
             * @param string $operator
             * @param string $second
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function leftJoinWhere($table, $first, $operator, $second)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->leftJoinWhere($table, $first, $operator, $second);
            }
             
                /**
             * Add a subquery left join to the query.
             *
             * @param \Closure|\Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder|string $query
             * @param string $as
             * @param \Closure|string $first
             * @param string|null $operator
             * @param string|null $second
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function leftJoinSub($query, $as, $first, $operator = null, $second = null)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->leftJoinSub($query, $as, $first, $operator, $second);
            }
             
                /**
             * Add a right join to the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $table
             * @param \Closure|string $first
             * @param string|null $operator
             * @param string|null $second
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function rightJoin($table, $first, $operator = null, $second = null)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->rightJoin($table, $first, $operator, $second);
            }
             
                /**
             * Add a "right join where" clause to the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $table
             * @param \Closure|string $first
             * @param string $operator
             * @param string $second
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function rightJoinWhere($table, $first, $operator, $second)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->rightJoinWhere($table, $first, $operator, $second);
            }
             
                /**
             * Add a subquery right join to the query.
             *
             * @param \Closure|\Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder|string $query
             * @param string $as
             * @param \Closure|string $first
             * @param string|null $operator
             * @param string|null $second
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function rightJoinSub($query, $as, $first, $operator = null, $second = null)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->rightJoinSub($query, $as, $first, $operator, $second);
            }
             
                /**
             * Add a "cross join" clause to the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $table
             * @param \Closure|string|null $first
             * @param string|null $operator
             * @param string|null $second
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function crossJoin($table, $first = null, $operator = null, $second = null)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->crossJoin($table, $first, $operator, $second);
            }
             
                /**
             * Add a subquery cross join to the query.
             *
             * @param \Closure|\Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder|string $query
             * @param string $as
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function crossJoinSub($query, $as)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->crossJoinSub($query, $as);
            }
             
                /**
             * Merge an array of where clauses and bindings.
             *
             * @param array $wheres
             * @param array $bindings
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function mergeWheres($wheres, $bindings)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->mergeWheres($wheres, $bindings);
            }
             
                /**
             * Prepare the value and operator for a where clause.
             *
             * @param string $value
             * @param string $operator
             * @param bool $useDefault
             * @return array 
             * @throws \InvalidArgumentException
             * @static 
             */ 
            public static function prepareValueAndOperator($value, $operator, $useDefault = false)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->prepareValueAndOperator($value, $operator, $useDefault);
            }
             
                /**
             * Add a "where" clause comparing two columns to the query.
             *
             * @param string|array $first
             * @param string|null $operator
             * @param string|null $second
             * @param string|null $boolean
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function whereColumn($first, $operator = null, $second = null, $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->whereColumn($first, $operator, $second, $boolean);
            }
             
                /**
             * Add an "or where" clause comparing two columns to the query.
             *
             * @param string|array $first
             * @param string|null $operator
             * @param string|null $second
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orWhereColumn($first, $operator = null, $second = null)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orWhereColumn($first, $operator, $second);
            }
             
                /**
             * Add a raw where clause to the query.
             *
             * @param string $sql
             * @param mixed $bindings
             * @param string $boolean
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function whereRaw($sql, $bindings = [], $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->whereRaw($sql, $bindings, $boolean);
            }
             
                /**
             * Add a raw or where clause to the query.
             *
             * @param string $sql
             * @param mixed $bindings
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orWhereRaw($sql, $bindings = [])
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orWhereRaw($sql, $bindings);
            }
             
                /**
             * Add a "where in" clause to the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $column
             * @param mixed $values
             * @param string $boolean
             * @param bool $not
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function whereIn($column, $values, $boolean = 'and', $not = false)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->whereIn($column, $values, $boolean, $not);
            }
             
                /**
             * Add an "or where in" clause to the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $column
             * @param mixed $values
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orWhereIn($column, $values)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orWhereIn($column, $values);
            }
             
                /**
             * Add a "where not in" clause to the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $column
             * @param mixed $values
             * @param string $boolean
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function whereNotIn($column, $values, $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->whereNotIn($column, $values, $boolean);
            }
             
                /**
             * Add an "or where not in" clause to the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $column
             * @param mixed $values
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orWhereNotIn($column, $values)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orWhereNotIn($column, $values);
            }
             
                /**
             * Add a "where in raw" clause for integer values to the query.
             *
             * @param string $column
             * @param \Illuminate\Contracts\Support\Arrayable|array $values
             * @param string $boolean
             * @param bool $not
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function whereIntegerInRaw($column, $values, $boolean = 'and', $not = false)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->whereIntegerInRaw($column, $values, $boolean, $not);
            }
             
                /**
             * Add an "or where in raw" clause for integer values to the query.
             *
             * @param string $column
             * @param \Illuminate\Contracts\Support\Arrayable|array $values
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orWhereIntegerInRaw($column, $values)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orWhereIntegerInRaw($column, $values);
            }
             
                /**
             * Add a "where not in raw" clause for integer values to the query.
             *
             * @param string $column
             * @param \Illuminate\Contracts\Support\Arrayable|array $values
             * @param string $boolean
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function whereIntegerNotInRaw($column, $values, $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->whereIntegerNotInRaw($column, $values, $boolean);
            }
             
                /**
             * Add an "or where not in raw" clause for integer values to the query.
             *
             * @param string $column
             * @param \Illuminate\Contracts\Support\Arrayable|array $values
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orWhereIntegerNotInRaw($column, $values)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orWhereIntegerNotInRaw($column, $values);
            }
             
                /**
             * Add a "where null" clause to the query.
             *
             * @param string|array|\Illuminate\Contracts\Database\Query\Expression $columns
             * @param string $boolean
             * @param bool $not
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function whereNull($columns, $boolean = 'and', $not = false)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->whereNull($columns, $boolean, $not);
            }
             
                /**
             * Add an "or where null" clause to the query.
             *
             * @param string|array|\Illuminate\Contracts\Database\Query\Expression $column
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orWhereNull($column)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orWhereNull($column);
            }
             
                /**
             * Add a "where not null" clause to the query.
             *
             * @param string|array|\Illuminate\Contracts\Database\Query\Expression $columns
             * @param string $boolean
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function whereNotNull($columns, $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->whereNotNull($columns, $boolean);
            }
             
                /**
             * Add a where between statement to the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $column
             * @param \Illuminate\Database\Query\iterable $values
             * @param string $boolean
             * @param bool $not
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function whereBetween($column, $values, $boolean = 'and', $not = false)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->whereBetween($column, $values, $boolean, $not);
            }
             
                /**
             * Add a where between statement using columns to the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $column
             * @param array $values
             * @param string $boolean
             * @param bool $not
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function whereBetweenColumns($column, $values, $boolean = 'and', $not = false)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->whereBetweenColumns($column, $values, $boolean, $not);
            }
             
                /**
             * Add an or where between statement to the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $column
             * @param \Illuminate\Database\Query\iterable $values
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orWhereBetween($column, $values)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orWhereBetween($column, $values);
            }
             
                /**
             * Add an or where between statement using columns to the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $column
             * @param array $values
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orWhereBetweenColumns($column, $values)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orWhereBetweenColumns($column, $values);
            }
             
                /**
             * Add a where not between statement to the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $column
             * @param \Illuminate\Database\Query\iterable $values
             * @param string $boolean
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function whereNotBetween($column, $values, $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->whereNotBetween($column, $values, $boolean);
            }
             
                /**
             * Add a where not between statement using columns to the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $column
             * @param array $values
             * @param string $boolean
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function whereNotBetweenColumns($column, $values, $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->whereNotBetweenColumns($column, $values, $boolean);
            }
             
                /**
             * Add an or where not between statement to the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $column
             * @param \Illuminate\Database\Query\iterable $values
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orWhereNotBetween($column, $values)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orWhereNotBetween($column, $values);
            }
             
                /**
             * Add an or where not between statement using columns to the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $column
             * @param array $values
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orWhereNotBetweenColumns($column, $values)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orWhereNotBetweenColumns($column, $values);
            }
             
                /**
             * Add an "or where not null" clause to the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $column
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orWhereNotNull($column)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orWhereNotNull($column);
            }
             
                /**
             * Add a "where date" statement to the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $column
             * @param string $operator
             * @param \DateTimeInterface|string|null $value
             * @param string $boolean
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function whereDate($column, $operator, $value = null, $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->whereDate($column, $operator, $value, $boolean);
            }
             
                /**
             * Add an "or where date" statement to the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $column
             * @param string $operator
             * @param \DateTimeInterface|string|null $value
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orWhereDate($column, $operator, $value = null)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orWhereDate($column, $operator, $value);
            }
             
                /**
             * Add a "where time" statement to the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $column
             * @param string $operator
             * @param \DateTimeInterface|string|null $value
             * @param string $boolean
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function whereTime($column, $operator, $value = null, $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->whereTime($column, $operator, $value, $boolean);
            }
             
                /**
             * Add an "or where time" statement to the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $column
             * @param string $operator
             * @param \DateTimeInterface|string|null $value
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orWhereTime($column, $operator, $value = null)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orWhereTime($column, $operator, $value);
            }
             
                /**
             * Add a "where day" statement to the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $column
             * @param string $operator
             * @param \DateTimeInterface|string|int|null $value
             * @param string $boolean
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function whereDay($column, $operator, $value = null, $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->whereDay($column, $operator, $value, $boolean);
            }
             
                /**
             * Add an "or where day" statement to the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $column
             * @param string $operator
             * @param \DateTimeInterface|string|int|null $value
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orWhereDay($column, $operator, $value = null)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orWhereDay($column, $operator, $value);
            }
             
                /**
             * Add a "where month" statement to the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $column
             * @param string $operator
             * @param \DateTimeInterface|string|int|null $value
             * @param string $boolean
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function whereMonth($column, $operator, $value = null, $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->whereMonth($column, $operator, $value, $boolean);
            }
             
                /**
             * Add an "or where month" statement to the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $column
             * @param string $operator
             * @param \DateTimeInterface|string|int|null $value
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orWhereMonth($column, $operator, $value = null)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orWhereMonth($column, $operator, $value);
            }
             
                /**
             * Add a "where year" statement to the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $column
             * @param string $operator
             * @param \DateTimeInterface|string|int|null $value
             * @param string $boolean
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function whereYear($column, $operator, $value = null, $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->whereYear($column, $operator, $value, $boolean);
            }
             
                /**
             * Add an "or where year" statement to the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $column
             * @param string $operator
             * @param \DateTimeInterface|string|int|null $value
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orWhereYear($column, $operator, $value = null)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orWhereYear($column, $operator, $value);
            }
             
                /**
             * Add a nested where statement to the query.
             *
             * @param \Closure $callback
             * @param string $boolean
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function whereNested($callback, $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->whereNested($callback, $boolean);
            }
             
                /**
             * Create a new query instance for nested where condition.
             *
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function forNestedWhere()
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->forNestedWhere();
            }
             
                /**
             * Add another query builder as a nested where to the query builder.
             *
             * @param \Illuminate\Database\Query\Builder $query
             * @param string $boolean
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function addNestedWhereQuery($query, $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->addNestedWhereQuery($query, $boolean);
            }
             
                /**
             * Add an exists clause to the query.
             *
             * @param \Closure|\Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder $callback
             * @param string $boolean
             * @param bool $not
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function whereExists($callback, $boolean = 'and', $not = false)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->whereExists($callback, $boolean, $not);
            }
             
                /**
             * Add an or exists clause to the query.
             *
             * @param \Closure|\Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder $callback
             * @param bool $not
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orWhereExists($callback, $not = false)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orWhereExists($callback, $not);
            }
             
                /**
             * Add a where not exists clause to the query.
             *
             * @param \Closure|\Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder $callback
             * @param string $boolean
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function whereNotExists($callback, $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->whereNotExists($callback, $boolean);
            }
             
                /**
             * Add a where not exists clause to the query.
             *
             * @param \Closure|\Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder $callback
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orWhereNotExists($callback)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orWhereNotExists($callback);
            }
             
                /**
             * Add an exists clause to the query.
             *
             * @param \Illuminate\Database\Query\Builder $query
             * @param string $boolean
             * @param bool $not
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function addWhereExistsQuery($query, $boolean = 'and', $not = false)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->addWhereExistsQuery($query, $boolean, $not);
            }
             
                /**
             * Adds a where condition using row values.
             *
             * @param array $columns
             * @param string $operator
             * @param array $values
             * @param string $boolean
             * @return \Illuminate\Database\Query\Builder 
             * @throws \InvalidArgumentException
             * @static 
             */ 
            public static function whereRowValues($columns, $operator, $values, $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->whereRowValues($columns, $operator, $values, $boolean);
            }
             
                /**
             * Adds an or where condition using row values.
             *
             * @param array $columns
             * @param string $operator
             * @param array $values
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orWhereRowValues($columns, $operator, $values)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orWhereRowValues($columns, $operator, $values);
            }
             
                /**
             * Add a "where JSON contains" clause to the query.
             *
             * @param string $column
             * @param mixed $value
             * @param string $boolean
             * @param bool $not
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function whereJsonContains($column, $value, $boolean = 'and', $not = false)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->whereJsonContains($column, $value, $boolean, $not);
            }
             
                /**
             * Add an "or where JSON contains" clause to the query.
             *
             * @param string $column
             * @param mixed $value
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orWhereJsonContains($column, $value)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orWhereJsonContains($column, $value);
            }
             
                /**
             * Add a "where JSON not contains" clause to the query.
             *
             * @param string $column
             * @param mixed $value
             * @param string $boolean
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function whereJsonDoesntContain($column, $value, $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->whereJsonDoesntContain($column, $value, $boolean);
            }
             
                /**
             * Add an "or where JSON not contains" clause to the query.
             *
             * @param string $column
             * @param mixed $value
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orWhereJsonDoesntContain($column, $value)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orWhereJsonDoesntContain($column, $value);
            }
             
                /**
             * Add a clause that determines if a JSON path exists to the query.
             *
             * @param string $column
             * @param string $boolean
             * @param bool $not
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function whereJsonContainsKey($column, $boolean = 'and', $not = false)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->whereJsonContainsKey($column, $boolean, $not);
            }
             
                /**
             * Add an "or" clause that determines if a JSON path exists to the query.
             *
             * @param string $column
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orWhereJsonContainsKey($column)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orWhereJsonContainsKey($column);
            }
             
                /**
             * Add a clause that determines if a JSON path does not exist to the query.
             *
             * @param string $column
             * @param string $boolean
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function whereJsonDoesntContainKey($column, $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->whereJsonDoesntContainKey($column, $boolean);
            }
             
                /**
             * Add an "or" clause that determines if a JSON path does not exist to the query.
             *
             * @param string $column
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orWhereJsonDoesntContainKey($column)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orWhereJsonDoesntContainKey($column);
            }
             
                /**
             * Add a "where JSON length" clause to the query.
             *
             * @param string $column
             * @param mixed $operator
             * @param mixed $value
             * @param string $boolean
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function whereJsonLength($column, $operator, $value = null, $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->whereJsonLength($column, $operator, $value, $boolean);
            }
             
                /**
             * Add an "or where JSON length" clause to the query.
             *
             * @param string $column
             * @param mixed $operator
             * @param mixed $value
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orWhereJsonLength($column, $operator, $value = null)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orWhereJsonLength($column, $operator, $value);
            }
             
                /**
             * Handles dynamic "where" clauses to the query.
             *
             * @param string $method
             * @param array $parameters
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function dynamicWhere($method, $parameters)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->dynamicWhere($method, $parameters);
            }
             
                /**
             * Add a "where fulltext" clause to the query.
             *
             * @param string|string[] $columns
             * @param string $value
             * @param string $boolean
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function whereFullText($columns, $value, $options = [], $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->whereFullText($columns, $value, $options, $boolean);
            }
             
                /**
             * Add a "or where fulltext" clause to the query.
             *
             * @param string|string[] $columns
             * @param string $value
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orWhereFullText($columns, $value, $options = [])
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orWhereFullText($columns, $value, $options);
            }
             
                /**
             * Add a "group by" clause to the query.
             *
             * @param array|\Illuminate\Contracts\Database\Query\Expression|string $groups
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function groupBy(...$groups)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->groupBy(...$groups);
            }
             
                /**
             * Add a raw groupBy clause to the query.
             *
             * @param string $sql
             * @param array $bindings
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function groupByRaw($sql, $bindings = [])
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->groupByRaw($sql, $bindings);
            }
             
                /**
             * Add a "having" clause to the query.
             *
             * @param \Closure|string $column
             * @param string|int|float|null $operator
             * @param string|int|float|null $value
             * @param string $boolean
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function having($column, $operator = null, $value = null, $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->having($column, $operator, $value, $boolean);
            }
             
                /**
             * Add an "or having" clause to the query.
             *
             * @param \Closure|string $column
             * @param string|int|float|null $operator
             * @param string|int|float|null $value
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orHaving($column, $operator = null, $value = null)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orHaving($column, $operator, $value);
            }
             
                /**
             * Add a nested having statement to the query.
             *
             * @param \Closure $callback
             * @param string $boolean
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function havingNested($callback, $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->havingNested($callback, $boolean);
            }
             
                /**
             * Add another query builder as a nested having to the query builder.
             *
             * @param \Illuminate\Database\Query\Builder $query
             * @param string $boolean
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function addNestedHavingQuery($query, $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->addNestedHavingQuery($query, $boolean);
            }
             
                /**
             * Add a "having null" clause to the query.
             *
             * @param string|array $columns
             * @param string $boolean
             * @param bool $not
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function havingNull($columns, $boolean = 'and', $not = false)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->havingNull($columns, $boolean, $not);
            }
             
                /**
             * Add an "or having null" clause to the query.
             *
             * @param string $column
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orHavingNull($column)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orHavingNull($column);
            }
             
                /**
             * Add a "having not null" clause to the query.
             *
             * @param string|array $columns
             * @param string $boolean
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function havingNotNull($columns, $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->havingNotNull($columns, $boolean);
            }
             
                /**
             * Add an "or having not null" clause to the query.
             *
             * @param string $column
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orHavingNotNull($column)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orHavingNotNull($column);
            }
             
                /**
             * Add a "having between " clause to the query.
             *
             * @param string $column
             * @param \Illuminate\Database\Query\iterable $values
             * @param string $boolean
             * @param bool $not
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function havingBetween($column, $values, $boolean = 'and', $not = false)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->havingBetween($column, $values, $boolean, $not);
            }
             
                /**
             * Add a raw having clause to the query.
             *
             * @param string $sql
             * @param array $bindings
             * @param string $boolean
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function havingRaw($sql, $bindings = [], $boolean = 'and')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->havingRaw($sql, $bindings, $boolean);
            }
             
                /**
             * Add a raw or having clause to the query.
             *
             * @param string $sql
             * @param array $bindings
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orHavingRaw($sql, $bindings = [])
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orHavingRaw($sql, $bindings);
            }
             
                /**
             * Add an "order by" clause to the query.
             *
             * @param \Closure|\Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder|\Illuminate\Contracts\Database\Query\Expression|string $column
             * @param string $direction
             * @return \Illuminate\Database\Query\Builder 
             * @throws \InvalidArgumentException
             * @static 
             */ 
            public static function orderBy($column, $direction = 'asc')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orderBy($column, $direction);
            }
             
                /**
             * Add a descending "order by" clause to the query.
             *
             * @param \Closure|\Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder|\Illuminate\Contracts\Database\Query\Expression|string $column
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orderByDesc($column)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orderByDesc($column);
            }
             
                /**
             * Put the query's results in random order.
             *
             * @param string|int $seed
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function inRandomOrder($seed = '')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->inRandomOrder($seed);
            }
             
                /**
             * Add a raw "order by" clause to the query.
             *
             * @param string $sql
             * @param array $bindings
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function orderByRaw($sql, $bindings = [])
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->orderByRaw($sql, $bindings);
            }
             
                /**
             * Alias to set the "offset" value of the query.
             *
             * @param int $value
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function skip($value)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->skip($value);
            }
             
                /**
             * Set the "offset" value of the query.
             *
             * @param int $value
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function offset($value)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->offset($value);
            }
             
                /**
             * Alias to set the "limit" value of the query.
             *
             * @param int $value
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function take($value)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->take($value);
            }
             
                /**
             * Set the "limit" value of the query.
             *
             * @param int $value
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function limit($value)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->limit($value);
            }
             
                /**
             * Set the limit and offset for a given page.
             *
             * @param int $page
             * @param int $perPage
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function forPage($page, $perPage = 15)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->forPage($page, $perPage);
            }
             
                /**
             * Constrain the query to the previous "page" of results before a given ID.
             *
             * @param int $perPage
             * @param int|null $lastId
             * @param string $column
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function forPageBeforeId($perPage = 15, $lastId = 0, $column = 'id')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->forPageBeforeId($perPage, $lastId, $column);
            }
             
                /**
             * Constrain the query to the next "page" of results after a given ID.
             *
             * @param int $perPage
             * @param int|null $lastId
             * @param string $column
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function forPageAfterId($perPage = 15, $lastId = 0, $column = 'id')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->forPageAfterId($perPage, $lastId, $column);
            }
             
                /**
             * Remove all existing orders and optionally add a new order.
             *
             * @param \Closure|\Illuminate\Database\Query\Builder|\Illuminate\Contracts\Database\Query\Expression|string|null $column
             * @param string $direction
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function reorder($column = null, $direction = 'asc')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->reorder($column, $direction);
            }
             
                /**
             * Add a union statement to the query.
             *
             * @param \Closure|\Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder $query
             * @param bool $all
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function union($query, $all = false)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->union($query, $all);
            }
             
                /**
             * Add a union all statement to the query.
             *
             * @param \Closure|\Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder $query
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function unionAll($query)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->unionAll($query);
            }
             
                /**
             * Lock the selected rows in the table.
             *
             * @param string|bool $value
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function lock($value = true)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->lock($value);
            }
             
                /**
             * Lock the selected rows in the table for updating.
             *
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function lockForUpdate()
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->lockForUpdate();
            }
             
                /**
             * Share lock the selected rows in the table.
             *
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function sharedLock()
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->sharedLock();
            }
             
                /**
             * Register a closure to be invoked before the query is executed.
             *
             * @param callable $callback
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function beforeQuery($callback)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->beforeQuery($callback);
            }
             
                /**
             * Invoke the "before query" modification callbacks.
             *
             * @return void 
             * @static 
             */ 
            public static function applyBeforeQueryCallbacks()
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                $instance->applyBeforeQueryCallbacks();
            }
             
                /**
             * Get the SQL representation of the query.
             *
             * @return string 
             * @static 
             */ 
            public static function toSql()
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->toSql();
            }
             
                /**
             * Get a single expression value from the first result of a query.
             *
             * @param string $expression
             * @param array $bindings
             * @return mixed 
             * @static 
             */ 
            public static function rawValue($expression, $bindings = [])
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->rawValue($expression, $bindings);
            }
             
                /**
             * Get the count of the total records for the paginator.
             *
             * @param array $columns
             * @return int 
             * @static 
             */ 
            public static function getCountForPagination($columns = [])
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->getCountForPagination($columns);
            }
             
                /**
             * Concatenate values of a given column as a string.
             *
             * @param string $column
             * @param string $glue
             * @return string 
             * @static 
             */ 
            public static function implode($column, $glue = '')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->implode($column, $glue);
            }
             
                /**
             * Determine if any rows exist for the current query.
             *
             * @return bool 
             * @static 
             */ 
            public static function exists()
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->exists();
            }
             
                /**
             * Determine if no rows exist for the current query.
             *
             * @return bool 
             * @static 
             */ 
            public static function doesntExist()
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->doesntExist();
            }
             
                /**
             * Execute the given callback if no rows exist for the current query.
             *
             * @param \Closure $callback
             * @return mixed 
             * @static 
             */ 
            public static function existsOr($callback)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->existsOr($callback);
            }
             
                /**
             * Execute the given callback if rows exist for the current query.
             *
             * @param \Closure $callback
             * @return mixed 
             * @static 
             */ 
            public static function doesntExistOr($callback)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->doesntExistOr($callback);
            }
             
                /**
             * Retrieve the "count" result of the query.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $columns
             * @return int 
             * @static 
             */ 
            public static function count($columns = '*')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->count($columns);
            }
             
                /**
             * Retrieve the minimum value of a given column.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $column
             * @return mixed 
             * @static 
             */ 
            public static function min($column)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->min($column);
            }
             
                /**
             * Retrieve the maximum value of a given column.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $column
             * @return mixed 
             * @static 
             */ 
            public static function max($column)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->max($column);
            }
             
                /**
             * Retrieve the sum of the values of a given column.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $column
             * @return mixed 
             * @static 
             */ 
            public static function sum($column)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->sum($column);
            }
             
                /**
             * Retrieve the average of the values of a given column.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $column
             * @return mixed 
             * @static 
             */ 
            public static function avg($column)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->avg($column);
            }
             
                /**
             * Alias for the "avg" method.
             *
             * @param \Illuminate\Contracts\Database\Query\Expression|string $column
             * @return mixed 
             * @static 
             */ 
            public static function average($column)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->average($column);
            }
             
                /**
             * Execute an aggregate function on the database.
             *
             * @param string $function
             * @param array $columns
             * @return mixed 
             * @static 
             */ 
            public static function aggregate($function, $columns = [])
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->aggregate($function, $columns);
            }
             
                /**
             * Execute a numeric aggregate function on the database.
             *
             * @param string $function
             * @param array $columns
             * @return float|int 
             * @static 
             */ 
            public static function numericAggregate($function, $columns = [])
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->numericAggregate($function, $columns);
            }
             
                /**
             * Insert new records into the database.
             *
             * @param array $values
             * @return bool 
             * @static 
             */ 
            public static function insert($values)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->insert($values);
            }
             
                /**
             * Insert new records into the database while ignoring errors.
             *
             * @param array $values
             * @return int 
             * @static 
             */ 
            public static function insertOrIgnore($values)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->insertOrIgnore($values);
            }
             
                /**
             * Insert a new record and get the value of the primary key.
             *
             * @param array $values
             * @param string|null $sequence
             * @return int 
             * @static 
             */ 
            public static function insertGetId($values, $sequence = null)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->insertGetId($values, $sequence);
            }
             
                /**
             * Insert new records into the table using a subquery.
             *
             * @param array $columns
             * @param \Closure|\Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder|string $query
             * @return int 
             * @static 
             */ 
            public static function insertUsing($columns, $query)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->insertUsing($columns, $query);
            }
             
                /**
             * Update records in a PostgreSQL database using the update from syntax.
             *
             * @param array $values
             * @return int 
             * @static 
             */ 
            public static function updateFrom($values)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->updateFrom($values);
            }
             
                /**
             * Insert or update a record matching the attributes, and fill it with values.
             *
             * @param array $attributes
             * @param array $values
             * @return bool 
             * @static 
             */ 
            public static function updateOrInsert($attributes, $values = [])
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->updateOrInsert($attributes, $values);
            }
             
                /**
             * Increment the given column's values by the given amounts.
             *
             * @param \Illuminate\Database\Query\array<string,  float|int|numeric-string>  $columns
             * @param \Illuminate\Database\Query\array<string,  mixed>  $extra
             * @return int 
             * @throws \InvalidArgumentException
             * @static 
             */ 
            public static function incrementEach($columns, $extra = [])
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->incrementEach($columns, $extra);
            }
             
                /**
             * Decrement the given column's values by the given amounts.
             *
             * @param \Illuminate\Database\Query\array<string,  float|int|numeric-string>  $columns
             * @param \Illuminate\Database\Query\array<string,  mixed>  $extra
             * @return int 
             * @throws \InvalidArgumentException
             * @static 
             */ 
            public static function decrementEach($columns, $extra = [])
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->decrementEach($columns, $extra);
            }
             
                /**
             * Run a truncate statement on the table.
             *
             * @return void 
             * @static 
             */ 
            public static function truncate()
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                $instance->truncate();
            }
             
                /**
             * Get all of the query builder's columns in a text-only array with all expressions evaluated.
             *
             * @return array 
             * @static 
             */ 
            public static function getColumns()
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->getColumns();
            }
             
                /**
             * Create a raw database expression.
             *
             * @param mixed $value
             * @return \Illuminate\Contracts\Database\Query\Expression 
             * @static 
             */ 
            public static function raw($value)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->raw($value);
            }
             
                /**
             * Get the current query value bindings in a flattened array.
             *
             * @return array 
             * @static 
             */ 
            public static function getBindings()
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->getBindings();
            }
             
                /**
             * Get the raw array of bindings.
             *
             * @return array 
             * @static 
             */ 
            public static function getRawBindings()
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->getRawBindings();
            }
             
                /**
             * Set the bindings on the query builder.
             *
             * @param array $bindings
             * @param string $type
             * @return \Illuminate\Database\Query\Builder 
             * @throws \InvalidArgumentException
             * @static 
             */ 
            public static function setBindings($bindings, $type = 'where')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->setBindings($bindings, $type);
            }
             
                /**
             * Add a binding to the query.
             *
             * @param mixed $value
             * @param string $type
             * @return \Illuminate\Database\Query\Builder 
             * @throws \InvalidArgumentException
             * @static 
             */ 
            public static function addBinding($value, $type = 'where')
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->addBinding($value, $type);
            }
             
                /**
             * Cast the given binding value.
             *
             * @param mixed $value
             * @return mixed 
             * @static 
             */ 
            public static function castBinding($value)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->castBinding($value);
            }
             
                /**
             * Merge an array of bindings into our bindings.
             *
             * @param \Illuminate\Database\Query\Builder $query
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function mergeBindings($query)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->mergeBindings($query);
            }
             
                /**
             * Remove all of the expressions from a list of bindings.
             *
             * @param array $bindings
             * @return array 
             * @static 
             */ 
            public static function cleanBindings($bindings)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->cleanBindings($bindings);
            }
             
                /**
             * Get the database query processor instance.
             *
             * @return \Illuminate\Database\Query\Processors\Processor 
             * @static 
             */ 
            public static function getProcessor()
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->getProcessor();
            }
             
                /**
             * Get the query grammar instance.
             *
             * @return \Illuminate\Database\Query\Grammars\Grammar 
             * @static 
             */ 
            public static function getGrammar()
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->getGrammar();
            }
             
                /**
             * Use the "write" PDO connection when executing the query.
             *
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function useWritePdo()
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->useWritePdo();
            }
             
                /**
             * Clone the query without the given properties.
             *
             * @param array $properties
             * @return static 
             * @static 
             */ 
            public static function cloneWithout($properties)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->cloneWithout($properties);
            }
             
                /**
             * Clone the query without the given bindings.
             *
             * @param array $except
             * @return static 
             * @static 
             */ 
            public static function cloneWithoutBindings($except)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->cloneWithoutBindings($except);
            }
             
                /**
             * Dump the current SQL and bindings.
             *
             * @return \Illuminate\Database\Query\Builder 
             * @static 
             */ 
            public static function dump()
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->dump();
            }
             
                /**
             * Die and dump the current SQL and bindings.
             *
             * @return \Illuminate\Database\Query\never 
             * @static 
             */ 
            public static function dd()
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->dd();
            }
             
                /**
             * Explains the query.
             *
             * @return \Illuminate\Support\Collection 
             * @static 
             */ 
            public static function explain()
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->explain();
            }
             
                /**
             * Register a custom macro.
             *
             * @param string $name
             * @param object|callable $macro
             * @return void 
             * @static 
             */ 
            public static function macro($name, $macro)
            {
                                \Illuminate\Database\Query\Builder::macro($name, $macro);
            }
             
                /**
             * Mix another object into the class.
             *
             * @param object $mixin
             * @param bool $replace
             * @return void 
             * @throws \ReflectionException
             * @static 
             */ 
            public static function mixin($mixin, $replace = true)
            {
                                \Illuminate\Database\Query\Builder::mixin($mixin, $replace);
            }
             
                /**
             * Flush the existing macros.
             *
             * @return void 
             * @static 
             */ 
            public static function flushMacros()
            {
                                \Illuminate\Database\Query\Builder::flushMacros();
            }
             
                /**
             * Dynamically handle calls to the class.
             *
             * @param string $method
             * @param array $parameters
             * @return mixed 
             * @throws \BadMethodCallException
             * @static 
             */ 
            public static function macroCall($method, $parameters)
            {
                                /** @var \Illuminate\Database\Query\Builder $instance */
                                return $instance->macroCall($method, $parameters);
            }
             
                /**
             * 
             *
             * @see \Spatie\LaravelRay\RayServiceProvider::registerMacros()
             * @static 
             */ 
            public static function ray()
            {
                                return \Illuminate\Database\Query\Builder::ray();
            }
                    }
            class L5Swagger extends \L5Swagger\L5SwaggerFacade {}
            class Image extends \Intervention\Image\Facades\Image {}
            class Horizon extends \Laravel\Horizon\Horizon {}
            class Livewire extends \Livewire\Livewire {}
            class LogViewer extends \Opcodes\LogViewer\Facades\LogViewer {}
            class Saloon extends \Saloon\Laravel\Facades\Saloon {}
            class GoogleReCaptchaV3 extends \TimeHunter\LaravelGoogleReCaptchaV3\Facades\GoogleReCaptchaV3 {}
     
}




