<?php

declare(strict_types=1);

use Domain\Confirmations\Channels\SmsChannel;

return [
    'resend_delay_in_seconds' => [
        SmsChannel::class => (int)env('SMS_CONFIRMATION_RESEND_DELAY_IN_SECONDS', 60),
    ],

    'message' => [
        SmsChannel::class => (int)env('SMS_CONFIRMATION_MESSAGE'),
    ],
];
