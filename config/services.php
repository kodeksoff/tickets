<?php

declare(strict_types=1);

return [
    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
     */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
        'scheme' => 'https',
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'sms' => [
        'base_uri' => env('SMS_CENTER_HOST', 'https://smsc.ru'),
        'fake' => env('SMS_CENTER_FAKE', true),
        'timeout' => env('SMS_CENTER_TIMEOUT', 5),
        'login' => env('SMS_CENTER_LOGIN'),
        'password' => env('SMS_CENTER_PASSWORD'),
        'sender' => env('SMS_CENTER_SENDER', 'Gosha'),
        'min_balance' => env('SMS_CENTER_MIN_BALANCE', 0),
    ],
    'yookassa' => [
        'store_id' => env('YOOKASSA_STORE_ID'),
        'secret_key' => env('YOOKASSA_SECRET_KEY'),
    ],
    'dadata' => [
        'base_uri' => env('DADATA__HOST', 'https://suggestions.dadata.ru'),
        'api_token' => env('DADATA_API_TOKEN'),
        'secret_key' => env('DADATA_SECRET_KEY'),
        'fake' => env('DADATA_FAKE', true),
        'timeout' => env('DADATA_TIMEOUT', 5),
    ],
];
