<?php

declare(strict_types=1);

namespace App\Filament\Resources;

use App\Filament\Resources\SettingResource\Pages;
use Domain\Settings\Models\Setting;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Exception;

class SettingResource extends Resource
{
    /** @var string|null */
    protected static ?string $model = Setting::class;

    /** @var string|null */
    protected static ?string $navigationIcon = 'heroicon-o-collection';

    /** @var string|null */
    protected static ?string $modelLabel = 'Настройки';
    /** @var string|null */
    protected static ?string $pluralModelLabel = 'Настройки';
    /** @var string|null */
    protected static ?string $navigationLabel = 'Настройки';
    /** @var string|null */
    protected static ?string $navigationGroup = 'Настройки';

    /** @var string|null */
    protected static ?string $recordTitleAttribute = 'environment';

    /**
     * @param Form $form
     *
     * @return Form
     */
    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Card::make()
                    ->schema([
                        Forms\Components\TextInput::make('booking_period')
                            ->label('Срок бронирования по-умолчанию')
                            ->integer()
                            ->placeholder('Введите кол-во дней'),
                        Forms\Components\KeyValue::make('contacts')
                            ->label('Контакты для футера')
                            ->keyLabel('Ключ')
                            ->valueLabel('Значение'),
                    ]),
            ]);
    }

    /**
     * @param Table $table
     *
     * @return Table
     * @throws Exception
     */
    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('environment')
                    ->label('Окружение')
                    ->searchable()
                    ->sortable(),
            ])
            ->filters([

            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    /** @return array */
    public static function getRelations(): array
    {
        return [

        ];
    }

    /** @return array */
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListSettings::route('/'),
            'edit' => Pages\EditSetting::route('/{record}/edit'),
        ];
    }
}
