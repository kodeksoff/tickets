<?php

declare(strict_types=1);

namespace App\Filament\Resources\ServiceResource\Pages;

use App\Filament\Resources\ServiceResource;
use Domain\Services\Enums\ServiceStatus;
use Domain\Services\Events\ServicePublishModerationEvent;
use Domain\Services\Events\ServiceRejectedModerationEvent;
use Exception;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;
use Filament\Forms;
use Illuminate\Database\DatabaseManager;
use Illuminate\Contracts\Auth\Factory;
use Illuminate\Events\Dispatcher;
use Illuminate\Routing\Redirector;
use Throwable;

class EditService extends EditRecord
{
    public function __construct($id = null)
    {
        parent::__construct($id);
    }

    /** @var string */
    protected static string $resource = ServiceResource::class;

    /**
     * @return array
     * @throws Exception
     */
    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }

    /** @throws Exception */
    protected function getFormActions(): array
    {
        return array_merge(parent::getFormActions(), [
            Actions\Action::make('publish')
                ->label('Опубликовать')
                ->action('approve'),
            Actions\Action::make('rejected')
                ->label('Вернуть на доработку')
                ->form([
                    Forms\Components\Textarea::make('reason')
                        ->label('Опишите причину отказа')
                        ->required(),
                ])
                ->requiresConfirmation()
                ->action('reject'),
            Actions\Action::make('saveAndRedirect')
                ->label('Сохранить и выйти')
                ->action('saveAndRedirect'),
        ]);
    }

    /** @return Redirector */
    public function saveAndRedirect(): Redirector
    {
        $this->save();

        return redirect($this->getResource()::getUrl('index'));
    }

    public function approve(Dispatcher $dispatcher): void
    {
        $this->data['status'] = ServiceStatus::PUBLISHED;

        $this->save();

        $dispatcher->dispatch(
            new ServicePublishModerationEvent($this->record)
        );
    }

    /** @throws Throwable */
    public function reject(
        array $data,
        DatabaseManager $databaseManager,
        Factory $authFactory,
        Dispatcher $dispatcher,
    ): void {
        $this->data['status'] = ServiceStatus::REJECTED;

        $databaseManager
            ->transaction(function () use ($authFactory, $data): void {
                $this
                    ->record
                    ->moderations()
                    ->create([
                        'user_id' => $authFactory
                            ->guard()
                            ->user()
                            ->id,
                        'reason' => $data['reason'],
                        'data' => $this->data,
                    ]);

                $this->save();
            });

        $dispatcher->dispatch(
            new ServiceRejectedModerationEvent($this->record)
        );
    }

    protected function mutateFormDataBeforeSave(array $data): array
    {
        return ['status' => $this->data['status'], ...$data];
    }

    protected function beforeFill(): void
    {
        $this->record->makeVisible(['fee']);
    }
}
