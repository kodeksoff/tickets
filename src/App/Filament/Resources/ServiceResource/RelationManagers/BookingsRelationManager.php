<?php

declare(strict_types=1);

namespace App\Filament\Resources\ServiceResource\RelationManagers;

use Exception;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\RelationManagers\RelationManager;
use Filament\Resources\Table;
use Filament\Tables;
use Support\Utils\MoneyFactory;

class BookingsRelationManager extends RelationManager
{
    /** @var string */
    protected static string $relationship = 'bookings';

    /** @var string|null */
    protected static ?string $recordTitleAttribute = 'title';

    /** @var string|null */
    protected static ?string $pluralLabel = 'Виды бронирования';

    /** @var string|null */
    protected static ?string $modelLabel = 'Виды бронирования';

    /** @var string|null */
    protected static ?string $pluralModelLabel = 'Виды бронирования';

    /**
     * @param  Form  $form
     *
     * @return Form
     */
    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Card::make([
                    Forms\Components\TextInput::make('title')
                        ->required()
                        ->label('Название'),
                    Forms\Components\TextInput::make('full_price')
                        ->afterStateHydrated(function (Forms\Components\TextInput $component, $state) {
                            $component->state($state->getAmount());
                        })
                        ->numeric()
                        ->label('Полная стоимость услуги'),
                    Forms\Components\TextInput::make('discount_price')
                        ->afterStateHydrated(function (Forms\Components\TextInput $component, $state) {
                            $component->state($state->getAmount());
                        })
                        ->numeric()
                        ->label('Скидка партнера'),
                    Forms\Components\TextInput::make('service_discount_price')
                        ->afterStateHydrated(
                            function (RelationManager $livewire, Forms\Components\TextInput $component, $state) {
                                $component->state($state->getAmount());
                            },
                        )
                        ->numeric()
                        ->label('Скидка Сервиса'),
                ])->columns(2),
                Forms\Components\Toggle::make('is_default')
                    ->label('Основной')
                    ->default(false),
            ]);
    }

    /** @throws Exception */
    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('title')
                    ->sortable()
                    ->label('Название'),
                Tables\Columns\TextColumn::make('full_price_amount')
                    ->label('Полная стоимость услуги'),
                Tables\Columns\TextColumn::make('discount_price_amount')
                    ->label('Скидка партнера'),
                Tables\Columns\TextColumn::make('service_discount_price')
                    ->label('Скидка сервиса'),
                Tables\Columns\TextColumn::make('service_fee')
                    ->label('Комиссия сервиса'),
                Tables\Columns\TextColumn::make('service_price_for_booking_amount')
                    ->label('Итоговая стоимость услуги'),
                Tables\Columns\TextColumn::make('full_price_with_discount_amount')
                    ->label('Стоимость купона'),
                Tables\Columns\TextColumn::make('surcharge_price_amount')
                    ->label('Сумма к доплате'),
                Tables\Columns\ToggleColumn::make('is_default')
                    ->sortable()
                    ->label('Основной'),
            ])
            ->filters([

            ])
            ->headerActions([
                Tables\Actions\CreateAction::make(),
            ])
            ->actions([
                Tables\Actions\EditAction::make()
                    ->mutateFormDataUsing(function (array $data): array {
                        $data['full_price'] = resolve(MoneyFactory::class)->of($data['full_price']);
                        $data['discount_price'] = resolve(MoneyFactory::class)->of($data['discount_price']);
                        $data['service_discount_price'] = resolve(MoneyFactory::class)->of(
                            $data['service_discount_price'],
                        );

                        return $data;
                    }),
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }
}
