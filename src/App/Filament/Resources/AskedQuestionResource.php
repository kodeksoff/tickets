<?php

declare(strict_types=1);

namespace App\Filament\Resources;

use App\Filament\Resources\AskedQuestionResource\Pages;
use Domain\AskedQuestions\Models\AskedQuestion;
use Filament\Forms\Components\Repeater;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Exception;

class AskedQuestionResource extends Resource
{
    /** @var string|null */
    protected static ?string $model = AskedQuestion::class;
    /** @var string|null */
    protected static ?string $navigationIcon = 'heroicon-o-collection';
    /** @var string|null */
    protected static ?string $modelLabel = 'FAQ';
    /** @var string|null */
    protected static ?string $pluralModelLabel = 'FAQ';
    /** @var string|null */
    protected static ?string $navigationLabel = 'FAQ';
    /** @var string|null */
    protected static ?string $recordTitleAttribute = 'type';

    /**
     * @param  Form  $form
     *
     * @return Form
     */
    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                TextInput::make('type')
                    ->label('Тип')
                    ->default('default')
                    ->required(),
                Repeater::make('questions')
                    ->label('Вопросы')
                    ->schema([
                        TextInput::make('question')
                            ->label('Вопрос')
                            ->required(),
                        Textarea::make('answer')
                            ->label('Ответ')
                            ->required(),
                    ]),
            ]);
    }

    /**
     * @param  Table  $table
     *
     * @return Table
     * @throws Exception
     */
    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('type')
                    ->label('Тип группы вопросов')
                    ->sortable(),
            ])
            ->filters([

            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    /** @return array */
    public static function getRelations(): array
    {
        return [

        ];
    }

    /** @return array */
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListAskedQuestions::route('/'),
            'create' => Pages\CreateAskedQuestion::route('/create'),
            'edit' => Pages\EditAskedQuestion::route('/{record}/edit'),
        ];
    }
}
