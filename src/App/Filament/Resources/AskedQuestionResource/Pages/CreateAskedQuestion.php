<?php

declare(strict_types=1);

namespace App\Filament\Resources\AskedQuestionResource\Pages;

use App\Filament\Resources\AskedQuestionResource;
use Filament\Resources\Pages\CreateRecord;

class CreateAskedQuestion extends CreateRecord
{
    protected static string $resource = AskedQuestionResource::class;
}
