<?php

declare(strict_types=1);

namespace App\Filament\Resources\AskedQuestionResource\Pages;

use App\Filament\Resources\AskedQuestionResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListAskedQuestions extends ListRecords
{
    protected static string $resource = AskedQuestionResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
