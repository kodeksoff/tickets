<?php

declare(strict_types=1);

namespace App\Filament\Resources;

use App\Filament\Resources\GuestHouseResource\Pages;
use Domain\GuestHouses\Models\GuestHouse;
use Domain\Integrations\DaData\DaDataConnectorInterface;
use Domain\PartnerHouses\Models\PartnerHouse;
use Exception;
use Filament\Facades\Filament;
use Filament\Forms\Components\Actions\Action;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;

class GuestHouseResource extends Resource
{
    /** @var string|null */
    protected static ?string $model = GuestHouse::class;

    /** @var string|null */
    protected static ?string $navigationIcon = 'heroicon-o-collection';

    /** @var string|null */
    protected static ?string $modelLabel = 'Гостевой дом';

    /** @var string|null */
    protected static ?string $pluralModelLabel = 'Гостевые дома';

    /** @var string|null */
    protected static ?string $navigationLabel = 'Гостевые дома';

    /** @var string|null */
    protected static ?string $recordTitleAttribute = 'title';

    /**
     * @param  Form  $form
     *
     * @return Form
     */
    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Select::make('partner_house_id')
                    ->label('Партнер')
                    ->options(PartnerHouse::all()->pluck('full_name', 'id'))
                    ->searchable(),
                TextInput::make('code')
                    ->label('Код')
                    ->placeholder('Генерируется автоматически'),
                TextInput::make('title')
                    ->label('Название')
                    ->required(),
                TextInput::make('address')
                    ->label('Полный адрес')
                    ->suffixAction(
                        fn ($state, $set) => Action::make('search-action')
                            ->icon('heroicon-o-search')
                            ->action(function (DaDataConnectorInterface $daDataConnector) use ($state, $set) {
                                if (blank($state)) {
                                    Filament::notify('danger', 'Please enter a text');

                                    return;
                                }

                                $data = $daDataConnector->getSuggestions($state);

                                $set(
                                    'address',
                                    $data->suggestions->first()?->unrestrictedValue ?? 'Не найдено',
                                );
                                $set(
                                    'short_address',
                                    $data->suggestions->first()
                                        ? sprintf(
                                            '%s, %s. %s',
                                            $data->suggestions->first()?->data->streetWithType,
                                            $data->suggestions->first()?->data->houseType,
                                            $data->suggestions->first()?->data->house,
                                        )
                                        : 'Не найдено',
                                );
                            }),
                    )
                    ->required(),
                TextInput::make('short_address')
                    ->label('Короткий адрес'),
            ]);
    }

    /**
     * @param  Table  $table
     *
     * @return Table
     * @throws Exception
     */
    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('title')
                    ->label('Название')
                    ->searchable(),
                Tables\Columns\TextColumn::make('address')
                    ->label('Полный адрес')
                    ->sortable()
                    ->searchable(),
                Tables\Columns\TextColumn::make('short_address')
                    ->label('Короткий адресс')
                    ->sortable()
                    ->searchable(),
                Tables\Columns\TextColumn::make('code')
                    ->label('Код')
                    ->sortable()
                    ->searchable(),
            ])
            ->filters([
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    /** @return array */
    public static function getRelations(): array
    {
        return [
        ];
    }

    /** @return array{index: mixed[], create: mixed[], edit: mixed[]} */
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListGuestHouses::route('/'),
            'create' => Pages\CreateGuestHouse::route('/create'),
            'edit' => Pages\EditGuestHouse::route('/{record}/edit'),
        ];
    }
}
