<?php

declare(strict_types=1);

namespace App\Filament\Resources;

use App\Filament\Resources\ServiceResource\Pages;
use App\Filament\Resources\ServiceResource\RelationManagers\BookingsRelationManager;
use Domain\PartnerServices\Models\PartnerService;
use Domain\ServiceCategories\Models\ServiceCategory;
use Domain\Services\Enums\ServiceStatus;
use Domain\Services\Models\Service;
use Exception;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 *
 */
class ServiceResource extends Resource
{
    /** @var string|null */
    protected static ?string $model = Service::class;
    /** @var string|null */
    protected static ?string $navigationIcon = 'heroicon-o-collection';
    /** @var string|null */
    protected static ?string $modelLabel = 'Сервисы';
    /** @var string|null */
    protected static ?string $pluralModelLabel = 'Сервисы';
    /** @var string|null */
    protected static ?string $navigationLabel = 'Сервисы';
    /** @var string|null */
    protected static ?string $navigationGroup = 'Сервисы';

    /** @var string|null */
    protected static ?string $recordTitleAttribute = 'title';

    /**
     * @param  Form  $form
     *
     * @return Form
     */
    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Card::make([
                    Forms\Components\TextInput::make('title')
                        ->label('Название')
                        ->required(),
                    Forms\Components\Select::make('partner_service_id')
                        ->label('Партнер')
                        ->options(PartnerService::all()->pluck('full_name', 'id'))
                        ->searchable(),
                    Forms\Components\Select::make('category_ids')
                        ->label('Категории')
                        ->multiple()
                        ->options(ServiceCategory::all()->pluck('title', 'id'))
                        ->searchable(),
                    Forms\Components\TextInput::make('fee')
                        ->minValue(0)
                        ->label('Комиссия сервиса')
                        ->required(),
                ]),
                Forms\Components\Card::make([
                    Forms\Components\SpatieMediaLibraryFileUpload::make('main_image')
                        ->label('Основное изображение')
                        ->collection('main_image'),
                    Forms\Components\SpatieMediaLibraryFileUpload::make('additional_images')
                        ->label('Дополнительные изображения')
                        ->multiple()
                        ->maxFiles(4)
                        ->enableReordering()
                        ->collection('additional_images'),

                ]),
                Forms\Components\Card::make([
                    Forms\Components\Select::make('service_season_from')
                        ->label('Месяц начала')
                        ->options([
                            1 => 'Январь',
                            2 => 'Февраль',
                            3 => 'Март',
                            4 => 'Апрель',
                            5 => 'Май',
                            6 => 'Июнь',
                            7 => 'Июль',
                            8 => 'Август',
                            9 => 'Сентябрь',
                            10 => 'Октябрь',
                            11 => 'Ноябрь',
                            12 => 'Декабрь',
                        ]),
                    Forms\Components\Select::make('service_season_to')
                        ->label('Месяц окончания')
                        ->options([
                            1 => 'Январь',
                            2 => 'Февраль',
                            3 => 'Март',
                            4 => 'Апрель',
                            5 => 'Май',
                            6 => 'Июнь',
                            7 => 'Июль',
                            8 => 'Август',
                            9 => 'Сентябрь',
                            10 => 'Октябрь',
                            11 => 'Ноябрь',
                            12 => 'Декабрь',
                        ]),
                    Forms\Components\Toggle::make('service_all_year')
                        ->label('Круглый год'),
                ]),

                Forms\Components\Card::make([
                    Forms\Components\Textarea::make('description')
                        ->label('Описание услуги'),
                    Forms\Components\Textarea::make('about_included')
                        ->label('Что включено в стоимость'),
                    Forms\Components\Textarea::make('about_additional')
                        ->label('Что оплачивается отдельно'),
                ]),
            ]);
    }

    /**
     * @param  Table  $table
     *
     * @return Table
     * @throws Exception
     */
    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('title')
                    ->label('Название')
                    ->sortable()
                    ->searchable(),
                Tables\Columns\TextColumn::make('partnerService.full_name')
                    ->label('ФИО Партнера')
                    ->sortable()
                    ->searchable(),
                Tables\Columns\BadgeColumn::make('status')
                    ->label('Статус')
                    ->enum([
                        ServiceStatus::PUBLISHED->value => 'Опубликовано',
                        ServiceStatus::HIDDEN->value => 'Скрыто',
                        ServiceStatus::MODERATION->value => 'На модерации',
                        ServiceStatus::REJECTED->value => 'Отклонено модератором',
                    ])
                    ->colors([
                        'secondary' => ServiceStatus::HIDDEN->value,
                        'warning' => ServiceStatus::MODERATION->value,
                        'success' => ServiceStatus::PUBLISHED->value,
                        'danger' => ServiceStatus::REJECTED->value,
                    ])
                    ->sortable(),
            ])
            ->filters([
                Tables\Filters\SelectFilter::make('status')
                    ->label('Статус')
                    ->options([
                        ServiceStatus::PUBLISHED->value => 'Опубликовано',
                        ServiceStatus::HIDDEN->value => 'Cнято с публикации',
                        ServiceStatus::MODERATION->value => 'На модерации',
                        ServiceStatus::REJECTED->value => 'Отклонено модератором',
                    ]),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    /** @return array */
    public static function getRelations(): array
    {
        return [
            BookingsRelationManager::class,
        ];
    }

    /** @return array{index: mixed[], create: mixed[], edit: mixed[]} */
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListServices::route('/'),
            'create' => Pages\CreateService::route('/create'),
            'edit' => Pages\EditService::route('/{record}/edit'),
        ];
    }

    /** @return Builder */
    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()->withoutGlobalScopes();
    }

    /** @return Builder */
    protected static function getGlobalSearchEloquentQuery(): Builder
    {
        return parent::getGlobalSearchEloquentQuery()->with(['partnerService', 'categories']);
    }

    /** @return string[] */
    public static function getGloballySearchableAttributes(): array
    {
        return ['title', 'partnerService.full_name', 'categories.title', ];
    }

    /**
     * @param  Model  $record
     *
     * @return array
     */
    public static function getGlobalSearchResultDetails(Model $record): array
    {
        return [
            'Партнер' => $record->partnerService->full_name,
        ];
    }
}
