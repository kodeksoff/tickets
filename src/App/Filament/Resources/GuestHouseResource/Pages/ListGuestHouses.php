<?php

declare(strict_types=1);

namespace App\Filament\Resources\GuestHouseResource\Pages;

use App\Filament\Resources\GuestHouseResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListGuestHouses extends ListRecords
{
    protected static string $resource = GuestHouseResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
