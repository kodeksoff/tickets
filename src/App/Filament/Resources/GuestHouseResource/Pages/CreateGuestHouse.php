<?php

declare(strict_types=1);

namespace App\Filament\Resources\GuestHouseResource\Pages;

use App\Filament\Resources\GuestHouseResource;
use Filament\Resources\Pages\CreateRecord;

class CreateGuestHouse extends CreateRecord
{
    protected static string $resource = GuestHouseResource::class;
}
