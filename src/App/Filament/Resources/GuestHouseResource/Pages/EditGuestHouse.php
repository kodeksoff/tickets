<?php

declare(strict_types=1);

namespace App\Filament\Resources\GuestHouseResource\Pages;

use App\Filament\Resources\GuestHouseResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;
use Illuminate\Routing\Redirector;
use Exception;

class EditGuestHouse extends EditRecord
{
    protected static string $resource = GuestHouseResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }

    /**
     * @return array
     * @throws Exception
     */
    protected function getFormActions(): array
    {
        return array_merge(parent::getFormActions(), [
            Actions\Action::make('saveAndRedirect')
                ->label('Сохранить и выйти')
                ->action('saveAndRedirect'),
        ]);
    }

    /** @return Redirector */
    public function saveAndRedirect(): Redirector
    {
        $this->save();

        return redirect($this->getResource()::getUrl('index'));
    }
}
