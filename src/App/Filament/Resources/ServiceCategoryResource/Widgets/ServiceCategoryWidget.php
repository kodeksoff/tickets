<?php

declare(strict_types=1);

namespace App\Filament\Resources\ServiceCategoryResource\Widgets;

use Domain\ServiceCategories\Models\ServiceCategory;
use Filament\Notifications\Notification;
use SolutionForest\FilamentTree\Actions\Action;
use SolutionForest\FilamentTree\Actions\ActionGroup;
use SolutionForest\FilamentTree\Actions\DeleteAction;
use SolutionForest\FilamentTree\Actions\EditAction;
use SolutionForest\FilamentTree\Actions\ViewAction;
use SolutionForest\FilamentTree\Widgets\Tree as BaseWidget;

class ServiceCategoryWidget extends BaseWidget
{
    protected static string $model = ServiceCategory::class;
    protected static int $maxDepth = 2;
    protected ?string $treeTitle = 'Дерево категорий';
    protected bool $enableTreeTitle = true;

    public function getTreeRecordIcon(?\Illuminate\Database\Eloquent\Model $model = null): ?string
    {
        return null;
    }

    protected function getFormSchema(): array
    {
        return [
        ];
    }

    // CUSTOMIZE ACTION OF EACH RECORD, CAN DELETE
    // protected function getTreeActions(): array
    // {
    //     return [
    //         Action::make('helloWorld')
    //             ->action(function () {
    //                 Notification::make()->success()->title('Hello.php World')->send();
    //             }),
    //         // ViewAction::make(),
    //         // EditAction::make(),
    //         ActionGroup::make([
    //
    //             ViewAction::make(),
    //             EditAction::make(),
    //         ]),
    //         DeleteAction::make(),
    //     ];
    // }
    // OR OVERRIDE FOLLOWING METHODS
    // protected function hasDeleteAction(): bool
    // {
    //    return true;
    // }
    // protected function hasEditAction(): bool
    // {
    //    return true;
    // }
    // protected function hasViewAction(): bool
    // {
    //    return true;
    // }
}
