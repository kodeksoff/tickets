<?php

declare(strict_types=1);

namespace App\Filament\Resources\SettingResource\Pages;

use App\Filament\Resources\SettingResource;
use Filament\Resources\Pages\EditRecord;
use Filament\Pages\Actions;
use Illuminate\Routing\Redirector;
use Exception;

class EditSetting extends EditRecord
{
    protected static string $resource = SettingResource::class;

    protected function getActions(): array
    {
        return [

        ];
    }

    /**
     * @return array
     * @throws Exception
     */
    protected function getFormActions(): array
    {
        return array_merge(parent::getFormActions(), [
            Actions\Action::make('saveAndRedirect')
                ->label('Сохранить и выйти')
                ->action('saveAndRedirect'),
        ]);
    }

    /** @return Redirector */
    public function saveAndRedirect(): Redirector
    {
        $this->save();

        return redirect($this->getResource()::getUrl('index'));
    }
}
