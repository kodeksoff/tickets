<?php

declare(strict_types=1);

namespace App\Filament\Resources;

use App\Filament\Resources\UserResource\Pages;
use App\Filament\Resources\UserResource\RelationManagers\GuestHousesRelationManager;
use App\Filament\Resources\UserResource\RelationManagers\PartnerHouseRelationManager;
use App\Filament\Resources\UserResource\RelationManagers\PartnerServiceRelationManager;
use App\Filament\Resources\UserResource\RelationManagers\TouristRelationManager;
use Domain\Users\Models\User;
use Exception;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Hash;
use Filament\Forms;

class UserResource extends Resource
{
    /** @var string|null */
    protected static ?string $model = User::class;

    /** @var string|null */
    protected static ?string $navigationIcon = 'heroicon-o-users';

    /** @var string|null */
    protected static ?string $modelLabel = 'Пользователи';

    /** @var string|null */
    protected static ?string $pluralModelLabel = 'Пользователи';

    /** @var string|null */
    protected static ?string $navigationLabel = 'Пользователи';

    /** @var string|null */
    protected static ?string $recordTitleAttribute = 'email';

    /**
     * @param  Form  $form
     *
     * @return Form
     */
    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Card::make()
                    ->schema([
                        TextInput::make('phone')
                            ->label('Телефон')
                            ->required(),
                        TextInput::make('email')
                            ->label('E-mail')
                            ->required(),
                        TextInput::make('password')
                            ->label('Пароль')
                            ->password()
                            ->dehydrateStateUsing(fn ($state) => Hash::make($state))
                            ->dehydrated(fn ($state) => filled($state)),
                    ]),
            ]);
    }

    /** @throws Exception */
    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('full_name')
                    ->label('ФИО')
                    ->searchable(query: fn (
                        Builder $builder,
                        string $search,
                    ): Builder => $builder->whereAllRolesFullNameLike($search)),
                Tables\Columns\TextColumn::make('email')
                    ->label('Email')
                    ->sortable()
                    ->searchable(),
                Tables\Columns\TextColumn::make('phone')
                    ->label('Телефон')
                    ->sortable()
                    ->searchable(),
                Tables\Columns\TagsColumn::make('user_roles')
                    ->label('Роль'),
            ])
            ->filters([
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    /** @return string[] */
    public static function getRelations(): array
    {
        return [
            TouristRelationManager::class,
            PartnerHouseRelationManager::class,
            PartnerServiceRelationManager::class,
            GuestHousesRelationManager::class,
        ];
    }

    /** @return array{index: mixed[], create: mixed[], edit: mixed[]} */
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListUsers::route('/'),
            'create' => Pages\CreateUser::route('/create'),
            'edit' => Pages\EditUser::route('/{record}/edit'),
        ];
    }
}
