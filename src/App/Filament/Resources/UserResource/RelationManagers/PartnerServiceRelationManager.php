<?php

declare(strict_types=1);

namespace App\Filament\Resources\UserResource\RelationManagers;

use Domain\Users\Enums\UserRole;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\RelationManagers\RelationManager;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Model;
use Exception;

class PartnerServiceRelationManager extends RelationManager
{
    protected static string $relationship = 'partnerService';
    protected static ?string $pluralModelLabel = 'Детальная информация | Партнер поставщик услуг';
    protected static ?string $recordTitleAttribute = 'full_name';

    /**
     * @param Form $form
     * @return Form
     */
    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('full_name')
                    ->label('ФИО')
                    ->required(),
                Forms\Components\TextInput::make('inn')
                    ->label('ИНН')
                    ->required(),
            ]);
    }

    /**
     * @param Table $table
     * @return Table
     * @throws Exception
     */
    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('full_name')
                    ->label('ФИО'),
                Tables\Columns\TextColumn::make('inn')
                    ->label('ИНН'),
            ])
            ->filters([

            ])
            ->headerActions([
                //Tables\Actions\CreateAction::make(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                //Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                //Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    /**
     * @param Model $ownerRecord
     * @return bool
     */
    public static function canViewForRecord(Model $ownerRecord): bool
    {
        return $ownerRecord->userRole === UserRole::PARTNER_SERVICE();
    }
}
