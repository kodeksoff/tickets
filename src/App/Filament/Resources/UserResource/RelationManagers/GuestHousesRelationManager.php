<?php

declare(strict_types=1);

namespace App\Filament\Resources\UserResource\RelationManagers;

use Domain\Users\Enums\UserRole;
use Filament\Resources\Form;
use Filament\Resources\RelationManagers\RelationManager;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Model;
use Exception;

class GuestHousesRelationManager extends RelationManager
{
    protected static string $relationship = 'guestHouses';
    protected static ?string $pluralModelLabel = 'Гостевые дома';
    protected static ?string $recordTitleAttribute = 'title';

    /**
     * @param Form $form
     * @return Form
     */
    public static function form(Form $form): Form
    {
        return $form->schema([]);
    }

    /**
     * @param Table $table
     * @return Table
     * @throws Exception
     */
    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('title')
                    ->label('Наименование'),
                Tables\Columns\TextColumn::make('short_address')
                    ->label('Адрес'),
                Tables\Columns\TextColumn::make('code')
                    ->label('Код'),
            ])
            ->filters([

            ])
            ->actions([
                //Tables\Actions\EditAction::make(),
                //Tables\Actions\DeleteAction::make(),
            ]);
    }

    /**
     * @param Model $ownerRecord
     * @return bool
     */
    public static function canViewForRecord(Model $ownerRecord): bool
    {
        return $ownerRecord->userRole === UserRole::PARTNER_HOUSE();
    }
}
