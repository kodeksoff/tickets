<?php

declare(strict_types=1);

namespace App\Filament\Resources\UserResource\Pages;

use App\Filament\Resources\UserResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;
use Illuminate\Routing\Redirector;
use Exception;

class EditUser extends EditRecord
{
    protected static string $resource = UserResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }

    /**
     * @return array
     * @throws Exception
     */
    protected function getFormActions(): array
    {
        return array_merge(parent::getFormActions(), [
            Actions\Action::make('saveAndRedirect')
                ->label('Сохранить и выйти')
                ->action('saveAndRedirect'),
        ]);
    }

    /** @return Redirector */
    public function saveAndRedirect(): Redirector
    {
        $this->save();

        return redirect($this->getResource()::getUrl('index'));
    }
}
