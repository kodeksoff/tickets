<?php

declare(strict_types=1);

namespace App\Filament\Resources;

use App\Filament\Resources\ServiceCategoryResource\Pages;
use Domain\ServiceCategories\Models\ServiceCategory;
use Exception;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;

class ServiceCategoryResource extends Resource
{
    /** @var string|null */
    protected static ?string $model = ServiceCategory::class;

    /** @var string|null */
    protected static ?string $navigationIcon = 'heroicon-o-collection';

    /** @var string|null */
    protected static ?string $modelLabel = 'Категории';

    /** @var string|null */
    protected static ?string $pluralModelLabel = 'Категории';

    /** @var string|null */
    protected static ?string $navigationLabel = 'Категории';

    /** @var string|null */
    protected static ?string $navigationGroup = 'Сервисы';

    /** @var string|null */
    protected static ?string $recordTitleAttribute = 'title';

    /**
     * @param  Form  $form
     *
     * @return Form
     */
    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Card::make()
                    ->schema([
                        Forms\Components\TextInput::make('title')
                            ->label('Название'),
                        Forms\Components\Select::make('parent_id')
                            ->label('Родительская категория')
                            ->options(ServiceCategory::all()->pluck('title', 'id'))
                            ->searchable(),
                    ])
                    ->columns(),
                Forms\Components\Card::make()
                    ->schema([
                        Forms\Components\Textarea::make('description')
                            ->label('Описание'),
                    ]),
                Forms\Components\Toggle::make('is_published')
                    ->label('Опубликовано'),
            ]);
    }

    /** @throws Exception */
    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('title')
                    ->label('Название')
                    ->searchable()
                    ->sortable(),
                Tables\Columns\ToggleColumn::make('is_published')
                    ->label('Опубликовано')
                    ->sortable(),
            ])
            ->filters([
                Tables\Filters\SelectFilter::make('is_published')
                    ->label('Статус')
                    ->options([
                        true => 'Опубликовано',
                        false => 'Cнято с публикации',
                    ]),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    /** @return array */
    public static function getRelations(): array
    {
        return [
        ];
    }

    /** @return array{index: mixed[], create: mixed[], edit: mixed[]} */
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListServiceCategories::route('/'),
            'create' => Pages\CreateServiceCategory::route('/create'),
            'edit' => Pages\EditServiceCategory::route('/{record}/edit'),
        ];
    }

    /** @return Builder */
    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()->withoutGlobalScopes();
    }
}
