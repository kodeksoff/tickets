<?php

declare(strict_types=1);

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Routing\RouteRegistrar;
use Illuminate\Support\Facades\RateLimiter;

class RouteServiceProvider extends ServiceProvider
{
    final public const HOME = '/home';

    /** @throws BindingResolutionException */
    public function boot(): void
    {
        $this->configureRateLimiting();

        $this->mapRoutes(
            'web',
            [
                'middleware' => 'web',
            ],
        );

        $this->mapRoutes(
            'api/v1',
            [
                'as' => 'api:v1:',
                'prefix' => 'api/v1',
                'middleware' => 'api/v1',
            ],
        );
    }

    protected function configureRateLimiting(): void
    {
        RateLimiter::for(
            'api',
            static fn (Request $request): Limit => Limit::perMinute(60)->by($request->ip()),
        );
    }

    /**
     * @param array<string, string> $options
     *
     * @throws BindingResolutionException
     */
    protected function mapRoutes(string $namespace, array $options): void
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        /** @var Router|RouteRegistrar $router */
        $router = $this->app->make(Router::class);

        /** @noinspection PhpUnhandledExceptionInspection */
        /** @var Filesystem $filesystem */
        $filesystem = $this->app->make(Filesystem::class);

        $baseDir = base_path("routes/{$namespace}");

        foreach ($filesystem->files($baseDir) as $file) {
            $router->group(
                $options,
                $file->getRealPath(),
            );
        }
    }
}
