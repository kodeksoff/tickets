<?php

declare(strict_types=1);

namespace App\Providers;

use Faker\Factory as FakerFactory;
use Faker\Generator as FakerGenerator;
use Illuminate\Support\ServiceProvider;
use Support\FakerProvider;

class FakerServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this
            ->app
            ->singleton(
                FakerGenerator::class,
                function ($app): FakerGenerator {
                    $fakerGenerator = FakerFactory::create($app['config']->get('app.faker_locale', 'ru_RU'));

                    $fakerGenerator->addProvider(new FakerProvider($fakerGenerator));

                    return $fakerGenerator;
                },
            );
    }
}
