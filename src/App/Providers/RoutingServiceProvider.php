<?php

declare(strict_types=1);

namespace App\Providers;

use App\Application;
use Illuminate\Contracts\Routing\ResponseFactory as ResponseFactoryContract;
use Illuminate\Contracts\View\Factory as ViewFactoryContract;
use Illuminate\Routing\ResponseFactory;
use Illuminate\Routing\RoutingServiceProvider as Base;

class RoutingServiceProvider extends Base
{
    protected function registerResponseFactory(): void
    {
        $this
            ->app
            ->singleton(
                ResponseFactoryContract::class,
                fn (Application $application): ResponseFactory => new ResponseFactory(
                    $application[ViewFactoryContract::class],
                    $application['redirect'],
                ),
            );
    }
}
