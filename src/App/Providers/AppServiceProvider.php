<?php

declare(strict_types=1);

namespace App\Providers;

use Domain\Integrations\DaData\DaDataConnector;
use Domain\Integrations\SmsCenter\SmsCenterConnector;
use Domain\Integrations\Yookassa\YookassaConnector;
use Domain\Services\Models\Service;
use Domain\Tickets\Models\Ticket;
use Domain\Users\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        SmsCenterConnector::register($this->app);
        YookassaConnector::register($this->app);
        DaDataConnector::register($this->app);
    }

    public function boot(): void
    {
        Model::shouldBeStrict(
            !$this
                ->app
                ->isProduction(),
        );

        Model::handleLazyLoadingViolationUsing(function (Model $model, string $relation): void {
            Log::warning('N+1 Query', ['model' => get_class($model), 'relation' => $relation]);
        });

        Relation::enforceMorphMap([
            'users' => User::class,
            'services' => Service::class,
            'tickets' => Ticket::class,
        ]);

        JsonResource::withoutWrapping();
    }
}
