<?php

declare(strict_types=1);

namespace App\Providers;

use Closure;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class FactoryServiceProvider extends ServiceProvider
{
    private const NAMESPACE = '\\Tests\\Factories\\';

    public function boot(): void
    {
        Factory::useNamespace(self::NAMESPACE);

        Factory::guessFactoryNamesUsing($this->makeFactoryNameResolver());
    }

    /** @return Closure(class-string<Model>): class-string<Factory> */
    public function makeFactoryNameResolver(): Closure
    {
        /**
         * @var Closure(class-string<Model>): class-string<Factory> $resolver
         *
         * @noinspection PhpRedundantVariableDocTypeInspection
         * @noinspection PhpDocSignatureInspection
         * @noinspection PhpDocSignatureIsNotCompleteInspection
         */
        $resolver = function (string $modelName): string {
            $appNamespace = $this->app->getNamespace();

            $modelName = Str::after($modelName, $appNamespace);

            if (class_exists(self::NAMESPACE . $modelName . 'Factory')) {
                return self::NAMESPACE . $modelName . 'Factory';
            }

            return self::NAMESPACE . class_basename($modelName) . 'Factory';
        };

        return $resolver;
    }
}
