<?php

declare(strict_types=1);

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate;
use Laravel\Horizon\Horizon;
use Laravel\Horizon\HorizonApplicationServiceProvider;

class HorizonServiceProvider extends HorizonApplicationServiceProvider
{
    public function __construct($app, private readonly Gate $gate)
    {
        parent::__construct($app);
    }

    public function boot(): void
    {
        parent::boot();

        Horizon::night();
    }

    protected function gate(): void
    {
        $this
            ->gate
            ->define(
                'viewHorizon',
                fn ($user): bool => $this->app->isLocal(),
            );
    }
}
