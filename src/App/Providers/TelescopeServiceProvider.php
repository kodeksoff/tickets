<?php

declare(strict_types=1);

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate;
use Laravel\Telescope\IncomingEntry;
use Laravel\Telescope\Telescope;
use Laravel\Telescope\TelescopeApplicationServiceProvider;

class TelescopeServiceProvider extends TelescopeApplicationServiceProvider
{
    public function __construct($app, private readonly Gate $gate)
    {
        parent::__construct($app);
    }

    public function register(): void
    {
        Telescope::night();

        $this->hideSensitiveRequestDetails();

        Telescope::filter(function (IncomingEntry $incomingEntry): bool {
            if ($this->app->isLocal()) {
                return true;
            }

            return
                $incomingEntry->isReportableException() ||
                $incomingEntry->isFailedRequest() ||
                $incomingEntry->isFailedJob() ||
                $incomingEntry->isScheduledTask() ||
                $incomingEntry->hasMonitoredTag();
        });
    }

    protected function hideSensitiveRequestDetails(): void
    {
        if ($this->app->isLocal()) {
            return;
        }

        Telescope::hideRequestParameters(['_token']);

        Telescope::hideRequestHeaders([
            'cookie',
            'x-csrf-token',
            'x-xsrf-token',
        ]);
    }

    protected function gate(): void
    {
        $this->gate->define(
            'viewTelescope',
            fn ($user): bool => $this->app->isLocal(),
        );
    }
}
