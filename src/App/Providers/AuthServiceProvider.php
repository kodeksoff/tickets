<?php

declare(strict_types=1);

namespace App\Providers;

use App\Api\V1\Policies\NotificationPolicy;
use App\Api\V1\Policies\OrderPolicy;
use App\Api\V1\Policies\ServicePolicy;
use Domain\Orders\Models\Order;
use Domain\Services\Models\Service;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Notifications\DatabaseNotification;

class AuthServiceProvider extends ServiceProvider
{
    /** @var array<class-string, class-string> */
    protected $policies = [
        Service::class => ServicePolicy::class,
        DatabaseNotification::class => NotificationPolicy::class,
        Order::class => OrderPolicy::class,
    ];

    public function boot(Repository $configRepository): void
    {
        $this->registerPolicies();

        ResetPassword::createUrlUsing(
            fn (CanResetPassword $canResetPassword, string $token): string => sprintf(
                '%s/password-reset/%s?email=%s',
                $configRepository->get('app.frontend_url'),
                $token,
                $canResetPassword->getEmailForPasswordReset(),
            ),
        );
    }
}
