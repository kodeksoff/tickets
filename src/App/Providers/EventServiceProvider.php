<?php

declare(strict_types=1);

namespace App\Providers;

use Domain\GuestHouses\Subscribers\GuestHouseSubscriber;
use Domain\Integrations\SmsCenter\Events\SmsCenterRequestSent;
use Domain\Integrations\SmsCenter\Listeners\SmsCenterMinBalanceListener;
use Domain\Orders\Events\OrderRefundEvent;
use Domain\Orders\Listeners\OrderRefundListener;
use Domain\Orders\Subscribers\OrderStatusSubscriber;
use Domain\Services\Events\ServicePublishModerationEvent;
use Domain\Services\Events\ServiceRejectedModerationEvent;
use Domain\Services\Listeners\SendServicePublishedNotificationListener;
use Domain\Services\Listeners\SendServiceRejectedNotificationListener;
use Domain\Services\Subscribers\ServiceSubscriber;
use Domain\Tickets\Subscribers\TicketSubscriber;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Support\Listeners\HttpRequestSentSubscriber;
use Domain\Integrations\SmsCenter\Listeners\SmsCenterRequestFailedListener;

class EventServiceProvider extends ServiceProvider
{
    /** @var array[] */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        SmsCenterRequestSent::class => [
            SmsCenterRequestFailedListener::class,
            SmsCenterMinBalanceListener::class,
        ],
        ServicePublishModerationEvent::class => [
            SendServicePublishedNotificationListener::class,
        ],
        ServiceRejectedModerationEvent::class => [
            SendServiceRejectedNotificationListener::class,
        ],
        OrderRefundEvent::class => [
            OrderRefundListener::class,
        ],
    ];

    /** @var string[] */
    protected $subscribe = [
        HttpRequestSentSubscriber::class,
        ServiceSubscriber::class,
        GuestHouseSubscriber::class,
        TicketSubscriber::class,
        OrderStatusSubscriber::class,
    ];

    /** @return void */
    public function boot(): void
    {
    }
}
