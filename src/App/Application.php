<?php

declare(strict_types=1);

namespace App;

use Illuminate\Foundation\Application as Base;

class Application extends Base
{
    protected $namespace = 'App\\';
}
