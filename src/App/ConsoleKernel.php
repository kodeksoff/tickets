<?php

declare(strict_types=1);

namespace App;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel;

class ConsoleKernel extends Kernel
{
    /** @var array<int, class-string> */
    protected $commands = [
    ];

    protected function schedule(Schedule $schedule): void
    {
        $schedule
            ->command('sanctum:prune-expired --hours=24')
            ->daily();
        $schedule
            ->command('set_overdue_status')
            ->dailyAt('00:00');
    }

    protected function commands(): void
    {
        $this->load(__DIR__ . '/Console/Commands');
    }
}
