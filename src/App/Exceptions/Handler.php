<?php

declare(strict_types=1);

namespace App\Exceptions;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Container\Container;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Propaganistas\LaravelPhone\Exceptions\NumberParseException;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Support\Responses\JsonResponse;
use Throwable;

class Handler extends ExceptionHandler
{
    /** @var array<class-string<Throwable>, \Psr\Log\LogLevel::*> */
    protected $levels = [
    ];

    /** @var array<int, class-string<Throwable>> */
    protected $dontReport = [
        AuthenticationException::class,
        TokenMismatchException::class,
    ];

    /** @var array<int, string> */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * @param  Container  $container
     * @param  UrlGenerator  $urlGenerator
     */
    public function __construct(
        Container $container,
        private readonly UrlGenerator $urlGenerator,
    ) {
        parent::__construct($container);
    }

    /** @return void */
    public function register(): void
    {
        $this->reportable(function (NumberParseException $e) {
            throw new PhoneNumberParseException();
        });
    }

    /**
     * @param $request
     * @param  Throwable  $e
     *
     * @return JsonResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    protected function prepareJsonResponse($request, Throwable $e): JsonResponse
    {
        return JsonResponse::exception($e);
    }

    /**
     * @param $request
     * @param  ValidationException  $exception
     *
     * @return JsonResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    protected function invalidJson($request, ValidationException $exception): JsonResponse
    {
        return JsonResponse::exception($exception);
    }

    /**
     * @param $request
     * @param  AuthenticationException  $exception
     *
     * @return JsonResponse|RedirectResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    protected function unauthenticated(
        $request,
        AuthenticationException $exception,
    ): JsonResponse|RedirectResponse {
        return $this->shouldReturnJson($request, $exception)
            ? JsonResponse::exception($exception)
            : resolve(Redirector::class)->guest(
                $exception->redirectTo() ?? $this
                    ->urlGenerator
                    ->route('filament.auth.login'),
            );
    }

    /**
     * @param $request
     * @param  Throwable  $e
     *
     * @return bool
     */
    protected function shouldReturnJson($request, Throwable $e): bool
    {
        return parent::shouldReturnJson($request, $e) || $this->isApiRequest($request);
    }

    /**
     * @param  Request  $request
     *
     * @return bool
     */
    protected function isApiRequest(Request $request): bool
    {
        return Str::startsWith($request->path(), 'api/');
    }
}
