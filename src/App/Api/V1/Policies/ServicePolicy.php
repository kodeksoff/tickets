<?php

declare(strict_types=1);

namespace App\Api\V1\Policies;

use Domain\Services\Models\Service;
use Domain\Users\Enums\UserRole;
use Domain\Users\Models\User;

class ServicePolicy
{
    /**
     * @param  User  $user
     *
     * @return bool
     */
    public function viewAny(User $user): bool
    {
        return true;
    }

    /**
     * @param  User  $user
     * @param  Service  $service
     *
     * @return bool
     */
    public function view(User $user, Service $service): bool
    {
        return true;
    }

    /**
     * @param  User  $user
     *
     * @return bool
     */
    public function create(User $user): bool
    {
        return $user->hasRole(UserRole::PARTNER_SERVICE());
    }

    /**
     * @param  User  $user
     * @param  Service  $service
     *
     * @return bool
     */
    public function update(User $user, Service $service): bool
    {
        return $user->is_admin ||
            $user->hasRole(UserRole::PARTNER_SERVICE()) &&
            $user->partnerService->id === $service->partner_service_id;
    }

    /**
     * @param  User  $user
     * @param  Service  $service
     *
     * @return bool
     */
    public function delete(User $user, Service $service): bool
    {
        return $user->hasRole(UserRole::PARTNER_SERVICE()) &&
            $user->partnerService->id === $service->partner_service_id;
    }

    /**
     * @param  User  $user
     * @param  Service  $service
     *
     * @return bool
     */
    public function restore(User $user, Service $service): bool
    {
        return $user->hasRole(UserRole::PARTNER_SERVICE()) &&
            $user->partnerService->id === $service->partner_service_id;
    }

    /**
     * @param  User  $user
     * @param  Service  $service
     *
     * @return bool
     */
    public function forceDelete(User $user, Service $service): bool
    {
        return $user->hasRole(UserRole::PARTNER_SERVICE()) &&
            $user->partnerService->id === $service->partner_service_id;
    }
}
