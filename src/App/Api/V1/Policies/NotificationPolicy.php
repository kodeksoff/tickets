<?php

declare(strict_types=1);

namespace App\Api\V1\Policies;

use Domain\Users\Models\User;
use Illuminate\Notifications\DatabaseNotification;

class NotificationPolicy
{
    /**
     * @param  DatabaseNotification  $notification
     *
     * @return bool
     */
    public function viewAny(DatabaseNotification $notification): bool
    {
        return true;
    }

    /**
     * @param  User  $user
     * @param  DatabaseNotification  $notification
     *
     * @return bool
     */
    public function view(User $user, DatabaseNotification $notification): bool
    {
        return true;
    }

    /**
     * @param  User  $user
     *
     * @return bool
     */
    public function create(User $user): bool
    {
        return true;
    }

    /**
     * @param  User  $user
     * @param  DatabaseNotification  $notification
     *
     * @return bool
     */
    public function update(User $user, DatabaseNotification $notification): bool
    {
        return $user->is_admin || $user->id === $notification->notifiable_id;
    }

    /**
     * @param  User  $user
     * @param  DatabaseNotification  $notification
     *
     * @return bool
     */
    public function delete(User $user, DatabaseNotification $notification): bool
    {
        return $user->is_admin || $user->id === $notification->notifiable_id;
    }

    /**
     * @param  User  $user
     * @param  DatabaseNotification  $notification
     *
     * @return bool
     */
    public function restore(User $user, DatabaseNotification $notification): bool
    {
        return $user->is_admin || $user->id === $notification->notifiable_id;
    }

    /**
     * @param  User  $user
     * @param  DatabaseNotification  $notification
     *
     * @return bool
     */
    public function forceDelete(User $user, DatabaseNotification $notification): bool
    {
        return $user->is_admin || $user->id === $notification->notifiable_id;
    }
}
