<?php

declare(strict_types=1);

namespace App\Api\V1\Policies;

use Domain\Orders\Models\Order;
use Domain\Users\Enums\UserRole;
use Domain\Users\Models\User;

class OrderPolicy
{
    /**
     * @param User $user
     *
     * @return bool
     */
    public function viewAny(User $user): bool
    {
        return true;
    }

    /**
     * @param User $user
     * @param Order $order
     * @return bool
     */
    public function viewTourist(User $user, Order $order): bool
    {
        return $user->is_admin ||
            $user->hasRole(UserRole::TOURIST()) &&
            $user->id === $order->user_id;
    }

    /**
     * @param User $user
     * @param Order $order
     * @return bool
     */
    public function viewPartnerService(User $user, Order $order): bool
    {
        return $user->hasRole(UserRole::PARTNER_SERVICE()) &&
            in_array(
                $order->service_id,
                $user->partnerService->services->pluck('id')->toArray()
            );
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    public function create(User $user): bool
    {
        return $user->hasRole(UserRole::TOURIST());
    }

    /**
     * @param User $user
     * @param Order $order
     * @return bool
     */
    public function update(User $user, Order $order): bool
    {
        return $user->is_admin ||
            $user->hasRole(UserRole::TOURIST()) &&
            $user->id === $order->user_id;
    }

    /**
     * @param User $user
     * @param Order $order
     * @return bool
     */
    public function delete(User $user, Order $order): bool
    {
        return true;
    }
}
