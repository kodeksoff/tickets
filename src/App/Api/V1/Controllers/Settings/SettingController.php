<?php

declare(strict_types=1);

namespace App\Api\V1\Controllers\Settings;

use App\Api\V1\Resources\ContactResource;
use Domain\Settings\Actions\GetSettingsAction;
use Support\Responses\JsonResponse;
use Support\Responses\ResponseFactory;

class SettingController
{
    /**
     * @param  GetSettingsAction  $getSettingsAction
     * @param  ResponseFactory  $responseFactory
     *
     * @return JsonResponse
     */
    public function index(GetSettingsAction $getSettingsAction, ResponseFactory $responseFactory): JsonResponse
    {
        return $responseFactory->json(
            $getSettingsAction(),
        );
    }

    /**
     * @param  GetSettingsAction  $getSettingsAction
     * @param  ResponseFactory  $responseFactory
     *
     * @return JsonResponse
     */
    public function contacts(GetSettingsAction $getSettingsAction, ResponseFactory $responseFactory): JsonResponse
    {
        return $responseFactory->json(
            new ContactResource(
                $getSettingsAction(
                    'production',
                    'contacts',
                ),
            ),
        );
    }
}
