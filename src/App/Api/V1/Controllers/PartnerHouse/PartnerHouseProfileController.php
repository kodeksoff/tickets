<?php

declare(strict_types=1);

namespace App\Api\V1\Controllers\PartnerHouse;

use App\Api\V1\Resources\PartnerHouseProfileResource;
use Domain\PartnerHouses\Actions\ShowProfileAction;
use Support\Responses\JsonResponse;
use Support\Responses\ResponseFactory;
use Throwable;

class PartnerHouseProfileController
{
    /**
     * @param  ShowProfileAction  $showProfileAction
     * @param  ResponseFactory  $responseFactory
     *
     * @return JsonResponse
     * @throws Throwable
     */
    public function show(
        ShowProfileAction $showProfileAction,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        return $responseFactory->json(
            new PartnerHouseProfileResource(
                $showProfileAction(),
            ),
        );
    }
}
