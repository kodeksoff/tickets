<?php

declare(strict_types=1);

namespace App\Api\V1\Controllers\ServiceCategories;

use Domain\ServiceCategories\Actions\ServiceCategoriesAction;
use Support\Responses\JsonResponse;
use Support\Responses\ResponseFactory;

class ServiceCategoriesController
{
    /**
     * @param  ServiceCategoriesAction  $serviceCategoriesListAction
     * @param  ResponseFactory  $responseFactory
     *
     * @return JsonResponse
     */
    public function __invoke(
        ServiceCategoriesAction $serviceCategoriesListAction,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        return $responseFactory->json(
            $serviceCategoriesListAction(),
        );
    }
}
