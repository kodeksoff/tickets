<?php

declare(strict_types=1);

namespace App\Api\V1\Controllers\Users;

use App\Api\V1\Requests\StoreRegistrationRequest;
use Domain\Users\Actions\Register\RegisterUserAction;
use Domain\Users\DataTransferObjects\CreateUserData;
use Domain\Users\Enums\UserRole;
use Support\Responses\JsonResponse;
use Support\Responses\ResponseFactory;
use Throwable;

class RegisterUserController
{
    /** @throws Throwable */
    public function __invoke(
        StoreRegistrationRequest $request,
        UserRole $userRole,
        RegisterUserAction $registerUserAction,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        return $responseFactory->json(
            $registerUserAction(
                CreateUserData::from($request->validated()),
            ),
        );
    }
}
