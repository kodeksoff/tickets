<?php

declare(strict_types=1);

namespace App\Api\V1\Controllers\Users;

use App\Api\V1\Requests\RegistrationRequest;
use Domain\Users\Actions\Register\RegisterValidationAction;
use Domain\Users\Enums\UserRole;
use Illuminate\Contracts\Container\BindingResolutionException;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Support\Responses\JsonResponse;
use Support\Responses\ResponseFactory;

class RegisterValidationController
{
    /**
     * @param  RegistrationRequest  $registrationRequest
     * @param  UserRole  $userRole
     * @param  RegisterValidationAction  $registerValidationAction
     * @param  ResponseFactory  $responseFactory
     *
     * @return JsonResponse
     * @throws BindingResolutionException
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(
        RegistrationRequest $registrationRequest,
        UserRole $userRole,
        RegisterValidationAction $registerValidationAction,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        return $responseFactory->json(
            $registrationRequest->validated(),
        );
    }
}
