<?php

declare(strict_types=1);

namespace App\Api\V1\Controllers\Users;

use App\Api\V1\Requests\ResetPasswordRequest;
use Domain\Users\Actions\ResetPasswordAction;
use Domain\Users\DataTransferObjects\ResetPasswordData;
use Support\Responses\JsonResponse;
use Support\Responses\ResponseFactory;
use Throwable;

class ResetPasswordController
{
    /** @throws Throwable */
    public function __invoke(
        ResetPasswordRequest $request,
        ResetPasswordAction $resetPasswordAction,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        return $responseFactory->json(
            $resetPasswordAction(
                ResetPasswordData::from($request->validated()),
            ),
        );
    }
}
