<?php

declare(strict_types=1);

namespace App\Api\V1\Controllers\Users;

use App\Api\V1\Requests\UpdatePasswordRequest;
use App\Api\V1\Requests\UpdatePhoneRequest;
use App\Api\V1\Resources\ProfileResource;
use Domain\Users\Actions\Auth\LogoutAction;
use Domain\Users\Actions\UpdatePasswordAction;
use Domain\Users\Actions\UpdatePhoneAction;
use Domain\Users\DataTransferObjects\UpdatePasswordData;
use Domain\Users\DataTransferObjects\UpdatePhoneData;
use Illuminate\Contracts\Auth\Factory;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Support\Responses\JsonResponse;
use Support\Responses\ResponseFactory;
use Throwable;

class ProfileController
{
    public function show(ResponseFactory $responseFactory, Factory $authFactory): JsonResponse
    {
        $user = $authFactory
            ->guard()
            ->user();

        return $responseFactory->json(
            new ProfileResource(
                $user
            ),
        );
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws Throwable
     */
    public function updatePhone(
        UpdatePhoneRequest $request,
        UpdatePhoneAction $updatePhoneAction,
        ResponseFactory $responseFactory
    ): JsonResponse {
        $updatePhoneAction(
            UpdatePhoneData::from($request->validated()),
        );

        return $responseFactory->json();
    }

    /** @throws Throwable */
    public function updatePassword(
        UpdatePasswordRequest $request,
        UpdatePasswordAction $updatePasswordAction,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        $updatePasswordAction(
            UpdatePasswordData::from($request->validated()),
        );

        return $responseFactory->json();
    }

    /** @throws Throwable */
    public function logout(ResponseFactory $responseFactory, LogoutAction $logoutAction): JsonResponse
    {
        $logoutAction();

        return $responseFactory->json();
    }
}
