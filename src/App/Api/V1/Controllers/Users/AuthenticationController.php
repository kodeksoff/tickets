<?php

declare(strict_types=1);

namespace App\Api\V1\Controllers\Users;

use App\Api\V1\Requests\LoginRequest;
use Domain\Users\Actions\Auth\LoginAction;
use Domain\Users\DataTransferObjects\LoginData;
use Domain\Users\Enums\AuthUserRole;
use Illuminate\Validation\ValidationException;
use Support\Responses\JsonResponse;
use Support\Responses\ResponseFactory;
use Throwable;

class AuthenticationController
{
    /**
     * @throws Throwable
     * @throws ValidationException
     */
    public function __invoke(
        LoginRequest $request,
        AuthUserRole $authUserRole,
        LoginAction $loginAction,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        return $responseFactory->json(
            $loginAction(
                LoginData::from($request->validated()),
            ),
        );
    }
}
