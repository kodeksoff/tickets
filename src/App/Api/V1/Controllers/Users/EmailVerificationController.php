<?php

declare(strict_types=1);

namespace App\Api\V1\Controllers\Users;

use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Contracts\Foundation\Application as ApplicationContract;
use Illuminate\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Throwable;

class EmailVerificationController
{
    /** @throws Throwable */
    public function __invoke(
        EmailVerificationRequest $request
    ): Application|Redirector|RedirectResponse|ApplicationContract {
        $request->fulfill();

        return redirect('/');
    }
}
