<?php

declare(strict_types=1);

namespace App\Api\V1\Controllers\Users;

use App\Api\V1\Requests\RecoveryPasswordRequest;
use Domain\Users\Actions\RecoveryPasswordAction;
use Domain\Users\Enums\AuthUserRole;
use Support\Responses\JsonResponse;
use Support\Responses\ResponseFactory;
use Throwable;

class RecoveryPasswordController
{
    /** @throws Throwable */
    public function __invoke(
        RecoveryPasswordRequest $recoveryPasswordRequest,
        AuthUserRole $authUserRole,
        RecoveryPasswordAction $recoveryPasswordAction,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        return $responseFactory->json(
            $recoveryPasswordAction(
                $authUserRole,
                $recoveryPasswordRequest->get('phone'),
            ),
        );
    }
}
