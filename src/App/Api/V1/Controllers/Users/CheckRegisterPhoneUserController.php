<?php

declare(strict_types=1);

namespace App\Api\V1\Controllers\Users;

use App\Api\V1\Requests\CheckRegistrationRequest;
use Domain\Users\Actions\CheckRegisterUserPhoneAction;
use Support\Responses\JsonResponse;
use Support\Responses\ResponseFactory;
use Throwable;

class CheckRegisterPhoneUserController
{
    /** @throws Throwable */
    public function __invoke(
        CheckRegistrationRequest $checkRegistrationRequest,
        CheckRegisterUserPhoneAction $checkRegisterUserPhoneAction,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        return $responseFactory->json(
            $checkRegisterUserPhoneAction(
                $checkRegistrationRequest->get('phone'),
            )
        );
    }
}
