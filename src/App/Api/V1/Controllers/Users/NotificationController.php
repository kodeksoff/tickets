<?php

declare(strict_types=1);

namespace App\Api\V1\Controllers\Users;

use App\Api\V1\Resources\NotificationCollection;
use Domain\Notifications\Actions\DeleteNotificationAction;
use Domain\Notifications\Actions\GetNotificationsAction;
use Domain\Notifications\Actions\ReadNotificationAction;
use Illuminate\Notifications\DatabaseNotification;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Support\Responses\JsonResponse;
use Support\Responses\ResponseFactory;

class NotificationController
{
    /**
     * @param  GetNotificationsAction  $getNotificationsAction
     * @param  ResponseFactory  $responseFactory
     *
     * @return JsonResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function index(
        GetNotificationsAction $getNotificationsAction,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        return $responseFactory->json(
            new NotificationCollection(
                $getNotificationsAction()
            ),
        );
    }

    /**
     * @param  DatabaseNotification  $notification
     * @param  ReadNotificationAction  $readNotificationAction
     * @param  ResponseFactory  $responseFactory
     *
     * @return JsonResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function read(
        DatabaseNotification $notification,
        ReadNotificationAction $readNotificationAction,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        $readNotificationAction($notification);

        return $responseFactory->json();
    }

    /**
     * @param  DatabaseNotification  $notification
     * @param  DeleteNotificationAction  $deleteNotificationAction
     * @param  ResponseFactory  $responseFactory
     *
     * @return JsonResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function delete(
        DatabaseNotification $notification,
        DeleteNotificationAction $deleteNotificationAction,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        $deleteNotificationAction($notification);

        return $responseFactory->json();
    }
}
