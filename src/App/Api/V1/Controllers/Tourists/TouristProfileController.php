<?php

declare(strict_types=1);

namespace App\Api\V1\Controllers\Tourists;

use App\Api\V1\Requests\SaveTouristProfileRequest;
use App\Api\V1\Resources\TouristProfileResource;
use Domain\Tourists\Actions\ShowProfileAction;
use Domain\Users\Actions\SaveTouristProfileAction;
use Domain\Users\DataTransferObjects\UpdateProfileTouristData;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Support\Responses\JsonResponse;
use Support\Responses\ResponseFactory;
use Throwable;

class TouristProfileController
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws Throwable
     */
    public function update(
        SaveTouristProfileRequest $request,
        SaveTouristProfileAction $touristProfileAction,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        $touristProfileAction(
            UpdateProfileTouristData::from($request->validated()),
        );

        return $responseFactory->json();
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws Throwable
     * @throws NotFoundExceptionInterface
     */
    public function show(
        ShowProfileAction $showProfileAction,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        return $responseFactory->json(
            new TouristProfileResource(
                $showProfileAction(),
            ),
        );
    }
}
