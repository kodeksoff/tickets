<?php

declare(strict_types=1);

namespace App\Api\V1\Controllers\PartnerServices;

use App\Api\V1\Resources\PartnerServiceProfileResource;
use Domain\PartnerServices\Actions\DeleteProfileAction;
use Domain\PartnerServices\Actions\ShowProfileAction;
use Support\Responses\JsonResponse;
use Support\Responses\ResponseFactory;
use Throwable;

class PartnerServiceProfileController
{
    /**
     * @param  ShowProfileAction  $showProfileAction
     * @param  ResponseFactory  $responseFactory
     *
     * @return JsonResponse
     * @throws Throwable
     */
    public function show(
        ShowProfileAction $showProfileAction,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        return $responseFactory->json(
            new PartnerServiceProfileResource(
                $showProfileAction(),
            ),
        );
    }

    /**
     * @param  DeleteProfileAction  $deleteProfileAction
     * @param  ResponseFactory  $responseFactory
     *
     * @return JsonResponse
     * @throws Throwable
     */
    public function delete(
        DeleteProfileAction $deleteProfileAction,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        $deleteProfileAction();

        return $responseFactory->json();
    }
}
