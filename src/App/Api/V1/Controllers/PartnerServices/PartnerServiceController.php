<?php

declare(strict_types=1);

namespace App\Api\V1\Controllers\PartnerServices;

use App\Api\V1\Resources\Orders\PartnerServiceOrderListResource;
use App\Api\V1\Resources\Services\ServiceListCollection;
use Domain\PartnerServices\Actions\Services\GetHiddenServicesAction;
use Domain\PartnerServices\Actions\Services\GetPublishedServicesAction;
use Domain\PartnerServices\Actions\Services\GetPurchasedServicesAction;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Support\Responses\JsonResponse;
use Support\Responses\ResponseFactory;
use Support\Utils\PaginationFactory;

class PartnerServiceController
{
    /**
     * @param GetPurchasedServicesAction $getPurchasedServicesAction
     * @param PaginationFactory $paginationFactory
     * @param ResponseFactory $responseFactory
     *
     * @return JsonResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function purchased(
        GetPurchasedServicesAction $getPurchasedServicesAction,
        PaginationFactory $paginationFactory,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        return $responseFactory->json(
            resolve(PaginationFactory::class)->toPaginationResource(
                PartnerServiceOrderListResource::collection(
                    $paginationFactory->fromQueryBuilder(
                        $getPurchasedServicesAction(),
                    )
                )
            ),
        );
    }

    /**
     * @param GetPublishedServicesAction $getPublishedServicesAction
     * @param PaginationFactory $paginationFactory
     * @param ResponseFactory $responseFactory
     *
     * @return JsonResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function published(
        GetPublishedServicesAction $getPublishedServicesAction,
        PaginationFactory $paginationFactory,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        return $responseFactory->json(
            new ServiceListCollection(
                $paginationFactory->fromQueryBuilder(
                    $getPublishedServicesAction(),
                )
            ),
        );
    }

    /**
     * @param GetHiddenServicesAction $getHiddenServicesAction
     * @param PaginationFactory $paginationFactory
     * @param ResponseFactory $responseFactory
     *
     * @return JsonResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function hidden(
        GetHiddenServicesAction $getHiddenServicesAction,
        PaginationFactory $paginationFactory,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        return $responseFactory->json(
            new ServiceListCollection(
                $paginationFactory->fromQueryBuilder(
                    $getHiddenServicesAction(),
                ),
            )
        );
    }
}
