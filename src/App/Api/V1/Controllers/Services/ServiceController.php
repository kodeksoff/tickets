<?php

declare(strict_types=1);

namespace App\Api\V1\Controllers\Services;

use App\Api\V1\Requests\PublishServiceRequest;
use App\Api\V1\Requests\ServiceQueryStringRequest;
use App\Api\V1\Requests\StoreServiceRequest;
use App\Api\V1\Requests\UpdateServiceRequest;
use App\Api\V1\Resources\Services\ServiceListCollection;
use App\Api\V1\Resources\Services\ServiceEditResource;
use App\Api\V1\Resources\Services\ServiceResource;
use Domain\Services\Actions\GetServiceAction;
use Domain\Services\Actions\GetServicesAction;
use Domain\Services\Actions\PublishServiceAction;
use Domain\Services\Actions\StoreServiceAction;
use Domain\Services\Actions\UpdateServiceAction;
use Domain\Services\DataTransferObjects\QueryStringData;
use Domain\Services\DataTransferObjects\StoreServiceData;
use Domain\Services\DataTransferObjects\UpdateServiceData;
use Domain\Services\Models\Service;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Support\Responses\JsonResponse;
use Support\Responses\ResponseFactory;
use Support\Utils\PaginationFactory;
use Throwable;

class ServiceController
{
    /**
     * @param  ServiceQueryStringRequest  $request
     * @param  GetServicesAction  $getServicesAction
     * @param  PaginationFactory  $paginationFactory
     * @param  ResponseFactory  $responseFactory
     *
     * @return JsonResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function index(
        ServiceQueryStringRequest $request,
        GetServicesAction $getServicesAction,
        PaginationFactory $paginationFactory,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        return $responseFactory->json(
            new ServiceListCollection(
                $paginationFactory->fromQueryBuilder(
                    $getServicesAction(
                        QueryStringData::from(
                            $request->validated(),
                        ),
                    ),
                )
            ),
        );
    }

    /**
     * @param  Service  $service
     * @param  GetServiceAction  $getServiceAction
     * @param  ResponseFactory  $responseFactory
     *
     * @return JsonResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function show(
        Service $service,
        GetServiceAction $getServiceAction,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        return $responseFactory->json(
            new ServiceResource($getServiceAction($service)),
        );
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function store(
        StoreServiceRequest $request,
        StoreServiceAction $storeServiceAction,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        return $responseFactory->json(
            $storeServiceAction(
                StoreServiceData::from(
                    $request->validated(),
                ),
            ),
        );
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function edit(
        Service $service,
        GetServiceAction $getServiceAction,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        return $responseFactory->json(
            new ServiceEditResource(
                $getServiceAction($service)
            ),
        );
    }

    /**
     * @param  UpdateServiceRequest  $request
     * @param  Service  $service
     * @param  UpdateServiceAction  $updateServiceAction
     * @param  ResponseFactory  $responseFactory
     *
     * @return JsonResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface|Throwable
     */
    public function update(
        UpdateServiceRequest $request,
        Service $service,
        UpdateServiceAction $updateServiceAction,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        return $responseFactory->json(
            new ServiceEditResource(
                $updateServiceAction(
                    $request,
                    $service,
                    UpdateServiceData::from(
                        $request->validated(),
                    ),
                )
            ),
        );
    }

    /**
     * @param  Service  $service
     *
     * @return void
     */
    public function delete(
        Service $service,
    ) {

    }

    /**
     * @param  PublishServiceRequest  $request
     * @param  Service  $service
     * @param  PublishServiceAction  $publishServiceAction
     * @param  ResponseFactory  $responseFactory
     *
     * @return JsonResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws Throwable
     */
    public function publish(
        PublishServiceRequest $request,
        Service $service,
        PublishServiceAction $publishServiceAction,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        return $responseFactory->json(
            new ServiceEditResource(
                $publishServiceAction(
                    $request,
                    $service,
                    UpdateServiceData::from(
                        $request->validated(),
                    ),
                )
            ),
        );
    }

    /**
     * @param  Service  $service
     *
     * @return void
     */
    public function unpublished(
        Service $service,
    ) {

    }
}
