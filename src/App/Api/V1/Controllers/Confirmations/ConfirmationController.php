<?php

declare(strict_types=1);

namespace App\Api\V1\Controllers\Confirmations;

use App\Api\V1\Requests\ApproveConfirmationRequest;
use App\Api\V1\Requests\SendPhoneConfirmationRequest;
use Domain\Confirmations\Actions\ApproveConfirmationAction;
use Domain\Confirmations\Actions\BuildRegistrationConfirmationAction;
use Domain\Confirmations\DataTransferObjects\ApproveConfirmationData;
use Illuminate\Contracts\Container\BindingResolutionException;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Support\Responses\JsonResponse;
use Support\Responses\ResponseFactory;
use Throwable;

class ConfirmationController
{
    /**
     * @param  SendPhoneConfirmationRequest  $request
     * @param  BuildRegistrationConfirmationAction  $buildRegistrationConfirmationAction
     * @param  ResponseFactory  $responseFactory
     *
     * @return JsonResponse
     * @throws BindingResolutionException
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function phoneSend(
        SendPhoneConfirmationRequest $request,
        BuildRegistrationConfirmationAction $buildRegistrationConfirmationAction,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        return $responseFactory->json(
            $buildRegistrationConfirmationAction($request->get('phone')),
        );
    }

    /** @throws Throwable */
    public function phoneApprove(
        ApproveConfirmationRequest $request,
        ApproveConfirmationAction $approveConfirmationAction,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        return $responseFactory->json(
            $approveConfirmationAction(
                ApproveConfirmationData::from(
                    $request->validated(),
                ),
            ),
        );
    }
}
