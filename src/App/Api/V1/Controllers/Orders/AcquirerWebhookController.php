<?php

declare(strict_types=1);

namespace App\Api\V1\Controllers\Orders;

use Domain\Acquiring\Actions\HandleWebhookAction;
use Domain\Acquiring\Enums\Acquirer;
use Illuminate\Http\Request;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Support\Responses\JsonResponse;
use Support\Responses\ResponseFactory;

class AcquirerWebhookController
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(
        Request $request,
        Acquirer $acquirer,
        HandleWebhookAction $handleWebhookAction,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        $handleWebhookAction($request, $acquirer);

        return $responseFactory->json();
    }
}
