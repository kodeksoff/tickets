<?php

declare(strict_types=1);

namespace App\Api\V1\Controllers\Orders;

use App\Api\V1\Requests\StoreOrderRequest;
use App\Api\V1\Resources\Orders\OrderResource;
use App\Api\V1\Resources\Orders\TouristOrderListResource;
use Domain\Orders\Actions\GetTouristOrders;
use Domain\Orders\Actions\RefundOrderAction;
use Domain\Orders\Actions\ShowOrderAction;
use Domain\Orders\Actions\StoreOrderAction;
use Domain\Orders\DataTransferObjects\StoreOrderData;
use Domain\Orders\Models\Order;
use Illuminate\Database\DatabaseManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Support\Responses\JsonResponse;
use Support\Responses\ResponseFactory;
use Throwable;
use YooKassa\Common\AbstractObject;

class TouristOrdersController
{
    public function __construct(protected DatabaseManager $databaseManager)
    {
    }

    /**
     * @param GetTouristOrders $getTouristOrders
     * @param ResponseFactory $responseFactory
     *
     * @return JsonResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws Throwable
     */
    public function index(
        GetTouristOrders $getTouristOrders,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        return $responseFactory->json(
            TouristOrderListResource::collection(
                $getTouristOrders(),
            )
        );
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws Throwable
     */
    public function store(
        StoreOrderRequest $request,
        StoreOrderAction $storeOrderAction,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        return $responseFactory->json(
            $this
                ->databaseManager
                ->transaction(fn (): AbstractObject => $storeOrderAction(
                    StoreOrderData::from(
                        $request->validated(),
                    ),
                ))
        );
    }

    /**
     * @param Order $order
     * @param ShowOrderAction $showOrderAction
     * @param ResponseFactory $responseFactory
     *
     * @return JsonResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws Throwable
     */
    public function show(
        Order $order,
        ShowOrderAction $showOrderAction,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        return $responseFactory->json(
            new OrderResource(
                $showOrderAction($order),
            ),
        );
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws Throwable
     */
    public function refund(
        Order $order,
        RefundOrderAction $refundOrderAction,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        return $responseFactory->json(
            $this
                ->databaseManager
                ->transaction(
                    fn () => $refundOrderAction($order)
                )
        );
    }
}
