<?php

declare(strict_types=1);

namespace App\Api\V1\Controllers\Orders;

use Illuminate\Database\DatabaseManager;

class OrderController
{
    public function __construct(protected DatabaseManager $databaseManager)
    {
    }
}
