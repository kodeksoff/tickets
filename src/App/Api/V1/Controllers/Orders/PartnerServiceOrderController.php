<?php

declare(strict_types=1);

namespace App\Api\V1\Controllers\Orders;

use App\Api\V1\Requests\UseTicketRequest;
use App\Api\V1\Resources\Orders\OrderResource;
use Domain\Orders\Actions\ShowOrderAction;
use Domain\Orders\Models\Order;
use Domain\Tickets\Actions\UseTicketAction;
use Domain\Tickets\DataTransferObjects\UseTicketData;
use Domain\Tickets\Models\Ticket;
use Illuminate\Database\DatabaseManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Support\Responses\JsonResponse;
use Support\Responses\ResponseFactory;
use Throwable;

class PartnerServiceOrderController
{
    public function __construct(protected DatabaseManager $databaseManager)
    {
    }

    /**
     * @param Order $order
     * @param ShowOrderAction $showOrderAction
     * @param ResponseFactory $responseFactory
     *
     * @return JsonResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws Throwable
     */
    public function show(
        Order $order,
        ShowOrderAction $showOrderAction,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        return $responseFactory->json(
            new OrderResource(
                $showOrderAction($order),
            ),
        );
    }

    public function useTicket(
        Ticket $ticket,
        UseTicketRequest $request,
        UseTicketAction $useTicketAction,
        ResponseFactory $responseFactory,
    ): JsonResponse {
        return $responseFactory->json(
            $useTicketAction(
                $ticket,
                UseTicketData::from(
                    $request->validated(),
                )
            ),
        );
    }
}
