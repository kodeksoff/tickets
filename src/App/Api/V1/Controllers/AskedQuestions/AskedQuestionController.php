<?php

declare(strict_types=1);

namespace App\Api\V1\Controllers\AskedQuestions;

use Domain\AskedQuestions\Actions\GetAskedQuestionsAction;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Support\Responses\JsonResponse;
use Support\Responses\ResponseFactory;

class AskedQuestionController
{
    /**
     * @param GetAskedQuestionsAction $getAskedQuestionsAction
     * @param ResponseFactory $responseFactory
     * @return JsonResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function index(
        GetAskedQuestionsAction $getAskedQuestionsAction,
        ResponseFactory $responseFactory
    ): JsonResponse {
        return $responseFactory->json(
            $getAskedQuestionsAction(),
        );
    }
}
