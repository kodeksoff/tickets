<?php

declare(strict_types=1);

namespace App\Api\V1\Resources\Services;

use Brick\Math\BigDecimal;
use Domain\Services\Enums\ImageCollection;
use Domain\Services\Enums\ImageSetting;
use Domain\Services\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ServiceListResource extends JsonResource
{
    /**
     * @param  Request  $request
     *
     * @return array
     */
    public function toArray(Request $request): array
    {
        /** @var Service|JsonResource $this */
        return [
            'id' => $this->id,
            'title' => $this->title,
            'status' => $this->status,
            'image' => $this->whenLoaded(
                'media',
                fn (): ?string => $this->getImageUrl(
                    ImageCollection::MAIN_IMAGE,
                    ImageSetting::MEDIUM,
                ),
            ),
            'full_price' => $this->whenLoaded(
                'lowestBooking',
                fn (): BigDecimal => $this->lowest_booking->full_price_amount,
            ),
            'full_price_with_discount' => $this->whenLoaded(
                'lowestBooking',
                fn (): BigDecimal => $this->lowest_booking->full_price_with_discount_amount,
            ),
            'discount_price' => $this->whenLoaded(
                'lowestBooking',
                fn (): BigDecimal => $this->lowest_booking->discount_price_amount,
            ),
            'moderation' => $this->whenLoaded(
                'latestModeration',
                fn (): ModerationResource => new ModerationResource($this->latestModeration),
            ),
            'categories' => $this->categories,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
