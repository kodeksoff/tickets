<?php

declare(strict_types=1);

namespace App\Api\V1\Resources\Services;

use Domain\ServiceBooking\Models\ServiceBooking;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ServiceEditBookingResource extends JsonResource
{
    /**
     * @param  Request  $request
     *
     * @return array
     */
    public function toArray(Request $request): array
    {
        /** @var ServiceBooking|JsonResource $this */
        return [
            'id' => $this->id,
            'title' => $this->title,
            'full_price' => $this->full_price_amount,
            'full_price_with_discount' => $this->full_price_with_discount_amount,
            'discount_price' => $this->discount_price_amount,
            'is_default' => $this->is_default,
            'disabled' => false,
        ];
    }
}
