<?php

declare(strict_types=1);

namespace App\Api\V1\Resources\Services;

use Domain\Services\Enums\ImageCollection;
use Domain\Services\Enums\ImageSetting;
use Domain\Services\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ServiceEditResource extends JsonResource
{
    /**
     * @param  Request  $request
     *
     * @return array
     */
    public function toArray(Request $request): array
    {
        /** @var Service|JsonResource $this */
        return [
            'id' => $this->id,
            'title' => $this->title,
            'status' => $this->status,
            'categories' => $this->categories,
            'main_image' => $this->whenLoaded(
                'media',
                fn (): ?string => $this->getImageUrl(
                    ImageCollection::MAIN_IMAGE,
                    ImageSetting::MEDIUM,
                ),
            ),
            'additional_images' => $this->whenLoaded(
                'media',
                fn () => $this->getAdditionalImages(
                    ImageCollection::ADDITIONAL_IMAGES,
                    ImageSetting::THUMB_LARGE,
                ),
            ),
            'service_season_from' => $this->service_season_from,
            'service_season_to' => $this->service_season_to,
            'service_all_year' => $this->service_all_year,
            'booking_types' => $this->whenLoaded(
                'bookings',
                fn () => ServiceEditBookingResource::collection($this->bookings),
            ),
            'description' => $this->description,
            'about_included' => $this->about_included,
            'about_additional' => $this->about_additional,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
