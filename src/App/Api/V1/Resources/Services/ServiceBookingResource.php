<?php

declare(strict_types=1);

namespace App\Api\V1\Resources\Services;

use Domain\ServiceBooking\Models\ServiceBooking;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ServiceBookingResource extends JsonResource
{
    /**
     * @param  Request  $request
     *
     * @return array
     */
    public function toArray(Request $request): array
    {
        /** @var ServiceBooking|JsonResource $this */
        return [
            'id' => $this->id,
            'title' => $this->title,
            'full_price' => $this->full_price_amount,
            'service_price_for_booking_amount' => $this->service_price_for_booking_amount,
            'full_price_with_discount' => $this->full_price_with_discount_amount,
            'surcharge_price_amount' => $this->surcharge_price_amount,
        ];
    }
}
