<?php

declare(strict_types=1);

namespace App\Api\V1\Resources\Services;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Support\Utils\PaginationFactory;

/** @see \Domain\Services\Models\Service */
class ServiceListCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @return array<int|string, mixed>
     */
    public function toArray(Request $request): array
    {
        return resolve(PaginationFactory::class)->toPaginationResource($this);
    }
}
