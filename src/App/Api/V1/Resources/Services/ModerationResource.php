<?php

declare(strict_types=1);

namespace App\Api\V1\Resources\Services;

use Domain\Services\Models\ServiceModeration;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ModerationResource extends JsonResource
{
    /**
     * @param  Request  $request
     *
     * @return array
     */
    public function toArray(Request $request): array
    {
        /** @var ServiceModeration|JsonResource $this */
        return [
            'id' => $this->id,
            'reason' => $this->reason,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
