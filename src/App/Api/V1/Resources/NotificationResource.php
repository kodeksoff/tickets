<?php

declare(strict_types=1);

namespace App\Api\V1\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Notifications\DatabaseNotificationCollection;

class NotificationResource extends JsonResource
{
    /**
     * @param  Request  $request
     *
     * @return array
     */
    public function toArray(Request $request): array
    {
        /** @var DatabaseNotificationCollection|JsonResource $this */
        return [
            'id' => $this->id,
            'data' => $this->data,
        ];
    }
}
