<?php

declare(strict_types=1);

namespace App\Api\V1\Resources;

use Domain\Settings\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ContactResource extends JsonResource
{
    /** @return array{id: mixed, full_name: mixed, phone: mixed, email: mixed, role: mixed} */
    public function toArray(Request $request): array
    {
        /** @var Setting $this */
        return $this->contacts;
    }
}
