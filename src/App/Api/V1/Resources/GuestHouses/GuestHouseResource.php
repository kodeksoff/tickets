<?php

declare(strict_types=1);

namespace App\Api\V1\Resources\GuestHouses;

use Domain\GuestHouses\Models\GuestHouse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class GuestHouseResource extends JsonResource
{
    /**
     * @param  Request  $request
     *
     * @return array
     */
    public function toArray(Request $request): array
    {
        /** @var GuestHouse|JsonResource $this */
        return [
            'id' => $this->id,
            'title' => $this->title,
            'address' => $this->address,
            'short_address' => $this->short_address,
            'code' => $this->code,
        ];
    }
}
