<?php

declare(strict_types=1);

namespace App\Api\V1\Resources;

use App\Api\V1\Resources\GuestHouses\GuestHouseResource;
use Domain\PartnerHouses\Models\PartnerHouse;
use Domain\Users\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class PartnerHouseProfileResource extends JsonResource
{
    /** @return array{id: mixed, full_name: mixed, phone: mixed, email: mixed, inn: mixed} */
    public function toArray(Request $request): array
    {
        /** @var User|PartnerHouse $this */
        return [
            'id' => $this->id,
            'full_name' => $this->full_name,
            'phone' => $this->phone,
            'email' => $this->email,
            'inn' => $this->partner_house->inn,
            'guest_houses' => $this->when(
                $this->relationLoaded('partnerHouse') &&
                $this->partnerHouse->relationLoaded('guestHouses'),
                fn (): AnonymousResourceCollection => GuestHouseResource::collection(
                    $this->partner_house->guest_houses,
                ),
            ),
            'role' => $this->user_role,
        ];
    }
}
