<?php

declare(strict_types=1);

namespace App\Api\V1\Resources\Orders;

use Domain\Tickets\Models\Ticket;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TicketResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        /** @var Ticket $this */
        return [
            'id' => $this->id,
            'status' => $this->status,
            'full_price_amount' => $this->serviceBooking->full_price_amount,
            'surcharge_price_amount' => $this->serviceBooking->surcharge_price_amount,
            'title' => $this->serviceBooking->title,
            'code' => $this->code,
            'number' => $this->number,
            'closed_at' => $this->closed_at,
            'qr' => $this
                ->getMedia('qr_image')
                ->first()
                ?->getUrl(),
        ];
    }
}
