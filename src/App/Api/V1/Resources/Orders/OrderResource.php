<?php

declare(strict_types=1);

namespace App\Api\V1\Resources\Orders;

use Domain\Orders\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        /** @var Order $this */
        return [
            'id' => $this->id,
            'status' => $this->status,
            'full_price_amount' => $this->full_price_amount,
            'surcharge_price_amount' => $this->surcharge_price_amount,
            'title' => $this->service->title,
            'items_count' => count($this->items),
            'closed_at' => $this->items[0]->ticket?->closed_at,
            'user' => $this->when(
                $this->relationLoaded('user'),
                [
                    'id' => $this->user->id,
                    'full_name' => $this->user->tourist->full_name,
                    'phone' => $this->user->phone,
                    'guest_house' => [
                        'address' => $this->when(
                            $this->relationLoaded('user'),
                            $this->user->tourist->guestHouse?->address
                        ),
                        'title' => $this->when(
                            $this->relationLoaded('user'),
                            $this->user->tourist->guestHouse?->title
                        ),
                    ],
                ]
            ),
            'tickets' => $this->when(
                $this->relationLoaded('tickets'),
                fn (): AnonymousResourceCollection => TicketResource::collection(
                    $this->tickets,
                ),
            ),
        ];
    }
}
