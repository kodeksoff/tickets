<?php

declare(strict_types=1);

namespace App\Api\V1\Resources\Orders;

use Domain\Orders\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PartnerServiceOrderListResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        /** @var Order $this */
        return [
            'id' => $this->id,
            'status' => $this->status,
            'full_price_amount' => $this->full_price_amount,
            'surcharge_price_amount' => $this->surcharge_price_amount,
            'title' => $this->service->title,
            'items_count' => $this->tickets_count,
            'closed_at' => $this->tickets[0]->closed_at,
            'user_phone' => $this->user->phone,
        ];
    }
}
