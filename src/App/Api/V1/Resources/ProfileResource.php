<?php

declare(strict_types=1);

namespace App\Api\V1\Resources;

use Domain\Users\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProfileResource extends JsonResource
{
    /** @return array{id: mixed, full_name: mixed, phone: mixed, email: mixed, role: mixed} */
    public function toArray(Request $request): array
    {
        /** @var User $this */
        return [
            'id' => $this->id,
            'full_name' => $this->full_name,
            'phone' => $this->phone,
            'email' => $this->email,
            'role' => $this->user_role,
        ];
    }
}
