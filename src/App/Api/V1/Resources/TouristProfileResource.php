<?php

declare(strict_types=1);

namespace App\Api\V1\Resources;

use Domain\PartnerHouses\Models\PartnerHouse;
use Domain\Users\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TouristProfileResource extends JsonResource
{
    /** @return array{id: mixed, full_name: mixed, phone: mixed, email: mixed, inn: mixed} */
    public function toArray(Request $request): array
    {
        /** @var User|PartnerHouse $this */
        return [
            'id' => $this->id,
            'full_name' => $this->full_name,
            'phone' => $this->phone,
            'guest_house_code' => $this->tourist?->guestHouse?->code,
            'role' => $this->user_role,
        ];
    }
}
