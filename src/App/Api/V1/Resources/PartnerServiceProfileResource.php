<?php

declare(strict_types=1);

namespace App\Api\V1\Resources;

use Domain\PartnerServices\Models\PartnerService;
use Domain\Users\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PartnerServiceProfileResource extends JsonResource
{
    /** @return array{id: mixed, full_name: mixed, phone: mixed, email: mixed, inn: mixed} */
    public function toArray(Request $request): array
    {
        /** @var User|PartnerService $this */
        return [
            'id' => $this->id,
            'full_name' => $this->full_name,
            'phone' => $this->phone,
            'email' => $this->email,
            'inn' => $this->partner_service->inn,
            'role' => $this->user_role,
        ];
    }
}
