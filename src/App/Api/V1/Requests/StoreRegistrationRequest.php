<?php

declare(strict_types=1);

namespace App\Api\V1\Requests;

use Domain\Confirmations\Models\Confirmation;
use Domain\Confirmations\Rules\NotConfirmedTokenRule;
use Domain\Users\Rules\InnExistsRule;
use Illuminate\Validation\Rule;

class StoreRegistrationRequest extends RegistrationRequest
{
    /** @var Confirmation|null */
    protected ?Confirmation $confirmation = null;

    /** @return array */
    public function rules(): array
    {
        return [
            'full_name' => [
                Rule::requiredIf(
                    fn (): bool => !$this->isTourist(),
                ),
                'string',
            ],
            'phone' => [
                'required',
                'unique:Domain\Users\Models\User',
                'phone:RU',
            ],
            'email' => [
                Rule::requiredIf(
                    fn (): bool => !$this->isTourist(),
                ),
                'email',
                'unique:Domain\Users\Models\User',
            ],
            'inn' => [
                'nullable',
                'string',
                'digits_between:10,12',
                new InnExistsRule(
                    $this->user,
                    $this->isTourist(),
                ),
                Rule::requiredIf(
                    fn (): bool => !$this->isTourist(),
                ),
            ],
            'guest_house_code' => [
                'nullable',
                'integer',
                'exists:Domain\GuestHouses\Models\GuestHouse,code',
            ],
            'agreement' => [
                'required',
                'accepted',
            ],
            'password' => [
                Rule::requiredIf(
                    fn (): bool => !$this->isTourist(),
                ),
                'min:5',
                'max:255',
                'confirmed',
            ],
            'confirmation_id' => [
                'required',
                'string',
                new NotConfirmedTokenRule($this->confirmation, $this->isTourist()),
            ],
        ];
    }

    /** @return void */
    public function prepareForValidation(): void
    {
        /** @phpstan-ignore-next-line */
        $this->confirmation = Confirmation::findOrFail($this->input('confirmation_id'));
    }
}
