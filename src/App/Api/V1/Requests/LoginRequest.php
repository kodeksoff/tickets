<?php

declare(strict_types=1);

namespace App\Api\V1\Requests;

use Domain\Confirmations\Models\Confirmation;
use Domain\Confirmations\Rules\MatchTokenRule;
use Domain\Confirmations\Rules\TokenAlreadyUsedRule;
use Domain\Users\Enums\AuthUserRole;
use Domain\Users\Models\User;
use Domain\Users\Rules\UserLoginRule;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Validation\Rule;

class LoginRequest extends RegistrationRequest
{
    /** @var Confirmation|null */
    protected ?Confirmation $confirmation = null;

    /** @return array */
    public function rules(): array
    {
        return [
            'password' => [
                Rule::requiredIf(
                    fn (): bool => !$this->isTourist(),
                ),
                'nullable',
                'string',
            ],
            'phone' => [
                'required',
                'phone:RU',
                new UserLoginRule(
                    $this->user,
                    $this->isTourist(),
                    AuthUserRole::from(
                        $this
                            ->route('authUserRole')
                            ->value,
                    )
                ),
            ],
            'token' => [
                Rule::requiredIf(
                    fn (): bool => $this->isTourist(),
                ),
                'string',
                new MatchTokenRule($this->confirmation),
                new TokenAlreadyUsedRule($this->confirmation),
            ],
            'confirmation_id' => [
                Rule::requiredIf(
                    fn (): bool => $this->isTourist(),
                ),
                'string',
            ],
        ];
    }

    /**
     * @param  mixed|null  $key
     * @param  mixed|null  $default
     *
     * @throws BindingResolutionException
     */
    public function validated($key = null, $default = null): array
    {
        $result = parent::validated($key, $default);
        $result['user_role'] = $this->route('authUserRole');

        return $result;
    }

    /**
     * @return void
     * @throws BindingResolutionException
     */
    public function prepareForValidation(): void
    {
        if ($this->hasAny(['phone', 'email', 'inn'])) {
            $this->user = User::query()
                ->whereAllCredential(
                    $this->input('phone'),
                    $this->input('email'),
                    $this->input('inn'),
                )
                ->first();
        }

        if ($this->isTourist()) {
            /** @phpstan-ignore-next-line */
            $this->confirmation = Confirmation::findOrFail($this->input('confirmation_id'));
        }
    }

    public function isTourist(): bool
    {
        return $this->route('authUserRole') === AuthUserRole::TOURIST;
    }
}
