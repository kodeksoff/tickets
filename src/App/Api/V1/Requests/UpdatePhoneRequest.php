<?php

declare(strict_types=1);

namespace App\Api\V1\Requests;

use Domain\Confirmations\Models\Confirmation;
use Domain\Confirmations\Rules\ConfirmedTokenRule;
use Domain\Confirmations\Rules\MatchTokenRule;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePhoneRequest extends FormRequest
{
    /** @var Confirmation */
    protected Confirmation $confirmation;

    /** @return array */
    public function rules(): array
    {
        return [
            'confirmation_id' => [
                'required',
                'string',
            ],
            'token' => [
                'required',
                'string',
                'digits:5',
                new MatchTokenRule($this->confirmation),
                new ConfirmedTokenRule($this->confirmation),
            ],
            'phone' => [
                'required',
                'string',
            ],
        ];
    }

    /** @return void */
    public function prepareForValidation(): void
    {
        /** @phpstan-ignore-next-line */
        $this->confirmation = Confirmation::findOrFail($this->input('confirmation_id'));
    }
}
