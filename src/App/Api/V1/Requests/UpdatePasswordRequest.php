<?php

declare(strict_types=1);

namespace App\Api\V1\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePasswordRequest extends FormRequest
{
    /** @return array[] */
    public function rules(): array
    {
        return [
            'current_password' => [
                'required',
                'string',
                'current_password:api',
                'min:5',
                'max:255',
            ],
            'password' => [
                'required',
                'string',
                'confirmed',
                'min:5',
                'max:255',
            ],
        ];
    }
}
