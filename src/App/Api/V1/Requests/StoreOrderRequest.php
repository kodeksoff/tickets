<?php

declare(strict_types=1);

namespace App\Api\V1\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrderRequest extends FormRequest
{
    /** @return array[] */
    public function rules(): array
    {
        return [
            'service_id' => [
                'integer',
                'required',
            ],
            'return_url' => [
                'string',
                'required',
            ],
            'full_price_amount' => [
                'numeric',
            ],
            'booking_price_amount' => [
                'numeric',
            ],
            'booking_tickets' => [
                'array',
            ],
            'booking_tickets.*.service_booking_id' => [
                'required_with:booking_tickets',
                'numeric',
            ],
            'booking_tickets.*.full_price_amount' => [
                'required_with:booking_tickets',
                'numeric',
            ],
            'booking_tickets.*.discount_price_amount' => [
                'required_with:booking_tickets',
                'numeric',
            ],
            'booking_tickets.*.full_price_with_discount_amount' => [
                'required_with:booking_tickets',
                'numeric',
            ],
            'booking_tickets.*.service_price_for_booking_amount' => [
                'required_with:booking_tickets',
                'numeric',
            ],
            'booking_tickets.*.count' => [
                'required_with:booking_tickets',
                'integer',
            ],
        ];
    }
}
