<?php

declare(strict_types=1);

namespace App\Api\V1\Requests;

use Domain\ServiceBooking\Rules\IsDefaultRule;
use Domain\Services\Rules\PublishServiceRule;

class PublishServiceRequest extends UpdateServiceRequest
{
    /** @return array[] */
    public function rules(): array
    {
        return [
            'title' => [
                'string',
                'min:3',
                'max:255',
                new PublishServiceRule($this->route('any_service')),
            ],
            'category_ids' => [
                'array',
            ],
            'category_ids.*' => [
                'sometimes',
                'nullable',
                'required_with:category_ids',
                'integer',
                'exists:Domain\ServiceCategories\Models\ServiceCategory,id',
            ],
            'main_image' => [
                'image',
                'max:10240',
                'dimensions:min_width=1024,min_height=768',
            ],
            'additional_images' => [
                'array',
                'max:4',
            ],
            'additional_images.*' => [
                'image',
                'max:10240',
                'dimensions:min_width=1024,min_height=768',
            ],
            'service_season_from' => [
                'nullable',
                'integer',
                'min:1',
                'max:12',
                'lt:service_season_to',
            ],
            'service_season_to' => [
                'nullable',
                'integer',
                'min:1',
                'max:12',
            ],
            'service_all_year' => [
                'boolean',
            ],
            'booking_types' => [
                'array',
                new IsDefaultRule(),
            ],
            'booking_types.*.id' => [
                'int',
            ],
            'booking_types.*.title' => [
                'required_with:booking_types',
                'string',
            ],
            'booking_types.*.full_price' => [
                'required_with:booking_types',
                'integer',
            ],
            'booking_types.*.discount_price' => [
                'required_with:booking_types',
                'numeric',
            ],
            'booking_types.*.is_default' => [
                'required_with:booking_types',
                'boolean',
            ],
            'description' => [
                'string',
            ],
            'about_included' => [
                'string',
            ],
            'about_additional' => [
                'string',
            ],
        ];
    }
}
