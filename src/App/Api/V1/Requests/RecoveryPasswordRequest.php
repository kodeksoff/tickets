<?php

declare(strict_types=1);

namespace App\Api\V1\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RecoveryPasswordRequest extends FormRequest
{
    /** @return array[] */
    public function rules(): array
    {
        return [
            'phone' => [
                'required',
                'phone:RU',
            ],
        ];
    }
}
