<?php

declare(strict_types=1);

namespace App\Api\V1\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveTouristProfileRequest extends FormRequest
{
    /** @return array */
    public function rules(): array
    {
        return [
            'full_name' => [
                //'required',
                'string',
            ],
            'guest_house_code' => [
                //'required',
                'integer',
                'exists:Domain\GuestHouses\Models\GuestHouse,code',
            ],
        ];
    }
}
