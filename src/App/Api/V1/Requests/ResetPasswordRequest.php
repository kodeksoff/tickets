<?php

declare(strict_types=1);

namespace App\Api\V1\Requests;

use Domain\Confirmations\Models\Confirmation;
use Domain\Confirmations\Rules\MatchTokenRule;
use Domain\Confirmations\Rules\NotConfirmedTokenRule;
use Illuminate\Foundation\Http\FormRequest;

class ResetPasswordRequest extends FormRequest
{
    /** @var Confirmation */
    protected Confirmation $confirmation;

    /** @return array[] */
    public function rules(): array
    {
        return [
            'password' => [
                'required',
                'string',
                'confirmed',
            ],
            'token' => [
                'required',
                'string',
                new MatchTokenRule($this->confirmation),
                new NotConfirmedTokenRule($this->confirmation),
            ],
            'confirmation_id' => [
                'required',
                'string',
            ],
        ];
    }

    /** @return void */
    public function prepareForValidation(): void
    {
        /** @phpstan-ignore-next-line */
        $this->confirmation = Confirmation::findOrFail($this->input('confirmation_id'));
    }
}
