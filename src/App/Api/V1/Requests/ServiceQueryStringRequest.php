<?php

declare(strict_types=1);

namespace App\Api\V1\Requests;

use Domain\Services\Enums\ServiceOrdering;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class ServiceQueryStringRequest extends FormRequest
{
    /** @return array */
    public function rules(): array
    {
        return [
            'category_ids' => [
                'array',
            ],
            'category_ids.*' => [
                'required_with:category_ids',
                'integer',
            ],
            'order_by' => [
                'string',
                new Enum(ServiceOrdering::class),
            ],
            'order_direction' => [
                'string',
                'in:asc,desc',
            ],
        ];
    }
}
