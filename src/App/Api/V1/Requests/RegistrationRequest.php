<?php

declare(strict_types=1);

namespace App\Api\V1\Requests;

use Domain\Users\Enums\UserRole;
use Domain\Users\Models\User;
use Domain\Users\Rules\InnExistsRule;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Support\Utils\MobilePhoneFormatter;

use function array_key_exists;

class RegistrationRequest extends FormRequest
{
    /** @var User|null */
    protected ?User $user = null;

    /** @return array */
    public function rules(): array
    {
        return [
            'full_name' => [
                'required',
                'string',
                'min:3',
            ],
            'phone' => [
                'required',
                'unique:Domain\Users\Models\User',
                'phone:RU',
            ],
            'email' => [
                'email',
                'unique:Domain\Users\Models\User',
                Rule::requiredIf(
                    fn (): bool => !$this->isTourist(),
                ),
            ],
            'inn' => [
                'nullable',
                'string',
                'digits_between:10,12',
                new InnExistsRule(
                    $this->user,
                    $this->isTourist(),
                ),
                Rule::requiredIf(
                    fn (): bool => !$this->isTourist(),
                ),
            ],
            'guest_house_id' => [
                'nullable',
                'integer',
                Rule::requiredIf(
                    fn (): bool => $this->isTourist(),
                ),
                'exists:Domain\GuestHouses\Models\GuestHouse,id',
            ],
            'agreement' => [
                'required',
                'accepted',
            ],
        ];
    }

    /**
     * @param mixed|null $key
     * @param mixed|null $default
     *
     * @throws BindingResolutionException
     */
    public function validated($key = null, $default = null): array
    {
        $result = [
            'user_role' => $this->route('userRole'),
            ...parent::validated($key, $default),
        ];

        if (array_key_exists('phone', $result)) {
            $result['phone'] = resolve(MobilePhoneFormatter::class)->make($result['phone']);
        }

        return $result;
    }

    /**
     * @return void
     * @throws BindingResolutionException
     */
    public function prepareForValidation(): void
    {
        /** @phpstan-ignore-next-line */
        $this->user = User::query()
            ->whereAllCredential(
                $this->input('phone'),
                $this->input('email'),
                $this->input('inn'),
            )
            ->first();
    }

    /** @return bool */
    public function isTourist(): bool
    {
        return $this->route('userRole') === UserRole::TOURIST;
    }
}
