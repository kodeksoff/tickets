<?php

declare(strict_types=1);

namespace App\Api\V1\Requests;

class CheckRegistrationRequest extends RegistrationRequest
{
    /** @return array[] */
    public function rules(): array
    {
        return [
            'phone' => [
                'required',
                'phone:RU',
            ],
        ];
    }
}
