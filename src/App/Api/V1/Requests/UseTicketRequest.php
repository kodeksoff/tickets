<?php

declare(strict_types=1);

namespace App\Api\V1\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UseTicketRequest extends FormRequest
{
    /** @return array */
    public function rules(): array
    {
        return [
            'code' => [
                'required',
                'integer',
            ],
        ];
    }
}
