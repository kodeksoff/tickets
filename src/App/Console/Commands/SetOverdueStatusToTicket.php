<?php

declare(strict_types=1);

namespace App\Console\Commands;

use Domain\Orders\Enums\OrderStatus;
use Domain\Orders\Models\Order;
use Domain\Tickets\Enums\TicketStatuses;
use Domain\Tickets\Models\Ticket;
use Illuminate\Console\Command;

/**
 *
 */
class SetOverdueStatusToTicket extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'set_overdue_status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get signature.
     *
     * @return string
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /** Execute the console command. */
    public function handle()
    {
        Ticket::query()
            ->where('closed_at', '<', now())
            ->update([
                'status' => TicketStatuses::OVERDUE->value,
            ]);

        $orders = Order::query()
            ->where('status', OrderStatus::ACTIVE)
            ->with(
                'tickets',
                fn ($q) => $q->whereIn(
                    'status',
                    [
                        TicketStatuses::ACTIVE,
                        TicketStatuses::RETURN_REQUEST,
                    ]
                )
            )
            ->get();

        foreach ($orders as $order) {
            if ($order->tickets->count() < 1) {
                $order->update([
                    'status' => OrderStatus::INACTIVE->value,
                ]);
            }
        }
    }
}
