<?php

/** @noinspection TraitsPropertiesConflictsInspection */
declare(strict_types=1);

namespace App\Console\Commands;

use Illuminate\Console\Command as Base;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Support\Str;
use Illuminate\Translation\Translator;
use Support\InteractsWithIO;

abstract class Command extends Base
{
    use InteractsWithIO;

    public function __construct(
        private readonly Translator $translator,
        private readonly Repository $configRepository,
    ) {
        if ($this->name === '' || $this->name === '0') {
            $this->name = $this->guessCommandName();
        }

        if (!$this->description) {
            $this->description = (string)$this
                ->translator
                ->get(sprintf('artisan.commands.%s.description', $this->name));
        }

        parent::__construct();
    }

    protected function guessCommandName(): string
    {
        $parts = explode('\\', Str::after(static::class, __NAMESPACE__));

        $parts = array_values(
            array_filter(
                $parts,
            ),
        );

        $parts = array_map(
            static fn ($part): string => str_replace('_', '-', Str::snake($part)),
            $parts,
        );

        $appName = Str::kebab((string)$this->configRepository->get('app.name'));

        if ($parts[0] !== $appName) {
            array_unshift($parts, $appName);
        }

        return implode(':', $parts);
    }
}
