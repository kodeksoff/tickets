<?php

declare(strict_types=1);

namespace Documentation\Requests;

use Attribute;
use Documentation\Responses\PaginationJsonResponse;
use OpenApi\Attributes\Get as GetRequest;
use Documentation\Parameters\UrlQuery\CurrentPageUrlQuery;
use Documentation\Parameters\UrlQuery\CursorUrlQuery;
use Documentation\Parameters\UrlQuery\PaginationTypeUrlQuery;
use Documentation\Parameters\UrlQuery\PerPageUrlQuery;

#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_METHOD | Attribute::IS_REPEATABLE)]
class PaginationRequest extends GetRequest
{
    public function __construct(
        string $path,
        string $summary,
        array $tags,
        array $parameters = [],
        array $responses = [],
        array|object $items = null,
    ) {
        $parameters = array_merge(
            $parameters,
            [
                new PaginationTypeUrlQuery(),
                new PerPageUrlQuery(),
                new CursorUrlQuery(),
                new CurrentPageUrlQuery(),
            ],
        );

        $responses = array_merge(
            $responses,
            [
                new PaginationJsonResponse(items: $items),
            ],
        );

        parent::__construct(
            path: $path,
            summary: $summary,
            tags: $tags,
            parameters: $parameters,
            responses: $responses,
        );
    }
}
