<?php

declare(strict_types=1);

namespace Documentation\Parameters\Route\Service;

use Documentation\Enums\ParameterIn;
use OpenApi\Attributes\Parameter;

class ServiceCategoryParameter extends Parameter
{
    public function __construct()
    {
        parent::__construct(
            name: 'category_ids[]',
            description: 'ID категории',
            in: ParameterIn::QUERY(),
            required: false,
            example: 1,
        );
    }
}
