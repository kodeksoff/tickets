<?php

declare(strict_types=1);

namespace Documentation\Parameters\Route\Service;

use Documentation\Enums\ParameterIn;
use OpenApi\Attributes\Parameter;

class ServiceOrderByParameter extends Parameter
{
    public function __construct()
    {
        parent::__construct(
            name: 'order_by',
            description: 'Определение критерия сортировки',
            in: ParameterIn::QUERY(),
            required: false,
            example: 'title',
        );
    }
}
