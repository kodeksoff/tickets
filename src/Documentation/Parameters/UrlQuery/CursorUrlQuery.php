<?php

declare(strict_types=1);

namespace Documentation\Parameters\UrlQuery;

use Documentation\Enums\ValueType;
use OpenApi\Attributes\ExternalDocumentation;
use OpenApi\Attributes\Schema;

class CursorUrlQuery extends UrlQuery
{
    public function __construct()
    {
        parent::__construct(
            name: 'cursor',
            description: 'Указатель на следующую/предыдущую страницу. Используется при курсорной пагинации',
            required: false,
            schema: new Schema(
                type: ValueType::STRING(),
                externalDocs: new ExternalDocumentation(
                    url: 'https://laravel.com/docs/9.x/pagination#cursor-pagination'
                ),
                example: 'eyJpZCI6MTUsIl9wb2ludHNUb05leHRJdGVtcyI6dHJ1ZX0'
            ),
        );
    }
}
