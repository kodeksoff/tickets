<?php

declare(strict_types=1);

namespace Documentation\Parameters\UrlQuery;

use Documentation\Enums\ValueType;
use OpenApi\Attributes\Schema;

class CurrentPageUrlQuery extends UrlQuery
{
    public function __construct()
    {
        parent::__construct(
            name: 'page',
            description: 'Номер страницы',
            required: false,
            schema: new Schema(
                type: ValueType::INTEGER(),
                example: 1
            )
        );
    }
}
