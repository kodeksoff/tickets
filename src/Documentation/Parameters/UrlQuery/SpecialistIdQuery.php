<?php

declare(strict_types=1);

namespace Documentation\Parameters\UrlQuery;

use Documentation\Enums\ValueType;
use OpenApi\Attributes\Schema;

class SpecialistIdQuery extends UrlQuery
{
    public function __construct()
    {
        parent::__construct(
            name: 'specialist_id',
            description: 'ID специалиста',
            required: false,
            schema: new Schema(
                type: ValueType::INTEGER(),
                example: 1
            ),
        );
    }
}
