<?php

declare(strict_types=1);

namespace Documentation\Parameters\UrlQuery;

use Documentation\Enums\ValueType;
use OpenApi\Attributes\Schema;

class PerPageUrlQuery extends UrlQuery
{
    public function __construct()
    {
        parent::__construct(
            name: 'per_page',
            description: 'Количество моделей на страницу',
            required: false,
            schema: new Schema(
                type: ValueType::INTEGER(),
                minimum: 1,
                example: 10,
            ),
        );
    }
}
