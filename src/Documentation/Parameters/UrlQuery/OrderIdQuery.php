<?php

declare(strict_types=1);

namespace Documentation\Parameters\UrlQuery;

use Documentation\Enums\ValueType;
use OpenApi\Attributes\Schema;

class OrderIdQuery extends UrlQuery
{
    public function __construct()
    {
        parent::__construct(
            name: 'order_id',
            description: 'ID заказа',
            required: false,
            schema: new Schema(
                type: ValueType::INTEGER(),
                example: 1
            ),
        );
    }
}
