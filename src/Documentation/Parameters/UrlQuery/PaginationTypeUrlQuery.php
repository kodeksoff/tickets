<?php

declare(strict_types=1);

namespace Documentation\Parameters\UrlQuery;

use Documentation\Enums\ValueType;
use OpenApi\Attributes\Schema;

class PaginationTypeUrlQuery extends UrlQuery
{
    public function __construct()
    {
        parent::__construct(
            name: 'pagination_type',
            description: 'Тип пагинации (курсорная, постраничная)',
            required: false,
            schema: new Schema(
                type: ValueType::STRING(),
                default: 'length_aware',
                enum: [
                    'cursor',
                    'length_aware',
                ],
            ),
        );
    }
}
