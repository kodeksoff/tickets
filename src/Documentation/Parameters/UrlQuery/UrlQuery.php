<?php

declare(strict_types=1);

namespace Documentation\Parameters\UrlQuery;

use Documentation\Enums\ParameterIn;
use OpenApi\Attributes\Parameter;
use OpenApi\Attributes\Schema;

class UrlQuery extends Parameter
{
    /**
     * @param  string|null  $name
     * @param  string|null  $description
     * @param  bool|null  $required
     * @param  Schema|null  $schema
     */
    public function __construct(
        ?string $name = null,
        ?string $description = null,
        ?bool $required = null,
        ?Schema $schema = null,
    ) {
        parent::__construct(
            name: $name,
            description: $description,
            in: ParameterIn::QUERY(),
            required: $required,
            schema: $schema,
        );
    }
}
