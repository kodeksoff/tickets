<?php

declare(strict_types=1);

namespace Documentation\Responses;

use OpenApi\Attributes\Items;
use OpenApi\Attributes\JsonContent;
use OpenApi\Attributes\Property;
use OpenApi\Attributes\Response;
use Symfony\Component\HttpFoundation\Response as SymphonyResponse;

class ValidationJsonResponse extends Response
{
    public function __construct(string $description = 'Переданные поля невалидны')
    {
        parent::__construct(
            response: SymphonyResponse::HTTP_UNPROCESSABLE_ENTITY,
            description: $description,
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'message',
                        type: 'string',
                    ),
                    new Property(
                        property: 'payload',
                        properties: [
                            new Property(
                                property: 'errors',
                                type: 'object',
                            ),
                            new Property(
                                property: 'messages',
                                properties: [
                                    new Property(
                                        property: 'field_name',
                                        type: 'array',
                                        items: new Items(
                                            type: 'string',
                                            example: 'The phone number must be valid',
                                        ),
                                    ),
                                ],
                                type: 'object',
                            ),
                        ],
                        type: 'object',
                    ),
                ],
            ),
        );
    }
}
