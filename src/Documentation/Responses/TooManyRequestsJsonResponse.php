<?php

declare(strict_types=1);

namespace Documentation\Responses;

use OpenApi\Attributes\JsonContent;
use OpenApi\Attributes\Property;
use OpenApi\Attributes\Response;
use Symfony\Component\HttpFoundation\Response as SymphonyResponse;

class TooManyRequestsJsonResponse extends Response
{
    /** @param array|Property[] $payload */
    public function __construct(int $attemptsPerMinute = 10, array $payload = [])
    {
        parent::__construct(
            response: SymphonyResponse::HTTP_TOO_MANY_REQUESTS,
            description: 'Запросить новый код можно через 59 сек.',
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'message',
                        type: 'string',
                        example: 'Too many requests',
                    ),
                    new Property(
                        property: 'payload',
                        properties: $payload,
                        type: 'object',
                    ),
                ],
            ),
        );
    }
}
