<?php

declare(strict_types=1);

namespace Documentation\Responses;

use OpenApi\Attributes\JsonContent;
use OpenApi\Attributes\Property;
use OpenApi\Attributes\Response;
use Symfony\Component\HttpFoundation\Response as SymphonyResponse;

class BadRequestResponse extends Response
{
    public function __construct(string $message)
    {
        parent::__construct(
            response: SymphonyResponse::HTTP_BAD_REQUEST,
            description: $message,
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'message',
                        type: 'string',
                        example: $message,
                    ),
                    new Property(
                        property: 'payload',
                        type: 'object',
                        example: null,
                    ),
                ],
            ),
        );
    }
}
