<?php

declare(strict_types=1);

namespace Documentation\Responses;

use OpenApi\Attributes\JsonContent;
use OpenApi\Attributes\Property;
use OpenApi\Attributes\Response;
use Symfony\Component\HttpFoundation\Response as SymphonyResponse;

class AccessDeniedJsonResponse extends Response
{
    public function __construct(string $description = '', string $message = 'Доступ запрещен')
    {
        parent::__construct(
            response: SymphonyResponse::HTTP_FORBIDDEN,
            description: $description,
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'message',
                        type: 'string',
                        example: $message,
                    ),
                    new Property(
                        property: 'payload',
                        type: 'object',
                        example: null,
                    ),
                ],
            ),
        );
    }
}
