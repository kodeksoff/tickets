<?php

declare(strict_types=1);

namespace Documentation\Responses;

use Documentation\Enums\ValueType;
use OpenApi\Attributes\Items;
use OpenApi\Attributes\Property;
use stdClass;

class PaginationJsonResponse extends SuccessJsonResponse
{
    public function __construct(string $description = '', array|object $items = null)
    {
        parent::__construct(
            $description,
            [
                new Property(
                    property: 'current_page',
                    description: 'Текущая страница (только с length_aware)',
                    type: ValueType::INTEGER(),
                    example: 1,
                    nullable: false,
                ),
                new Property(
                    property: 'data',
                    type: ValueType::ARRAY(),
                    items: new Items(
                        type: ValueType::COLLECTION(),
                        example: $items ?? new stdClass(),
                    )
                ),
                new Property(
                    property: 'next_page_url',
                    description: 'Указатель на следующую страницу',
                    type: ValueType::STRING(),
                    example: 'http://0.0.0.0:800/api/v1/services?page=2',
                    nullable: true,
                ),
                new Property(
                    property: 'last_page_url',
                    description: 'Ссылка на последнюю страницу',
                    type: ValueType::STRING(),
                    example: 'http://0.0.0.0:800/api/v1/services?page=10',
                    nullable: true,
                ),
                new Property(
                    property: 'prev_page_url',
                    description: 'Указатель на предыдущую страницу',
                    type: ValueType::STRING(),
                    example: 'http://0.0.0.0:800/api/v1/services?page=1',
                    nullable: true,
                ),
                new Property(
                    property: 'path',
                    description: 'Коревая ссылка',
                    type: ValueType::STRING(),
                    example: 'http://0.0.0.0:800/api/v1/services',
                    nullable: false,
                ),
                new Property(
                    property: 'from',
                    description: 'Модели от',
                    type: ValueType::INTEGER(),
                    example: 1,
                    nullable: true,
                ),
                new Property(
                    property: 'to',
                    description: 'Общее кол-во моделей',
                    type: ValueType::INTEGER(),
                    example: 100,
                    nullable: false,
                ),
                new Property(
                    property: 'last_page',
                    description: 'Последняя страница',
                    type: ValueType::INTEGER(),
                    example: 5,
                    nullable: true,
                ),
                new Property(
                    property: 'per_page',
                    description: 'Количество моделей на страницу',
                    type: ValueType::INTEGER(),
                    example: 15,
                ),
                new Property(
                    property: 'total',
                    description: 'Общее количество моделей',
                    type: ValueType::INTEGER(),
                    example: 100,
                    nullable: false,
                ),
            ],
        );
    }
}
