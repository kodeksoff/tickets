<?php

declare(strict_types=1);

namespace Documentation\Responses;

use OpenApi\Attributes\JsonContent;
use OpenApi\Attributes\Property;
use OpenApi\Attributes\Response;
use Symfony\Component\HttpFoundation\Response as SymphonyResponse;

class ModelNotFoundResponse extends Response
{
    public function __construct(string $model, string $description = 'Модель не найдена')
    {
        parent::__construct(
            response: SymphonyResponse::HTTP_NOT_FOUND,
            description: $description,
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'message',
                        type: 'string',
                        example: "No query results for model [{$model}]",
                    ),
                    new Property(
                        property: 'payload',
                        type: 'object',
                        example: null,
                    ),
                ],
            ),
        );
    }
}
