<?php

declare(strict_types=1);

namespace Documentation\Responses;

use OpenApi\Attributes\JsonContent;
use OpenApi\Attributes\Property;
use OpenApi\Attributes\Response;
use Symfony\Component\HttpFoundation\Response as SymphonyResponse;

class UnauthorizedJsonResponse extends Response
{
    public function __construct()
    {
        parent::__construct(
            response: SymphonyResponse::HTTP_UNAUTHORIZED,
            description: 'Пользователь не авторизован',
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'message',
                        type: 'string',
                        example: 'Unauthenticated',
                    ),
                    new Property(
                        property: 'payload',
                        type: 'object',
                        example: null,
                    ),
                ],
            ),
        );
    }
}
