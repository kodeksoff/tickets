<?php

declare(strict_types=1);

namespace Documentation\Responses;

use Documentation\Enums\ValueType;
use OpenApi\Attributes\Items;
use OpenApi\Attributes\JsonContent;
use OpenApi\Attributes\Property;
use OpenApi\Attributes\Response;
use Symfony\Component\HttpFoundation\Response as SymphonyResponse;

use function is_array;

class SuccessJsonResponse extends Response
{
    /** @param mixed[]|\OpenApi\Attributes\Property[]|string $payload */
    public function __construct(string $description = '', Items|array|string $payload = null)
    {
        if (is_array($payload) || $payload === null) {
            $payloadProperty = new Property(
                property: 'payload',
                properties: $payload,
                type: ValueType::OBJECT(),
            );
        } elseif ($payload instanceof Items) {
            $payloadProperty = new Property(
                property: 'payload',
                type: ValueType::ARRAY(),
                items: $payload,
            );
        } else {
            $payloadProperty = new Property(
                property: 'payload',
                type: $payload,
            );
        }

        parent::__construct(
            response: SymphonyResponse::HTTP_OK,
            description: $description,
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'description',
                        type: 'string',
                        example: 'OK',
                    ),
                    $payloadProperty,
                ],
            ),
        );
    }
}
