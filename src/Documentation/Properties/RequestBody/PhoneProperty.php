<?php

declare(strict_types=1);

namespace Documentation\Properties\RequestBody;

use Documentation\Enums\ValueType;
use OpenApi\Attributes\Property;

class PhoneProperty extends Property
{
    public function __construct(bool $nullable = false)
    {
        parent::__construct(
            property: 'phone',
            type: ValueType::STRING(),
            example: '+79111111111',
            nullable: $nullable,
        );
    }
}
