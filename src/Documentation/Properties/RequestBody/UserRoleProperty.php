<?php

declare(strict_types=1);

namespace Documentation\Properties\RequestBody;

use Documentation\Enums\ValueType;
use OpenApi\Attributes\Property;

class UserRoleProperty extends Property
{
    public function __construct()
    {
        parent::__construct(
            property: 'user_rule',
            type: ValueType::STRING(),
            enum: [
                'customer',
                'specialist',
            ],
            example: 'specialist',
        );
    }
}
