<?php

declare(strict_types=1);

namespace Documentation\Enums;

use ArchTech\Enums\InvokableCases;

/**
 * @method static string PATH()
 * @method static string QUERY()
 */
enum ParameterIn: string
{
    use InvokableCases;

    case PATH = 'path';
    case QUERY = 'query';
}
