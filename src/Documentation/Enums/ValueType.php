<?php

declare(strict_types=1);

namespace Documentation\Enums;

use ArchTech\Enums\InvokableCases;

/**
 * @method static string STRING()
 * @method static string INTEGER()
 * @method static string ARRAY()
 * @method static string OBJECT()
 * @method static string COLLECTION()
 */
enum ValueType: string
{
    use InvokableCases;

    case STRING = 'string';
    case INTEGER = 'integer';
    case ARRAY = 'array';
    case OBJECT = 'object';
    case COLLECTION = 'collection';
}
