<?php

declare(strict_types=1);

namespace Documentation\Schemas;

use OpenApi\Attributes\Property;
use OpenApi\Attributes\Schema;

#[Schema]
abstract class BookingTypeList
{
    #[Property(example: 2, nullable: false)]
    public int $id;

    #[Property(example: '3285.96', nullable: false)]
    public string $full_price;

    #[Property(example: '3785.96', nullable: true)]
    public string $full_price_with_discount;

    #[Property(example: '37.96', nullable: true)]
    public string $discount_price;

    #[Property(example: 'name', nullable: false)]
    public string $title;

    #[Property(example: true, nullable: true)]
    public bool $is_default;
}
