<?php

declare(strict_types=1);

namespace Documentation\Schemas;

use OpenApi\Attributes\Items;
use OpenApi\Attributes\Property;
use OpenApi\Attributes\Schema;

#[Schema]
abstract class AskedQuestionList
{
    #[Property(
        items: new Items(AskedQuestionItem::class),
        nullable: false,
    ),
    ]
    public array $questions;
}
