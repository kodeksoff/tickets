<?php

declare(strict_types=1);

namespace Documentation\Schemas;

use OpenApi\Attributes\Items;
use OpenApi\Attributes\Property;
use OpenApi\Attributes\Schema;

#[Schema]
abstract class TouristsOrderShow
{
    #[Property(example: 2, nullable: false)]
    public int $id;

    #[Property(example: 'active', nullable: true)]
    public string $status;

    #[Property(example: 1234, nullable: false)]
    public int $full_price_amount;

    #[Property(example: 345, nullable: false)]
    public int $surcharge_price_amount;

    #[Property(example: 'Lorem Ipsum', nullable: true)]
    public string $title;

    #[Property(example: 3, nullable: true)]
    public int $tickets_count;

    #[Property(example: '2023-07-24T14:07:52.000000Z', nullable: true)]
    public string $closed_at;

    #[Property(example: [
        'id' => 12,
        'full_name' => 'Ivanov',
        'phone' => +79374672,
        'guest_house' => [
            'address' => 'United street 34',
            'title' => 'Mir Hotel',
        ],
    ], nullable: false)]
    public object $user;

    #[Property(
        items: new Items(TicketItem::class),
        nullable: false,
    )]
    public array $tickets;
}
