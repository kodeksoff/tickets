<?php

declare(strict_types=1);

namespace Documentation\Schemas;

use OpenApi\Attributes\Property;
use OpenApi\Attributes\Schema;

#[Schema]
abstract class TicketItem
{
    #[Property(example: 12, nullable: false)]
    public string $id;
    #[Property(example: 'active', nullable: false)]
    public string $status;
    #[Property(example: 123, nullable: false)]
    public int $full_price_amount;
    #[Property(example: 4537, nullable: false)]
    public int $surcharge_price_amount;
    #[Property(example: 'Hello world', nullable: false)]
    public string $title;
    #[Property(example: 1236, nullable: false)]
    public int $code;
    #[Property(example: 8643, nullable: false)]
    public int $number;
    #[Property(example: '2023-07-27T18:39:49.000000Z', nullable: false)]
    public string $closed_at;
    #[Property(example: 'http://0.0.0.0:800/storage/media/tickets/2/202/qrcode.svg', nullable: false)]
    public string $qr;
}
