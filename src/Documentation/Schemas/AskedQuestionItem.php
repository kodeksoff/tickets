<?php

declare(strict_types=1);

namespace Documentation\Schemas;

use OpenApi\Attributes\Property;
use OpenApi\Attributes\Schema;

#[Schema]
abstract class AskedQuestionItem
{
    #[Property(example: 'What is your name?', nullable: true)]
    public string $question;
    #[Property(example: 'Elon Reeve Musk', nullable: false)]
    public string $answer;
}
