<?php

declare(strict_types=1);

namespace Documentation\Schemas;

use OpenApi\Attributes\Property;
use OpenApi\Attributes\Schema;

#[Schema]
abstract class GuestHouseItem
{
    #[Property(example: '33', nullable: false)]
    public int $id;

    #[Property(example: 'ООО Компания ITАлмаз', nullable: false)]
    public string $title;

    #[Property(example: '949531, Рязанская область, город Зарайск, въезд Космонавтов, 68', nullable: false)]
    public string $address;

    #[Property(example: 'въезд Балканская, 46', nullable: false)]
    public string $short_address;

    #[Property(example: '67186', nullable: false)]
    public int $code;
}
