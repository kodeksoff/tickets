<?php

declare(strict_types=1);

namespace Documentation\Schemas;

use OpenApi\Attributes\Property;
use OpenApi\Attributes\Schema;

#[Schema]
abstract class TouristsOrderList
{
    #[Property(example: 2, nullable: false)]
    public int $id;

    #[Property(example: 'active', nullable: true)]
    public string $status;

    #[Property(example: 1234, nullable: false)]
    public int $full_price;

    #[Property(example: 345, nullable: false)]
    public int $price;

    #[Property(example: 'Lorem Ipsum', nullable: true)]
    public string $title;

    #[Property(example: 3, nullable: true)]
    public int $tickets_count;
}
