<?php

declare(strict_types=1);

namespace Documentation\Schemas;

use OpenApi\Attributes\Items;
use OpenApi\Attributes\Property;
use OpenApi\Attributes\Schema;

#[Schema]
abstract class ServicePreview
{
    #[Property(example: 2, nullable: false)]
    public int $id;

    #[Property(example: 'Ab et sed voluptates et reprehenderit et.', nullable: false)]
    public string $title;

    #[Property(example: 'http://0.0.0.0:800/storage/media/services/6/conversions/CCCCCC-preview.webp', nullable: true)]
    public string $image;

    #[Property(example: '3285.96', nullable: false)]
    public string $price;

    #[Property(example: '3785.96', nullable: true)]
    public string $price_with_discount;

    #[Property(example: '500', nullable: true)]
    public string $discount;

    #[Property(items: new Items(ServiceCategoryList::class), nullable: false)]
    public array $categories;
}
