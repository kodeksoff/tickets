<?php

declare(strict_types=1);

namespace Documentation\Schemas;

use OpenApi\Attributes\Property;
use OpenApi\Attributes\Schema;

#[Schema]
abstract class PartnerServiceProfileItem
{
    #[Property(example: '12', nullable: false)]
    public int $id;

    #[Property(example: 'Ivanov Ivan', nullable: false)]
    public string $full_name;

    #[Property(example: '+79049798108', nullable: false)]
    public string $phone;

    #[Property(example: 'test12@gmail.com', nullable: false)]
    public string $email;

    #[Property(example: '35298979742', nullable: false)]
    public string $inn;
}
