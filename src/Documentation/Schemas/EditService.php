<?php

declare(strict_types=1);

namespace Documentation\Schemas;

use OpenApi\Attributes\Items;
use OpenApi\Attributes\Property;
use OpenApi\Attributes\Schema;

#[Schema]
abstract class EditService
{
    #[Property(example: 2, nullable: false)]
    public int $id;

    #[Property(example: 'Ab et sed voluptates et reprehenderit et.', nullable: false)]
    public string $title;

    #[Property(example: 'http://0.0.0.0:800/storage/media/services/6/conversions/CCCCCC-preview.webp', nullable: true)]
    public string $main_image;

    #[Property(items: new Items(
    ), example: ['http://0.0.0.0:800/storage/media/services/6/conversions/CCCCCC-preview.webp'], nullable: false)]
    public array $additional_images;

    #[Property(example: 2, nullable: false)]
    public int $service_season_from;

    #[Property(example: 7, nullable: true)]
    public int $service_season_to;

    #[Property(example: false, nullable: true)]
    public bool $service_all_year;

    #[Property(example: 'Информация об услуге', nullable: true)]
    public string $about_included;

    #[Property(example: 'Просмотр купленного купона туристом', nullable: true)]
    public string $description;

    #[Property(example: 'Информация о купоне и билетах', nullable: true)]
    public string $about_additional;

    #[Property(example: '2023-07-24T14:07:52.000000Z', nullable: true)]
    public string $created_at;

    #[Property(example: '2023-07-24T14:07:52.000000Z', nullable: true)]
    public string $updated_at;

    #[Property(items: new Items(ServiceCategoryList::class), nullable: false)]
    public array $categories;

    #[Property(items: new Items(BookingTypeList::class), nullable: false)]
    public array $booking_types;
}
