<?php

declare(strict_types=1);

namespace Documentation\Schemas;

use OpenApi\Attributes\Property;
use OpenApi\Attributes\Schema;

#[Schema]
abstract class ServiceCategoryList
{
    #[Property(example: 2, nullable: false)]
    public int $id;

    #[Property(example: 1, nullable: true)]
    public int $parent_id;

    #[Property(example: 'category name', nullable: false)]
    public string $title;

    #[Property(example: 'Lorem Ipsum', nullable: true)]
    public string $description;
}
