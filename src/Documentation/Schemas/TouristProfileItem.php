<?php

declare(strict_types=1);

namespace Documentation\Schemas;

use OpenApi\Attributes\Property;
use OpenApi\Attributes\Schema;

#[Schema]
abstract class TouristProfileItem
{
    #[Property(example: '12', nullable: false)]
    public int $id;

    #[Property(example: 'Ivanov Ivan', nullable: true)]
    public string $full_name;

    #[Property(example: '23464', nullable: true)]
    public int $guest_house_code;

    #[Property(example: 'spa_tourist', nullable: false)]
    public int $role;

}
