<?php

declare(strict_types=1);

namespace Documentation\Api;

use Documentation\Responses\SuccessJsonResponse;
use Documentation\Schemas\PartnerHouseProfileItem;
use OpenApi\Attributes\Get;

abstract class PartnerHouseProfile
{
    private const TAGS = ['Профиль гостевого дома'];

    #[Get(
        path: '/api/v1/cabinet/partner-house/profile',
        summary: 'Профиль гостевого дома',
        tags: self::TAGS,
        responses: [
            new SuccessJsonResponse(
                'OK',
                PartnerHouseProfileItem::class
            ),
        ],
    )]
    public function partnerHouseProfile(): void
    {
    }
}
