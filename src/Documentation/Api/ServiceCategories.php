<?php

declare(strict_types=1);

namespace Documentation\Api;

use Documentation\Responses\SuccessJsonResponse;
use Documentation\Schemas\ServiceCategoryList;
use OpenApi\Attributes\Get;
use OpenApi\Attributes\Items;

abstract class ServiceCategories
{
    private const TAGS = ['Категории сервисов'];

    #[Get(
        path: '/api/v1/services/categories',
        summary: 'Список категорий',
        tags: self::TAGS,
        responses: [
            new SuccessJsonResponse(
                'OK',
                new Items(ServiceCategoryList::class)
            ),
        ],
    )]
    public function serviceCategories(): void
    {
    }
}
