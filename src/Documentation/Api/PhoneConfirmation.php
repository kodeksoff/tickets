<?php

declare(strict_types=1);

namespace Documentation\Api;

use Documentation\Properties\RequestBody\PhoneProperty;
use Documentation\Responses\BadRequestResponse;
use Documentation\Responses\ModelNotFoundResponse;
use Documentation\Responses\SuccessJsonResponse;
use Documentation\Responses\TooManyRequestsJsonResponse;
use Documentation\Responses\ValidationJsonResponse;
use Domain\Confirmations\Models\Confirmation;
use OpenApi\Attributes\JsonContent;
use OpenApi\Attributes\Post;
use OpenApi\Attributes\Property;
use OpenApi\Attributes\RequestBody;

abstract class PhoneConfirmation
{
    private const TAGS = ['Подтверждение номера телефона'];

    #[Post(
        path: '/api/v1/confirmations/phones/send',
        summary: 'Запрос на отправку проверочного кода',
        requestBody: new RequestBody(
            required: true,
            content: new JsonContent(
                properties: [
                    new PhoneProperty(),
                ],
            ),
        ),
        tags: self::TAGS,
        responses: [
            new SuccessJsonResponse('OK', [
                new Property(
                    property: 'deliver_to',
                    type: 'string',
                    example: '+79061241344',
                    nullable: false,
                ),
                new Property(
                    property: 'id',
                    type: 'string',
                    example: '993a6a2f-b85e-442f-995e-a3daf2fd6b14',
                    nullable: false,
                ),
                new Property(
                    property: 'resend_delay_in_seconds',
                    type: 'integer',
                    example: 60,
                    nullable: false,
                ),
            ]),
            new ValidationJsonResponse('Number does not match the provided country.'),
            new TooManyRequestsJsonResponse(),
        ],
    )]
    public function phoneSend(): void
    {
    }

    #[Post(
        path: '/api/v1/confirmations/phones/approve',
        summary: 'Подтверждение номера телефона',
        requestBody: new RequestBody(
            required: true,
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'confirmation_id',
                        type: 'string',
                        example: '9939cfea-64f7-49f2-892b-3faf4b507baf',
                        nullable: false,
                    ),
                    new Property(
                        property: 'token',
                        type: 'string',
                        example: '12345',
                        nullable: false,
                    ),
                ],
            ),
        ),
        tags: self::TAGS,
        responses: [
            new SuccessJsonResponse('OK', [
                new Property(
                    property: 'confirmed',
                    type: 'boolean',
                    example: true,
                    nullable: false,
                ),
                new Property(
                    property: 'id',
                    type: 'string',
                    example: '993a6a2f-b85e-442f-995e-a3daf2fd6b14',
                    nullable: false,
                ),
            ]),
            new BadRequestResponse('Проверочный код уже использован'),
            new ModelNotFoundResponse(Confirmation::class),
        ],
    )]
    public function phoneApprove(): void
    {
    }
}
