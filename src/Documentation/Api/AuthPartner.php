<?php

declare(strict_types=1);

namespace Documentation\Api;

use Documentation\Properties\RequestBody\PhoneProperty;
use Documentation\Responses\SuccessJsonResponse;
use Documentation\Responses\TooManyRequestsJsonResponse;
use Documentation\Responses\ValidationJsonResponse;
use OpenApi\Attributes\JsonContent;
use OpenApi\Attributes\Post;
use OpenApi\Attributes\Property;
use OpenApi\Attributes\RequestBody;

abstract class AuthPartner
{
    private const TAGS = ['Авторизация/регистрация партнера'];

    #[Post(
        path:'/api/v1/auth/register-validation/partner-service',
        summary:'Первичная валидация партнерского сервиса',
        requestBody:new RequestBody(
            required:true,
            content:new JsonContent(
                properties:[
                    new PhoneProperty(),
                    new Property(
                        property:'full_name',
                        type:'string',
                        example:'Вася Пупкин Пупович',
                        nullable:false,
                    ),
                    new Property(
                        property:'email',
                        type:'string',
                        example:'example@gmail.com',
                        nullable:false,
                    ),
                    new Property(
                        property:'inn',
                        type:'string',
                        example:'35298979743',
                        nullable:false,
                    ),
                    new Property(
                        property:'agreement',
                        type:'boolean',
                        example:true,
                        nullable:false,
                    ),
                ],
            ),
        ),
        tags:self::TAGS,
        responses:[
            new SuccessJsonResponse('OK', [
                new PhoneProperty(),
                new Property(
                    property:'full_name',
                    type:'string',
                    example:'Вася Пупкин Пупович',
                    nullable:false,
                ),
                new Property(
                    property:'email',
                    type:'string',
                    example:'example@gmail.com',
                    nullable:false,
                ),
                new Property(
                    property:'inn',
                    type:'string',
                    example:'35298979743',
                    nullable:false,
                ),
                new Property(
                    property:'agreement',
                    type:'boolean',
                    example:true,
                    nullable:false,
                ),
            ]),
        ],
    )]
    public function registerValidationPartnerService(): void
    {
    }

    #[Post(
        path:'/api/v1/auth/register-validation/partner-house',
        summary:'Первичная валидация партнерского дома',
        requestBody:new RequestBody(
            required:true,
            content:new JsonContent(
                properties:[
                    new PhoneProperty(),
                    new Property(
                        property:'full_name',
                        type:'string',
                        example:'Вася Пупкин Пупович',
                        nullable:false,
                    ),
                    new Property(
                        property:'email',
                        type:'string',
                        example:'example@gmail.com',
                        nullable:false,
                    ),
                    new Property(
                        property:'inn',
                        type:'string',
                        example:'35298979743',
                        nullable:false,
                    ),
                    new Property(
                        property:'agreement',
                        type:'boolean',
                        example:true,
                        nullable:false,
                    ),
                ],
            ),
        ),
        tags:self::TAGS,
        responses:[
            new SuccessJsonResponse('OK', [
                new PhoneProperty(),
                new Property(
                    property:'full_name',
                    type:'string',
                    example:'Вася Пупкин Пупович',
                    nullable:false,
                ),
                new Property(
                    property:'email',
                    type:'string',
                    example:'example@gmail.com',
                    nullable:false,
                ),
                new Property(
                    property:'inn',
                    type:'string',
                    example:'35298979743',
                    nullable:false,
                ),
                new Property(
                    property:'agreement',
                    type:'boolean',
                    example:true,
                    nullable:false,
                ),
            ]),
        ],
    )]
    public function registerValidationPartnerHouse(): void
    {
    }

    #[Post(
        path:'/api/v1/auth/register/partner-service',
        summary:'Регистрация партнерского сервиса',
        requestBody:new RequestBody(
            required:true,
            content:new JsonContent(
                properties:[
                    new PhoneProperty(),
                    new Property(
                        property:'full_name',
                        type:'string',
                        example:'Вася Пупкин Пупович',
                        nullable:false,
                    ),
                    new Property(
                        property:'email',
                        type:'string',
                        example:'example@gmail.com',
                        nullable:false,
                    ),
                    new Property(
                        property:'inn',
                        type:'string',
                        example:'35298979743',
                        nullable:false,
                    ),
                    new Property(
                        property:'agreement',
                        type:'boolean',
                        example:true,
                        nullable:false,
                    ),
                    new Property(
                        property:'password',
                        type:'string',
                        example:'password',
                        nullable:false,
                    ),
                    new Property(
                        property:'password_confirmation',
                        type:'string',
                        example:'password',
                        nullable:false,
                    ),
                    new Property(
                        property:'confirmation_id',
                        type:'string',
                        example:'994ce6ae-d121-4ca8-958f-d9456a78f0db',
                        nullable:false,
                    ),
                ],
            ),
        ),
        tags:self::TAGS,
        responses:[
            new SuccessJsonResponse('OK', '2|ClroOpMuJedfynXc1NJ4XuSTwnZOi80h6BbD79t4'),
        ],
    )]
    public function registerPartnerService(): void
    {
    }

    #[Post(
        path:'/api/v1/auth/register/partner-house',
        summary:'Регистрация партнерского дома',
        requestBody:new RequestBody(
            required:true,
            content:new JsonContent(
                properties:[
                    new PhoneProperty(),
                    new Property(
                        property:'full_name',
                        type:'string',
                        example:'Вася Пупкин Пупович',
                        nullable:false,
                    ),
                    new Property(
                        property:'email',
                        type:'string',
                        example:'example@gmail.com',
                        nullable:false,
                    ),
                    new Property(
                        property:'inn',
                        type:'string',
                        example:'35298979743',
                        nullable:false,
                    ),
                    new Property(
                        property:'agreement',
                        type:'boolean',
                        example:true,
                        nullable:false,
                    ),
                    new Property(
                        property:'password',
                        type:'string',
                        example:'password',
                        nullable:false,
                    ),
                    new Property(
                        property:'password_confirmation',
                        type:'string',
                        example:'password',
                        nullable:false,
                    ),
                    new Property(
                        property:'confirmation_id',
                        type:'string',
                        example:'994ce6ae-d121-4ca8-958f-d9456a78f0db',
                        nullable:false,
                    ),
                ],
            ),
        ),
        tags:self::TAGS,
        responses:[
            new SuccessJsonResponse('OK', '2|ClroOpMuJedfynXc1NJ4XuSTwnZOi80h6BbD79t4'),
        ],
    )]
    public function registerPartnerHouse(): void
    {
    }

    #[Post(
        path:'/api/v1/auth/login/partner',
        summary:'Авторизация',
        requestBody:new RequestBody(
            required:true,
            content:new JsonContent(
                properties:[
                    new PhoneProperty(),
                    new Property(
                        property:'password',
                        type:'string',
                        example:'password',
                        nullable:false,
                    ),
                ],
            ),
        ),
        tags:self::TAGS,
        responses:[
            new SuccessJsonResponse('OK', [
                new Property(
                    property:'user-role',
                    type:'string',
                    example:'partner-service',
                    nullable:false,
                ),
                new Property(
                    property:'token',
                    type:'string',
                    example:'4|VtyLyNUCZUHqhzFl7iPNAE9bRMkzrlfZEz4I3oq4',
                    nullable:false,
                ),
            ]),
        ],
    )]
    public function authPartner(): void
    {
    }

    #[Post(
        path:'/api/v1/auth/password-recovery/partner',
        summary:'Запрос кода для восстановления пароля',
        requestBody:new RequestBody(
            required:true,
            content:new JsonContent(
                properties:[
                    new Property(
                        property:'phone',
                        type:'string',
                        example:'+79061241339',
                        nullable:false,
                    ),
                ],
            ),
        ),
        tags:self::TAGS,
        responses:[
            new SuccessJsonResponse('OK', [
                new Property(
                    property:'deliver_to',
                    type:'string',
                    example:'+79061241344',
                    nullable:false,
                ),
                new Property(
                    property:'id',
                    type:'string',
                    example:'993a6a2f-b85e-442f-995e-a3daf2fd6b14',
                    nullable:false,
                ),
                new Property(
                    property:'resend_delay_in_seconds',
                    type:'integer',
                    example:60,
                    nullable:false,
                ),
            ]),
            new ValidationJsonResponse('Number does not match the provided country.'),
            new TooManyRequestsJsonResponse(),
        ],
    )]
    public function passwordRecoveryRequest(): void
    {
    }

    #[Post(
        path:'/api/v1/auth/password-reset',
        summary:'Обновление пароля при восстановлении',
        requestBody:new RequestBody(
            required:true,
            content:new JsonContent(
                properties:[
                    new Property(
                        property:'token',
                        type:'string',
                        example:'12345',
                        nullable:false,
                    ),
                    new Property(
                        property:'confirmation_id',
                        type:'string',
                        example:'99808ee9-7e5b-4d6d-bcd6-4cbe874995af',
                        nullable:false,
                    ),
                    new Property(
                        property:'password',
                        type:'string',
                        example:'secret',
                        nullable:false,
                    ),
                    new Property(
                        property:'password_confirmation',
                        type:'string',
                        example:'secret',
                        nullable:false,
                    ),
                ],
            ),
        ),
        tags:self::TAGS,
        responses:[
            new SuccessJsonResponse('OK', [
                new Property(
                    property:'user-role',
                    type:'string',
                    example:'partner-service',
                    nullable:false,
                ),
                new Property(
                    property:'token',
                    type:'string',
                    example:'4|VtyLyNUCZUHqhzFl7iPNAE9bRMkzrlfZEz4I3oq4',
                    nullable:false,
                ),
            ]),
        ],
    )]
    public function passwordRecovery(): void
    {
    }
}
