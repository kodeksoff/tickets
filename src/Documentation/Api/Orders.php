<?php

declare(strict_types=1);

namespace Documentation\Api;

use Documentation\Responses\SuccessJsonResponse;
use OpenApi\Attributes\Items;
use OpenApi\Attributes\JsonContent;
use OpenApi\Attributes\Post;
use OpenApi\Attributes\Property;
use OpenApi\Attributes\RequestBody;

abstract class Orders
{
    private const TAGS = ['Бронирование'];

    #[Post(
        path: '/api/v1/orders/tourist',
        summary: 'Бронирование услуги',
        requestBody: new RequestBody(
            required: true,
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'service_id',
                        type: 'int',
                        example: 3,
                        nullable: false,
                    ),
                    new Property(
                        property: 'return_url',
                        type: 'string',
                        example: 'https://www.google.com/',
                        nullable: false,
                    ),
                    new Property(
                        property: 'full_price_amount',
                        type: 'numeric',
                        example: 344.56,
                        nullable: false,
                    ),
                    new Property(
                        property: 'booking_price_amount',
                        type: 'numeric',
                        example: 3448.56,
                        nullable: false,
                    ),
                    new Property(
                        property: 'booking_tickets',
                        type: 'array',
                        items: new Items(),
                        example: [
                            [
                                'service_booking_id' => 13,
                                'full_price_amount' => 22332,
                                'discount_price_amount' => 4537,
                                'full_price_with_discount_amount' => 4563,
                                'service_price_for_booking_amount' => 4583,
                                'count' => 2,
                            ],
                        ],
                        nullable: false,
                    ),
                ],
            ),
        ),
        tags: self::TAGS,
        responses: [
            new SuccessJsonResponse(
                'OK',
                [
                    new Property(
                        property: 'enforce',
                        type: 'bool',
                        example: false,
                        nullable: false,
                    ),
                    new Property(
                        property: 'confirmation_url',
                        type: 'string',
                        example: 'https://yoomoney.ru/checkout/payments/v2/contract?orderId=2c404606-000f-5000-8000-1622f54d0472',
                        nullable: false,
                    ),
                    new Property(
                        property: 'type',
                        type: 'string',
                        example: 'redirect',
                        nullable: false,
                    ),
                ]
            ),
        ],
    )]
    public function storeOrder(): void
    {
    }
}
