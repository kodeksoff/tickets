<?php

declare(strict_types=1);

namespace Documentation\Api;

use Documentation\Responses\SuccessJsonResponse;
use Documentation\Schemas\TouristsOrderShow;
use Documentation\Schemas\TouristProfileItem;
use Documentation\Schemas\TouristsOrderList;
use OpenApi\Attributes\Get;
use OpenApi\Attributes\Items;
use OpenApi\Attributes\JsonContent;
use OpenApi\Attributes\Post;
use OpenApi\Attributes\Property;
use OpenApi\Attributes\RequestBody;

abstract class TouristProfile
{
    private const TAGS = ['Профиль туриста'];

    #[Post(
        path: '/api/v1/cabinet/tourist/profile/update',
        summary: 'Обновление данных профиля',
        requestBody: new RequestBody(
            required: true,
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'full_name',
                        type: 'string',
                        example: 'Костинаа Зоя Андреевна',
                        nullable: false,
                    ),
                    new Property(
                        property: 'guest_house_code',
                        type: 'int',
                        example: 12345,
                        nullable: false,
                    ),
                ],
            ),
        ),
        tags: self::TAGS,
        responses: [
            new SuccessJsonResponse('OK', []),
        ],
    )]
    public function updateProfile(): void
    {
    }

    #[Get(
        path: '/api/v1/cabinet/tourist/profile',
        summary: 'Профиль туриста',
        tags: self::TAGS,
        responses: [
            new SuccessJsonResponse(
                'OK',
                TouristProfileItem::class
            ),
        ],
    )]
    public function touristProfile(): void
    {
    }

    #[Get(
        path: '/api/v1/orders/tourist/',
        summary: 'Список купленных услуг',
        tags: self::TAGS,
        responses: [
            new SuccessJsonResponse(
                'OK',
                new Items(TouristsOrderList::class)
            ),
        ],
    )]
    public function touristOrders(): void
    {
    }

    #[Get(
        path: '/api/v1/orders/tourist/{order}',
        summary: 'Просмотр купленного купона',
        tags: self::TAGS,
        responses: [
            new SuccessJsonResponse(
                'OK',
                TouristsOrderShow::class
            ),
        ],
    )]
    public function showOrders(): void
    {
    }

    #[Post(
        path: '/api/v1/orders/tourist/refund/{order}',
        summary: 'Запрос на возврат средств',
        tags: self::TAGS,
        responses: [
            new SuccessJsonResponse('OK', []),
        ],
    )]
    public function refundOrders(): void
    {
    }
}
