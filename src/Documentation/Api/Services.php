<?php

declare(strict_types=1);

namespace Documentation\Api;

use Documentation\Parameters\Route\Service\ServiceCategoryParameter;
use Documentation\Parameters\Route\Service\ServiceOrderByParameter;
use Documentation\Parameters\Route\Service\ServiceOrderDirectionParameter;
use Documentation\Requests\PaginationRequest;
use Documentation\Responses\SuccessJsonResponse;
use OpenApi\Attributes\Items;
use OpenApi\Attributes\JsonContent;
use OpenApi\Attributes\Post;
use OpenApi\Attributes\Property;
use OpenApi\Attributes\RequestBody;

abstract class Services
{
    private const TAGS = ['Сервисы'];

    #[PaginationRequest(
        path: '/api/v1/services',
        summary: 'Список сервисов с сортировкой',
        tags: self::TAGS,
        parameters: [
            new ServiceCategoryParameter(),
            new ServiceOrderByParameter(),
            new ServiceOrderDirectionParameter(),
        ],
        items: [
            'id' => 1,
            'title' => 'Aut laudantium id aut doloribus ut veritatis.',
            'image' => 'http://0.0.0.0:800/storage/media/services/22/conversions/CCCCCC-preview.webp',
            'price' => '2772.24',
            'price_with_discount' => '2272.24',
            'discount' => '500',
            'categories' => [
                [
                    'id' => 1,
                    'parent_id' => null,
                    'title' => 'Aspernatur nemo quo.',
                ],
                [
                    'id' => 2,
                    'parent_id' => 1,
                    'title' => 'Aspernatur nemo quo.',
                ],
            ],
        ],
    )]
    public function servicesList(): void
    {
    }

    #[Post(
        path: '/api/v1/services/',
        summary: 'Создание услуги',
        tags: self::TAGS,
        responses: [
            new SuccessJsonResponse('OK', [
                new Property(
                    property: 'service_id',
                    type: 'int',
                    example: 12,
                    nullable: false,
                ),
            ]),
        ],
    )]
    public function storeServices(): void
    {
    }

    #[Post(
        path: '/api/v1/services/{service}',
        summary: 'Редактирование услуги',
        requestBody: new RequestBody(
            required: true,
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'title',
                        type: 'string',
                        example: 'Fugit quia culpa',
                        nullable: false,
                    ),
                    new Property(
                        property: 'category_ids',
                        type: 'array',
                        items: new Items(),
                        example: [12, 34],
                        nullable: false,
                    ),
                    new Property(
                        property: 'main_image',
                        type: 'image',
                        nullable: false,
                    ),
                    new Property(
                        property: 'additional_images',
                        type: 'array',
                        items: new Items(),
                        example: [
                            new Property(
                                property: 'additional_image',
                                type: 'image',
                                nullable: false,
                            ),
                        ],
                        nullable: true,
                    ),
                    new Property(
                        property: 'deleted_main_image',
                        type: 'bool',
                        example: true,
                        nullable: false,
                    ),
                    new Property(
                        property: 'deleted_additional_images',
                        type: 'array',
                        items: new Items(),
                        example: [
                            1, 3,
                        ],
                        nullable: true,
                    ),
                    new Property(
                        property: 'service_season_from',
                        type: 'integer',
                        example: 5,
                        nullable: false,
                    ),
                    new Property(
                        property: 'service_season_to',
                        type: 'integer',
                        example: 8,
                        nullable: false,
                    ),
                    new Property(
                        property: 'service_all_year',
                        type: 'boolean',
                        example: false,
                        nullable: false,
                    ),
                    new Property(
                        property: 'booking_types',
                        type: 'array',
                        items: new Items(),
                        example: [
                            [
                                'title' => 'rrrrr',
                                'full_price' => 22332,
                                'discount_price' => 4537,
                            ],
                        ],
                        nullable: false,
                    ),
                    new Property(
                        property: 'description',
                        type: 'string',
                        example: 'Quis ab consequatur eos quaerat harum.',
                        nullable: true,
                    ),
                    new Property(
                        property: 'about_included',
                        type: 'string',
                        example: 'Eligendi vero odit nemo beatae vel libero rerum.',
                        nullable: true,
                    ),
                    new Property(
                        property: 'about_additional',
                        type: 'string',
                        example: 'Amet aliquam blanditiis doloremque rerum recusandae ea.',
                        nullable: true,
                    ),
                ],
            ),
        ),
        tags: self::TAGS,
        responses: [
            new SuccessJsonResponse('OK', []),
        ],
    )]
    public function updateServices(): void
    {
    }

    #[Post(
        path: '/api/v1/services/{service}/publish',
        summary: 'Публикация услуги',
        requestBody: new RequestBody(
            required: true,
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'title',
                        type: 'string',
                        example: 'Fugit quia culpa',
                        nullable: false,
                    ),
                    new Property(
                        property: 'category_ids',
                        type: 'array',
                        items: new Items(),
                        example: [12, 34],
                        nullable: false,
                    ),
                    new Property(
                        property: 'main_image',
                        type: 'image',
                        nullable: false,
                    ),
                    new Property(
                        property: 'additional_images',
                        type: 'array',
                        items: new Items(),
                        example: [
                            new Property(
                                property: 'main_image',
                                type: 'image',
                                nullable: false,
                            ),
                        ],
                        nullable: true,
                    ),
                    new Property(
                        property: 'service_season_from',
                        type: 'integer',
                        example: 5,
                        nullable: false,
                    ),
                    new Property(
                        property: 'service_season_to',
                        type: 'integer',
                        example: 8,
                        nullable: false,
                    ),
                    new Property(
                        property: 'service_all_year',
                        type: 'boolean',
                        example: false,
                        nullable: false,
                    ),
                    new Property(
                        property: 'booking_types',
                        type: 'array',
                        items: new Items(),
                        example: [
                            [
                                'title' => 'rrrrr',
                                'full_price' => 22332,
                                'discount_price' => 4537,
                            ],
                        ],
                        nullable: false,
                    ),
                    new Property(
                        property: 'description',
                        type: 'string',
                        example: 'Quis ab consequatur eos quaerat harum.',
                        nullable: true,
                    ),
                    new Property(
                        property: 'about_included',
                        type: 'string',
                        example: 'Eligendi vero odit nemo beatae vel libero rerum.',
                        nullable: true,
                    ),
                    new Property(
                        property: 'about_additional',
                        type: 'string',
                        example: 'Amet aliquam blanditiis doloremque rerum recusandae ea.',
                        nullable: true,
                    ),
                ],
            ),
        ),
        tags: self::TAGS,
        responses: [
            new SuccessJsonResponse('OK', []),
        ],
    )]
    public function publish(): void
    {
    }
}
