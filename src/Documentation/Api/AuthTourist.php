<?php

declare(strict_types=1);

namespace Documentation\Api;

use Documentation\Properties\RequestBody\PhoneProperty;
use Documentation\Responses\SuccessJsonResponse;
use OpenApi\Attributes\JsonContent;
use OpenApi\Attributes\Post;
use OpenApi\Attributes\Property;
use OpenApi\Attributes\RequestBody;

abstract class AuthTourist
{
    private const TAGS = ['Авторизация/регистрация туриста'];

    #[Post(
        path: '/api/v1/auth/register-check/phone',
        summary: 'Проверка регистрации по номеру телефона',
        requestBody: new RequestBody(
            required: true,
            content: new JsonContent(
                properties: [
                    new PhoneProperty(),
                ],
            ),
        ),
        tags: self::TAGS,
        responses: [
            new SuccessJsonResponse('OK', [
                new Property(
                    property: 'registered',
                    type: 'boolean',
                    example: true,
                    nullable: false,
                ),
                new Property(
                    property: 'user_role',
                    type: 'string',
                    example: 'tourist',
                    nullable: false,
                ),
            ]),
        ],
    )]
    public function registerCheckTourist(): void
    {
    }

    #[Post(
        path: '/api/v1/auth/register/tourist',
        summary: 'Регистрация',
        requestBody: new RequestBody(
            required: true,
            content: new JsonContent(
                properties: [
                    new PhoneProperty(),
                    new Property(
                        property: 'full_name',
                        type: 'string',
                        example: 'Вася Пупкин Пупович',
                        nullable: false,
                    ),
                    new Property(
                        property: 'confirmation_id',
                        type: 'string',
                        example: '996c9ecb-59f0-4a11-911c-0e7dcec8e32e',
                        nullable: false,
                    ),
                    new Property(
                        property: 'agreement',
                        type: 'boolean',
                        example: true,
                        nullable: false,
                    ),
                    new Property(
                        property: 'guest_house_code',
                        type: 'integer',
                        example: 12345,
                        nullable: false,
                    ),
                ],
            ),
        ),
        tags: self::TAGS,
        responses: [
            new SuccessJsonResponse('OK', '2|ClroOpMuJedfynXc1NJ4XuSTwnZOi80h6BbD79t4'),
        ],
    )]
    public function registerTourist(): void
    {
    }

    #[Post(
        path: '/api/v1/auth/login/tourist',
        summary: 'Авторизация',
        requestBody: new RequestBody(
            required: true,
            content: new JsonContent(
                properties: [
                    new PhoneProperty(),
                    new Property(
                        property: 'confirmation_id',
                        type: 'string',
                        example: '996c9ecb-59f0-4a11-911c-0e7dcec8e32e',
                        nullable: false,
                    ),
                    new Property(
                        property: 'token',
                        type: 'string',
                        example: '12345',
                        nullable: false,
                    ),
                ],
            ),
        ),
        tags: self::TAGS,
        responses: [
            new SuccessJsonResponse('OK', '2|ClroOpMuJedfynXc1NJ4XuSTwnZOi80h6BbD79t4'),
        ],
    )]
    public function authTourist(): void
    {
    }
}
