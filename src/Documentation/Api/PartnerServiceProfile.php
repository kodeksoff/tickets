<?php

declare(strict_types=1);

namespace Documentation\Api;

use Documentation\Requests\PaginationRequest;
use Documentation\Responses\SuccessJsonResponse;
use Documentation\Schemas\EditService;
use Documentation\Schemas\PartnerOrderShow;
use Documentation\Schemas\PartnerServiceProfileItem;
use OpenApi\Attributes\Get;

abstract class PartnerServiceProfile
{
    private const TAGS = ['Профиль поставщика услуг'];

    #[Get(
        path: '/api/v1/cabinet/partner-service/profile',
        summary: 'Профиль поставщика услуг',
        tags: self::TAGS,
        responses: [
            new SuccessJsonResponse(
                'OK',
                PartnerServiceProfileItem::class
            ),
        ],
    )]
    public function partnerServiceProfile(): void
    {
    }

    #[PaginationRequest(
        path: '/api/v1/cabinet/partner-service/services/published',
        summary: 'Опубликованные услуги',
        tags: self::TAGS,
        items: [
            'id' => 1,
            'title' => 'Aut laudantium id aut doloribus ut veritatis.',
            'status' => 'published',
            'categories' => [
                [
                    'id' => 1,
                    'parent_id' => null,
                    'order' => 0,
                    'is_published' => true,
                    'title' => 'Veritatis nesciunt.',
                    'description' => 'Quam soluta ad qui qui. Blanditiis laboriosam cupiditate asperiores rerum quis sed. Aperiam harum nemo consequatur magnam. Nobis minima eius iusto excepturi dolorem sunt impedit.',
                    'created_at' => '2023-07-06T09:09:33.000000Z',
                    'updated_at' => '2023-07-06T09:09:33.000000Z',
                ],
                [
                    'id' => 11,
                    'parent_id' => 1,
                    'order' => 0,
                    'is_published' => true,
                    'title' => 'Distinctio omnis.',
                    'description' => 'Officiis error perspiciatis officiis. Dolores enim eveniet vel sed. Suscipit aspernatur explicabo neque enim fugiat placeat omnis ad. Pariatur ipsa qui ab et ea reiciendis inventore.',
                    'created_at' => '2023-07-06T09:09:33.000000Z',
                    'updated_at' => '2023-07-06T09:09:33.000000Z',
                ],
            ],
        ],
    )]
    public function servicesPublished(): void
    {
    }

    #[PaginationRequest(
        path: '/api/v1/cabinet/partner-service/services/purchased',
        summary: 'Купленные услуги',
        tags: self::TAGS,
        items: [
            'id' => 51,
            'status' => 'succeeded',
            'user_phone' => '+79015138170',
            'full_price_amount' => '34',
            'surcharge_price_amount' => '354',
            'created_at' => '2023-07-10T12:58:27.000000Z',
            'updated_at' => '2023-07-10T12:58:28.000000Z',
            'tickets_count' => 5,
        ],
    )]
    public function servicesPurchased(): void
    {
    }

    #[PaginationRequest(
        path: '/api/v1/cabinet/partner-service/services/hidden',
        summary: 'Скрытые услуги',
        tags: self::TAGS,
        items: [
            'id' => 1,
            'title' => 'Aut laudantium id aut doloribus ut veritatis.',
            'status' => 'rejected',
            'created_at' => '2023-07-06T09:09:34.000000Z',
            'updated_at' => '2023-07-12T09:22:42.000000Z',
            'moderation' => [
                'id' => 1,
                'reason' => 'danger',
                'created_at' => '2023-07-12T09:10:00.000000Z',
                'updated_at' => '2023-07-12T09:10:00.000000Z',
            ],
            'categories' => [
                [
                    'id' => 1,
                    'parent_id' => null,
                    'order' => 0,
                    'is_published' => true,
                    'title' => 'Veritatis nesciunt.',
                    'description' => 'Quam soluta ad qui qui. Blanditiis laboriosam cupiditate asperiores rerum quis sed. Aperiam harum nemo consequatur magnam. Nobis minima eius iusto excepturi dolorem sunt impedit.',
                    'created_at' => '2023-07-06T09:09:33.000000Z',
                    'updated_at' => '2023-07-06T09:09:33.000000Z',
                ],
                [
                    'id' => 11,
                    'parent_id' => 1,
                    'order' => 0,
                    'is_published' => true,
                    'title' => 'Distinctio omnis.',
                    'description' => 'Officiis error perspiciatis officiis. Dolores enim eveniet vel sed. Suscipit aspernatur explicabo neque enim fugiat placeat omnis ad. Pariatur ipsa qui ab et ea reiciendis inventore.',
                    'created_at' => '2023-07-06T09:09:33.000000Z',
                    'updated_at' => '2023-07-06T09:09:33.000000Z',
                ],
            ],
        ],
    )]
    public function servicesHidden(): void
    {
    }

    #[Get(
        path: '/api/v1/services/{service}/edit',
        summary: 'Получение данных для редактирования',
        tags: self::TAGS,
        responses: [
            new SuccessJsonResponse(
                'OK',
                EditService::class
            ),
        ],
    )]
    public function editService(): void
    {
    }

    #[Get(
        path: '/api/v1/orders/partner-service/{order}',
        summary: 'Просмотр купленного купона',
        tags: self::TAGS,
        responses: [
            new SuccessJsonResponse(
                'OK',
                PartnerOrderShow::class
            ),
        ],
    )]
    public function showOrders(): void
    {
    }
}
