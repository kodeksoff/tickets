<?php

declare(strict_types=1);

namespace Documentation\Api;

use Documentation\Responses\SuccessJsonResponse;
use Documentation\Schemas\AskedQuestionList;
use OpenApi\Attributes\Get;

abstract class AskedQuestions
{
    private const TAGS = ['FAQ'];

    #[Get(
        path: '/api/v1/asked-questions',
        summary: 'Список вопосов-ответов',
        tags: self::TAGS,
        responses: [
            new SuccessJsonResponse(
                'OK',
                AskedQuestionList::class
            ),
        ],
    )]
    public function askedQuestions(): void
    {
    }
}
