<?php

declare(strict_types=1);

namespace Domain\Confirmations\Rules;

use Closure;

class NotConfirmedTokenRule extends AbstractTokenRule
{
    /**
     * @param  string  $attribute
     * @param  mixed  $value
     * @param  Closure  $fail
     *
     * @return void
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if (!$this->confirmation->confirmed_at) {
            $fail('Проверочный код не подтвержден');
        }
    }
}
