<?php

declare(strict_types=1);

namespace Domain\Confirmations\Rules;

use Closure;

class TokenAlreadyUsedRule extends AbstractTokenRule
{
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if ($this->isTourist && ($this->confirmation->executed_at || $this->confirmation->confirmed_at)) {
            $fail('Проверочный код уже использован');
        }
    }
}
