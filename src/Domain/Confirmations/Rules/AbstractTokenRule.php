<?php

declare(strict_types=1);

namespace Domain\Confirmations\Rules;

use Domain\Confirmations\Models\Confirmation;
use Illuminate\Contracts\Validation\ValidationRule;

abstract class AbstractTokenRule implements ValidationRule
{
    public function __construct(protected ?Confirmation $confirmation, protected bool $isTourist = false)
    {
    }
}
