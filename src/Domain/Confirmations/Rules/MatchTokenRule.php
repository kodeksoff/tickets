<?php

declare(strict_types=1);

namespace Domain\Confirmations\Rules;

use Closure;

class MatchTokenRule extends AbstractTokenRule
{
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if ($this->confirmation->token !== $value) {
            $fail('Проверочный код не совпадает');
        }
    }
}
