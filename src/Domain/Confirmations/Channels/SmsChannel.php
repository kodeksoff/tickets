<?php

declare(strict_types=1);

namespace Domain\Confirmations\Channels;

use Domain\Confirmations\Models\Confirmation;
use Domain\Users\Notifications\SendConfirmationToken;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Notifications\AnonymousNotifiable;

class SmsChannel extends AbstractChannel
{
    protected const TTL_IN_SECONDS = 300;

    public function __construct(
        protected AnonymousNotifiable $anonymousNotifiable,
        protected Repository $configRepository,
    ) {
        parent::__construct($configRepository);
    }

    public function send(Confirmation $confirmation): bool
    {
        $this
            ->anonymousNotifiable
            ->route('toSmsCenter', $confirmation->deliver_to)
            ->notifyNow(
                new SendConfirmationToken(
                    $confirmation,
                ),
            );

        return true;
    }
}
