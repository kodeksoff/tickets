<?php

declare(strict_types=1);

namespace Domain\Confirmations\Channels;

use Domain\Confirmations\Models\Confirmation;
use Domain\Integrations\SmsCenter\SmsCenterSettings;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Support\Carbon;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

abstract class AbstractChannel
{
    protected const TTL_IN_SECONDS = 0;

    public function __construct(protected Repository $configRepository)
    {
    }

    abstract public function send(Confirmation $confirmation): bool;

    public function getExpirationDate(): Carbon
    {
        return Carbon::now()->addSeconds(static::TTL_IN_SECONDS);
    }

    public function resendDelayInSeconds(): int
    {
        return $this
            ->configRepository
            ->get(
                sprintf(
                    'confirmations.resend_delay_in_seconds.%s',
                    static::class,
                ),
            );
    }

    public function toMessage(Confirmation $confirmation): string
    {
        return sprintf(
            $this
                ->configRepository
                ->get(
                    sprintf(
                        'confirmations.resend_delay_in_seconds.%s',
                        static::class,
                    ),
                ),
            $confirmation->token,
        );
    }

    /**
     * @return string
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function generateToken(): string
    {
        $smsCenterSettings = SmsCenterSettings::fromConfig();

        if ($smsCenterSettings->fake) {
            return '12345';
        }

        return (string)random_int(11111, 99999);
    }
}
