<?php

declare(strict_types=1);

namespace Domain\Confirmations\Exceptions;

use Exception;
use Illuminate\Contracts\Support\Responsable;
use Support\Responses\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class TooManyConfirmationAttempts extends Exception implements Responsable
{
    public function __construct(private readonly int $delay)
    {
        parent::__construct(
            sprintf('Запросить новый код можно через %s сек.', $this->delay),
            Response::HTTP_TOO_MANY_REQUESTS,
        );
    }

    public function toResponse($request): JsonResponse
    {
        return JsonResponse::error(
            $this->message,
            Response::HTTP_TOO_MANY_REQUESTS,
        );
    }
}
