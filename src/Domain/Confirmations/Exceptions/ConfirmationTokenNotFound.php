<?php

declare(strict_types=1);

namespace Domain\Confirmations\Exceptions;

use Exception;
use Illuminate\Contracts\Support\Responsable;
use Support\Responses\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ConfirmationTokenNotFound extends Exception implements Responsable
{
    protected $message = 'Проверочный код не найден';

    public function toResponse($request): JsonResponse
    {
        return JsonResponse::error(
            $this->message,
            Response::HTTP_NOT_FOUND,
        );
    }
}
