<?php

declare(strict_types=1);

namespace Domain\Confirmations\Exceptions;

use Exception;
use Illuminate\Contracts\Support\Responsable;
use Support\Responses\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ConfirmationTokenAlreadyUsed extends Exception implements Responsable
{
    protected $message = 'Проверочный код уже использован';

    public function toResponse($request): JsonResponse
    {
        return JsonResponse::error(
            $this->message,
            Response::HTTP_BAD_REQUEST,
        );
    }
}
