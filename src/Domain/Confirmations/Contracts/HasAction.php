<?php

declare(strict_types=1);

namespace Domain\Confirmations\Contracts;

use Domain\Confirmations\Models\Confirmation;

interface HasAction
{
    public function __invoke(Confirmation $confirmation);
}
