<?php

declare(strict_types=1);

namespace Domain\Confirmations\QueryBuilders;

use Support\EloquentBuilder;

class ConfirmationQueryBuilder extends EloquentBuilder
{
    public function whereTokenAndId(string $token, string $uuid): self
    {
        return $this
            ->where('id', $uuid)
            ->where('token', $token);
    }
}
