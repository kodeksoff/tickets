<?php

declare(strict_types=1);

namespace Domain\Confirmations\DataTransferObjects;

use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;

#[MapName(SnakeCaseMapper::class)]
class ApproveConfirmationData extends Data
{
    public function __construct(
        public readonly string $confirmationId,
        public readonly string $token,
    ) {
    }
}
