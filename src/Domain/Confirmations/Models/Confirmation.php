<?php

declare(strict_types=1);

namespace Domain\Confirmations\Models;

use Carbon\Carbon;
use Domain\Confirmations\Channels\AbstractChannel;
use Domain\Confirmations\QueryBuilders\ConfirmationQueryBuilder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Support\Model;

/**
 * Domain\Confirmations\Models\Confirmation.
 *
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string|null $action
 * @property string $class
 * @property AbstractChannel $channel
 * @property string $deliver_to
 * @property string $token
 * @property array|null $options
 * @property \Illuminate\Support\Carbon|null $expires_at
 * @property \Illuminate\Support\Carbon|null $executed_at
 * @property-read bool $is_expired
 * @property-read int $resend_delay_in_seconds
 * @method static \Support\EloquentBuilder|Confirmation newModelQuery()
 * @method static \Support\EloquentBuilder|Confirmation newQuery()
 * @method static \Support\EloquentBuilder|Confirmation query()
 * @method static \Support\EloquentBuilder|Confirmation whereAction($value)
 * @method static \Support\EloquentBuilder|Confirmation whereChannel($value)
 * @method static \Support\EloquentBuilder|Confirmation whereClass($value)
 * @method static \Support\EloquentBuilder|Confirmation whereCreatedAt($value)
 * @method static \Support\EloquentBuilder|Confirmation whereDeliverTo($value)
 * @method static \Support\EloquentBuilder|Confirmation whereExecutedAt($value)
 * @method static \Support\EloquentBuilder|Confirmation whereExpiresAt($value)
 * @method static \Support\EloquentBuilder|Confirmation whereId($value)
 * @method static \Support\EloquentBuilder|Confirmation whereIn(string $column, string $values, string $boolean = 'and', string $not = false)
 * @method static \Support\EloquentBuilder|Confirmation whereOptions($value)
 * @method static \Support\EloquentBuilder|Confirmation whereToken($value)
 * @method static \Support\EloquentBuilder|Confirmation whereUpdatedAt($value)
 * @property \Carbon\CarbonImmutable|null $confirmed_at
 * @method static ConfirmationQueryBuilder|Confirmation whereConfirmedAt($value)
 * @method static ConfirmationQueryBuilder|Confirmation whereTokenAndId(string $token, string $uuid)
 * @mixin \Eloquent
 */
class Confirmation extends Model
{
    use HasUuids;

    public const CHANNEL = null;

    /** @var array<string, string> */
    public $casts = [
        'token' => 'string',
        'options' => 'array',
        'expires_at' => 'immutable_datetime',
        'confirmed_at' => 'immutable_datetime',
        'executed_at' => 'immutable_datetime',
        'created_at' => 'immutable_datetime',
        'updated_at' => 'immutable_datetime',
    ];

    /** @var array<int, string> */
    public $visible = [
        'id',
        'deliver_to',
        'resend_delay_in_seconds',
    ];

    /** @var string */
    protected $table = 'confirmations';

    /** @var array<string> */
    protected $fillable = [
        'action',
        'class',
        'channel',
        'deliver_to',
        'token',
        'options',
        'expires_at',
        'confirmed_at',
        'executed_at',
        'created_at',
        'updated_at',
    ];

    /** @var array<int, string> */
    protected $hidden = [
        'token',
    ];

    /** @var array<int, string> */
    protected $appends = [
        'resend_delay_in_seconds',
    ];

    /**
     * @param $query
     *
     * @return ConfirmationQueryBuilder
     */
    public function newEloquentBuilder($query): ConfirmationQueryBuilder
    {
        return new ConfirmationQueryBuilder($query);
    }

    /** @return Attribute */
    public function channel(): Attribute
    {
        return new Attribute(
            fn (string $channel): AbstractChannel => resolve($channel),
        );
    }

    /** @return Attribute */
    public function resendDelayInSeconds(): Attribute
    {
        return new Attribute(
            fn (): int => $this->channel->resendDelayInSeconds(),
        );
    }

    /** @return string[] */
    public function getVisible(): array
    {
        if (is_dev_env()) {
            $this->visible[] = 'token';

            return $this->visible;
        }

        return $this->visible;
    }

    /** @return Attribute */
    public function isExpired(): Attribute
    {
        return new Attribute(
            fn (): bool => $this->expires_at && $this->expires_at->isPast(),
        );
    }

    /** @return array|string[] */
    public function getHidden(): array
    {
        if (is_dev_env()) {
            return [];
        }

        return $this->hidden;
    }

    /** @return bool */
    public function send(): bool
    {
        return $this
            ->channel
            ->send($this);
    }
}
