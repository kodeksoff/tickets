<?php

declare(strict_types=1);

namespace Domain\Confirmations\Models;

use Carbon\Carbon;
use Domain\Confirmations\Channels\SmsChannel;

/**
 * Domain\Confirmations\Models\PhoneConfirmation.
 *
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string|null $action
 * @property string $class
 * @property \Domain\Confirmations\Channels\AbstractChannel $channel
 * @property string $deliver_to
 * @property string $token
 * @property array|null $options
 * @property \Illuminate\Support\Carbon|null $expires_at
 * @property \Illuminate\Support\Carbon|null $executed_at
 * @property-read bool $is_expired
 * @property-read int $resend_delay_in_seconds
 * @method static \Support\EloquentBuilder|PhoneConfirmation newModelQuery()
 * @method static \Support\EloquentBuilder|PhoneConfirmation newQuery()
 * @method static \Support\EloquentBuilder|PhoneConfirmation query()
 * @method static \Support\EloquentBuilder|PhoneConfirmation whereAction($value)
 * @method static \Support\EloquentBuilder|PhoneConfirmation whereChannel($value)
 * @method static \Support\EloquentBuilder|PhoneConfirmation whereClass($value)
 * @method static \Support\EloquentBuilder|PhoneConfirmation whereCreatedAt($value)
 * @method static \Support\EloquentBuilder|PhoneConfirmation whereDeliverTo($value)
 * @method static \Support\EloquentBuilder|PhoneConfirmation whereExecutedAt($value)
 * @method static \Support\EloquentBuilder|PhoneConfirmation whereExpiresAt($value)
 * @method static \Support\EloquentBuilder|PhoneConfirmation whereId($value)
 * @method static \Support\EloquentBuilder|PhoneConfirmation whereIn(string $column, string $values, string $boolean = 'and', string $not = false)
 * @method static \Support\EloquentBuilder|PhoneConfirmation whereOptions($value)
 * @method static \Support\EloquentBuilder|PhoneConfirmation whereToken($value)
 * @method static \Support\EloquentBuilder|PhoneConfirmation whereUpdatedAt($value)
 * @property \Carbon\CarbonImmutable|null $confirmed_at
 * @method static \Domain\Confirmations\QueryBuilders\ConfirmationQueryBuilder|PhoneConfirmation whereConfirmedAt($value)
 * @method static \Domain\Confirmations\QueryBuilders\ConfirmationQueryBuilder|PhoneConfirmation whereTokenAndId(string $token, string $uuid)
 * @mixin \Eloquent
 */
class PhoneConfirmation extends Confirmation
{
    final public const CHANNEL = SmsChannel::class;
}
