<?php

declare(strict_types=1);

namespace Domain\Confirmations\Actions;

use Domain\Confirmations\Exceptions\ConfirmationTokenAlreadyUsed;
use Domain\Confirmations\Exceptions\ConfirmationTokenNotConfirmed;
use Domain\Confirmations\Models\Confirmation;
use Throwable;

class CheckApprovedConfirmationAction
{
    /** @throws Throwable */
    public function __invoke(string $confirmationId): Confirmation
    {
        $model = Confirmation::query()->findOrFail($confirmationId);

        throw_if(!$model->confirmed_at, ConfirmationTokenNotConfirmed::class);
        throw_if($model->executed_at, ConfirmationTokenAlreadyUsed::class);

        $model->update([
            'executed_at' => now(),
        ]);

        return $model;
    }
}
