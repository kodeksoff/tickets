<?php

declare(strict_types=1);

namespace Domain\Confirmations\Actions;

use Domain\Confirmations\DataTransferObjects\ApproveConfirmationData;
use Throwable;

class ApproveConfirmationAction
{
    public function __construct(protected UseConfirmationAction $useConfirmationAction)
    {
    }

    /**
     * @return array{confirmed: true, id: int}
     *
     * @throws Throwable
     */
    public function __invoke(ApproveConfirmationData $approveConfirmationData): array
    {
        return [
            'confirmed' => true,
            'id' => ($this->useConfirmationAction)(
                $approveConfirmationData->confirmationId,
                $approveConfirmationData->token,
                false,
            )->id,
        ];
    }
}
