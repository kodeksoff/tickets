<?php

declare(strict_types=1);

namespace Domain\Confirmations\Actions;

use Domain\Confirmations\Models\Confirmation;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Support\Utils\MobilePhoneFormatter;

class SendPhoneConfirmationAction
{
    /**
     * @throws BindingResolutionException
     * @throws Exception
     */
    public function __invoke(
        string|Confirmation $confirmation,
        string $phone,
    ): Confirmation {
        /** @var Confirmation $model */
        $model = new $confirmation();

        $model->channel = $confirmation::CHANNEL;
        $channel = $model->channel;

        return $model->create([
            'class' => $confirmation,
            'channel' => $confirmation::CHANNEL,
            'deliver_to' => resolve(MobilePhoneFormatter::class)->make($phone),
            'token' => $channel->generateToken(),
            'expires_at' => $channel->getExpirationDate(),
        ]);
    }
}
