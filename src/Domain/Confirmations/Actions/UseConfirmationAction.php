<?php

declare(strict_types=1);

namespace Domain\Confirmations\Actions;

use Domain\Confirmations\Exceptions\ConfirmationTokenAlreadyUsed;
use Domain\Confirmations\Exceptions\ConfirmationTokenDoesNotMatch;
use Domain\Confirmations\Models\Confirmation;
use Throwable;

class UseConfirmationAction
{
    /** @throws Throwable */
    public function __invoke(string $uuid, string $token, bool $executed = true): Confirmation
    {
        $model = Confirmation::query()->findOrFail($uuid);

        throw_if(
            $model->executed_at || $model->confirmed_at,
            ConfirmationTokenAlreadyUsed::class,
        );
        throw_if(
            $model->token !== $token,
            ConfirmationTokenDoesNotMatch::class,
        );

        if ($model->action) {
            (new $model->action())($model);
        }

        $date = now();

        $model->update([
            'confirmed_at' => $date,
            'executed_at' => $executed ? $date : null,
        ]);

        return $model;
    }
}
