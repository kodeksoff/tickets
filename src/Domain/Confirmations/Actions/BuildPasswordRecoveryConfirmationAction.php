<?php

declare(strict_types=1);

namespace Domain\Confirmations\Actions;

use Domain\Confirmations\Models\Confirmation;
use Domain\Confirmations\Models\PhoneConfirmation;
use Illuminate\Contracts\Container\BindingResolutionException;

class BuildPasswordRecoveryConfirmationAction
{
    public function __construct(protected SendPhoneConfirmationAction $sendPhoneConfirmationAction)
    {
    }

    /** @throws BindingResolutionException */
    public function __invoke(string $phone): Confirmation
    {
        $confirmation = ($this->sendPhoneConfirmationAction)(
            PhoneConfirmation::class,
            $phone,
        );

        $confirmation->send();

        return $confirmation;
    }
}
