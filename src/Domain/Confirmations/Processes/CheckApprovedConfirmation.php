<?php

declare(strict_types=1);

namespace Domain\Confirmations\Processes;

use Closure;
use Domain\Confirmations\Actions\CheckApprovedConfirmationAction;
use Domain\Users\DataTransferObjects\CreateUserData;
use Throwable;

class CheckApprovedConfirmation
{
    public function __construct(protected CheckApprovedConfirmationAction $checkApprovedConfirmationAction)
    {
    }

    /** @throws Throwable */
    public function __invoke(CreateUserData $createUserData, Closure $next): CreateUserData
    {
        ($this->checkApprovedConfirmationAction)($createUserData->confirmationId);

        return $next($createUserData);
    }
}
