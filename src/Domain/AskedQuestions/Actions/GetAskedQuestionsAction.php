<?php

declare(strict_types=1);

namespace Domain\AskedQuestions\Actions;

use Domain\AskedQuestions\Models\AskedQuestion;
use Illuminate\Database\Eloquent\Model;

class GetAskedQuestionsAction
{
    /**
     * @param string $type
     * @param array|string $columns
     * @return Model
     */
    public function __invoke(
        string $type = 'default',
        array|string $columns = 'questions',
    ): Model {
        return AskedQuestion::query()
            ->where('type', $type)
            ->select($columns)
            ->firstOrFail();
    }
}
