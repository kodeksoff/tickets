<?php

declare(strict_types=1);

namespace Domain\AskedQuestions\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Support\Model;

/**
 * Domain\AskedQuestions\Models\AskedQuestion.
 *
 * @method static \Tests\Factories\AskedQuestionFactory factory($count = null, $state = [])
 * @property int $id
 * @property array $questions
 * @property string $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Support\EloquentBuilder|AskedQuestion newModelQuery()
 * @method static \Support\EloquentBuilder|AskedQuestion newQuery()
 * @method static \Support\EloquentBuilder|AskedQuestion query()
 * @method static \Support\EloquentBuilder|AskedQuestion whereCreatedAt($value)
 * @method static \Support\EloquentBuilder|AskedQuestion whereId($value)
 * @method static \Support\EloquentBuilder|AskedQuestion whereIn(string $column, string $values, string $boolean = 'and', string $not = false)
 * @method static \Support\EloquentBuilder|AskedQuestion whereQuestions($value)
 * @method static \Support\EloquentBuilder|AskedQuestion whereType($value)
 * @method static \Support\EloquentBuilder|AskedQuestion whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AskedQuestion extends Model
{
    use HasFactory;

    /** @var string[] */
    protected $fillable = [
        'questions',
        'type',
    ];

    /** @var string[] */
    protected $casts = [
        'questions' => 'array',
    ];
}
