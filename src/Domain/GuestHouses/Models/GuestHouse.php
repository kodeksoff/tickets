<?php

declare(strict_types=1);

namespace Domain\GuestHouses\Models;

use Carbon\Carbon;
use Domain\GuestHouses\Events\GuestHouseCreatingEvent;
use Domain\PartnerHouses\Models\PartnerHouse;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Support\Model;

/**
 * Domain\GuestHouses\Models\GuestHouse.
 *
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property int $partner_house_id
 * @property string $title
 * @property string $address
 * @property string $short_address
 * @property int $code
 * @property-read PartnerHouse $partnerHouse
 * @method static \Tests\Factories\GuestHouseFactory factory($count = null, $state = [])
 * @method static \Support\EloquentBuilder|GuestHouse newModelQuery()
 * @method static \Support\EloquentBuilder|GuestHouse newQuery()
 * @method static \Support\EloquentBuilder|GuestHouse query()
 * @method static \Support\EloquentBuilder|GuestHouse whereAddress($value)
 * @method static \Support\EloquentBuilder|GuestHouse whereCreatedAt($value)
 * @method static \Support\EloquentBuilder|GuestHouse whereId($value)
 * @method static \Support\EloquentBuilder|GuestHouse whereIn(string $column, string $values, string $boolean = 'and', string $not = false)
 * @method static \Support\EloquentBuilder|GuestHouse wherePartnerHouseId($value)
 * @method static \Support\EloquentBuilder|GuestHouse whereTitle($value)
 * @method static \Support\EloquentBuilder|GuestHouse whereUpdatedAt($value)
 * @method static \Support\EloquentBuilder|GuestHouse whereCode($value)
 * @method static \Support\EloquentBuilder|GuestHouse whereShortAddress($value)
 * @mixin \Eloquent
 */
class GuestHouse extends Model
{
    use HasFactory;

    /** @var string[] */
    protected $dispatchesEvents = [
        'creating' => GuestHouseCreatingEvent::class,
    ];

    /** @var string[] */
    protected $fillable = [
        'partner_house_id',
        'title',
        'address',
        'short_address',
        'created_at',
        'updated_at',
        'code',
    ];

    /** @var array<string, string> */
    protected $casts = [
        'created_at' => 'immutable_datetime',
        'updated_at' => 'immutable_datetime',
    ];

    /** @return BelongsTo */
    public function partnerHouse(): BelongsTo
    {
        return $this->belongsTo(PartnerHouse::class);
    }
}
