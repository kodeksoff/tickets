<?php

declare(strict_types=1);

namespace Domain\GuestHouses\Events;

use Domain\GuestHouses\Models\GuestHouse;
use Illuminate\Foundation\Events\Dispatchable;

class GuestHouseCreatingEvent
{
    use Dispatchable;

    /** @param  GuestHouse  $guestHouse */
    public function __construct(public GuestHouse $guestHouse)
    {
    }
}
