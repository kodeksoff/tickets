<?php

declare(strict_types=1);

namespace Domain\GuestHouses\Exceptions;

use Exception;

class GuestHouseNotFoundException extends Exception
{
    /** @var string */
    protected $message = 'Гостевой дом не найден';
}
