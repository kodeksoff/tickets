<?php

declare(strict_types=1);

namespace Domain\GuestHouses\Subscribers;

use Domain\GuestHouses\Events\GuestHouseCreatingEvent;
use Domain\GuestHouses\Models\GuestHouse;
use Exception;
use Illuminate\Events\Dispatcher;

class GuestHouseSubscriber
{
    /** @throws Exception */
    public function generateGuestHouseCode(GuestHouseCreatingEvent $event): void
    {
        do {
            $code = random_int(10000, 99999);
        } while (
            GuestHouse::query()
                ->where('code', $code)
                ->first()
        );

        $event->guestHouse->code = $code;
    }

    public function subscribe(Dispatcher $dispatcher): array
    {
        return [
            GuestHouseCreatingEvent::class => 'generateGuestHouseCode',
        ];
    }
}
