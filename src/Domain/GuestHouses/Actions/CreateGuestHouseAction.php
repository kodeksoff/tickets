<?php

declare(strict_types=1);

namespace Domain\GuestHouses\Actions;

use Domain\PartnerHouses\Models\PartnerHouse;
use Domain\Users\DataTransferObjects\CreateUserData;

class CreateGuestHouseAction
{
    public function __invoke(CreateUserData $createUserData): PartnerHouse
    {
        $createUserData
            ->user
            ->partnerHouse
            ->guestHouses()
            ->create();

        return $createUserData
            ->user
            ->partnerHouse
            ->load(['guestHouses']);
    }
}
