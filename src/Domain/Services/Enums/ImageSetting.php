<?php

declare(strict_types=1);

namespace Domain\Services\Enums;

use ArchTech\Enums\InvokableCases;

enum ImageSetting: string
{
    use InvokableCases;

    case LARGE = 'large';

    case MEDIUM = 'medium';

    case THUMB_LARGE = 'thumb-large';

    case THUMB_MEDIUM = 'thumb-medium';
    case THUMB_SMALL = 'thumb-small';
}
