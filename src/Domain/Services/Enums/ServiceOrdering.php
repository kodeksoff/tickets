<?php

declare(strict_types=1);

namespace Domain\Services\Enums;

use ArchTech\Enums\InvokableCases;

/**
 * @method PRICE()
 * @method TITLE()
 */
enum ServiceOrdering: string
{
    use InvokableCases;

    case PRICE = 'price';
    case TITLE = 'title';
}
