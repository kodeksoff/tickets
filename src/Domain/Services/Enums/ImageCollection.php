<?php

declare(strict_types=1);

namespace Domain\Services\Enums;

use ArchTech\Enums\InvokableCases;

enum ImageCollection: string
{
    use InvokableCases;

    case MAIN_IMAGE = 'main_image';
    case ADDITIONAL_IMAGES = 'additional_images';
}
