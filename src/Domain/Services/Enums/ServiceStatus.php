<?php

declare(strict_types=1);

namespace Domain\Services\Enums;

enum ServiceStatus: string
{
    case PUBLISHED = 'published';
    case HIDDEN = 'hidden';
    case MODERATION = 'moderation';
    case REJECTED = 'rejected';
}
