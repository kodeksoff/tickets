<?php

declare(strict_types=1);

namespace Domain\Services\Enums;

use ArchTech\Enums\InvokableCases;

enum ImageTypes: int
{
    use InvokableCases;

    case FIRST = 1;
    case SECOND = 2;
    case THIRD = 3;
    case FOURTH = 4;
}
