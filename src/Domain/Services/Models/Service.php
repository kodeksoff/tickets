<?php

declare(strict_types=1);

namespace Domain\Services\Models;

use Carbon\Carbon;
use Domain\Orders\Models\Order;
use Domain\PartnerServices\Models\PartnerService;
use Domain\ServiceBooking\Models\ServiceBooking;
use Domain\ServiceCategories\Models\ServiceCategory;
use Domain\Services\DataTransferObjects\QueryStringData;
use Domain\Services\Enums\ImageCollection;
use Domain\Services\Enums\ImageSetting;
use Domain\Services\Enums\ServiceStatus;
use Domain\Services\Events\ServiceCreatingEvent;
use Domain\Services\Events\ServiceUpdatingEvent;
use Domain\Services\QueryBuilders\ServiceQueryBuilder;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Collection;
use Laravel\Scout\Searchable;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Staudenmeir\EloquentJsonRelations\HasJsonRelationships;
use Staudenmeir\EloquentJsonRelations\Relations\BelongsToJson;
use Support\Model;

/**
 * Domain\Services\Models\Service.
 *
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property int $partner_service_id
 * @property string $title
 * @property string $description
 * @property string $about_included
 * @property string $about_additional
 * @property bool $is_published
 * @property-read PartnerService $partnerService
 * @method static \Tests\Factories\ServiceFactory factory($count = null, $state = [])
 * @method static \Support\EloquentBuilder|Service newModelQuery()
 * @method static \Support\EloquentBuilder|Service newQuery()
 * @method static \Support\EloquentBuilder|Service query()
 * @method static \Support\EloquentBuilder|Service whereAboutAdditional($value)
 * @method static \Support\EloquentBuilder|Service whereAboutIncluded($value)
 * @method static \Support\EloquentBuilder|Service whereCreatedAt($value)
 * @method static \Support\EloquentBuilder|Service whereDescription($value)
 * @method static \Support\EloquentBuilder|Service whereId($value)
 * @method static \Support\EloquentBuilder|Service whereIn(string $column, string $values, string $boolean = 'and', string $not = false)
 * @method static \Support\EloquentBuilder|Service whereIsPublished($value)
 * @method static \Support\EloquentBuilder|Service wherePartnerServiceId($value)
 * @method static \Support\EloquentBuilder|Service whereTitle($value)
 * @method static \Support\EloquentBuilder|Service whereUpdatedAt($value)
 * @method \Support\EloquentBuilder|Service forPartnerService(int $partnerServiceId)
 * @method \Support\EloquentBuilder|Service hiddenServices()
 * @property array|null $category_ids
 * @property \Brick\Money\Money|null|null $price
 * @property-read \Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection<int, Media> $media
 * @property-read int|null $media_count
 * @method static ServiceQueryBuilder|Service whereCategoryIds($value)
 * @method static ServiceQueryBuilder|Service whereJsonbAnyContains(string $column, \Illuminate\Contracts\Support\Arrayable|array $values, string $boolean = 'and')
 * @method static ServiceQueryBuilder|Service wherePrice($value)
 * @method static ServiceQueryBuilder|Service categorySorting(QueryStringData $queryStringData)
 * @method static ServiceQueryBuilder|Service ordering(QueryStringData $queryStringData)
 * @property int|null $service_season_from
 * @property int|null $service_season_to
 * @property bool $service_all_year
 * @property-read \Illuminate\Database\Eloquent\Collection<int, ServiceBooking> $bookings
 * @property-read int|null $bookings_count
 * @property-read ServiceBooking|null $lowestBooking
 * @method static ServiceQueryBuilder|Service whereServiceAllYear($value)
 * @method static ServiceQueryBuilder|Service whereServiceSeasonFrom($value)
 * @method static ServiceQueryBuilder|Service whereServiceSeasonTo($value)
 * @property ServiceStatus|null $status
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \Domain\Services\Models\ServiceModeration> $moderations
 * @property-read int|null $moderations_count
 * @property-read Order|null $order
 * @method static ServiceQueryBuilder|Service whereStatus($value)
 * @property int $fee
 * @method static ServiceQueryBuilder|Service publishedService()
 * @method static ServiceQueryBuilder|Service serviceOnModeration()
 * @method static ServiceQueryBuilder|Service serviceRejected()
 * @method static ServiceQueryBuilder|Service whereFee($value)
 * @mixin \Eloquent
 */
class Service extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;
    use Searchable;
    use HasJsonRelationships;

    /** @var string[] */
    protected $dispatchesEvents = [
        'creating' => ServiceCreatingEvent::class,
        'updating' => ServiceUpdatingEvent::class,
    ];

    /** @var string[] */
    protected $fillable = [
        'partner_service_id',
        'category_ids',
        'title',
        'description',
        'about_included',
        'about_additional',
        'status',
        'fee',
        'service_season_from',
        'service_season_to',
        'service_all_year',
        'created_at',
        'updated_at',
    ];

    /** @var string[] */
    protected $hidden = [
        'fee',
    ];

    /** @var array<string, string> */
    protected $casts = [
        'status' => ServiceStatus::class,
        'service_all_year' => 'boolean',
        'category_ids' => 'array',
        'created_at' => 'immutable_datetime',
        'updated_at' => 'immutable_datetime',
    ];

    /** @return BelongsToJson */
    public function categories(): BelongsToJson
    {
        return $this->belongsToJson(ServiceCategory::class, 'category_ids', 'id');
    }

    /** @return BelongsTo */
    public function partnerService(): BelongsTo
    {
        return $this->belongsTo(PartnerService::class);
    }

    /** @return HasMany */
    public function bookings(): HasMany
    {
        return $this->hasMany(ServiceBooking::class);
    }

    /** @return HasOne */
    public function lowestBooking(): HasOne
    {
        return $this
            ->hasOne(ServiceBooking::class)
            ->ofMany(
                ['id' => 'min'],
                fn (Builder $builder) => $builder->where('is_default', true),
            );
    }

    /** @return HasOne */
    public function order(): HasOne
    {
        return $this->hasOne(Order::class);
    }

    /** @return HasMany */
    public function moderations(): HasMany
    {
        return $this->hasMany(ServiceModeration::class);
    }

    /** @return HasOne */
    public function latestModeration(): HasOne
    {
        return $this
            ->hasOne(ServiceModeration::class)
            ->ofMany();
    }

    /**
     * @param $query
     *
     * @return ServiceQueryBuilder
     */
    public function newEloquentBuilder($query): ServiceQueryBuilder
    {
        return new ServiceQueryBuilder($query);
    }

    /**
     * @param  ImageCollection  $collectionName
     * @param  ImageSetting  $imageSetting
     * @param  array  $filters
     *
     * @return string|null
     */
    public function getImageUrl(
        ImageCollection $collectionName,
        ImageSetting $imageSetting,
        array $filters = [],
    ): ?string {
        return $this
            ->loadMissing(['media.model'])
            ->getMedia($collectionName->value, $filters)
            ->first()
            ?->getUrl($imageSetting->value);
    }

    /**
     * @param  ImageCollection  $imageCollection
     * @param  ImageSetting  $imageSetting
     *
     * @return Collection
     */
    public function getAdditionalImages(ImageCollection $imageCollection, ImageSetting $imageSetting): Collection
    {
        return $this
            ->loadMissing(['media.model'])
            ->getMedia($imageCollection->value)
            ->map(function (Media $media) use ($imageSetting) {
                return $media->getUrl($imageSetting->value);
            });
    }

    /** @return void */
    public function registerMediaCollections(): void
    {
        $this
            ->addMediaCollection('main_image')
            ->registerMediaConversions(function (Media $media) {
                $this
                    ->addMediaConversion(ImageSetting::LARGE())
                    ->format('webp')
                    ->fit(Manipulations::FIT_CROP, 1071, 596)
                    ->nonQueued();

                $this
                    ->addMediaConversion(ImageSetting::MEDIUM())
                    ->format('webp')
                    ->fit(Manipulations::FIT_CROP, 690, 384)
                    ->nonQueued();

                $this
                    ->addMediaConversion(ImageSetting::THUMB_LARGE())
                    ->format('webp')
                    ->fit(Manipulations::FIT_CROP, 384, 271)
                    ->nonQueued();

                $this
                    ->addMediaConversion(ImageSetting::THUMB_MEDIUM())
                    ->format('webp')
                    ->fit(Manipulations::FIT_CROP, 282, 190)
                    ->nonQueued();

                $this
                    ->addMediaConversion(ImageSetting::THUMB_SMALL())
                    ->format('webp')
                    ->fit(Manipulations::FIT_CROP, 250, 190)
                    ->nonQueued();
            });

        $this
            ->addMediaCollection('additional_images')
            ->registerMediaConversions(function (Media $media) {
                $this
                    ->addMediaConversion(ImageSetting::LARGE())
                    ->format('webp')
                    ->fit(Manipulations::FIT_CROP, 1071, 596)
                    ->nonQueued();

                $this
                    ->addMediaConversion(ImageSetting::MEDIUM())
                    ->format('webp')
                    ->fit(Manipulations::FIT_CROP, 690, 384)
                    ->nonQueued();

                $this
                    ->addMediaConversion(ImageSetting::THUMB_LARGE())
                    ->format('webp')
                    ->fit(Manipulations::FIT_CROP, 384, 271)
                    ->nonQueued();

                $this
                    ->addMediaConversion(ImageSetting::THUMB_MEDIUM())
                    ->format('webp')
                    ->fit(Manipulations::FIT_CROP, 282, 190)
                    ->nonQueued();

                $this
                    ->addMediaConversion(ImageSetting::THUMB_SMALL())
                    ->format('webp')
                    ->fit(Manipulations::FIT_CROP, 250, 190)
                    ->nonQueued();
            });
    }
}
