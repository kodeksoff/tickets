<?php

declare(strict_types=1);

namespace Domain\Services\Models;

use Carbon\Carbon;
use Domain\Users\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Support\Model;

/**
 * Domain\Services\Models\ServiceModeration
 *
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property int $user_id
 * @property int $service_id
 * @property string|null $reason
 * @property array|null $data
 * @property-read \Domain\Services\Models\Service $service
 * @property-read User $user
 * @method static \Support\EloquentBuilder|ServiceModeration newModelQuery()
 * @method static \Support\EloquentBuilder|ServiceModeration newQuery()
 * @method static \Support\EloquentBuilder|ServiceModeration query()
 * @method static \Support\EloquentBuilder|ServiceModeration whereCreatedAt($value)
 * @method static \Support\EloquentBuilder|ServiceModeration whereData($value)
 * @method static \Support\EloquentBuilder|ServiceModeration whereId($value)
 * @method static \Support\EloquentBuilder|ServiceModeration whereIn(string $column, string $values, string $boolean = 'and', string $not = false)
 * @method static \Support\EloquentBuilder|ServiceModeration whereReason($value)
 * @method static \Support\EloquentBuilder|ServiceModeration whereServiceId($value)
 * @method static \Support\EloquentBuilder|ServiceModeration whereUpdatedAt($value)
 * @method static \Support\EloquentBuilder|ServiceModeration whereUserId($value)
 * @mixin \Eloquent
 */
class ServiceModeration extends Model
{
    /** @var string[] */
    protected $fillable = [
        'user_id',
        'service_id',
        'reason',
        'data',
    ];

    /** @var string[] */
    protected $casts = [
        'data' => 'array',
    ];

    /** @return BelongsTo */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /** @return BelongsTo */
    public function service(): BelongsTo
    {
        return $this->belongsTo(Service::class);
    }
}
