<?php

declare(strict_types=1);

namespace Domain\Services\Rules;

use Closure;
use Domain\Services\Enums\ServiceStatus;
use Domain\Services\Models\Service;
use Illuminate\Contracts\Validation\ValidationRule;

class PublishServiceRule implements ValidationRule
{
    /** @param  Service  $service */
    public function __construct(protected Service $service)
    {
    }

    /**
     * @param  string  $attribute
     * @param  mixed  $value
     * @param  Closure  $fail
     *
     * @return void
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if ($this->service->status === ServiceStatus::MODERATION) {
            $fail('Услуга ожидает проверки модератором');
        }
    }
}
