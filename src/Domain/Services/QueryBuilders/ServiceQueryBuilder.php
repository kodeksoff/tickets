<?php

declare(strict_types=1);

namespace Domain\Services\QueryBuilders;

use Domain\Services\DataTransferObjects\QueryStringData;
use Domain\Services\Enums\ServiceStatus;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Builder;
use Support\EloquentBuilder;

class ServiceQueryBuilder extends EloquentBuilder
{
    /**
     * @param  int  $partnerServiceId
     *
     * @return self
     */
    public function forPartnerService(int $partnerServiceId): self
    {
        return $this->where('partner_service_id', $partnerServiceId);
    }

    /** @return self */
    public function publishedService(): self
    {
        return $this->where('status', ServiceStatus::PUBLISHED);
    }

    /** @return self */
    public function hiddenServices(): self
    {
        return $this->where('status', ServiceStatus::HIDDEN);
    }

    /** @return self */
    public function serviceOnModeration(): self
    {
        return $this->where('status', ServiceStatus::MODERATION);
    }

    /** @return self */
    public function serviceRejected(): self
    {
        return $this->where('status', ServiceStatus::REJECTED);
    }

    /**
     * @param  QueryStringData  $queryStringData
     *
     * @return self
     */
    public function categorySorting(QueryStringData $queryStringData): self
    {
        return $this->when(
            $queryStringData->categoryIds,
            fn (): Builder => $this->whereJsonbAnyContains('category_ids', $queryStringData->categoryIds),
        );
    }

    /**
     * @param  QueryStringData  $queryStringData
     *
     * @return self
     */
    public function ordering(QueryStringData $queryStringData): self
    {
        return $this->when(
            $queryStringData->orderBy,
            fn (Builder $builder): Builder => $builder->when(
                $queryStringData->isPriceOrdering(),
                fn (Builder $builder) => $builder->orderBy('price->amount', $queryStringData->orderDirection),
                fn (Builder $builder) => $builder->orderBy($queryStringData->orderBy, $queryStringData->orderDirection),
            ),
            fn (Builder $builder): Builder => $builder->orderBy('title'),
        );
    }

    /**
     * @param  string  $column
     * @param  array|Arrayable  $values
     * @param  string  $boolean
     *
     * @return self
     */
    public function whereJsonbAnyContains(string $column, array|Arrayable $values, string $boolean = 'and'): self
    {
        $values = $values instanceof Arrayable
            ? $values->toArray()
            : $values;

        return $this->whereRaw(
            sprintf(
                '%s::jsonb @> ANY (ARRAY %s::jsonb[])',
                $column,
                sprintf(
                    "['%s']",
                    implode(
                        "', '",
                        $values,
                    ),
                ),
            ),
        );
    }
}
