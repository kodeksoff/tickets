<?php

declare(strict_types=1);

namespace Domain\Services\Listeners;

use Domain\Services\Events\ServiceRejectedModerationEvent;
use Domain\Users\Notifications\ServiceRejectedModerationNotification;
use Illuminate\Contracts\Notifications\Dispatcher;

class SendServiceRejectedNotificationListener
{
    /** @param  Dispatcher  $dispatcher */
    public function __construct(protected Dispatcher $dispatcher)
    {
    }

    /**
     * @param  ServiceRejectedModerationEvent  $event
     *
     * @return void
     */
    public function handle(ServiceRejectedModerationEvent $event): void
    {
        $this
            ->dispatcher
            ->send(
                $event->service->partnerService->user,
                new ServiceRejectedModerationNotification($event->service),
            );
    }
}
