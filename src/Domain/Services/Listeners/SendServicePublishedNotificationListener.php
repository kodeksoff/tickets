<?php

declare(strict_types=1);

namespace Domain\Services\Listeners;

use Domain\Services\Events\ServicePublishModerationEvent;
use Domain\Users\Notifications\ServicePublishModerationNotification;
use Illuminate\Contracts\Notifications\Dispatcher;

class SendServicePublishedNotificationListener
{
    /** @param  Dispatcher  $dispatcher */
    public function __construct(protected Dispatcher $dispatcher)
    {
    }

    /**
     * @param  ServicePublishModerationEvent  $event
     *
     * @return void
     */
    public function handle(ServicePublishModerationEvent $event): void
    {
        $this
            ->dispatcher
            ->send(
                $event->service->partnerService->user,
                new ServicePublishModerationNotification($event->service),
            );
    }
}
