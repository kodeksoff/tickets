<?php

declare(strict_types=1);

namespace Domain\Services\Events;

use Domain\Services\Models\Service;
use Illuminate\Foundation\Events\Dispatchable;

class ServiceCreatingEvent
{
    use Dispatchable;

    /** @param  Service  $service */
    public function __construct(public Service $service)
    {
    }
}
