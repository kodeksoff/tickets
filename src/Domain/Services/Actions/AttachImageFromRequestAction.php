<?php

declare(strict_types=1);

namespace Domain\Services\Actions;

use App\Api\V1\Requests\UpdateServiceRequest;
use Domain\Services\Models\Service;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;

class AttachImageFromRequestAction
{
    /**
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     */
    public function __invoke(UpdateServiceRequest $updateServiceRequest, Service $service): void
    {
        $service->loadMissing(['media.model']);

        if ($updateServiceRequest->has('deleted_additional_images')) {
            foreach ($updateServiceRequest->deleted_additional_images as $keyAdditionalImage) {
                $imageCollection = $service
                    ->getMedia('additional_images');

                if ($imageCollection->has($keyAdditionalImage)) {
                    $imageCollection[$keyAdditionalImage]->delete();
                }
            }
        }

        if ($updateServiceRequest->hasFile('main_image')) {
            $service->clearMediaCollection('main_image');
            $service
                ->addMedia($updateServiceRequest->file('main_image'))
                ->toMediaCollection('main_image');
        }
        if ($updateServiceRequest->has('additional_images')) {
            foreach ($updateServiceRequest->additional_images as $key => $additionalImage) {
                $imageCollection = $service
                    ->getMedia('additional_images');

                if ($imageCollection->has($key)) {
                    $imageCollection[$key]->delete();
                }

                $service
                    ->addMedia($additionalImage)
                    ->toMediaCollection('additional_images');
            }
        }

        if (
            $updateServiceRequest->has('deleted_main_image')
            && $updateServiceRequest->deleted_main_image
        ) {
            $service->clearMediaCollection('main_image');
            $imageCollection = $service
                ->getMedia('additional_images');
            $service
                ->addMedia($imageCollection[0])
                ->toMediaCollection('main_image');
            //$imageCollection[0]->delete();
        }
    }
}
