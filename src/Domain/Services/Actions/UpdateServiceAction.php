<?php

declare(strict_types=1);

namespace Domain\Services\Actions;

use App\Api\V1\Requests\UpdateServiceRequest;
use Domain\ServiceBooking\Actions\SyncServiceBookingsAction;
use Domain\Services\DataTransferObjects\UpdateServiceData;
use Domain\Services\Enums\ServiceStatus;
use Domain\Services\Models\Service;
use Illuminate\Database\DatabaseManager;
use Throwable;

class UpdateServiceAction
{
    /**
     * @param  DatabaseManager  $databaseManager
     * @param  SyncServiceBookingsAction  $syncServiceBookingsAction
     * @param  AttachImageFromRequestAction  $attachImageAction
     */
    public function __construct(
        protected DatabaseManager $databaseManager,
        protected SyncServiceBookingsAction $syncServiceBookingsAction,
        protected AttachImageFromRequestAction $attachImageAction,
    ) {
    }

    /**
     * @param  UpdateServiceRequest  $updateServiceRequest
     * @param  Service  $service
     * @param  UpdateServiceData  $updateServiceData
     * @param  ServiceStatus  $status
     *
     * @return Service
     * @throws Throwable
     */
    public function __invoke(
        UpdateServiceRequest $updateServiceRequest,
        Service $service,
        UpdateServiceData $updateServiceData,
        ServiceStatus $status = ServiceStatus::HIDDEN
    ): Service {
        $this
            ->databaseManager
            ->transaction(
                function () use (
                    $updateServiceRequest,
                    $updateServiceData,
                    $service,
                    $status
                ): void {
                    ($this->syncServiceBookingsAction)(
                        $updateServiceData,
                        $service,
                    );

                    ($this->attachImageAction)(
                        $updateServiceRequest,
                        $service,
                    );

                    $service
                        ->categories()
                        ->sync($updateServiceData->categoryIds);

                    $service->update([
                        'title' => $updateServiceData->title,
                        'service_season_from' => $updateServiceData->serviceSeasonFrom,
                        'service_season_to' => $updateServiceData->serviceSeasonTo,
                        'service_all_year' => $updateServiceData->serviceAllYear,
                        'description' => $updateServiceData->description,
                        'about_included' => $updateServiceData->aboutIncluded,
                        'about_additional' => $updateServiceData->aboutIncluded,
                        'status' => $status,
                    ]);
                },
            );

        return $service->loadMissing(['media.model', 'bookings']);
    }
}
