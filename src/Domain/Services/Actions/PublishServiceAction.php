<?php

declare(strict_types=1);

namespace Domain\Services\Actions;

use App\Api\V1\Requests\PublishServiceRequest;
use Domain\Services\DataTransferObjects\UpdateServiceData;
use Domain\Services\Enums\ServiceStatus;
use Domain\Services\Models\Service;
use Domain\Users\Models\User;
use Domain\Users\Notifications\ServiceOnModerationNotification;
use Throwable;

class PublishServiceAction
{
    /** @param  UpdateServiceAction  $updateServiceAction */
    public function __construct(protected UpdateServiceAction $updateServiceAction)
    {
    }

    /**
     * @param  PublishServiceRequest  $request
     * @param  Service  $service
     * @param  UpdateServiceData  $updateServiceData
     *
     * @return Service
     * @throws Throwable
     */
    public function __invoke(
        PublishServiceRequest $request,
        Service $service,
        UpdateServiceData $updateServiceData,
    ): Service {
        $service = ($this->updateServiceAction)(
            $request,
            $service,
            $updateServiceData,
            ServiceStatus::MODERATION,
        );

        User::query()
            ->role('admin', 'web')
            ->first()
            ->notify(new ServiceOnModerationNotification($service));

        return $service;
    }
}
