<?php

declare(strict_types=1);

namespace Domain\Services\Actions;

use Domain\Services\DataTransferObjects\StoreServiceData;
use Domain\Services\Enums\ServiceStatus;
use Domain\Services\Models\Service;
use Illuminate\Contracts\Auth\Factory;

class StoreServiceAction
{
    /** @param  Factory  $factory */
    public function __construct(protected Factory $factory)
    {
    }

    /**
     * @param  StoreServiceData  $storeServiceData
     *
     * @return Service
     */
    public function __invoke(StoreServiceData $storeServiceData): Service
    {
        return Service::query()->create([
            'partner_service_id' => $this
                ->factory
                ->guard()
                ->id(),
            'status' => ServiceStatus::HIDDEN,
        ]);
    }
}
