<?php

declare(strict_types=1);

namespace Domain\Services\Actions;

use Domain\Services\Models\Service;
use Staudenmeir\EloquentJsonRelations\Relations\Postgres\HasMany;

class GetServiceAction
{
    /**
     * @param  Service  $service
     *
     * @return Service
     */
    public function __invoke(Service $service): Service
    {
        $service->loadMissing([
            'categories',
            'bookings' => fn (HasMany $builder) => $builder
                ->orderBy('is_default', 'desc')
                ->orderBy('full_price->amount'),
            'media.model',
        ]);

        return $service;
    }
}
