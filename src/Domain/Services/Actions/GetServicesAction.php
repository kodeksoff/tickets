<?php

declare(strict_types=1);

namespace Domain\Services\Actions;

use Domain\Services\DataTransferObjects\QueryStringData;
use Domain\Services\Models\Service;
use Illuminate\Database\Eloquent\Builder;
use Staudenmeir\EloquentJsonRelations\Relations\BelongsToJson;

class GetServicesAction
{
    /**
     * @param  QueryStringData  $queryStringData
     *
     * @return Builder
     */
    public function __invoke(QueryStringData $queryStringData): Builder
    {
        return Service::query()
            ->with([
                'lowestBooking',
                'media.model',
                'categories' => fn (BelongsToJson $builder): BelongsToJson => $builder->select(
                    'id',
                    'parent_id',
                    'title',
                ),
            ])
            ->categorySorting($queryStringData)
            ->ordering($queryStringData);
    }
}
