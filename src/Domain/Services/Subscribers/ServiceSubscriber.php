<?php

declare(strict_types=1);

namespace Domain\Services\Subscribers;

use Domain\Services\Events\ServiceCreatingEvent;
use Domain\Services\Events\ServiceUpdatingEvent;
use Illuminate\Events\Dispatcher;

class ServiceSubscriber
{
    /**
     * @param  ServiceCreatingEvent|ServiceUpdatingEvent  $event
     *
     * @return void
     */
    public function castCategoriesToInt(ServiceCreatingEvent|ServiceUpdatingEvent $event): void
    {
        $event
            ->service
            ->category_ids = collect($event->service->category_ids)
            ->map(function (string|int $id): int {
                return (int)$id;
            })
            ->toArray();
    }

    /**
     * @param  Dispatcher  $dispatcher
     *
     * @return string[]
     */
    public function subscribe(Dispatcher $dispatcher): array
    {
        return [
            ServiceCreatingEvent::class => 'castCategoriesToInt',
            ServiceUpdatingEvent::class => 'castCategoriesToInt',
        ];
    }
}
