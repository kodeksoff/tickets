<?php

declare(strict_types=1);

namespace Domain\Services\DataTransferObjects;

use Domain\ServiceBooking\DataTransferObjects\StoreBookingTypesData;
use Spatie\LaravelData\Attributes\DataCollectionOf;
use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\DataCollection;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;

#[MapName(SnakeCaseMapper::class)]
class ServiceCreatedData extends Data
{
    public function __construct(
        public readonly ?string $title,
        public readonly ?array $categoryIds,
        public readonly ?int $serviceSeasonFrom,
        public readonly ?int $serviceSeasonTo,
        public readonly ?bool $serviceAllYear,
        #[DataCollectionOf(StoreBookingTypesData::class)]
        public readonly ?DataCollection $bookingTypes,
        public readonly ?string $description,
        public readonly ?string $aboutIncluded,
        public readonly ?string $aboutAdditional,
    ) {
    }
}
