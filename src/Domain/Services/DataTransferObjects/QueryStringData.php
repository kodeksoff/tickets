<?php

declare(strict_types=1);

namespace Domain\Services\DataTransferObjects;

use Domain\Services\Enums\ServiceOrdering;
use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;

#[MapName(SnakeCaseMapper::class)]
class QueryStringData extends Data
{
    /**
     * @param  array|null  $categoryIds
     * @param  string|null  $orderBy
     * @param  string  $orderDirection
     */
    public function __construct(
        public readonly ?array $categoryIds,
        public readonly ?string $orderBy,
        public readonly string $orderDirection = 'asc',
    ) {
    }

    /** @return bool */
    public function isPriceOrdering(): bool
    {
        return $this->orderBy === ServiceOrdering::PRICE();
    }
}
