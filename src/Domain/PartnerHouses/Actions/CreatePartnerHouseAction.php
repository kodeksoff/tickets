<?php

declare(strict_types=1);

namespace Domain\PartnerHouses\Actions;

use Domain\Users\DataTransferObjects\CreateUserData;
use Domain\Users\Enums\UserRole;
use Domain\Users\Models\User;

class CreatePartnerHouseAction
{
    public function __invoke(CreateUserData $createUserData): User
    {
        $createUserData
            ->user
            ->partnerHouse()
            ->firstOrCreate([
                'full_name' => $createUserData->fullName,
                'inn' => $createUserData->inn,
            ], [
                'full_name' => $createUserData->fullName,
                'inn' => $createUserData->inn,
            ]);

        $createUserData
            ->user
            ->assignRole(UserRole::PARTNER_HOUSE->value);

        return $createUserData
            ->user
            ->load(['partnerHouse']);
    }
}
