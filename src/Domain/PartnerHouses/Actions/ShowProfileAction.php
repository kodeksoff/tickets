<?php

declare(strict_types=1);

namespace Domain\PartnerHouses\Actions;

use Illuminate\Contracts\Auth\Factory;
use Throwable;

class ShowProfileAction
{
    public function __construct(protected Factory $authFactory)
    {
    }

    /** @throws Throwable */
    public function __invoke()
    {
        return $this
            ->authFactory
            ->guard()
            ->user()
            ->loadMissing([
                'partnerHouse.guestHouses',
            ]);
    }
}
