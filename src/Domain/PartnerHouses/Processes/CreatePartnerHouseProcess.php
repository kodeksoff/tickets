<?php

declare(strict_types=1);

namespace Domain\PartnerHouses\Processes;

use Closure;
use Domain\PartnerHouses\Actions\CreatePartnerHouseAction;
use Domain\Users\DataTransferObjects\CreateUserData;

class CreatePartnerHouseProcess
{
    public function __construct(protected CreatePartnerHouseAction $createPartnerHouseAction)
    {
    }

    public function __invoke(CreateUserData $createUserData, Closure $next): CreateUserData
    {
        if ($createUserData->isPartnerHouse()) {
            ($this->createPartnerHouseAction)($createUserData);
        }

        return $next($createUserData);
    }
}
