<?php

declare(strict_types=1);

namespace Domain\PartnerHouses\Models;

use Carbon\Carbon;
use Domain\GuestHouses\Models\GuestHouse;
use Domain\Users\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Support\Model;

/**
 * Domain\PartnerHouses\Models\PartnerHouse.
 *
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property int $user_id
 * @property string $full_name
 * @property string|null $inn
 * @property-read \Illuminate\Database\Eloquent\Collection<int, GuestHouse> $guestHouses
 * @property-read int|null $guest_houses_count
 * @property-read User $user
 * @method static \Tests\Factories\PartnerHouseFactory factory($count = null, $state = [])
 * @method static \Support\EloquentBuilder|PartnerHouse newModelQuery()
 * @method static \Support\EloquentBuilder|PartnerHouse newQuery()
 * @method static \Support\EloquentBuilder|PartnerHouse query()
 * @method static \Support\EloquentBuilder|PartnerHouse whereCreatedAt($value)
 * @method static \Support\EloquentBuilder|PartnerHouse whereFullName($value)
 * @method static \Support\EloquentBuilder|PartnerHouse whereId($value)
 * @method static \Support\EloquentBuilder|PartnerHouse whereIn(string $column, string $values, string $boolean = 'and', string $not = false)
 * @method static \Support\EloquentBuilder|PartnerHouse whereInn($value)
 * @method static \Support\EloquentBuilder|PartnerHouse whereUpdatedAt($value)
 * @method static \Support\EloquentBuilder|PartnerHouse whereUserId($value)
 * @mixin \Eloquent
 */
class PartnerHouse extends Model
{
    use HasFactory;

    /** @var string[] */
    protected $fillable = [
        'user_id',
        'full_name',
        'inn',
        'created_at',
        'updated_at',
    ];

    /** @var array<string, string> */
    protected $casts = [
        'created_at' => 'immutable_datetime',
        'updated_at' => 'immutable_datetime',
    ];

    /** @return BelongsTo */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /** @return HasMany */
    public function guestHouses(): HasMany
    {
        return $this->hasMany(GuestHouse::class);
    }
}
