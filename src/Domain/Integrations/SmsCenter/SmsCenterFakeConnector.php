<?php

declare(strict_types=1);

namespace Domain\Integrations\SmsCenter;

class SmsCenterFakeConnector implements SmsCenterConnectorInterface
{
    public function sendConfirmationToken(string $phone, string $message): array
    {
        return [];
    }
}
