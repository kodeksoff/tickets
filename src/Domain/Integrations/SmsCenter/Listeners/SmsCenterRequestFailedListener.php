<?php

declare(strict_types=1);

namespace Domain\Integrations\SmsCenter\Listeners;

use Domain\Integrations\SmsCenter\Events\SmsCenterRequestSent;
use Domain\Users\Models\User;
use Domain\Users\Notifications\SendSmsCenterRequestFailedNotification;
use Illuminate\Contracts\Notifications\Dispatcher;

class SmsCenterRequestFailedListener
{
    /** @param  Dispatcher  $dispatcher */
    public function __construct(public Dispatcher $dispatcher)
    {
    }

    /**
     * @param  SmsCenterRequestSent  $event
     *
     * @return void
     */
    public function handle(SmsCenterRequestSent $event): void
    {
        if ($event->response->failed()) {
            $this
                ->dispatcher
                ->send(
                    User::query()
                        ->role('admin')
                        ->get(),
                    new SendSmsCenterRequestFailedNotification(
                        $event
                            ->response
                            ->toException()
                            ->getMessage()
                    ),
                );
        }
    }
}
