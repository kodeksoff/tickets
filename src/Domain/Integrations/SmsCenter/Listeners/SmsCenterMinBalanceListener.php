<?php

declare(strict_types=1);

namespace Domain\Integrations\SmsCenter\Listeners;

use Domain\Integrations\SmsCenter\Events\SmsCenterRequestSent;
use Domain\Integrations\SmsCenter\SmsCenterSettings;
use Domain\Users\Models\User;
use Domain\Users\Notifications\SendSmsCenterMinBalanceNotification;
use Illuminate\Contracts\Notifications\Dispatcher;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class SmsCenterMinBalanceListener
{
    /** @param  Dispatcher  $dispatcher */
    public function __construct(protected Dispatcher $dispatcher)
    {

    }

    /**
     * @param  SmsCenterRequestSent  $event
     *
     * @return void
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function handle(SmsCenterRequestSent $event): void
    {
        $minBalance = SmsCenterSettings::fromConfig()->minBalance;

        if (
            !$event->response->failed() &&
            $minBalance !== 0 &&
            $event->sendConfirmationMessageResponseData->balance < $minBalance
        ) {
            $this
                ->dispatcher
                ->send(
                    User::query()
                        ->role('admin')
                        ->first(),
                    new SendSmsCenterMinBalanceNotification(
                        $event
                            ->sendConfirmationMessageResponseData
                            ->balance
                    ),
                );
        }
    }
}
