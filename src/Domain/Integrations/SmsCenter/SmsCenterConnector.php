<?php

declare(strict_types=1);

namespace Domain\Integrations\SmsCenter;

use Illuminate\Contracts\Foundation\Application;
use Domain\Integrations\SmsCenter\Auth\BodyAuth;
use Domain\Integrations\SmsCenter\ConfirmationMessage\Requests\SendConfirmationMessageRequest;
use Domain\Integrations\SmsCenter\Exceptions\SmsCenterRequestException;
use Illuminate\Support\Str;
use ReflectionException;
use Saloon\Contracts\Response;
use Saloon\Exceptions\InvalidResponseClassException;
use Saloon\Exceptions\PendingRequestException;
use Saloon\Http\Connector as SaloonConnector;
use Saloon\Traits\Plugins\AcceptsJson;
use Saloon\Traits\Plugins\AlwaysThrowOnErrors;
use Throwable;

class SmsCenterConnector extends SaloonConnector implements SmsCenterConnectorInterface
{
    use AcceptsJson;
    use AlwaysThrowOnErrors;

    /** @param  SmsCenterSettings  $smsCenterSettings */
    public function __construct(protected SmsCenterSettings $smsCenterSettings)
    {
    }

    /** The Base URL of the API. */
    public function resolveBaseUrl(): string
    {
        return $this
            ->smsCenterSettings
            ->baseUri;
    }

    /**
     * @param  Response  $response
     *
     * @return bool|null
     */
    public function hasRequestFailed(Response $response): ?bool
    {
        return Str::contains($response->body(), 'error');
    }

    /**
     * @param  Response  $response
     * @param  Throwable|null  $senderException
     *
     * @return Throwable|null
     */
    public function getRequestException(Response $response, ?Throwable $senderException): ?Throwable
    {
        return SmsCenterRequestException::fromResponse(
            $response,
            $senderException,
        );
    }

    /**
     * @throws InvalidResponseClassException
     * @throws PendingRequestException
     * @throws ReflectionException
     */
    public function sendConfirmationToken(string $phone, string $message): array
    {
        return $this
            ->authenticate(
                new BodyAuth(
                    $this->smsCenterSettings,
                ),
            )
            ->send(
                new SendConfirmationMessageRequest(
                    $phone,
                    $message,
                ),
            )
            ->dtoOrFail();
    }

    /**
     * Default headers for every request.
     *
     * @return string[]
     */
    protected function defaultHeaders(): array
    {
        return [];
    }

    /**
     * Default HTTP client options.
     *
     * @return array{timeout: int}
     */
    protected function defaultConfig(): array
    {
        return [
            'timeout' => $this->smsCenterSettings->timeout,
        ];
    }

    /**
     * @param  Application  $app
     *
     * @return void
     */
    public static function register(Application $app): void
    {
        $app
            ->singleton(
                SmsCenterConnectorInterface::class,
                function (): SmsCenterConnectorInterface {
                    $smsCenterSettings = SmsCenterSettings::fromConfig();

                    if ($smsCenterSettings->fake) {
                        return new SmsCenterFakeConnector();
                    }

                    return new self($smsCenterSettings);
                },
            );
    }
}
