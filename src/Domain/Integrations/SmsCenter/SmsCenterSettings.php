<?php

declare(strict_types=1);

namespace Domain\Integrations\SmsCenter;

use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;
use Support\Utils\SettingsDataTransferObject;

#[MapName(SnakeCaseMapper::class)]
class SmsCenterSettings extends SettingsDataTransferObject
{
    protected static string $configKey = 'services.sms';

    public function __construct(
        public readonly string $baseUri,
        public readonly bool $fake,
        public readonly int $timeout,
        public readonly string $login,
        public readonly string $password,
        public readonly string $sender,
        public readonly int $minBalance,
    ) {
    }
}
