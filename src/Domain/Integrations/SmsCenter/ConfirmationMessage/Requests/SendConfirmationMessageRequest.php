<?php

declare(strict_types=1);

namespace Domain\Integrations\SmsCenter\ConfirmationMessage\Requests;

use Domain\Integrations\SmsCenter\ConfirmationMessage\DataTransferObjects\SendConfirmationMessageResponseData;
use Domain\Integrations\SmsCenter\Enums\ResponseCostFormat;
use Domain\Integrations\SmsCenter\Enums\ResponseFormat;
use Illuminate\Contracts\Container\BindingResolutionException;
use Saloon\Contracts\Body\HasBody;
use Saloon\Contracts\Response;
use Saloon\Enums\Method;
use Saloon\Http\Request;
use Saloon\Traits\Body\HasJsonBody;
use Support\Utils\MobilePhoneFormatter;

class SendConfirmationMessageRequest extends Request implements HasBody
{
    use HasJsonBody;

    /** Define the HTTP method. */
    protected Method $method = Method::POST;

    public function __construct(
        protected string $phone,
        protected string $message,
    ) {
    }

    /** Define the endpoint for the request. */
    public function resolveEndpoint(): string
    {
        return '/rest/send/';
    }

    /**
     * @return array{phone: string, mes: string, fmt: mixed, cost: mixed}
     *
     * @throws BindingResolutionException
     */
    public function defaultBody(): array
    {
        return [
            'phones' => resolve(MobilePhoneFormatter::class)->make($this->phone),
            'mes' => $this->message,
            'fmt' => ResponseFormat::JSON(),
            'cost' => ResponseCostFormat::WITH_BALANCE(),
        ];
    }

    public function createDtoFromResponse(Response $response): SendConfirmationMessageResponseData
    {
        return SendConfirmationMessageResponseData::fromResponse($response);
    }
}
