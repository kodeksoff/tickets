<?php

declare(strict_types=1);

namespace Domain\Integrations\SmsCenter\ConfirmationMessage\Responses;

use Saloon\Http\Response;

class SendConfirmationMessageResponse extends Response
{
}
