<?php

declare(strict_types=1);

namespace Domain\Integrations\SmsCenter\ConfirmationMessage\DataTransferObjects;

use Saloon\Contracts\Response;
use Saloon\Helpers\Arr;
use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;

#[MapName(SnakeCaseMapper::class)]
class SendConfirmationMessageResponseData extends Data
{
    /**
     * @param  int|null  $id
     * @param  int|null  $cost
     * @param  int|null  $balance
     * @param  string|null  $error
     * @param  int|null  $errorCode
     */
    public function __construct(
        public readonly ?int $id,
        public readonly ?int $cost,
        public readonly ?int $balance,
        public readonly ?string $error,
        public readonly ?int $errorCode,
    ) {
    }

    /**
     * @param  Response  $response
     *
     * @return self
     */
    public static function fromResponse(Response $response): self
    {
        $data = $response->json();

        return new self(
            Arr::get($data, 'id'),
            Arr::get($data, 'cost'),
            Arr::get($data, 'balance'),
            Arr::get($data, 'error'),
            Arr::get($data, 'error_code'),
        );
    }
}
