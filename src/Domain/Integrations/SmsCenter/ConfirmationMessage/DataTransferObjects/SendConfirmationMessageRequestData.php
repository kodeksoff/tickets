<?php

declare(strict_types=1);

namespace Domain\Integrations\SmsCenter\ConfirmationMessage\DataTransferObjects;

use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;

#[MapName(SnakeCaseMapper::class)]
class SendConfirmationMessageRequestData extends Data
{
    public function __construct(
        public readonly string $login,
        public readonly string $password,
        public readonly string $sender,
        public readonly string $phone,
        public readonly string $token,
    ) {
    }
}
