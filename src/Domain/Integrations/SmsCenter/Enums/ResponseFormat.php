<?php

declare(strict_types=1);

namespace Domain\Integrations\SmsCenter\Enums;

use ArchTech\Enums\InvokableCases;

/**
 * @method JSON()
 */
enum ResponseFormat: int
{
    use InvokableCases;

    case JSON = 3;
}
