<?php

declare(strict_types=1);

namespace Domain\Integrations\SmsCenter\Enums;

use ArchTech\Enums\InvokableCases;

/**
 * @method WITH_BALANCE()
 */
enum ResponseCostFormat: int
{
    use InvokableCases;

    case WITH_BALANCE = 3;
}
