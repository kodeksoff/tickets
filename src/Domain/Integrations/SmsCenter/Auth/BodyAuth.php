<?php

declare(strict_types=1);

namespace Domain\Integrations\SmsCenter\Auth;

use Domain\Integrations\SmsCenter\SmsCenterSettings;
use Saloon\Contracts\Authenticator;
use Saloon\Contracts\PendingRequest;

class BodyAuth implements Authenticator
{
    public function __construct(protected SmsCenterSettings $smsCenterSettings)
    {
    }

    /** Apply the authentication to the request. */
    public function set(PendingRequest $pendingRequest): void
    {
        $pendingRequest
            ->body()
            ->merge([
                'login' => $this->smsCenterSettings->login,
                'psw' => $this->smsCenterSettings->password,
                'sender' => $this->smsCenterSettings->sender,
            ]);
    }
}
