<?php

declare(strict_types=1);

namespace Domain\Integrations\SmsCenter\Events;

use Domain\Integrations\SmsCenter\ConfirmationMessage\DataTransferObjects\SendConfirmationMessageResponseData;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Saloon\Contracts\Response;

class SmsCenterRequestSent
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    /**
     * @param  Response  $response
     * @param  SendConfirmationMessageResponseData  $sendConfirmationMessageResponseData
     */
    public function __construct(
        public Response $response,
        public SendConfirmationMessageResponseData $sendConfirmationMessageResponseData,
    ) {
    }
}
