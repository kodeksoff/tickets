<?php

declare(strict_types=1);

namespace Domain\Integrations\SmsCenter;

interface SmsCenterConnectorInterface
{
    public function sendConfirmationToken(string $phone, string $message): array;
}
