<?php

declare(strict_types=1);

namespace Domain\Integrations\SmsCenter\Exceptions;

use Exception;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Saloon\Contracts\Response;
use Support\Responses\JsonResponse;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Throwable;

class SmsCenterRequestException extends Exception implements Responsable
{
    /**
     * @param  string  $message
     * @param  int  $code
     * @param  Throwable|null  $throwable
     */
    public function __construct(string $message = '', int $code = 0, ?Throwable $throwable = null)
    {
        parent::__construct($message, $code, $throwable);
    }

    /**
     * @param  Response  $response
     * @param  Throwable|null  $throwable
     *
     * @return self
     */
    public static function fromResponse(
        Response $response,
        ?Throwable $throwable,
    ): self {
        $hasRequestFailed = false;

        if (Str::contains($response->body(), 'error')) {
            $hasRequestFailed = true;
        }

        $message = $hasRequestFailed ? sprintf(
            'SMS Center Error: %s',
            Arr::get($response->json(), 'error'),
        ) : $throwable->getMessage();
        $code = $hasRequestFailed ? 500 : $throwable->getCode();

        return new self($message, $code);
    }

    /**
     * @param $request
     *
     * @return JsonResponse|HttpResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function toResponse($request): JsonResponse|HttpResponse
    {
        return JsonResponse::error(
            $this->message,
            $this->code,
        );
    }
}
