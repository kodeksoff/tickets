<?php

declare(strict_types=1);

namespace Domain\Integrations\DaData\Exceptions;

use Exception;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Arr;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Saloon\Contracts\Response;
use Support\Responses\JsonResponse;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Throwable;

class DaDataRequestException extends Exception implements Responsable, Renderable
{
    /**
     * @param  string  $message
     * @param  int  $code
     * @param  Throwable|null  $throwable
     */
    public function __construct(string $message = '', int $code = 0, ?Throwable $throwable = null)
    {
        parent::__construct($message, $code, $throwable);
    }

    /**
     * @param  Response  $response
     * @param  Throwable|null  $throwable
     *
     * @return self
     */
    public static function fromResponse(
        Response $response,
        ?Throwable $throwable,
    ): self {
        $hasRequestFailed = false;

        if (Arr::has($response->json(), 'message')) {
            $hasRequestFailed = true;
        }

        $message = $hasRequestFailed ? sprintf(
            'Dadata Error: %s',
            Arr::get($response->json(), 'message'),
        ) : $throwable->getMessage();
        $code = $hasRequestFailed ? $throwable->getCode() : 500;

        return new self($message, $code);
    }

    /**
     * @param $request
     *
     * @return JsonResponse|HttpResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function toResponse($request): JsonResponse|HttpResponse
    {
        return JsonResponse::error(
            $this->message,
            $this->code,
        );
    }

    public function render()
    {
        // TODO: Implement render() method.
    }
}
