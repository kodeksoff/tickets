<?php

declare(strict_types=1);

namespace Domain\Integrations\DaData;

use Domain\Integrations\DaData\AddressSuggestions\DataTransferObjects\AddressSuggestionsResponseData;
use Domain\Integrations\DaData\AddressSuggestions\Requests\AddressSuggestionsRequest;
use Domain\Integrations\DaData\Auth\HeaderAuth;
use Domain\Integrations\DaData\Exceptions\DaDataRequestException;
use Illuminate\Contracts\Foundation\Application;
use ReflectionException;
use Saloon\Contracts\Response;
use Saloon\Exceptions\InvalidResponseClassException;
use Saloon\Exceptions\PendingRequestException;
use Saloon\Http\Connector;
use Saloon\Traits\Plugins\AcceptsJson;
use Saloon\Traits\Plugins\AlwaysThrowOnErrors;
use Throwable;

class DaDataConnector extends Connector implements DaDataConnectorInterface
{
    use AcceptsJson;
    use AlwaysThrowOnErrors;

    /** @param  DaDataSettings  $daDataSettings */
    public function __construct(protected DaDataSettings $daDataSettings)
    {
    }

    /**
     * The Base URL of the API
     *
     * @return string
     */
    public function resolveBaseUrl(): string
    {
        return $this
            ->daDataSettings
            ->baseUri;
    }

    /**
     * @param  Response  $response
     * @param  Throwable|null  $senderException
     *
     * @return Throwable|null
     */
    public function getRequestException(Response $response, ?Throwable $senderException): ?Throwable
    {
        return DaDataRequestException::fromResponse(
            $response,
            $senderException,
        );
    }

    /**
     * @param  string  $address
     *
     * @return AddressSuggestionsResponseData
     * @throws ReflectionException
     * @throws InvalidResponseClassException
     * @throws PendingRequestException
     */
    public function getSuggestions(string $address): AddressSuggestionsResponseData
    {
        return $this
            ->authenticate(
                new HeaderAuth(
                    $this->daDataSettings,
                ),
            )
            ->send(
                new AddressSuggestionsRequest(
                    $address,
                ),
            )
            ->dtoOrFail();
    }

    /**
     * Default headers for every request.
     *
     * @return string[]
     */
    protected function defaultHeaders(): array
    {
        return [];
    }

    /**
     * Default HTTP client options.
     *
     * @return array{timeout: int}
     */
    protected function defaultConfig(): array
    {
        return [
            'timeout' => $this->daDataSettings->timeout,
        ];
    }

    /**
     * @param  Application  $app
     *
     * @return void
     */
    public static function register(Application $app): void
    {
        $app
            ->singleton(
                DaDataConnectorInterface::class,
                function (): DaDataConnectorInterface {
                    $daDataSettings = DaDataSettings::fromConfig();

                    if ($daDataSettings->fake) {
                        return new DaDataFakeConnector();
                    }

                    return new self($daDataSettings);
                },
            );
    }
}
