<?php

declare(strict_types=1);

namespace Domain\Integrations\DaData\AddressSuggestions\DataTransferObjects;

use Saloon\Contracts\Response;
use Spatie\LaravelData\Attributes\DataCollectionOf;
use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\DataCollection;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;

#[MapName(SnakeCaseMapper::class)]
class AddressSuggestionsResponseData extends Data
{
    /** @param DataCollection $suggestions */
    public function __construct(
        #[DataCollectionOf(AddressSuggestionData::class)]
        public DataCollection $suggestions,
    ) {
    }

    /**
     * @param Response $response
     *
     * @return AddressSuggestionsResponseData
     */
    public static function fromResponse(Response $response): AddressSuggestionsResponseData
    {
        return new self(
            AddressSuggestionData::collection(
                $response
                    ->collect()
                    ->get('suggestions', []),
            )
        );
    }

    public static function fromArray(array $suggestions): AddressSuggestionsResponseData
    {
        return new self(
            AddressSuggestionData::collection(
                $suggestions
            )
        );
    }
}
