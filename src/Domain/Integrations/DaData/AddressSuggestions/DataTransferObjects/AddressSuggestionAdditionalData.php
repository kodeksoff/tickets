<?php

declare(strict_types=1);

namespace Domain\Integrations\DaData\AddressSuggestions\DataTransferObjects;

use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;

#[MapName(SnakeCaseMapper::class)]
class AddressSuggestionAdditionalData extends Data
{
    public function __construct(
        public readonly ?string $country,
        public readonly ?string $regionWithType,
        public readonly ?string $regionType,
        public readonly ?string $regionTypeFull,
        public readonly ?string $region,
        public readonly ?string $cityWithType,
        public readonly ?string $cityType,
        public readonly ?string $cityTypeFull,
        public readonly ?string $city,
        public readonly ?string $streetWithType,
        public readonly ?string $streetType,
        public readonly ?string $streetTypeFull,
        public readonly ?string $street,
        public readonly ?string $houseType,
        public readonly ?string $houseTypeFull,
        public readonly ?string $house,
    ) {
    }
}
