<?php

declare(strict_types=1);

namespace Domain\Integrations\DaData\AddressSuggestions\DataTransferObjects;

use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;

#[MapName(SnakeCaseMapper::class)]
class AddressSuggestionData extends Data
{
    public function __construct(
        public readonly ?string $value,
        public readonly ?string $unrestrictedValue,
        public readonly AddressSuggestionAdditionalData $data,
    ) {
    }
}
