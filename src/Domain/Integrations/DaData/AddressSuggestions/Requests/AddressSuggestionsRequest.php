<?php

declare(strict_types=1);

namespace Domain\Integrations\DaData\AddressSuggestions\Requests;

use Domain\Integrations\DaData\AddressSuggestions\DataTransferObjects\AddressSuggestionsResponseData;
use Saloon\Contracts\Body\HasBody;
use Saloon\Contracts\Response;
use Saloon\Enums\Method;
use Saloon\Http\Request;
use Saloon\Traits\Body\HasJsonBody;

class AddressSuggestionsRequest extends Request implements HasBody
{
    use HasJsonBody;

    /** Define the HTTP method. */
    protected Method $method = Method::POST;

    /** @param  string  $address */
    public function __construct(
        protected string $address,
    ) {
    }

    /** Define the endpoint for the request. */
    public function resolveEndpoint(): string
    {
        return '/suggestions/api/4_1/rs/suggest/address';
    }

    /** @return array{phone: string, mes: string, fmt: mixed, cost: mixed} */
    public function defaultBody(): array
    {
        return [
            'query' => $this->address,
        ];
    }

    /**
     * @param  Response  $response
     *
     * @return AddressSuggestionsResponseData
     */
    public function createDtoFromResponse(Response $response): AddressSuggestionsResponseData
    {
        return AddressSuggestionsResponseData::fromResponse($response);
    }
}
