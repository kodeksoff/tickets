<?php

declare(strict_types=1);

namespace Domain\Integrations\DaData;

use Domain\Integrations\DaData\AddressSuggestions\DataTransferObjects\AddressSuggestionsResponseData;

interface DaDataConnectorInterface
{
    /**
     * @param string $address
     * @return AddressSuggestionsResponseData
     */
    public function getSuggestions(string $address): AddressSuggestionsResponseData;
}
