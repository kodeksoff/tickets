<?php

declare(strict_types=1);

namespace Domain\Integrations\DaData\Auth;

use Domain\Integrations\DaData\DaDataSettings;
use Saloon\Contracts\Authenticator;
use Saloon\Contracts\PendingRequest;

class HeaderAuth implements Authenticator
{
    public function __construct(protected DaDataSettings $daDataSettings)
    {
    }

    /** Apply the authentication to the request. */
    public function set(PendingRequest $pendingRequest): void
    {
        $pendingRequest
            ->headers()
            ->add('Authorization', 'Token ' . $this->daDataSettings->apiToken)
            ->add('X-Secret', $this->daDataSettings->secretKey);
    }
}
