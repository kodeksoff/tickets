<?php

declare(strict_types=1);

namespace Domain\Integrations\DaData;

use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;
use Support\Utils\SettingsDataTransferObject;

#[MapName(SnakeCaseMapper::class)]
class DaDataSettings extends SettingsDataTransferObject
{
    protected static string $configKey = 'services.dadata';

    public function __construct(
        public readonly bool $fake,
        public readonly string $baseUri,
        public readonly int $timeout,
        public readonly string $apiToken,
        public readonly string $secretKey,
    ) {
    }
}
