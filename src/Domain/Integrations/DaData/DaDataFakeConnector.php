<?php

declare(strict_types=1);

namespace Domain\Integrations\DaData;

use Domain\Integrations\DaData\AddressSuggestions\DataTransferObjects\AddressSuggestionsResponseData;

class DaDataFakeConnector implements DaDataConnectorInterface
{
    /**
     * @param string $address
     * @return AddressSuggestionsResponseData
     */
    public function getSuggestions(string $address): AddressSuggestionsResponseData
    {
        $defaultSuggestions = [
            [
                'value' => 'default',
                'unrestrictedValue' => '889049, Тестовая область, город Для теста, ул Ленина, д 63',
                'data' => [
                    'streetWithType' => 'ул Ленина',
                    'houseType' => 'дом',
                    'house' => '63',
                ],
            ],
        ];

        return AddressSuggestionsResponseData::fromArray($defaultSuggestions);
    }
}
