<?php

declare(strict_types=1);

namespace Domain\Integrations\Yookassa;

use Domain\Acquiring\Contracts\AcquiringInterface;
use YooKassa\Client;
use Illuminate\Contracts\Foundation\Application;
use YooKassa\Common\Exceptions\ApiConnectionException;
use YooKassa\Common\Exceptions\ApiException;
use YooKassa\Common\Exceptions\AuthorizeException;
use YooKassa\Common\Exceptions\BadApiRequestException;
use YooKassa\Common\Exceptions\ExtensionNotFoundException;
use YooKassa\Common\Exceptions\ForbiddenException;
use YooKassa\Common\Exceptions\InternalServerError;
use YooKassa\Common\Exceptions\NotFoundException;
use YooKassa\Common\Exceptions\ResponseProcessingException;
use YooKassa\Common\Exceptions\TooManyRequestsException;
use YooKassa\Common\Exceptions\UnauthorizedException;
use YooKassa\Request\Payments\CreatePaymentRequestInterface;
use YooKassa\Request\Payments\CreatePaymentResponse;

class YookassaConnector implements AcquiringInterface
{
    /** @param  Client  $client */
    public function __construct(protected Client $client)
    {
    }

    /**
     * @throws NotFoundException
     * @throws ApiException
     * @throws ResponseProcessingException
     * @throws BadApiRequestException
     * @throws ExtensionNotFoundException
     * @throws AuthorizeException
     * @throws InternalServerError
     * @throws ForbiddenException
     * @throws TooManyRequestsException
     * @throws UnauthorizedException
     * @throws ApiConnectionException
     */
    public function createPayment(CreatePaymentRequestInterface|array $payment): ?CreatePaymentResponse
    {
        return $this
            ->client
            ->createPayment(
                $payment,
                uniqid('', true),
            );
    }

    /**
     * @param  Application  $app
     *
     * @return void
     */
    public static function register(Application $app): void
    {
        $app
            ->singleton(
                AcquiringInterface::class,
                function (): AcquiringInterface {
                    $config = YookassaSettings::fromConfig();

                    return new self(
                        (new Client())->setAuth(
                            $config->storeId,
                            $config->secretKey,
                        )
                    );
                },
            );
    }
}
