<?php

declare(strict_types=1);

namespace Domain\Integrations\Yookassa;

use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;
use Support\Utils\SettingsDataTransferObject;

#[MapName(SnakeCaseMapper::class)]
class YookassaSettings extends SettingsDataTransferObject
{
    protected static string $configKey = 'services.yookassa';

    public function __construct(
        public readonly string $storeId,
        public readonly string $secretKey,
    ) {
    }
}
