<?php

declare(strict_types=1);

namespace Domain\Integrations\Yookassa\Handlers;

use Domain\Acquiring\Contracts\AcquirerHandler;
use Domain\Integrations\Yookassa\Exceptions\UndefinedAcquirerNotificationException;
use Domain\Orders\Enums\OrderPaymentStatus;
use Domain\Orders\Enums\OrderStatus;
use Domain\Orders\Models\Order;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use YooKassa\Model\Notification\NotificationCanceled;
use YooKassa\Model\Notification\NotificationEventType;
use YooKassa\Model\Notification\NotificationSucceeded;
use YooKassa\Model\Notification\NotificationWaitingForCapture;
use Throwable;

class YookassaAcquirerHandler implements AcquirerHandler
{
    public function __construct()
    {
    }

    /** @throws UndefinedAcquirerNotificationException
     * @throws Throwable
     */
    public function handle(array $request): Order
    {
        $notification = match ($request['event']) {
            NotificationEventType::PAYMENT_SUCCEEDED => new NotificationSucceeded($request),
            NotificationEventType::PAYMENT_WAITING_FOR_CAPTURE => new NotificationWaitingForCapture(
                $request
            ),
            NotificationEventType::PAYMENT_CANCELED => new NotificationCanceled($request),
            default => throw new UndefinedAcquirerNotificationException()
        };

        $notification = $notification->getObject();

        $order = Order::query()
            ->whereHas(
                'payment',
                fn (Builder $builder): Builder => $builder->where('acquirer_payment_id', $notification->getId()),
            )
            ->firstOrFail();

        DB::transaction(function () use ($notification, $order) {
            $order->update([
                'status' => OrderStatus::ACTIVE->value,
            ]);

            $order
                ->payment
                ->update([
                    'status' => OrderPaymentStatus::from($notification->getStatus()),
                ]);
        });

        return $order;
    }
}
