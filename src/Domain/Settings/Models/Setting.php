<?php

declare(strict_types=1);

namespace Domain\Settings\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Support\Model;
use Support\Traits\HasObservableCache;

/**
 * Domain\Settings\Models\Setting
 *
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $environment
 * @property array $contacts
 * @method static \Tests\Factories\SettingFactory factory($count = null, $state = [])
 * @method static \Support\EloquentBuilder|Setting newModelQuery()
 * @method static \Support\EloquentBuilder|Setting newQuery()
 * @method static \Support\EloquentBuilder|Setting query()
 * @method static \Support\EloquentBuilder|Setting whereContacts($value)
 * @method static \Support\EloquentBuilder|Setting whereCreatedAt($value)
 * @method static \Support\EloquentBuilder|Setting whereEnvironment($value)
 * @method static \Support\EloquentBuilder|Setting whereId($value)
 * @method static \Support\EloquentBuilder|Setting whereIn(string $column, string $values, string $boolean = 'and', string $not = false)
 * @method static \Support\EloquentBuilder|Setting whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Setting extends Model
{
    use HasFactory;
    use HasObservableCache;

    /** @var string[] */
    protected $fillable = [
        'environment',
        'contacts',
        'booking_period',
    ];

    /** @var string[] */
    protected $casts = [
        'contacts' => 'array',
    ];
}
