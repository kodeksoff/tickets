<?php

declare(strict_types=1);

namespace Domain\Settings\Actions;

use Domain\Settings\Models\Setting;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Support\Utils\CacheFactory;

class GetSettingsAction
{
    public function __construct(protected CacheFactory $cacheFactory)
    {
    }

    public function __invoke(
        string $environment = 'production',
        array|string $columns = null,
    ): Model {
        return $this
            ->cacheFactory
            ->fromQueryBuilderToResource(
                Setting::query()
                    ->where('environment', $environment)
                    ->when(
                        $columns,
                        fn (Builder $builder) => $builder->select($columns),
                    ),
                sprintf(
                    'settings:%s',
                    is_array($columns) ? implode(':', $columns) : $columns,
                ),
            );
    }
}
