<?php

declare(strict_types=1);

namespace Domain\Notifications\Actions;

use Illuminate\Notifications\DatabaseNotification;

class DeleteNotificationAction
{
    /**
     * @param  DatabaseNotification  $notification
     *
     * @return void
     */
    public function __invoke(DatabaseNotification $notification): void
    {
        $notification->delete();
    }
}
