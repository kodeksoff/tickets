<?php

declare(strict_types=1);

namespace Domain\Notifications\Actions;

use Illuminate\Contracts\Auth\Factory;
use Illuminate\Notifications\DatabaseNotificationCollection;

class GetNotificationsAction
{
    /** @param  Factory  $authFactory */
    public function __construct(protected Factory $authFactory)
    {
    }

    /** @return DatabaseNotificationCollection */
    public function __invoke(): DatabaseNotificationCollection
    {
        return $this
            ->authFactory
            ->guard()
            ->user()
            ->unreadNotifications;
    }
}
