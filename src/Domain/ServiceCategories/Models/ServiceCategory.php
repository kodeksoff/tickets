<?php

declare(strict_types=1);

namespace Domain\ServiceCategories\Models;

use Carbon\Carbon;
use Domain\ServiceCategories\QueryBuilders\ServiceCategoryQueryBuilder;
use Domain\Services\Models\Service;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use SolutionForest\FilamentTree\Concern\ModelTree;
use Staudenmeir\EloquentJsonRelations\HasJsonRelationships;
use Staudenmeir\EloquentJsonRelations\Relations\HasManyJson;
use Staudenmeir\LaravelAdjacencyList\Eloquent\HasRecursiveRelationships;
use Support\Model;
use Support\Scopes\IsPublishedScope;
use Support\Traits\HasObservableCache;

/**
 * Domain\ServiceCategories\Models\ServiceCategory
 *
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property int|null $parent_id
 * @property int $order
 * @property bool $is_published
 * @property string $title
 * @property string|null $description
 * @property-read \Staudenmeir\LaravelAdjacencyList\Eloquent\Collection<int, ServiceCategory> $children
 * @property-read int|null $children_count
 * @property-read ServiceCategory|null $parent
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Collection<int, static> all($columns = ['*'])
 * @method static ServiceCategoryQueryBuilder|ServiceCategory breadthFirst()
 * @method static ServiceCategoryQueryBuilder|ServiceCategory depthFirst()
 * @method static \Tests\Factories\ServiceCategoryFactory factory($count = null, $state = [])
 * @method static \Staudenmeir\LaravelAdjacencyList\Eloquent\Collection<int, static> get($columns = ['*'])
 * @method static ServiceCategoryQueryBuilder|ServiceCategory getExpressionGrammar()
 * @method static ServiceCategoryQueryBuilder|ServiceCategory hasChildren()
 * @method static ServiceCategoryQueryBuilder|ServiceCategory hasParent()
 * @method static ServiceCategoryQueryBuilder|ServiceCategory isLeaf()
 * @method static ServiceCategoryQueryBuilder|ServiceCategory isRoot()
 * @method static ServiceCategoryQueryBuilder|ServiceCategory newModelQuery()
 * @method static ServiceCategoryQueryBuilder|ServiceCategory newQuery()
 * @method static ServiceCategoryQueryBuilder|ServiceCategory ordered(string $direction = 'asc')
 * @method static ServiceCategoryQueryBuilder|ServiceCategory query()
 * @method static ServiceCategoryQueryBuilder|ServiceCategory tree($maxDepth = null)
 * @method static ServiceCategoryQueryBuilder|ServiceCategory treeOf(\Illuminate\Database\Eloquent\Model|callable $constraint, $maxDepth = null)
 * @method static ServiceCategoryQueryBuilder|ServiceCategory whereCreatedAt($value)
 * @method static ServiceCategoryQueryBuilder|ServiceCategory whereDepth($operator, $value = null)
 * @method static ServiceCategoryQueryBuilder|ServiceCategory whereDescription($value)
 * @method static ServiceCategoryQueryBuilder|ServiceCategory whereId($value)
 * @method static ServiceCategoryQueryBuilder|ServiceCategory whereIn(string $column, string $values, string $boolean = 'and', string $not = false)
 * @method static ServiceCategoryQueryBuilder|ServiceCategory whereIsPublished($value)
 * @method static ServiceCategoryQueryBuilder|ServiceCategory whereOrder($value)
 * @method static ServiceCategoryQueryBuilder|ServiceCategory whereParentId($value)
 * @method static ServiceCategoryQueryBuilder|ServiceCategory whereTitle($value)
 * @method static ServiceCategoryQueryBuilder|ServiceCategory whereUpdatedAt($value)
 * @method static ServiceCategoryQueryBuilder|ServiceCategory withGlobalScopes(array $scopes)
 * @method static ServiceCategoryQueryBuilder|ServiceCategory withRelationshipExpression($direction, callable $constraint, $initialDepth, $from = null, $maxDepth = null)
 * @mixin \Eloquent
 */
class ServiceCategory extends Model
{
    use HasFactory;
    use HasObservableCache;
    use HasRecursiveRelationships;
    use HasJsonRelationships;
    use ModelTree;

    /** @var string[] */
    protected $fillable = [
        'parent_id',
        'is_published',
        'title',
        'description',
        'created_at',
        'updated_at',
    ];

    /** @var array<string, string> */
    protected $casts = [
        'is_published' => 'boolean',
        'created_at' => 'immutable_datetime',
        'updated_at' => 'immutable_datetime',
    ];

    /** @return HasManyJson */
    public function services(): HasManyJson
    {
        return $this->hasManyJson(Service::class, 'category_ids', 'id');
    }

    /** @return HasMany */
    public function children(): HasMany
    {
        return $this->hasMany(static::class, $this->getParentKeyName(), $this->getLocalKeyName())->orderBy('title');
    }

    /** @return void */
    protected static function booted(): void
    {
        static::addGlobalScope(new isPublishedScope());
    }

    /**
     * @param $query
     *
     * @return ServiceCategoryQueryBuilder
     */
    public function newEloquentBuilder($query): ServiceCategoryQueryBuilder
    {
        return new ServiceCategoryQueryBuilder($query);
    }

    /**
     * @param  Builder  $query
     *
     * @return Builder
     */
    public function scopeIsRoot(Builder $query): Builder
    {
        return $query->whereNull(
            $this->getParentKeyName(),
        );
    }

    /** @return null */
    public static function defaultParentKey(): null
    {
        return null;
    }

    /** @return string */
    public function getLocalKeyName(): string
    {
        return 'id';
    }

    /** @return string */
    public function getParentKeyName(): string
    {
        return 'parent_id';
    }
}
