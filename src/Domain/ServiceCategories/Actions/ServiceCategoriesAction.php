<?php

declare(strict_types=1);

namespace Domain\ServiceCategories\Actions;

use Domain\ServiceCategories\Models\ServiceCategory;
use Illuminate\Support\Collection;
use Support\Utils\CacheFactory;

/**
 *
 */
class ServiceCategoriesAction
{
    /** @param  CacheFactory  $cacheFactory */
    public function __construct(protected CacheFactory $cacheFactory)
    {
    }

    /** @return Collection */
    public function __invoke(): Collection
    {
        return $this
            ->cacheFactory
            ->fromQueryBuilderToCollection(
                ServiceCategory::query()
                    ->select([
                        'id',
                        'parent_id',
                        'title',
                        'description',
                    ]),
                'category_list',
            );
    }
}
