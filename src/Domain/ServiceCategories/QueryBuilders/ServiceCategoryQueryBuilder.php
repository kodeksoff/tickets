<?php

declare(strict_types=1);

namespace Domain\ServiceCategories\QueryBuilders;

use Staudenmeir\LaravelAdjacencyList\Eloquent\Traits\BuildsAdjacencyListQueries;
use Support\EloquentBuilder;

class ServiceCategoryQueryBuilder extends EloquentBuilder
{
    use BuildsAdjacencyListQueries;
}
