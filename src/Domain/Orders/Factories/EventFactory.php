<?php

declare(strict_types=1);

namespace Domain\Orders\Factories;

use Domain\Acquiring\Exceptions\UndefinedAcquirerException;
use Domain\Orders\Enums\OrderPaymentStatus;
use Domain\Orders\Events\OrderCanceledEvent;
use Domain\Orders\Events\OrderRefundedEvent;
use Domain\Orders\Events\OrderSucceededEvent;
use Domain\Orders\Events\OrderWaitingForCaptureEvent;
use Domain\Orders\Models\Order;
use Illuminate\Contracts\Events\Dispatcher;
use Throwable;

class EventFactory
{
    /** @param  Dispatcher  $dispatcher */
    public function __construct(protected Dispatcher $dispatcher)
    {
    }

    /** @var array|string[] */
    protected static array $events = [
        OrderPaymentStatus::SUCCEEDED->value => OrderSucceededEvent::class,
        OrderPaymentStatus::WAITING_FOR_CAPTURE->value => OrderWaitingForCaptureEvent::class,
        OrderPaymentStatus::CANCELED->value => OrderCanceledEvent::class,
        OrderPaymentStatus::REFUNDED->value => OrderRefundedEvent::class,
    ];

    /**
     * @param  Order  $order
     *
     * @return void
     * @throws Throwable
     */
    public function handle(Order $order): void
    {
        throw_if(!array_key_exists($order->status->value, self::$events), new UndefinedAcquirerException());

        $event = self::$events[$order->payment->status->value];

        $this
            ->dispatcher
            ->dispatch(new $event($order));
    }
}
