<?php

declare(strict_types=1);

namespace Domain\Orders\Listeners;

use Domain\Orders\Events\OrderRefundEvent;
use Domain\Orders\Notifications\OrderRefundOnModeratorNotification;
use Illuminate\Contracts\Notifications\Dispatcher;
use Illuminate\Support\Facades\Notification;

class OrderRefundListener
{
    /** @param  Dispatcher  $dispatcher */
    public function __construct(protected Dispatcher $dispatcher)
    {

    }

    /**
     * @param OrderRefundEvent $event
     * @return void
     */
    public function handle(OrderRefundEvent $event): void
    {
        foreach($event->admins as $admin) {
            Notification::send($admin, new OrderRefundOnModeratorNotification($event->order));
        }

    }
}
