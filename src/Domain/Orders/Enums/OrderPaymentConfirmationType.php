<?php

declare(strict_types=1);

namespace Domain\Orders\Enums;

use ArchTech\Enums\InvokableCases;

enum OrderPaymentConfirmationType: string
{
    use InvokableCases;

    case REDIRECT = 'redirect';
    case EMBEDDED = 'embedded';
}
