<?php

declare(strict_types=1);

namespace Domain\Orders\Enums;

use ArchTech\Enums\InvokableCases;

enum OrderStatus: string
{
    use InvokableCases;
    //todo del
    case NEW = 'new';
    //todo del
    case PENDING = 'pending';

    case ACTIVE = 'active';
    case INACTIVE = 'inactive';
}
