<?php

declare(strict_types=1);

namespace Domain\Orders\Enums;

use ArchTech\Enums\InvokableCases;

enum OrderPaymentStatus: string
{
    use InvokableCases;

    case NEW = 'new';
    case PENDING = 'pending';
    case WAITING_FOR_CAPTURE = 'waiting_for_capture';
    case SUCCEEDED = 'succeeded';
    case CANCELED = 'canceled';
    case REFUNDED = 'refunded';
}
