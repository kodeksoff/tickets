<?php

declare(strict_types=1);

namespace Domain\Orders\Events;

use Illuminate\Foundation\Events\Dispatchable;

class OrderWaitingForCaptureEvent
{
    use Dispatchable;

    public function __construct()
    {
    }
}
