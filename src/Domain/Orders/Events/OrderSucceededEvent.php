<?php

declare(strict_types=1);

namespace Domain\Orders\Events;

use Domain\Orders\Models\Order;
use Illuminate\Foundation\Events\Dispatchable;

class OrderSucceededEvent
{
    use Dispatchable;

    public function __construct(public Order $order)
    {
    }
}
