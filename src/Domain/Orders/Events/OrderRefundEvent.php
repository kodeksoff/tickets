<?php

declare(strict_types=1);

namespace Domain\Orders\Events;

use Domain\Orders\Models\Order;
use Domain\Users\Models\User;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Support\Collection;

class OrderRefundEvent
{
    use Dispatchable;

    public Collection $admins;

    public function __construct(public Order $order)
    {
        $this->admins = User::query()
            ->whereHas('roles', function ($query) {
                $query->where('name', 'admin');
            })
            ->get();
    }
}
