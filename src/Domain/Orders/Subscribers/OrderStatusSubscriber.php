<?php

declare(strict_types=1);

namespace Domain\Orders\Subscribers;

use Domain\Orders\Events\OrderSucceededEvent;
use Domain\Tickets\Actions\StoreTicketAction;
use Illuminate\Events\Dispatcher;

class OrderStatusSubscriber
{
    public function __construct(
        protected StoreTicketAction $storeTicketAction,
    ) {
    }

    /**
     * @param  OrderSucceededEvent  $event
     *
     * @return void
     */
    public function orderSucceeded(OrderSucceededEvent $event): void
    {
        foreach ($event->order->items as $orderItem) {
            ($this->storeTicketAction)($orderItem);
        }
    }

    /**
     * @param  Dispatcher  $dispatcher
     *
     * @return string[]
     */
    public function subscribe(Dispatcher $dispatcher): array
    {
        return [
            OrderSucceededEvent::class => 'orderSucceeded',
        ];
    }
}
