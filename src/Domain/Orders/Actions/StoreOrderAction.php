<?php

declare(strict_types=1);

namespace Domain\Orders\Actions;

use Domain\Acquiring\Actions\CreatePaymentAction;
use Domain\Orders\DataTransferObjects\StoreOrderData;
use Domain\Orders\Models\Order;
use Illuminate\Contracts\Auth\Factory;
use YooKassa\Model\Payment\Confirmation\AbstractConfirmation;
use Throwable;

/**
 *
 */
class StoreOrderAction
{
    /**
     * @param Factory $factory
     * @param StoreOrderItemAction $storeOrderItemAction
     * @param StoreOrderPaymentAction $storeOrderPaymentAction
     * @param CreatePaymentAction $createPaymentAction
     */
    public function __construct(
        protected Factory $factory,
        protected StoreOrderItemAction $storeOrderItemAction,
        protected StoreOrderPaymentAction $storeOrderPaymentAction,
        protected CreatePaymentAction $createPaymentAction
    ) {
    }

    /**
     * @param StoreOrderData $storeOrderData
     * @return AbstractConfirmation
     * @throws Throwable
     */
    public function __invoke(StoreOrderData $storeOrderData)
    {
        $user = $this
            ->factory
            ->guard()
            ->user();

        $order = Order::query()->create([
            'user_id' => $user->id,
            'service_id' => $storeOrderData->serviceId,
            'status' => 'new',
            'full_price' => $storeOrderData->fullPriceAmount,
            'booking_price' => $storeOrderData->bookingPriceAmount,
        ]);

        ($this->storeOrderPaymentAction)($storeOrderData, $order);

        foreach ($storeOrderData->bookingTickets as $bookingTicket) {
            for ($i = 0; $i < $bookingTicket->count; $i++) {
                ($this->storeOrderItemAction)($bookingTicket, $order);
            }
        }

        return ($this->createPaymentAction)($order);
    }
}
