<?php

declare(strict_types=1);

namespace Domain\Orders\Actions;

use Domain\Orders\Events\OrderRefundEvent;
use Domain\Orders\Models\Order;
use Domain\Tickets\Enums\TicketStatuses;
use Illuminate\Events\Dispatcher;

/**
 *
 */
class RefundOrderAction
{
    /** @param Dispatcher $dispatcher */
    public function __construct(
        protected Dispatcher $dispatcher
    ) {
    }

    /** @param Order $order */
    public function __invoke(Order $order): void
    {
        $order->loadMissing([
            'tickets' => function ($query) {
                $query
                    ->where('status', 'active')
                    ->update(['status' => TicketStatuses::RETURN_REQUEST->value]);
            },
        ]);

        $this
            ->dispatcher
            ->dispatch(
                new OrderRefundEvent(
                    $order
                ),
            );
    }
}
