<?php

declare(strict_types=1);

namespace Domain\Orders\Actions;

use Domain\Orders\Enums\OrderStatus;
use Domain\Orders\Models\Order;
use Illuminate\Contracts\Auth\Factory;
use Throwable;

/**
 *
 */
class GetTouristOrders
{
    /** @param Factory $factory */
    public function __construct(
        protected Factory $factory,
    ) {
    }

    /** @throws Throwable */
    public function __invoke()
    {
        $user = $this
            ->factory
            ->guard()
            ->user();

        return Order::query()
            ->where('user_id', $user->id)
            ->whereNotIn(
                'status',
                [
                    OrderStatus::INACTIVE->value,
                ]
            )
            ->with(['service', 'items.ticket'])
            ->withCount('tickets')
            ->get();
    }
}
