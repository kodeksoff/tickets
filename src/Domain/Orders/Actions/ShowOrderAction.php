<?php

declare(strict_types=1);

namespace Domain\Orders\Actions;

use Domain\Orders\Models\Order;
use Illuminate\Contracts\Auth\Factory;

/**
 *
 */
class ShowOrderAction
{
    /** @param Factory $factory */
    public function __construct(
        protected Factory $factory,
    ) {
    }

    /**
     * @param Order $order
     * @return Order
     */
    public function __invoke(Order $order): Order
    {
        //        $user = $this
        //            ->factory
        //            ->guard()
        //            ->user();

        $order->loadMissing([
            'items',
            'tickets',
            'user',
            'user.tourist',
            'user.tourist.guestHouse',
            'service',
        ]);

        return $order;
    }
}
