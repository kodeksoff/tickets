<?php

declare(strict_types=1);

namespace Domain\Orders\Actions;

use Domain\Acquiring\Enums\Acquirer;
use Domain\Orders\DataTransferObjects\StoreOrderData;
use Domain\Orders\Enums\OrderPaymentConfirmationType;
use Domain\Orders\Models\Order;
use Domain\Orders\Models\OrderPayment;

/**
 *
 */
class StoreOrderPaymentAction
{
    public function __construct()
    {
    }

    public function __invoke(StoreOrderData $storeOrderData, Order $order)
    {
        OrderPayment::query()->create([
            'order_id' => $order->id,
            'status' => 'new',
            'is_paid' => false,
            'acquirer' => Acquirer::YOOKASSA->value,
            'confirmation_type' => OrderPaymentConfirmationType::REDIRECT->value,
            'return_url' => $storeOrderData->returnUrl,
        ]);
    }
}
