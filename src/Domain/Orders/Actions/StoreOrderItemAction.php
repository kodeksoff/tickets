<?php

declare(strict_types=1);

namespace Domain\Orders\Actions;

use Domain\Orders\DataTransferObjects\StoreBookingTicketsData;
use Domain\Orders\Models\Order;
use Domain\Orders\Models\OrderItem;

/**
 *
 */
class StoreOrderItemAction
{
    public function __construct()
    {
    }

    public function __invoke(StoreBookingTicketsData $bookingTicket, Order $order)
    {
        return OrderItem::query()->create([
            'order_id' => $order->id,
            'service_booking_id' => $bookingTicket->serviceBookingId,
            'count' => 1,
            'price' => $bookingTicket->fullPriceWithDiscountAmount,
            'full_price' => $bookingTicket->fullPriceWithDiscountAmount,
            'booking_price' => $bookingTicket->servicePriceForBookingAmount,
        ]);
    }
}
