<?php

declare(strict_types=1);

namespace Domain\Orders\Models;

use Brick\Math\BigDecimal;
use Carbon\Carbon;
use Domain\ServiceBooking\Casts\Money;
use Domain\ServiceBooking\Models\ServiceBooking;
use Domain\Tickets\Models\Ticket;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Support\Model;
use Support\Utils\MoneyFactory;

/**
 * Domain\Orders\Models\OrderItem
 *
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property int $order_id
 * @property int $service_booking_id
 * @property int $count
 * @property \Brick\Money\Money|null|null $price
 * @property \Brick\Money\Money|null|null $full_price
 *  * @property \Brick\Money\Money|null|null $booking_price
 * @property-read \Domain\Orders\Models\Order $order
 * @property-read ServiceBooking $serviceBooking
 * @method static \Tests\Factories\OrderItemFactory factory($count = null, $state = [])
 * @method static \Support\EloquentBuilder|OrderItem newModelQuery()
 * @method static \Support\EloquentBuilder|OrderItem newQuery()
 * @method static \Support\EloquentBuilder|OrderItem query()
 * @method static \Support\EloquentBuilder|OrderItem whereCount($value)
 * @method static \Support\EloquentBuilder|OrderItem whereCreatedAt($value)
 * @method static \Support\EloquentBuilder|OrderItem whereFullPrice($value)
 * @method static \Support\EloquentBuilder|OrderItem whereId($value)
 * @method static \Support\EloquentBuilder|OrderItem whereIn(string $column, string $values, string $boolean = 'and', string $not = false)
 * @method static \Support\EloquentBuilder|OrderItem whereOrderId($value)
 * @method static \Support\EloquentBuilder|OrderItem wherePrice($value)
 * @method static \Support\EloquentBuilder|OrderItem whereServiceBookingId($value)
 * @method static \Support\EloquentBuilder|OrderItem whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class OrderItem extends Model
{
    use HasFactory;

    /** @var string[] */
    protected $fillable = [
        'order_id',
        'service_booking_id',
        'count',
        'price',
        'full_price',
        'booking_price',
        'created_at',
        'updated_at',
    ];

    /** @var array */
    protected $casts = [
        'price' => Money::class,
        'full_price' => Money::class,
        'booking_price' => Money::class,
        'created_at' => 'immutable_datetime',
        'updated_at' => 'immutable_datetime',
    ];

    /** @return BelongsTo */
    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }

    /** @return BelongsTo */
    public function serviceBooking(): BelongsTo
    {
        return $this->belongsTo(ServiceBooking::class);
    }

    /** @return HasOne */
    public function ticket(): hasOne
    {
        return $this->hasOne(Ticket::class);
    }

    /** @return Attribute */
    public function fullPriceAmount(): Attribute
    {
        return new Attribute(
            fn (): BigDecimal => $this
                ->full_price
                ->getAmount()
        );
    }

    /** @return Attribute */
    public function bookingPriceAmount(): Attribute
    {
        return new Attribute(
            fn (): BigDecimal => $this
                ->booking_price
                ->getAmount()
        );
    }

    /** @return Attribute */
    public function surchargePriceAmount(): Attribute
    {
        return new Attribute(
            fn () => resolve(MoneyFactory::class)
                ->ofMinor(
                    $this
                        ->booking_price
                        ->minus($this->full_price)
                        ->getMinorAmount()
                        ->toInt()
                )
                ->getAmount()
        );
    }
}
