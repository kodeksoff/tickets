<?php

declare(strict_types=1);

namespace Domain\Orders\Models;

use Carbon\Carbon;
use Domain\Acquiring\Enums\Acquirer;
use Domain\Orders\Enums\OrderPaymentConfirmationType;
use Domain\Orders\Enums\OrderPaymentStatus;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Support\Model;

/**
 * Domain\Orders\Models\OrderPayment
 *
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property int $order_id
 * @property OrderPaymentStatus $status
 * @property string|null $acquirer_payment_id
 * @property string $acquirer
 * @property string $confirmation_type
 * @property string|null $return_url
 * @property bool $is_paid
 * @property mixed|null $acquirer_response
 * @property-read \Domain\Orders\Models\Order $order
 * @method static \Tests\Factories\OrderPaymentFactory factory($count = null, $state = [])
 * @method static \Support\EloquentBuilder|OrderPayment newModelQuery()
 * @method static \Support\EloquentBuilder|OrderPayment newQuery()
 * @method static \Support\EloquentBuilder|OrderPayment query()
 * @method static \Support\EloquentBuilder|OrderPayment whereAcquirer($value)
 * @method static \Support\EloquentBuilder|OrderPayment whereAcquirerPaymentId($value)
 * @method static \Support\EloquentBuilder|OrderPayment whereAcquirerResponse($value)
 * @method static \Support\EloquentBuilder|OrderPayment whereConfirmationType($value)
 * @method static \Support\EloquentBuilder|OrderPayment whereCreatedAt($value)
 * @method static \Support\EloquentBuilder|OrderPayment whereId($value)
 * @method static \Support\EloquentBuilder|OrderPayment whereIn(string $column, string $values, string $boolean = 'and', string $not = false)
 * @method static \Support\EloquentBuilder|OrderPayment whereIsPaid($value)
 * @method static \Support\EloquentBuilder|OrderPayment whereOrderId($value)
 * @method static \Support\EloquentBuilder|OrderPayment whereReturnUrl($value)
 * @method static \Support\EloquentBuilder|OrderPayment whereStatus($value)
 * @method static \Support\EloquentBuilder|OrderPayment whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class OrderPayment extends Model
{
    use HasFactory;

    /** @var string[] */
    protected $fillable = [
        'order_id',
        'status',
        'acquirer_payment_id',
        'acquirer',
        'confirmation_type',
        'return_url',
        'is_paid',
        'acquirer_response',
        'created_at',
        'updated_at',
    ];

    /** @var array */
    protected $casts = [
        'confirmation_type' => OrderPaymentConfirmationType::class,
        'is_paid' => 'boolean',
        'status' => OrderPaymentStatus::class,
        'acquirer' => Acquirer::class,
        'acquirer_response' => 'array',
        'created_at' => 'immutable_datetime',
        'updated_at' => 'immutable_datetime',
    ];

    /** @return BelongsTo */
    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }
}
