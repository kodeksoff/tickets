<?php

declare(strict_types=1);

namespace Domain\Orders\Models;

use Brick\Math\BigDecimal;
use Carbon\Carbon;
use Domain\Orders\Enums\OrderStatus;
use Domain\ServiceBooking\Casts\Money;
use Domain\Services\Models\Service;
use Domain\Tickets\Models\Ticket;
use Domain\Users\Models\User;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Support\Model;
use Support\Utils\MoneyFactory;

/**
 * Domain\Orders\Models\Order
 *
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property int|null $user_id
 * @property int|null $service_id
 * @property string $acquirer
 * @property OrderStatus $status
 * @property bool $is_paid
 * @property \Brick\Money\Money|null|null $full_price
 * @property \Brick\Money\Money|null|null $booking_price
 * @property array|null $data
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \Domain\Orders\Models\OrderItem> $items
 * @property-read int|null $items_count
 * @property-read Service|null $service
 * @property-read User|null $user
 * @method static \Tests\Factories\OrderFactory factory($count = null, $state = [])
 * @method static \Support\EloquentBuilder|Order newModelQuery()
 * @method static \Support\EloquentBuilder|Order newQuery()
 * @method static \Support\EloquentBuilder|Order query()
 * @method static \Support\EloquentBuilder|Order whereAcquirer($value)
 * @method static \Support\EloquentBuilder|Order whereCreatedAt($value)
 * @method static \Support\EloquentBuilder|Order whereData($value)
 * @method static \Support\EloquentBuilder|Order whereFullPrice($value)
 * @method static \Support\EloquentBuilder|Order whereId($value)
 * @method static \Support\EloquentBuilder|Order whereIn(string $column, string $values, string $boolean = 'and', string $not = false)
 * @method static \Support\EloquentBuilder|Order whereIsPaid($value)
 * @method static \Support\EloquentBuilder|Order whereServiceId($value)
 * @method static \Support\EloquentBuilder|Order whereStatus($value)
 * @method static \Support\EloquentBuilder|Order whereUpdatedAt($value)
 * @method static \Support\EloquentBuilder|Order whereUserId($value)
 * @property string $confirmation_type
 * @method static \Support\EloquentBuilder|Order whereConfirmationType($value)
 * @property string|null $external_id
 * @property array|null $acquirer_response
 * @method static \Support\EloquentBuilder|Order whereAcquirerResponse($value)
 * @method static \Support\EloquentBuilder|Order whereExternalId($value)
 * @property-read \Domain\Orders\Models\OrderPayment|null $payment
 * @mixin \Eloquent
 */
class Order extends Model
{
    use HasFactory;

    /** @var string[] */
    protected $fillable = [
        'user_id',
        'service_id',
        'status',
        'full_price',
        'booking_price',
        'created_at',
        'updated_at',
    ];

    /** @var array */
    protected $casts = [
        'status' => OrderStatus::class,
        'full_price' => Money::class,
        'booking_price' => Money::class,
        'created_at' => 'immutable_datetime',
        'updated_at' => 'immutable_datetime',
    ];

    /** @return HasMany */
    public function items(): HasMany
    {
        return $this->hasMany(OrderItem::class);
    }

    /** @return HasOne */
    public function payment(): HasOne
    {
        return $this->hasOne(OrderPayment::class);
    }

    /** @return BelongsTo */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /** @return BelongsTo */
    public function service(): BelongsTo
    {
        return $this->belongsTo(Service::class);
    }

    public function tickets(): HasManyThrough
    {
        return $this->hasManyThrough(Ticket::class, OrderItem::class);
    }

    /** @return Attribute */
    public function fullPriceAmount(): Attribute
    {
        return new Attribute(
            fn (): BigDecimal => $this
                ->full_price
                ->getAmount()
        );
    }

    /** @return Attribute */
    public function bookingPriceAmount(): Attribute
    {
        return new Attribute(
            fn (): BigDecimal => $this
                ->booking_price
                ->getAmount()
        );
    }

    /** @return Attribute */
    public function surchargePriceAmount(): Attribute
    {
        return new Attribute(
            fn () => resolve(MoneyFactory::class)
                ->ofMinor(
                    $this
                        ->booking_price
                        ->minus($this->full_price)
                        ->getMinorAmount()
                        ->toInt()
                )
                ->getAmount()
        );
    }
}
