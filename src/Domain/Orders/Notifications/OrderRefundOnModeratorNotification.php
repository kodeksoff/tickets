<?php

declare(strict_types=1);

namespace Domain\Orders\Notifications;

use Domain\Orders\Models\Order;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification as BaseNotification;

class OrderRefundOnModeratorNotification extends BaseNotification
{
    use Queueable;

    /** @param Order $order */
    public function __construct(protected Order $order)
    {
    }

    /**
     * @param object $notifiable
     *
     * @return string[]
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    public function toMail(): MailMessage
    {
        return (new MailMessage())
            ->subject('Заявка на возврат купона ' . $this->order->id .
                ' от пользователя ' . $this->order->user->full_name .
                ' '. $this->order->user->phone)
            ->greeting(
                'Пользователь '.$this->order->user->full_name.
                ' '. $this->order->user->phone .
                ' запросил возврат купона '. $this->order->id.
                ' в '. now()
            )
            ->action(
                'Просмотреть услугу',
                route('filament.resources.orders.edit', $this->order->id),
            );
    }

    /**
     * @param object $notifiable
     *
     * @return array
     * @throws Exception
     */
    //    public function toDatabase(object $notifiable): array
    //    {
    //        return Notification::make()
    //            ->title('Новая услуга на модерации')
    //            ->body($this->service->title)
    //            ->actions([
    //                Action::make('Просмотреть')
    //                    ->url(route('filament.resources.services.edit', $this->service->id))
    //                    ->link(),
    //            ])
    //            ->success()
    //            ->getDatabaseMessage();
    //    }
}
