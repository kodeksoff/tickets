<?php

declare(strict_types=1);

namespace Domain\Orders\DataTransferObjects;

use Brick\Money\Money;
use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Attributes\WithCast;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;
use Support\DataCasts\MoneyCast;

#[MapName(SnakeCaseMapper::class)]
class StoreBookingTicketsData extends Data
{
    /**
     * @param int|null $serviceBookingId
     * @param Money $fullPrice
     * @param Money $discountPrice
     * @param Money $fullPriceWithDiscount
     * @param Money $servicePriceForBooking
     * @param int|null $count
     */
    public function __construct(
        public readonly ?int $serviceBookingId,
        #[WithCast(MoneyCast::class)]
        public readonly Money $fullPriceAmount,
        #[WithCast(MoneyCast::class)]
        public readonly Money $discountPriceAmount,
        #[WithCast(MoneyCast::class)]
        public readonly Money $fullPriceWithDiscountAmount,
        #[WithCast(MoneyCast::class)]
        public readonly Money $servicePriceForBookingAmount,
        public readonly ?int $count,
    ) {
    }
}
