<?php

declare(strict_types=1);

namespace Domain\Orders\DataTransferObjects;

use Brick\Money\Money;
use Spatie\LaravelData\Attributes\DataCollectionOf;
use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Attributes\WithCast;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\DataCollection;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;
use Support\DataCasts\MoneyCast;

#[MapName(SnakeCaseMapper::class)]
class StoreOrderData extends Data
{
    public function __construct(
        public readonly ?int $serviceId,
        public readonly ?string $returnUrl,
        #[WithCast(MoneyCast::class)]
        public readonly Money $fullPriceAmount,
        #[WithCast(MoneyCast::class)]
        public readonly Money $bookingPriceAmount,
        #[DataCollectionOf(StoreBookingTicketsData::class)]
        public readonly ?DataCollection $bookingTickets,
    ) {
    }
}
