<?php

declare(strict_types=1);

namespace Domain\Tickets\Actions;

use BaconQrCode\Renderer\Image\SvgImageBackEnd;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;
use Domain\Orders\Models\OrderItem;
use Domain\Settings\Models\Setting;
use Domain\Tickets\Models\Ticket;

/**
 *
 */
class StoreTicketAction
{
    public function __construct()
    {
    }

    public function __invoke(OrderItem $orderItem): Ticket
    {
        $bookingPeriod = Setting::query()
            ->where('environment', 'production')
            ->select('booking_period')
            ->first()
            ->booking_period;

        $ticket = Ticket::query()->create([
            'order_item_id' => $orderItem->id,
            'service_booking_id' => $orderItem->service_booking_id,
            'status' => 'active',
            'closed_at' => now()->addDays($bookingPeriod),
        ]);

        $renderer = new ImageRenderer(
            new RendererStyle(400),
            new SvgImageBackEnd()
        );
        $writer = new Writer($renderer);
        $writer->writeFile($ticket->code, 'storage/qrcode.svg');

        $ticket->addMedia('storage/qrcode.svg')
            ->toMediaCollection('qr_image');

        return $ticket;
    }
}
