<?php

declare(strict_types=1);

namespace Domain\Tickets\Actions;

use Domain\Tickets\DataTransferObjects\UseTicketData;
use Domain\Tickets\Enums\TicketStatuses;
use Domain\Tickets\Models\Ticket;
use Illuminate\Support\Facades\Log;
use Error;

/**
 *
 */
class UseTicketAction
{
    public function __construct()
    {
    }

    public function __invoke(Ticket $ticket, UseTicketData $useTicketData)
    {
        if ($ticket->status === TicketStatuses::ACTIVE->value
            && $ticket->code === $useTicketData->code) {
            Log::debug($ticket->code);
            $ticket->update(['status' => TicketStatuses::USED->value]);
        } else {
            throw new Error('Введен неверный код');
        }
    }
}
