<?php

declare(strict_types=1);

namespace Domain\Tickets\DataTransferObjects;

use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;

#[MapName(SnakeCaseMapper::class)]
class UseTicketData extends Data
{
    public function __construct(
        public readonly int $code,
    ) {
    }
}
