<?php

declare(strict_types=1);

namespace Domain\Tickets\Models;

use Domain\Orders\Models\OrderItem;
use Domain\ServiceBooking\Models\ServiceBooking;
use Domain\Tickets\Enums\TicketStatuses;
use Domain\Tickets\Events\TicketCreateEvent;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Domain\Orders\Models\Order;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;
use Laravel\Scout\Searchable;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Staudenmeir\EloquentJsonRelations\HasJsonRelationships;
use Support\Model;

/**
 * @method static \Tests\Factories\TicketFactory factory($count = null, $state = [])
 */
class Ticket extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;
    use Searchable;
    use HasJsonRelationships;

    /** @var string[] */
    protected $dispatchesEvents = [
        'creating' => TicketCreateEvent::class,
    ];
    /** @var string[] */
    protected $fillable = [
        'order_item_id',
        'service_booking_id',
        'number',
        'code',
        'status',
        'created_at',
        'updated_at',
        'closed_at',
    ];

    /** @var array */
    protected $casts = [
        'status' => TicketStatuses::class,
        'created_at' => 'immutable_datetime',
        'updated_at' => 'immutable_datetime',
        'closed_at' => 'immutable_datetime',
    ];

    /** @return HasOne */
    public function orderItem(): hasOne
    {
        return $this->hasOne(OrderItem::class);
    }

    /** @return BelongsTo */
    public function serviceBooking(): BelongsTo
    {
        return $this->belongsTo(ServiceBooking::class);
    }

    /** @return HasOneThrough */
    public function order(): HasOneThrough
    {
        return $this->hasOneThrough(OrderItem::class, Order::class);
    }
}
