<?php

declare(strict_types=1);

namespace Domain\Tickets\Events;

use Domain\Tickets\Models\Ticket;
use Illuminate\Foundation\Events\Dispatchable;

class TicketCreateEvent
{
    use Dispatchable;

    /** @param Ticket $ticket */
    public function __construct(public Ticket $ticket)
    {
    }
}
