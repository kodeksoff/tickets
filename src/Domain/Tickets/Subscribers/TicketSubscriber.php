<?php

declare(strict_types=1);

namespace Domain\Tickets\Subscribers;

use Domain\Tickets\Events\TicketCreateEvent;
use Domain\Tickets\Models\Ticket;
use Exception;
use Illuminate\Events\Dispatcher;

class TicketSubscriber
{
    /** @throws Exception */
    public function generateTicketCode(TicketCreateEvent $event): void
    {
        do {
            $code = random_int(10000, 99999);
        } while (
            Ticket::query()
                ->where('code', $code)
                ->first()
        );

        $event->ticket->code = $code;
    }

    public function generateTicketNumber(TicketCreateEvent $event): void
    {
        do {
            $number = random_int(100000, 999999);
        } while (
            Ticket::query()
                ->where('number', $number)
                ->first()
        );

        $event->ticket->number = $number;
    }

    public function subscribe(Dispatcher $dispatcher): array
    {
        return [
            TicketCreateEvent::class => ['generateTicketCode', 'generateTicketNumber'],
        ];
    }
}
