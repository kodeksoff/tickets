<?php

declare(strict_types=1);

namespace Domain\Tickets\Enums;

use ArchTech\Enums\InvokableCases;

/**
 */
enum TicketStatuses: string
{
    use InvokableCases;

    case ACTIVE = 'active';
    case OVERDUE = 'overdue';
    case USED = 'used';
    case RETURN_REQUEST = 'return_request';
    case CANCELED = 'canceled';
}
