<?php

declare(strict_types=1);

namespace Domain\Acquiring\Exceptions;

use Exception;
use Illuminate\Contracts\Support\Responsable;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Support\Responses\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class UndefinedAcquirerException extends Exception implements Responsable
{
    protected $message = 'Неизвестный тип эквайринга';

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function toResponse($request): JsonResponse
    {
        return JsonResponse::error(
            $this->message,
            Response::HTTP_BAD_REQUEST,
        );
    }
}
