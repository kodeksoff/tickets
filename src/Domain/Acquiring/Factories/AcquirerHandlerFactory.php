<?php

declare(strict_types=1);

namespace Domain\Acquiring\Factories;

use Domain\Acquiring\Contracts\AcquirerHandler;
use Domain\Acquiring\Enums\Acquirer;
use Domain\Acquiring\Exceptions\UndefinedAcquirerException;
use Domain\Integrations\Yookassa\Handlers\YookassaAcquirerHandler;
use Throwable;

class AcquirerHandlerFactory
{
    /** @var array|string[] */
    protected static array $acquirers = [
        Acquirer::YOOKASSA->value => YookassaAcquirerHandler::class,
    ];

    /**
     * @param  Acquirer  $acquirer
     *
     * @return AcquirerHandler
     * @throws Throwable
     */
    public function acquirer(Acquirer $acquirer): AcquirerHandler
    {
        throw_if(!array_key_exists($acquirer->value, self::$acquirers), new UndefinedAcquirerException());

        $acquirerHandler = self::$acquirers[$acquirer->value];

        return new $acquirerHandler();
    }
}
