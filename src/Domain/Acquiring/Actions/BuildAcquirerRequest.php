<?php

declare(strict_types=1);

namespace Domain\Acquiring\Actions;

use Domain\Orders\Models\Order;
use Domain\Orders\Models\OrderItem;
use YooKassa\Common\AbstractRequestInterface;
use YooKassa\Request\Payments\CreatePaymentRequestBuilder;
use YooKassa\Request\Payments\CreatePaymentRequestInterface;

class BuildAcquirerRequest
{
    public function __construct(
        protected CreatePaymentRequestBuilder $paymentBuilder,
        protected BuildConfirmationTypeAction $buildConfirmationTypeAction,
    ) {
    }

    public function __invoke(Order $order): AbstractRequestInterface|CreatePaymentRequestInterface
    {
        $builder = $this->paymentBuilder;
        $builder
            ->setAmount(
                $order
                    ->full_price
                    ->getAmount()
                    ->toFloat(),
            )
            ->setCurrency(
                $order
                    ->full_price
                    ->getCurrency()
                    ->getCurrencyCode(),
            )
            ->setCapture(true)
            ->setDescription(mb_strimwidth($order->service->title, 0, 128, '...'))
            ->setConfirmation(
                ($this->buildConfirmationTypeAction)(
                    $order->payment,
                ),
            )
            ->setReceiptEmail($order->user->email)
            ->setReceiptPhone(strval($order->user->phone))
            ->setMetadata([
                'order_id' => $order->id,
            ]);

        $order
            ->items
            ->each(function (OrderItem $orderItem) use ($builder) {
                $builder->addReceiptItem(
                    $orderItem->serviceBooking->title,
                    (string)$orderItem->price->getAmount(),
                    $orderItem->count,
                    2,
                );
            });

        return $builder->build();
    }
}
