<?php

declare(strict_types=1);

namespace Domain\Acquiring\Actions;

use Domain\Orders\Enums\OrderPaymentConfirmationType;
use Domain\Orders\Models\OrderPayment;
use YooKassa\Model\Payment\ConfirmationType;

class BuildConfirmationTypeAction
{
    /**
     * @param  OrderPayment  $orderPayment
     *
     * @return array
     */
    public function __invoke(OrderPayment $orderPayment): array
    {
        if ($orderPayment->confirmation_type === OrderPaymentConfirmationType::REDIRECT) {
            return [
                'type' => ConfirmationType::REDIRECT,
                'returnUrl' => $orderPayment->return_url,
            ];
        }

        return [
            'type' => ConfirmationType::EMBEDDED,
        ];
    }
}
