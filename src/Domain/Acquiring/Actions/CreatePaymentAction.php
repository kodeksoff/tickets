<?php

declare(strict_types=1);

namespace Domain\Acquiring\Actions;

use Domain\Acquiring\Contracts\AcquiringInterface;
use Domain\Orders\Enums\OrderPaymentStatus;
use Domain\Orders\Enums\OrderStatus;
use Domain\Orders\Exceptions\PaymentNotCreated;
use Domain\Orders\Models\Order;
use Illuminate\Database\DatabaseManager;
use YooKassa\Model\Payment\Confirmation\AbstractConfirmation;
use YooKassa\Request\Payments\CreatePaymentRequestBuilder;
use Throwable;

class CreatePaymentAction
{
    /**
     * @param  AcquiringInterface  $acquiring
     * @param  BuildAcquirerRequest  $buildAcquirerRequest
     * @param  CreatePaymentRequestBuilder  $paymentBuilder
     * @param  DatabaseManager  $databaseManager
     */
    public function __construct(
        protected AcquiringInterface $acquiring,
        protected BuildAcquirerRequest $buildAcquirerRequest,
        protected CreatePaymentRequestBuilder $paymentBuilder,
        protected DatabaseManager $databaseManager,
    ) {
    }

    /**
     * @param  Order  $order
     *
     * @return AbstractConfirmation
     * @throws Throwable
     */
    public function __invoke(Order $order): AbstractConfirmation
    {
        $order->loadMissing([
            'payment',
            'items.serviceBooking',
            'user.tourist',
            'service',
        ]);

        throw_if(!$order->payment, PaymentNotCreated::class);

        $request = $this
            ->acquiring
            ->createPayment(
                ($this->buildAcquirerRequest)(
                    $order,
                ),
            );

        $this
            ->databaseManager
            ->transaction(function () use ($request, $order) {
                $order->update([
                    //'status' => OrderStatus::PENDING,
                ]);

                $order
                    ->payment
                    ->update([
                        'status' => OrderPaymentStatus::PENDING,
                        'acquirer_payment_id' => $request->getId(),
                        'acquirer_response' => $request->toArray(),
                    ]);
            });

        return $request->getConfirmation();
    }
}
