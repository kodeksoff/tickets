<?php

declare(strict_types=1);

namespace Domain\Acquiring\Actions;

use Domain\Acquiring\Enums\Acquirer;
use Domain\Acquiring\Jobs\WebhookHandleJob;
use Illuminate\Contracts\Bus\Dispatcher;
use Illuminate\Http\Request;

class HandleWebhookAction
{
    public function __construct(protected Dispatcher $dispatcher)
    {
    }

    public function __invoke(Request $request, Acquirer $acquiring): void
    {
        $this
            ->dispatcher
            ->dispatch(new WebhookHandleJob(
                $request->all(),
                $acquiring
            ));
    }
}
