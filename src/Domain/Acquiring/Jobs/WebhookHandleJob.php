<?php

declare(strict_types=1);

namespace Domain\Acquiring\Jobs;

use Domain\Acquiring\Enums\Acquirer;
use Domain\Acquiring\Factories\AcquirerHandlerFactory;
use Domain\Orders\Factories\EventFactory;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Throwable;

class WebhookHandleJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * @param  Request  $request
     * @param  Acquirer  $acquiring
     */
    public function __construct(protected array $request, protected Acquirer $acquiring)
    {
    }

    /**
     * @param  AcquirerHandlerFactory  $acquirerHandlerFactory
     * @param  EventFactory  $eventFactory
     *
     * @return void
     * @throws Throwable
     */
    public function handle(AcquirerHandlerFactory $acquirerHandlerFactory, EventFactory $eventFactory): void
    {
        $eventFactory->handle(
            $acquirerHandlerFactory
                ->acquirer($this->acquiring)
                ->handle($this->request),
        );
    }
}
