<?php

declare(strict_types=1);

namespace Domain\Acquiring\Contracts;

use Illuminate\Contracts\Foundation\Application;
use YooKassa\Request\Payments\CreatePaymentRequestInterface;
use YooKassa\Request\Payments\CreatePaymentResponse;

interface AcquiringInterface
{
    /**
     * @param  CreatePaymentRequestInterface|array  $payment
     *
     * @return CreatePaymentResponse|null
     */
    public function createPayment(CreatePaymentRequestInterface|array $payment): ?CreatePaymentResponse;

    /**
     * @param  Application  $app
     *
     * @return void
     */
    public static function register(Application $app): void;
}
