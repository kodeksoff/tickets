<?php

declare(strict_types=1);

namespace Domain\Acquiring\Contracts;

use Domain\Orders\Models\Order;
use Illuminate\Http\Request;

interface AcquirerHandler
{
    /**
     * @param  Request  $request
     *
     * @return Order
     */
    public function handle(array $request): Order;
}
