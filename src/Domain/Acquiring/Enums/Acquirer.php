<?php

declare(strict_types=1);

namespace Domain\Acquiring\Enums;

use ArchTech\Enums\InvokableCases;

/**
 * @method YOOKASSA(): string;
 */
enum Acquirer: string
{
    use InvokableCases;

    case YOOKASSA = 'yookassa';
}
