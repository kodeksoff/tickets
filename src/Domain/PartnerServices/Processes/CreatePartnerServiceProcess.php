<?php

declare(strict_types=1);

namespace Domain\PartnerServices\Processes;

use Closure;
use Domain\PartnerServices\Actions\CreatePartnerServiceAction;
use Domain\Users\DataTransferObjects\CreateUserData;

class CreatePartnerServiceProcess
{
    public function __construct(protected CreatePartnerServiceAction $createPartnerServiceAction)
    {
    }

    public function __invoke(CreateUserData $createUserData, Closure $next): CreateUserData
    {
        if ($createUserData->isPartnerService()) {
            ($this->createPartnerServiceAction)($createUserData);
        }

        return $next($createUserData);
    }
}
