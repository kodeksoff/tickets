<?php

declare(strict_types=1);

namespace Domain\PartnerServices\Models;

use Carbon\Carbon;
use Domain\Services\Models\Service;
use Domain\Users\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Support\Model;

/**
 * Domain\PartnerServices\Models\PartnerService.
 *
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property int $user_id
 * @property string $full_name
 * @property string|null $inn
 * @property-read \Illuminate\Database\Eloquent\Collection<int, Service> $services
 * @property-read int|null $services_count
 * @property-read User $user
 * @method static \Tests\Factories\PartnerServiceFactory factory($count = null, $state = [])
 * @method static \Support\EloquentBuilder|PartnerService newModelQuery()
 * @method static \Support\EloquentBuilder|PartnerService newQuery()
 * @method static \Support\EloquentBuilder|PartnerService query()
 * @method static \Support\EloquentBuilder|PartnerService whereCreatedAt($value)
 * @method static \Support\EloquentBuilder|PartnerService whereFullName($value)
 * @method static \Support\EloquentBuilder|PartnerService whereId($value)
 * @method static \Support\EloquentBuilder|PartnerService whereIn(string $column, string $values, string $boolean = 'and', string $not = false)
 * @method static \Support\EloquentBuilder|PartnerService whereInn($value)
 * @method static \Support\EloquentBuilder|PartnerService whereUpdatedAt($value)
 * @method static \Support\EloquentBuilder|PartnerService whereUserId($value)
 * @mixin \Eloquent
 */
class PartnerService extends Model
{
    use HasFactory;

    /** @var string[] */
    protected $fillable = [
        'user_id',
        'full_name',
        'inn',
        'created_at',
        'updated_at',
    ];

    /** @var array<string, string> */
    protected $casts = [
        'created_at' => 'immutable_datetime',
        'updated_at' => 'immutable_datetime',
    ];

    /** @return BelongsTo */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /** @return HasMany */
    public function services(): HasMany
    {
        return $this->hasMany(Service::class);
    }
}
