<?php

declare(strict_types=1);

namespace Domain\PartnerServices\Actions;

use Domain\Users\DataTransferObjects\CreateUserData;
use Domain\Users\Enums\UserRole;
use Domain\Users\Models\User;

class CreatePartnerServiceAction
{
    public function __invoke(CreateUserData $createUserData): User
    {
        $createUserData
            ->user
            ->partnerService()
            ->firstOrCreate([
                'full_name' => $createUserData->fullName,
                'inn' => $createUserData->inn,
            ], [
                'full_name' => $createUserData->fullName,
                'inn' => $createUserData->inn,
            ]);

        $createUserData
            ->user
            ->assignRole(UserRole::PARTNER_SERVICE->value);

        return $createUserData->user->load(['partnerService']);
    }
}
