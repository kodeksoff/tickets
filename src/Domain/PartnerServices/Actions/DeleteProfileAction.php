<?php

declare(strict_types=1);

namespace Domain\PartnerServices\Actions;

use Illuminate\Contracts\Auth\Factory;
use Illuminate\Database\DatabaseManager;
use Throwable;

class DeleteProfileAction
{
    public function __construct(
        protected DatabaseManager $databaseManager,
        protected Factory $authFactory,
    ) {
    }

    /** @throws Throwable */
    public function __invoke(): void
    {
        $user = $this
            ->authFactory
            ->guard()
            ->user();

        $this
            ->databaseManager
            ->transaction(static function () use ($user): void {
                $user
                    ->partnerService
                    ->services()
                    ->withoutGlobalScopes()
                    ->delete();

                $user
                    ->partnerService()
                    ->delete();

                $user
                    ->tokens()
                    ->delete();

                $user->delete();
            });
    }
}
