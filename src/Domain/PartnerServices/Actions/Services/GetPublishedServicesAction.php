<?php

declare(strict_types=1);

namespace Domain\PartnerServices\Actions\Services;

use Domain\Services\Models\Service;
use Illuminate\Contracts\Auth\Factory;
use Illuminate\Database\Eloquent\Builder;

class GetPublishedServicesAction
{
    /** @param  Factory  $authFactory */
    public function __construct(protected Factory $authFactory)
    {
    }

    /** @return Builder */
    public function __invoke(): Builder
    {
        return Service::query()
            ->forPartnerService(
                $this
                    ->authFactory
                    ->guard()
                    ->user()
                    ->partnerService
                    ->id,
            )
            ->publishedService();
    }
}
