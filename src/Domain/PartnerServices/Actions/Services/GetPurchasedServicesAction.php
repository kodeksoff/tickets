<?php

declare(strict_types=1);

namespace Domain\PartnerServices\Actions\Services;

use Domain\Orders\Models\Order;
use Domain\Services\Models\Service;
use Illuminate\Contracts\Auth\Factory;
use Illuminate\Database\Eloquent\Builder;

class GetPurchasedServicesAction
{
    /** @param Factory $authFactory */
    public function __construct(protected Factory $authFactory)
    {
    }

    /** @return Builder */
    public function __invoke(): Builder
    {
        return Order::query()
            ->whereIn(
                'service_id',
                $this
                    ->authFactory
                    ->guard()
                    ->user()
                    ->partnerService
                    ->services
                    ->pluck('id')
            )
            ->withCount('tickets')
            ->withWhereHas('user')
            ->withWhereHas('tickets');

        //        return Service::query()
        //            ->forPartnerService(
        //                $this
        //                    ->authFactory
        //                    ->guard()
        //                    ->user()
        //                    ->partnerService
        //                    ->id,
        //            )
        //            ->withWhereHas('order');
    }
}
