<?php

declare(strict_types=1);

namespace Domain\PartnerServices\Actions\Services;

use Domain\Services\Models\Service;
use Illuminate\Contracts\Auth\Factory;
use Illuminate\Database\Eloquent\Builder;

class GetHiddenServicesAction
{
    /** @param Factory $authFactory */
    public function __construct(protected Factory $authFactory)
    {
    }

    /** @return Builder */
    public function __invoke(): Builder
    {
        return Service::query()
            ->forPartnerService(
                $this
                    ->authFactory
                    ->guard()
                    ->user()
                    ->partnerService
                    ->id,
            )
            ->with(['latestModeration'])
            ->where(
                function ($query) {
                    $query->where(fn (Builder $builder): Builder => $builder->hiddenServices())
                        ->orWhere(fn (Builder $builder): Builder => $builder->serviceOnModeration())
                        ->orWhere(fn (Builder $builder): Builder => $builder->serviceRejected());
                }
            );
    }
}
