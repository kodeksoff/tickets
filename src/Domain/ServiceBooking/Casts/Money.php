<?php

declare(strict_types=1);

namespace Domain\ServiceBooking\Casts;

use Brick\Math\Exception\MathException;
use Brick\Math\Exception\NumberFormatException;
use Brick\Math\Exception\RoundingNecessaryException;
use Brick\Money\Exception\UnknownCurrencyException;
use Brick\Money\Money as BrickMoney;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use Illuminate\Database\Eloquent\Model;
use Support\Utils\MoneyFactory;

class Money implements CastsAttributes
{
    /**
     * @param  Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     *
     * @return BrickMoney|null
     * @throws NumberFormatException
     * @throws RoundingNecessaryException
     * @throws UnknownCurrencyException
     */
    public function get(Model $model, string $key, mixed $value, array $attributes): ?BrickMoney
    {
        $fields = json_decode($value);

        return resolve(MoneyFactory::class)
            ->ofMinor($fields->amount);
    }

    /**
     * @param  Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     *
     * @return string|false
     * @throws MathException
     */
    public function set(Model $model, string $key, mixed $value, array $attributes): string|false
    {
        /** @var BrickMoney $value */
        return json_encode([
            'amount' => $value
                ->getMinorAmount()
                ->toInt(),
            'currency' => $value
                ->getCurrency()
                ->getCurrencyCode(),
        ]);
    }
}
