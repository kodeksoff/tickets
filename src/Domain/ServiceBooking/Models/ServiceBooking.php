<?php

declare(strict_types=1);

namespace Domain\ServiceBooking\Models;

use Brick\Math\BigDecimal;
use Carbon\Carbon;
use Domain\ServiceBooking\Casts\Money;
use Domain\Services\Models\Service;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Support\Model;
use Support\Utils\MoneyFactory;

/**
 * Domain\ServiceBooking\Models\ServiceBooking
 *
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property int $service_id
 * @property string|null $title
 * @property \Brick\Money\Money|null|null $discount_price
 * @property \Brick\Money\Money|null|null $full_price
 * @property bool $is_default
 * @property-read BigDecimal $discount_amount
 * @property-read BigDecimal $full_price_amount
 * @property-read BigDecimal $full_price_with_discount_amount
 * @property-read BigDecimal $service_price_for_booking_amount
 * @property-read BigDecimal $surcharge_price_amount
 * @property-read Service $service
 * @method static \Tests\Factories\ServiceBookingFactory factory($count = null, $state = [])
 * @method static \Support\EloquentBuilder|ServiceBooking newModelQuery()
 * @method static \Support\EloquentBuilder|ServiceBooking newQuery()
 * @method static \Support\EloquentBuilder|ServiceBooking query()
 * @method static \Support\EloquentBuilder|ServiceBooking whereCreatedAt($value)
 * @method static \Support\EloquentBuilder|ServiceBooking whereDiscountPrice($value)
 * @method static \Support\EloquentBuilder|ServiceBooking whereFullPrice($value)
 * @method static \Support\EloquentBuilder|ServiceBooking whereId($value)
 * @method static \Support\EloquentBuilder|ServiceBooking whereIn(string $column, string $values, string $boolean = 'and', string $not = false)
 * @method static \Support\EloquentBuilder|ServiceBooking whereIsDefault($value)
 * @method static \Support\EloquentBuilder|ServiceBooking whereServiceId($value)
 * @method static \Support\EloquentBuilder|ServiceBooking whereTitle($value)
 * @method static \Support\EloquentBuilder|ServiceBooking whereUpdatedAt($value)
 * @property-read BigDecimal $discount_price_amount
 * @property \Brick\Money\Money|null $service_discount_price
 * @method static \Support\EloquentBuilder|ServiceBooking whereServiceDiscountPrice($value)
 * @mixin \Eloquent
 */
class ServiceBooking extends Model
{
    use HasFactory;

    /** @var string[] */
    protected $fillable = [
        'service_id',
        'title',
        'discount_price',
        'full_price',
        'service_discount_price',
        'is_default',
        'created_at',
        'updated_at',
    ];

    /** @var string[] */
    protected $casts = [
        'discount_price' => Money::class,
        'full_price' => Money::class,
        'service_discount_price' => Money::class,
        'is_default' => 'boolean',
        'created_at' => 'immutable_datetime',
        'updated_at' => 'immutable_datetime',
    ];

    /** @var string[] */
    protected $hidden = [
        'service_discount_price',
    ];

    /** @return BelongsTo */
    public function service(): BelongsTo
    {
        return $this->belongsTo(Service::class);
    }

    /** @return array|string[] */
    public function getHidden(): array
    {
        if (request()->routeIs('livewire.*')) {
            $this->hidden = [];

            return $this->hidden;
        }

        return $this->hidden;
    }

    /** @return Attribute */
    public function fullPriceAmount(): Attribute
    {
        return new Attribute(
            fn (): BigDecimal => $this
                ->full_price
                ->getAmount()
        );
    }

    /** @return Attribute */
    public function discountPriceAmount(): Attribute
    {
        return new Attribute(
            fn (): BigDecimal => $this
                ->discount_price
                ->getAmount()
        );
    }

    /** @return Attribute */
    public function fullPriceWithDiscountAmount(): Attribute
    {
        return new Attribute(
            fn () => resolve(MoneyFactory::class)
                ->ofMinor(
                    $this
                        ->full_price
                        ->minus($this->discount_price)
                        ->getMinorAmount()
                        ->toInt() -
                    ($this
                        ->full_price
                        ->minus($this->discount_price)
                        ->getMinorAmount()
                        ->toInt() -
                        $this->service->fee * $this
                            ->full_price
                            ->minus($this->discount_price)
                            ->getMinorAmount()
                            ->toInt() / 100) -
                    $this
                        ->service_discount_price
                        ->getMinorAmount()
                        ->toInt(),
                )
                ->getAmount()
        );
    }

    /** @return Attribute */
    public function surchargePriceAmount(): Attribute
    {
        return new Attribute(
            fn () => resolve(MoneyFactory::class)
                ->ofMinor(
                    $this
                        ->full_price
                        ->minus($this->discount_price)
                        ->minus($this->service_discount_price)
                        ->getMinorAmount()
                        ->toInt() -
                    resolve(MoneyFactory::class)
                        ->ofMinor(
                            $this
                                ->full_price
                                ->minus($this->discount_price)
                                ->getMinorAmount()
                                ->toInt() -
                            ($this
                                ->full_price
                                ->minus($this->discount_price)
                                ->getMinorAmount()
                                ->toInt() -
                                $this->service->fee * $this
                                    ->full_price
                                    ->minus($this->discount_price)
                                    ->getMinorAmount()
                                    ->toInt() / 100) -
                            $this
                                ->service_discount_price
                                ->getMinorAmount()
                                ->toInt(),
                        )
                        ->getMinorAmount()
                        ->toInt(),
                )
                ->getAmount()
        );
    }

    /** @return Attribute */
    public function serviceFee(): Attribute
    {
        return new Attribute(
            fn () => $this->service->fee
        );
    }

    /** @return Attribute */
    public function servicePriceForBookingAmount(): Attribute
    {
        return new Attribute(
            fn () => $this
                ->full_price
                ->minus($this->discount_price)
                ->minus($this->service_discount_price)
                ->getAmount()
        );
    }
}
