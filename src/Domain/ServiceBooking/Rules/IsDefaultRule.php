<?php

declare(strict_types=1);

namespace Domain\ServiceBooking\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class IsDefaultRule implements ValidationRule
{
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $count = collect($value)
            ->where('is_default', true)
            ->count();

        if ($count === 0) {
            $fail('Необходимо выбрать основную цену бронирования');
        }

        if ($count > 1) {
            $fail('Можно выбрать только одну основную цену бронирования');
        }
    }
}
