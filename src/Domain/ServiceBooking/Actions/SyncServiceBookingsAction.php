<?php

declare(strict_types=1);

namespace Domain\ServiceBooking\Actions;

use Brick\Math\Exception\MathException;
use Domain\ServiceBooking\DataTransferObjects\UpdateBookingTypesData;
use Domain\Services\DataTransferObjects\UpdateServiceData;
use Domain\Services\Models\Service;

class SyncServiceBookingsAction
{
    /**
     * @param  UpdateServiceData  $updateServiceData
     * @param  Service  $service
     *
     * @return Service
     * @throws MathException
     */
    public function __invoke(UpdateServiceData $updateServiceData, Service $service): Service
    {
        if ($updateServiceData->bookingTypes) {
            $bookingTypesCollection = $updateServiceData
                ->bookingTypes
                ->toCollection();

            $bookingIdsToDelete = $bookingTypesCollection
                ->whereNotNull('id')
                ->pluck('id')
                ->unique()
                ->values();

            $bookingsToCheckForUpdate = $bookingTypesCollection
                ->whereNotNull('id')
                ->values();

            $bookingsToCreate = $bookingTypesCollection
                ->whereNull('id')
                ->values();

            /**
             * На этом этапе удаления, в дальнейшем,
             * будет проверка на факт наличия бронирования
             */
            $service
                ->bookings()
                ->whereNotIn(
                    'id',
                    $bookingIdsToDelete,
                )
                ->delete();

            /**
             * На этом этапе обновления, в дальнейшем,
             * будет проверка на факт наличия бронирования
             */
            $bookingsToCheckForUpdate->each(
                function (UpdateBookingTypesData $updateBookingTypesData) use ($service) {
                    $service
                        ->bookings()
                        ->where('id', $updateBookingTypesData->id)
                        ->update([
                            'title' => $updateBookingTypesData->title,
                            'full_price' => [
                                'amount' => $updateBookingTypesData
                                    ->fullPrice
                                    ->getMinorAmount()
                                    ->toInt(),
                                'currency' => $updateBookingTypesData
                                    ->fullPrice
                                    ->getCurrency()
                                    ->getCurrencyCode(),
                            ],
                            'discount_price' => [
                                'amount' => $updateBookingTypesData
                                    ->discountPrice
                                    ->getMinorAmount()
                                    ->toInt(),
                                'currency' => $updateBookingTypesData
                                    ->discountPrice
                                    ->getCurrency()
                                    ->getCurrencyCode(),
                            ],
                            'is_default' => $updateBookingTypesData->isDefault,
                        ]);
                },
            );

            $bookingsToCreate->each(
                function (UpdateBookingTypesData $updateBookingTypesData) use ($service) {
                    $service
                        ->bookings()
                        ->create([
                            'title' => $updateBookingTypesData->title,
                            'full_price' => $updateBookingTypesData->fullPrice,
                            'discount_price' => $updateBookingTypesData->discountPrice,
                            'is_default' => $updateBookingTypesData->isDefault,
                        ]);
                },
            );
        }

        return $service;
    }
}
