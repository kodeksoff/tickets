<?php

declare(strict_types=1);

namespace Domain\ServiceBooking\DataTransferObjects;

use Brick\Money\Money;
use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Attributes\WithCast;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;
use Support\DataCasts\MoneyCast;

#[MapName(SnakeCaseMapper::class)]
class UpdateBookingTypesData extends Data
{
    /**
     * @param  int|null  $id
     * @param  string|null  $title
     * @param  Money  $fullPrice
     * @param  Money  $discountPrice
     * @param  bool  $isDefault
     */
    public function __construct(
        public readonly ?int $id,
        public readonly ?string $title,
        #[WithCast(MoneyCast::class)]
        public readonly Money $fullPrice,
        #[WithCast(MoneyCast::class)]
        public readonly Money $discountPrice,
        public readonly ?bool $isDefault,
    ) {
    }
}
