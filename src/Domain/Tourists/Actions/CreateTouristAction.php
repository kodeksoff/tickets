<?php

declare(strict_types=1);

namespace Domain\Tourists\Actions;

use Domain\GuestHouses\Models\GuestHouse;
use Domain\Users\DataTransferObjects\CreateUserData;
use Domain\Users\Enums\UserRole;
use Domain\Users\Models\User;
use Throwable;

class CreateTouristAction
{
    /** @throws Throwable */
    public function __invoke(CreateUserData $createUserData): User
    {
        $model = GuestHouse::query()
            ->where('code', $createUserData->guestHouseCode)
            ->first();

        $createUserData
            ->user
            ->tourist()
            ->firstOrCreate([
                'full_name' => $createUserData->fullName,
                'guest_house_id' => $model?->id,
            ], [
                'full_name' => $createUserData->fullName,
                'guest_house_id' => $model?->id,
            ]);

        $createUserData
            ->user
            ->assignRole(UserRole::TOURIST->value);

        return $createUserData
            ->user
            ->load(['tourist']);
    }
}
