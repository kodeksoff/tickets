<?php

declare(strict_types=1);

namespace Domain\Tourists\Actions;

use Domain\Users\Models\User;
use Illuminate\Contracts\Auth\Factory;
use Throwable;

class ShowProfileAction
{
    public function __construct(protected Factory $authFactory)
    {
    }

    /** @throws Throwable */
    public function __invoke(): User
    {
        return $this
            ->authFactory
            ->guard()
            ->user()
            ->loadMissing(['tourist']);
    }
}
