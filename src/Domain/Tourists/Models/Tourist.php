<?php

declare(strict_types=1);

namespace Domain\Tourists\Models;

use Carbon\Carbon;
use Domain\GuestHouses\Models\GuestHouse;
use Domain\Users\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Support\Model;

/**
 * Domain\Tourists\Models\Tourist.
 *
 * @property int $id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property int $user_id
 * @property int $guest_house_id
 * @property string $full_name
 * @property-read GuestHouse|null $guestHouse
 * @property-read User $user
 * @method static \Tests\Factories\TouristFactory factory($count = null, $state = [])
 * @method static \Support\EloquentBuilder|Tourist newModelQuery()
 * @method static \Support\EloquentBuilder|Tourist newQuery()
 * @method static \Support\EloquentBuilder|Tourist query()
 * @method static \Support\EloquentBuilder|Tourist whereCreatedAt($value)
 * @method static \Support\EloquentBuilder|Tourist whereFullName($value)
 * @method static \Support\EloquentBuilder|Tourist whereGuestHouseId($value)
 * @method static \Support\EloquentBuilder|Tourist whereId($value)
 * @method static \Support\EloquentBuilder|Tourist whereIn(string $column, string $values, string $boolean = 'and', string $not = false)
 * @method static \Support\EloquentBuilder|Tourist whereUpdatedAt($value)
 * @method static \Support\EloquentBuilder|Tourist whereUserId($value)
 * @mixin \Eloquent
 */
class Tourist extends Model
{
    use HasFactory;

    /** @var string[] */
    protected $fillable = [
        'user_id',
        'guest_house_id',
        'full_name',
        'created_at',
        'updated_at',
    ];

    /** @var array<string, string> */
    protected $casts = [
        'created_at' => 'immutable_datetime',
        'updated_at' => 'immutable_datetime',
    ];

    /** @return BelongsTo */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /** @return BelongsTo */
    public function guestHouse(): BelongsTo
    {
        return $this->BelongsTo(GuestHouse::class);
    }
}
