<?php

declare(strict_types=1);

namespace Domain\Tourists\Processes;

use Closure;
use Domain\Tourists\Actions\CreateTouristAction;
use Domain\Users\DataTransferObjects\CreateUserData;
use Throwable;

class CreateTouristProcess
{
    public function __construct(protected CreateTouristAction $createTouristAction)
    {
    }

    /** @throws Throwable */
    public function __invoke(CreateUserData $createUserData, Closure $next): CreateUserData
    {
        if ($createUserData->isTourist()) {
            ($this->createTouristAction)($createUserData);
        }

        return $next($createUserData);
    }
}
