<?php

declare(strict_types=1);

namespace Domain\Users\Notifications;

use Illuminate\Bus\Queueable;
use Filament\Notifications\Notification;
use Illuminate\Notifications\Notification as BaseNotification;

class SendSmsCenterMinBalanceNotification extends BaseNotification
{
    use Queueable;

    /** @param  int  $balance */
    public function __construct(protected int $balance)
    {
    }

    /**
     * @param  object  $notifiable
     *
     * @return string[]
     */
    public function via(object $notifiable): array
    {
        return ['database'];
    }

    /**
     * @param  object  $notifiable
     *
     * @return array
     */
    public function toDatabase(object $notifiable): array
    {
        return Notification::make()
            ->title('Низкий баланс')
            ->body(
                sprintf('Текущий баланс: %s', $this->balance),
            )
            ->warning()
            ->getDatabaseMessage();
    }
}
