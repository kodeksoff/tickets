<?php

declare(strict_types=1);

namespace Domain\Users\Notifications;

use Domain\Services\Models\Service;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification as BaseNotification;
use Support\Utils\DatabaseNotificationBuilder;

class ServicePublishModerationNotification extends BaseNotification
{
    use Queueable;

    /** @param  Service  $service */
    public function __construct(protected Service $service)
    {
    }

    /**
     * @param  object  $notifiable
     *
     * @return string[]
     */
    public function via(object $notifiable): array
    {
        return ['database', 'mail'];
    }

    public function toMail(): MailMessage
    {
        return (new MailMessage())
            ->subject('Услуга успешно опубликована')
            ->greeting('Здравствуйте!')
            ->line(
                sprintf(
                    'Услуга «%s» была успешно опубликована',
                    $this->service->title,
                ),
            );
    }

    /**
     * @param  object  $notifiable
     *
     * @return array
     */
    public function toDatabase(object $notifiable): array
    {
        return resolve(DatabaseNotificationBuilder::class)
            ->title(
                sprintf(
                    'Услуга «%s» успешно опубликована',
                    $this->service->title,
                ),
            )
            ->url(
                route('api:v1:services:service:edit', $this->service->id),
                'Посмотреть детали',
            )
            ->success()
            ->toArray();
    }
}
