<?php

declare(strict_types=1);

namespace Domain\Users\Notifications;

use Domain\Services\Models\Service;
use Exception;
use Filament\Notifications\Actions\Action;
use Illuminate\Bus\Queueable;
use Filament\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification as BaseNotification;

class ServiceOnModerationNotification extends BaseNotification
{
    use Queueable;

    /** @param  Service  $service */
    public function __construct(protected Service $service)
    {
    }

    /**
     * @param  object  $notifiable
     *
     * @return string[]
     */
    public function via(object $notifiable): array
    {
        return ['database', 'mail'];
    }

    public function toMail(): MailMessage
    {
        return (new MailMessage())
            ->subject('Новая услуга на модерации')
            ->greeting('Новая услуга на модерации')
            ->action(
                'Просмотреть услугу',
                route('filament.resources.services.edit', $this->service->id),
            );
    }

    /**
     * @param  object  $notifiable
     *
     * @return array
     * @throws Exception
     */
    public function toDatabase(object $notifiable): array
    {
        return Notification::make()
            ->title('Новая услуга на модерации')
            ->body($this->service->title)
            ->actions([
                Action::make('Просмотреть')
                    ->url(route('filament.resources.services.edit', $this->service->id))
                    ->link(),
            ])
            ->success()
            ->getDatabaseMessage();
    }
}
