<?php

declare(strict_types=1);

namespace Domain\Users\Notifications;

use Domain\Services\Models\Service;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification as BaseNotification;
use Support\Utils\DatabaseNotificationBuilder;

class ServiceRejectedModerationNotification extends BaseNotification
{
    use Queueable;

    /** @param  Service  $service */
    public function __construct(protected Service $service)
    {
    }

    /**
     * @param  object  $notifiable
     *
     * @return string[]
     */
    public function via(object $notifiable): array
    {
        return ['database', 'mail'];
    }

    public function toMail(): MailMessage
    {
        return (new MailMessage())
            ->subject('Услуга отклонена модератором')
            ->greeting('Здравствуйте!')
            ->line(
                sprintf(
                    'Услуга «%s» была отклонена модератором',
                    $this->service->title,
                ),
            )
            ->line(
                sprintf(
                    'Причина: %s',
                    $this->service->latestModeration->reason,
                ),
            );
    }

    /**
     * @param  object  $notifiable
     *
     * @return array
     */
    public function toDatabase(object $notifiable): array
    {
        return resolve(DatabaseNotificationBuilder::class)
            ->title(
                sprintf(
                    'Услуга «%s» не прошла модерацию',
                    $this->service->title,
                ),
            )
            ->url(
                route('api:v1:services:service:edit', $this->service->id),
                'Посмотреть детали',
            )
            ->warning()
            ->toArray();
    }
}
