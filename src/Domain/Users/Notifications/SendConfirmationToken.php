<?php

declare(strict_types=1);

namespace Domain\Users\Notifications;

use Domain\Confirmations\Models\Confirmation;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Support\NotificationChannels\SmsCenterChannel;

class SendConfirmationToken extends Notification
{
    use Queueable;

    public function __construct(public Confirmation $confirmation)
    {
    }

    public function via(object $notifiable): string
    {
        return SmsCenterChannel::class;
    }

    public function toSmsCenter(object $notifiable): string
    {
        return sprintf('Ваш код для подтверждения номера телефона: %s', $this->confirmation->token);
    }
}
