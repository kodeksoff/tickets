<?php

declare(strict_types=1);

namespace Domain\Users\Notifications;

use Illuminate\Bus\Queueable;
use Filament\Notifications\Notification;
use Illuminate\Notifications\Notification as BaseNotification;

class SendSmsCenterRequestFailedNotification extends BaseNotification
{
    use Queueable;

    /** @param  string  $message */
    public function __construct(protected string $message)
    {
    }

    /**
     * @param  object  $notifiable
     *
     * @return string[]
     */
    public function via(object $notifiable): array
    {
        return ['database'];
    }

    /**
     * @param  object  $notifiable
     *
     * @return array
     */
    public function toDatabase(object $notifiable): array
    {
        return Notification::make()
            ->title('Ошибка отправки СМС')
            ->body($this->message)
            ->danger()
            ->getDatabaseMessage();
    }
}
