<?php

declare(strict_types=1);

namespace Domain\Users\Exceptions;

use Exception;
use Illuminate\Contracts\Support\Responsable;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Support\Responses\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class UndefinedUserRole extends Exception implements Responsable
{
    /** @var string */
    protected $message = 'Не определена группа пользователя';

    /**
     * @param $request
     *
     * @return JsonResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function toResponse($request): JsonResponse
    {
        return JsonResponse::error(
            $this->message,
            Response::HTTP_NOT_FOUND,
        );
    }
}
