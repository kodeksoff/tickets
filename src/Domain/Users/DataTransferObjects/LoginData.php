<?php

declare(strict_types=1);

namespace Domain\Users\DataTransferObjects;

use Domain\Users\Enums\AuthUserRole;
use Domain\Users\Enums\UserRole;
use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;

#[MapName(SnakeCaseMapper::class)]
class LoginData extends AbstractUserData
{
    /**
     * @param  string  $phone
     * @param  string|null  $password
     * @param  string|null  $token
     * @param  string|null  $confirmationId
     * @param  UserRole|AuthUserRole  $userRole
     */
    public function __construct(
        public readonly string $phone,
        public readonly ?string $password,
        public readonly ?string $token,
        public readonly ?string $confirmationId,
        public UserRole|AuthUserRole $userRole,
    ) {
        parent::__construct($userRole);
    }
}
