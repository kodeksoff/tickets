<?php

declare(strict_types=1);

namespace Domain\Users\DataTransferObjects;

use Domain\Users\Enums\UserRole;
use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;

#[MapName(SnakeCaseMapper::class)]
class AuthenticatedData extends Data
{
    public function __construct(
        public readonly UserRole $userRole,
        public readonly string $token,
    ) {
    }
}
