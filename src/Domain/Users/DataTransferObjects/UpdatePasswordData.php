<?php

declare(strict_types=1);

namespace Domain\Users\DataTransferObjects;

use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;

#[MapName(SnakeCaseMapper::class)]
class UpdatePasswordData extends Data
{
    public function __construct(
        public readonly string $currentPassword,
        public readonly string $password,
    ) {
    }
}
