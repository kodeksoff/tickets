<?php

declare(strict_types=1);

namespace Domain\Users\DataTransferObjects;

use Domain\Users\Enums\AuthUserRole;
use Domain\Users\Enums\UserRole;
use Domain\Users\Models\User;
use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;

#[MapName(SnakeCaseMapper::class)]
class CreateUserData extends AbstractUserData
{
    /**
     * @param  string|null  $fullName
     * @param  string  $phone
     * @param  string|null  $email
     * @param  string|null  $inn
     * @param  int|null  $guestHouseCode
     * @param  AuthUserRole|UserRole  $userRole
     * @param  string|null  $password
     * @param  string  $confirmationId
     * @param  User|null  $user
     */
    public function __construct(
        public readonly ?string $fullName,
        public readonly string $phone,
        public readonly ?string $email,
        public readonly ?string $inn,
        public readonly ?int $guestHouseCode,
        public UserRole|AuthUserRole $userRole,
        public readonly ?string $password,
        public readonly string $confirmationId,
        public readonly ?User $user,
    ) {
        parent::__construct($userRole);
    }
}
