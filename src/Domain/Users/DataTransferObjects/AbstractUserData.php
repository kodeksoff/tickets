<?php

declare(strict_types=1);

namespace Domain\Users\DataTransferObjects;

use Domain\Users\Enums\AuthUserRole;
use Domain\Users\Enums\UserRole;
use Spatie\LaravelData\Data;

abstract class AbstractUserData extends Data
{
    /** @param  UserRole|AuthUserRole  $userRole */
    public function __construct(
        public UserRole|AuthUserRole $userRole,
    ) {
    }

    /** @return bool */
    public function isTourist(): bool
    {
        return $this->userRole->value === UserRole::TOURIST();
    }

    /** @return bool */
    public function isPartner(): bool
    {
        return $this->userRole->value === AuthUserRole::PARTNER();
    }

    /** @return bool */
    public function isPartnerService(): bool
    {
        return $this->userRole->value === UserRole::PARTNER_SERVICE();
    }

    /** @return bool */
    public function isPartnerHouse(): bool
    {
        return $this->userRole->value === UserRole::PARTNER_HOUSE();
    }
}
