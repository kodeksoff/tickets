<?php

declare(strict_types=1);

namespace Domain\Users\Actions;

use Domain\Confirmations\Actions\CheckApprovedConfirmationAction;
use Domain\Users\DataTransferObjects\AuthenticatedData;
use Domain\Users\DataTransferObjects\ResetPasswordData;
use Domain\Users\Enums\UserRole;
use Domain\Users\Exceptions\UserNotFoundException;
use Domain\Users\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Events\Dispatcher;
use Throwable;

class ResetPasswordAction
{
    public function __construct(
        protected Hasher $hasher,
        protected Dispatcher $dispatcher,
        protected CheckApprovedConfirmationAction $checkApprovedConfirmationAction,
    ) {
    }

    /** @throws Throwable */
    public function __invoke(ResetPasswordData $resetPasswordData): AuthenticatedData
    {
        $confirmation = ($this->checkApprovedConfirmationAction)($resetPasswordData->confirmationId);

        $user = User::query()
            ->wherePhone($confirmation->deliver_to)
            ->first();

        throw_if(!$user, UserNotFoundException::class);

        $user
            ->forceFill([
                'password' => $this
                    ->hasher
                    ->make($resetPasswordData->password),
            ])
            ->save();

        $this
            ->dispatcher
            ->dispatch(
                new PasswordReset($user),
            );

        return new AuthenticatedData(
            UserRole::from($user->user_role),
            $user
                ->createToken(
                    sprintf('spa_%s', $user->user_role),
                    [sprintf('access:%s', $user->user_role)],
                )
                ->plainTextToken
        );
    }
}
