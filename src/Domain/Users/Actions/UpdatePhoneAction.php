<?php

declare(strict_types=1);

namespace Domain\Users\Actions;

use Domain\Confirmations\Actions\UseConfirmationAction;
use Domain\Users\DataTransferObjects\UpdatePhoneData;
use Illuminate\Contracts\Auth\Factory;
use Illuminate\Events\Dispatcher;
use Throwable;

class UpdatePhoneAction
{
    public function __construct(
        protected Dispatcher $dispatcher,
        protected Factory $authFactory,
        protected UseConfirmationAction $useConfirmationAction
    ) {
    }

    /** @throws Throwable */
    public function __invoke(UpdatePhoneData $updatePhoneData): void
    {
        ($this->useConfirmationAction)(
            $updatePhoneData->confirmationId,
            $updatePhoneData->token,
            false
        );

        $user = $this
            ->authFactory
            ->guard()
            ->user();

        $user
            ->forceFill([
                'phone' => $updatePhoneData->phone,
            ])
            ->save();
    }
}
