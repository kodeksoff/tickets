<?php

declare(strict_types=1);

namespace Domain\Users\Actions;

use Domain\GuestHouses\Models\GuestHouse;
use Domain\Users\DataTransferObjects\UpdateProfileTouristData;
use Illuminate\Contracts\Auth\Factory;
use Illuminate\Events\Dispatcher;
use Throwable;

class SaveTouristProfileAction
{
    public function __construct(
        protected Dispatcher $dispatcher,
        protected Factory $authFactory,
    ) {
    }

    /** @throws Throwable */
    public function __invoke(UpdateProfileTouristData $profileTouristData): void
    {
        $user = $this
            ->authFactory
            ->guard()
            ->user();

        if($profileTouristData->fullName) {
            $user
                ->tourist()
                ->update([
                    'full_name' => $profileTouristData->fullName,
                ]);
        }
        if($profileTouristData->guestHouseCode) {
            $guestHouse = GuestHouse::query()
                ->where('code', $profileTouristData->guestHouseCode)
                ->firstOrFail();
            $user->tourist()->update([
                'guest_house_id' => $guestHouse?->id,
            ]);
        }
    }
}
