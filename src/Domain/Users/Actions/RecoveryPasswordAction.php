<?php

declare(strict_types=1);

namespace Domain\Users\Actions;

use Domain\Confirmations\Actions\BuildPasswordRecoveryConfirmationAction;
use Domain\Users\Enums\AuthUserRole;
use Domain\Users\Exceptions\UserNotFoundException;
use Domain\Users\Models\User;
use Illuminate\Auth\AuthenticationException;
use Throwable;

class RecoveryPasswordAction
{
    public function __construct(
        protected BuildPasswordRecoveryConfirmationAction $buildPasswordRecoveryConfirmationAction,
    ) {
    }

    /** @throws Throwable */
    public function __invoke(AuthUserRole $authUserRole, string $phone)
    {
        throw_if($authUserRole === AuthUserRole::TOURIST, AuthenticationException::class);

        $user = User::query()
            ->wherePhone($phone)
            ->first();

        throw_if(!$user || $user->is_tourist, UserNotFoundException::class);

        return ($this->buildPasswordRecoveryConfirmationAction)($phone);
    }
}
