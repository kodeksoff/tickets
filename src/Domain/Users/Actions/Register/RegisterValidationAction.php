<?php

declare(strict_types=1);

namespace Domain\Users\Actions\Register;

use Domain\Users\DataTransferObjects\CreateUserData;
use Domain\Users\Exceptions\UserAlreadyRegistered;
use Domain\Users\Models\User;
use Support\Utils\MobilePhoneFormatter;
use Throwable;

class RegisterValidationAction
{
    /** @throws Throwable */
    public function __invoke(CreateUserData $createUserData): void
    {
        if (!$createUserData->isTourist()) {
            $user = User::query()
                ->where('phone', resolve(MobilePhoneFormatter::class)->make($createUserData->phone))
                ->orWhere('email', $createUserData->email)
                ->first();

            throw_if($user, UserAlreadyRegistered::class);
        }
    }
}
