<?php

declare(strict_types=1);

namespace Domain\Users\Actions\Register;

use Domain\Confirmations\Processes\CheckApprovedConfirmation;
use Domain\PartnerHouses\Processes\CreatePartnerHouseProcess;
use Domain\PartnerServices\Processes\CreatePartnerServiceProcess;
use Domain\Tourists\Processes\CreateTouristProcess;
use Domain\Users\Processes\CreateUserProcess;
use Support\AbstractProcess;

class RegisterUserPipe extends AbstractProcess
{
    /** @var array|string[] */
    public array $tasks = [
        CheckApprovedConfirmation::class,
        CreateUserProcess::class,
        CreateTouristProcess::class,
        CreatePartnerHouseProcess::class,
        CreatePartnerServiceProcess::class,
    ];
}
