<?php

declare(strict_types=1);

namespace Domain\Users\Actions\Register;

use Domain\Users\DataTransferObjects\CreateUserData;
use Illuminate\Auth\Events\Registered;
use Illuminate\Database\DatabaseManager;
use Illuminate\Events\Dispatcher;
use Throwable;

class RegisterUserAction
{
    public function __construct(
        protected RegisterUserPipe $registerUserPipe,
        protected DatabaseManager $databaseManager,
        protected Dispatcher $dispatcher,
    ) {
    }

    /** @throws Throwable */
    public function __invoke(CreateUserData $createUserData): string
    {
        $user = $this
            ->databaseManager
            ->transaction(fn (): CreateUserData => $this
                ->registerUserPipe
                ->handle($createUserData))->user;

        $this
            ->dispatcher
            ->dispatch(
                new Registered($user),
            );

        return $user
            ->createToken(
                sprintf('spa_%s', $createUserData->userRole->value),
                [sprintf('access:%s', $createUserData->userRole->value)],
            )
            ->plainTextToken;
    }
}
