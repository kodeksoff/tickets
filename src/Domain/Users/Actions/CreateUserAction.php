<?php

declare(strict_types=1);

namespace Domain\Users\Actions;

use Domain\Confirmations\Models\Confirmation;
use Domain\Users\DataTransferObjects\CreateUserData;
use Domain\Users\Enums\UserRole;
use Domain\Users\Exceptions\UserAlreadyRegistered;
use Domain\Users\Models\User;
use Illuminate\Contracts\Hashing\Hasher;
use Support\Utils\MobilePhoneFormatter;
use Throwable;

class CreateUserAction
{
    /** @param  Hasher  $hasher */
    public function __construct(protected Hasher $hasher)
    {
    }

    /** @throws Throwable */
    public function __invoke(CreateUserData $createUserData): User
    {
        $user = User::query()
            ->where('phone', resolve(MobilePhoneFormatter::class)->make($createUserData->phone))
            ->first();

        throw_if(
            $user && $user->hasRole($createUserData->userRole->value),
            UserAlreadyRegistered::class,
        );
        throw_if(
            $user && !$createUserData->isTourist() && $user->hasAnyRole([
                UserRole::PARTNER_HOUSE(),
                UserRole::PARTNER_SERVICE(),
            ]),
            UserAlreadyRegistered::class,
        );

        $phone = resolve(MobilePhoneFormatter::class)->make($createUserData->phone);

        return User::query()->firstOrcreate([
            'phone' => $phone,
            'email' => $createUserData->email,
        ], [
            'phone' => $phone,
            'email' => $createUserData->email,
            'password' => $createUserData->password
                ? $this
                    ->hasher
                    ->make($createUserData->password)
                : null,
            'phone_verified_at' => Confirmation::query()
                ->findOrFail($createUserData->confirmationId)
                ->confirmed_at,
        ]);
    }
}
