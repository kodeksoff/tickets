<?php

declare(strict_types=1);

namespace Domain\Users\Actions\Auth;

use Domain\Users\DataTransferObjects\LoginData;
use Domain\Users\Models\User;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Validation\ValidationException;
use Throwable;

class LoginPartnerAction
{
    /** @param  Hasher  $hasher */
    public function __construct(protected Hasher $hasher)
    {
    }

    /** @throws Throwable */
    public function __invoke(LoginData $loginData): User
    {
        $user = User::query()
            ->wherePhone($loginData->phone)
            ->first();

        throw_if(
            !$this->hasher->check($loginData->password, $user->password),
            ValidationException::withMessages([
                'password' => ['Пароль не совпадает'],
            ]),
        );

        return $user;
    }
}
