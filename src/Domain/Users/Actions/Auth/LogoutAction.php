<?php

declare(strict_types=1);

namespace Domain\Users\Actions\Auth;

use Illuminate\Contracts\Auth\Factory;
use Throwable;

class LogoutAction
{
    public function __construct(protected Factory $authFactory)
    {
    }

    /** @throws Throwable */
    public function __invoke(): void
    {
        $this
            ->authFactory
            ->guard()
            ->user()
            ->currentAccessToken()
            ->delete();
    }
}
