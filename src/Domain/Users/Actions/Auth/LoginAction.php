<?php

declare(strict_types=1);

namespace Domain\Users\Actions\Auth;

use Domain\Users\DataTransferObjects\AuthenticatedData;
use Domain\Users\DataTransferObjects\LoginData;
use Domain\Users\Enums\UserRole;
use Domain\Users\Exceptions\UndefinedUserRole;
use Illuminate\Contracts\Container\BindingResolutionException;
use Throwable;

class LoginAction
{
    public function __construct(
        protected LoginTouristAction $loginTouristAction,
        protected LoginPartnerAction $loginPartnerAction,
    ) {
    }

    /**
     * @throws BindingResolutionException
     * @throws Throwable
     */
    public function __invoke(LoginData $loginData): AuthenticatedData
    {
        $user = match ($loginData->isTourist()) {
            true => ($this->loginTouristAction)($loginData),
            false => ($this->loginPartnerAction)($loginData),
            default => throw new UndefinedUserRole(),
        };

        return new AuthenticatedData(
            UserRole::from($user->user_role),
            $user
                ->createToken(
                    sprintf('spa_%s', $user->user_role),
                    [sprintf('access:%s', $user->user_role)],
                )
                ->plainTextToken
        );
    }
}
