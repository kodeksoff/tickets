<?php

declare(strict_types=1);

namespace Domain\Users\Actions\Auth;

use Domain\Confirmations\Actions\UseConfirmationAction;
use Domain\Users\DataTransferObjects\LoginData;
use Domain\Users\Exceptions\UserNotFoundException;
use Domain\Users\Models\User;
use Throwable;

class LoginTouristAction
{
    /** @param  UseConfirmationAction  $useConfirmationAction */
    public function __construct(protected UseConfirmationAction $useConfirmationAction)
    {
    }

    /** @throws Throwable */
    public function __invoke(LoginData $loginData): User
    {
        $user = User::query()
            ->wherePhone($loginData->phone)
            ->first();

        throw_if(!$user || !$user->is_tourist, UserNotFoundException::class);

        ($this->useConfirmationAction)(
            $loginData->confirmationId,
            $loginData->token
        );

        return $user;
    }
}
