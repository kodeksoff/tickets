<?php

declare(strict_types=1);

namespace Domain\Users\Actions;

use Domain\Users\Models\User;
use Throwable;

class CheckRegisterUserPhoneAction
{
    /** @throws Throwable */
    public function __invoke(string $phone): array
    {
        return User::query()
            ->wherePhone($phone)
            ->exists() ? [
                'registered' => true,
                'user_role' => User::query()
                    ->wherePhone($phone)->first()->user_role,
            ] : ['registered' => false];
    }
}
