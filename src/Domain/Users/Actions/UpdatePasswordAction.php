<?php

declare(strict_types=1);

namespace Domain\Users\Actions;

use Domain\Users\DataTransferObjects\UpdatePasswordData;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Contracts\Auth\Factory;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Events\Dispatcher;
use Throwable;

class UpdatePasswordAction
{
    public function __construct(
        protected Hasher $hasher,
        protected Dispatcher $dispatcher,
        protected Factory $authFactory,
    ) {
    }

    /** @throws Throwable */
    public function __invoke(UpdatePasswordData $updatePasswordData): void
    {
        $user = $this
            ->authFactory
            ->guard()
            ->user();

        $user
            ->forceFill([
                'password' => $this
                    ->hasher
                    ->make($updatePasswordData->password),
            ])
            ->save();

        $this
            ->dispatcher
            ->dispatch(
                new PasswordReset($user),
            );
    }
}
