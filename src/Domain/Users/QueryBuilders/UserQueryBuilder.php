<?php

declare(strict_types=1);

namespace Domain\Users\QueryBuilders;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Eloquent\Builder;
use Support\EloquentBuilder;
use Support\Utils\MobilePhoneFormatter;

class UserQueryBuilder extends EloquentBuilder
{
    /**
     * @param  string  $name
     *
     * @return self
     */
    public function whereAllRolesFullNameLike(string $name): self
    {
        return $this
            ->whereHas(
                'tourist',
                fn (Builder $builder): Builder => $builder->where('full_name', 'like', "%{$name}%"),
            )
            ->orWhereHas(
                'partnerHouse',
                fn (Builder $builder): Builder => $builder->where('full_name', 'like', "%{$name}%"),
            )
            ->orWhereHas(
                'partnerService',
                fn (Builder $builder): Builder => $builder->where('full_name', 'like', "%{$name}%"),
            );
    }

    /** @throws BindingResolutionException */
    public function whereAllCredential(?string $phone, ?string $email, ?string $inn): self
    {
        return $this
            ->when(
                $phone,
                fn (Builder $builder): Builder => $this->wherePhone($phone),
            )
            ->when(
                $email,
                fn (Builder $builder): Builder => $builder->orWhere('email', $email),
            )
            ->when(
                $inn,
                fn (Builder $builder): Builder => $builder
                    ->orWhere(fn (Builder $q): Builder => $this->whereInn($inn)),
            );
    }

    /**
     * @param  string  $inn
     *
     * @return self
     */
    public function whereInn(string $inn): self
    {
        return $this
            ->whereHas(
                'partnerHouse',
                fn (Builder $builder): Builder => $builder->where('inn', $inn),
            )
            ->orWhereHas(
                'partnerService',
                fn (Builder $builder): Builder => $builder->where('inn', $inn),
            );
    }

    /** @throws BindingResolutionException */
    public function wherePhone(string $phone): self
    {
        return $this->where('phone', resolve(MobilePhoneFormatter::class)->make($phone));
    }
}
