<?php

declare(strict_types=1);

namespace Domain\Users\Processes;

use Closure;
use Domain\Users\Actions\CreateUserAction;
use Domain\Users\DataTransferObjects\CreateUserData;
use Throwable;

class CreateUserProcess
{
    /** @param  CreateUserAction  $createUserAction */
    public function __construct(protected CreateUserAction $createUserAction)
    {
    }

    /** @throws Throwable */
    public function __invoke(CreateUserData $createUserData, Closure $next): CreateUserData
    {
        $user = ($this->createUserAction)($createUserData);

        return $next(
            CreateUserData::from([
                'fullName' => $createUserData->fullName,
                'phone' => $createUserData->phone,
                'userRole' => $createUserData->userRole,
                'inn' => $createUserData->inn,
                'guestHouseCode' => $createUserData->guestHouseCode,
                'confirmationId' => $createUserData->confirmationId,
                'user' => $user,
            ]),
        );
    }
}
