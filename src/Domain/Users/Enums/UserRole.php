<?php

declare(strict_types=1);

namespace Domain\Users\Enums;

use ArchTech\Enums\InvokableCases;

/**
 * @method static ADMIN()
 * @method static TOURIST()
 * @method static PARTNER_SERVICE()
 * @method static PARTNER_HOUSE()
 */
enum UserRole: string
{
    use InvokableCases;

    case ADMIN = 'admin';
    case TOURIST = 'tourist';
    case PARTNER_SERVICE = 'partner-service';
    case PARTNER_HOUSE = 'partner-house';
}
