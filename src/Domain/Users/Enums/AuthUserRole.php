<?php

declare(strict_types=1);

namespace Domain\Users\Enums;

use ArchTech\Enums\InvokableCases;

/**
 * @method static TOURIST()
 * @method static PARTNER()
 */
enum AuthUserRole: string
{
    use InvokableCases;

    case TOURIST = 'tourist';
    case PARTNER = 'partner';
}
