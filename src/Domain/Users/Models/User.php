<?php

declare(strict_types=1);

namespace Domain\Users\Models;

use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Domain\PartnerHouses\Models\PartnerHouse;
use Domain\PartnerServices\Models\PartnerService;
use Domain\Tourists\Models\Tourist;
use Domain\Users\Enums\UserRole;
use Domain\Users\QueryBuilders\UserQueryBuilder;
use Filament\Models\Contracts\FilamentUser;
use Filament\Models\Contracts\HasName;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Contracts\Auth\MustVerifyEmail as MustVerifyEmailContract;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Scout\Searchable;
use Propaganistas\LaravelPhone\Casts\E164PhoneNumberCast;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Support\Model;

/**
 * Domain\Users\Models\User.
 *
 * @property int $id
 * @property string $phone
 * @property string $email
 * @property CarbonImmutable $email_verified_at
 * @property CarbonImmutable $phone_verified_at
 * @property string $password
 * @property string $remember_token
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property-read string $full_name
 * @property-read bool $is_admin
 * @property-read bool $is_partner_house
 * @property-read bool $is_partner_service
 * @property-read bool $is_tourist
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection<int,
 *     \Illuminate\Notifications\DatabaseNotification> $notifications
 * @property-read int|null $notifications_count
 * @property-read PartnerHouse|null $partnerHouse
 * @property-read PartnerService|null $partnerService
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \Spatie\Permission\Models\Permission> $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, Role> $roles
 * @property-read int|null $roles_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \Laravel\Sanctum\PersonalAccessToken> $tokens
 * @property-read int|null $tokens_count
 * @property-read Tourist|null $tourist
 * @property-read array $user_roles
 * @method static \Tests\Factories\UserFactory factory($count = null, $state = [])
 * @method static \Support\EloquentBuilder|User newModelQuery()
 * @method static \Support\EloquentBuilder|User newQuery()
 * @method static \Support\EloquentBuilder|User permission($permissions)
 * @method static \Support\EloquentBuilder|User query()
 * @method static \Support\EloquentBuilder|User role($roles, $guard = null)
 * @method static \Support\EloquentBuilder|User whereCreatedAt($value)
 * @method static \Support\EloquentBuilder|User whereEmail($value)
 * @method static \Support\EloquentBuilder|User whereEmailVerifiedAt($value)
 * @method static \Support\EloquentBuilder|User whereId($value)
 * @method static \Support\EloquentBuilder|User whereIn(string $column, string $values, string $boolean = 'and', string $not = false)
 * @method static \Support\EloquentBuilder|User wherePassword($value)
 * @method static \Support\EloquentBuilder|User wherePhone($value)
 * @method static \Support\EloquentBuilder|User wherePhoneVerifiedAt($value)
 * @method static \Support\EloquentBuilder|User whereRememberToken($value)
 * @method static \Support\EloquentBuilder|User whereUpdatedAt($value)
 * @method \Support\EloquentBuilder|User whereAllCredential(string $phone, ?string $email, ?string $inn)
 * @property-read string|null $inn
 * @property-read string|null $user_role
 * @method static UserQueryBuilder|User whereAllRolesFullNameLike(string $name)
 * @method static UserQueryBuilder|User whereInn(string $inn)
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection<int, \Illuminate\Notifications\DatabaseNotification> $notifications
 * @mixin \Eloquent
 */
class User extends Model implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract,
    FilamentUser,
    HasName,
    MustVerifyEmailContract
{
    use Authenticatable;
    use Authorizable;
    use CanResetPassword;
    use HasApiTokens;
    use HasFactory;
    use HasRoles;
    use MustVerifyEmail;
    use Notifiable;
    use Searchable;

    /** @var array<int, string> */
    protected $fillable = [
        'phone',
        'email',
        'password',
        'remember_token',
        'phone_verified_at',
        'email_verified_at',
        'remember_token',
        'created_at',
        'updated_at',
    ];

    /** @var array<int, string> */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /** @var array<string, string> */
    protected $casts = [
        'phone' => E164PhoneNumberCast::class . ':RU',
        'email_verified_at' => 'immutable_datetime',
        'phone_verified_at' => 'immutable_datetime',
        'created_at' => 'immutable_datetime',
        'updated_at' => 'immutable_datetime',
    ];

    /** @return HasOne */
    public function tourist(): HasOne
    {
        return $this->hasOne(Tourist::class);
    }

    /** @return HasOne */
    public function partnerHouse(): HasOne
    {
        return $this->hasOne(PartnerHouse::class);
    }

    /** @return HasOne */
    public function partnerService(): HasOne
    {
        return $this->hasOne(PartnerService::class);
    }

    /**
     * @param $query
     *
     * @return UserQueryBuilder
     */
    public function newEloquentBuilder($query): UserQueryBuilder
    {
        return new UserQueryBuilder($query);
    }

    /** @return bool */
    public function canAccessFilament(): bool
    {
        return $this->is_admin;
    }

    /** @return string */
    public function getFilamentName(): string
    {
        return $this->email;
    }

    /** @return array */
    public function toSearchableArray(): array
    {
        return [
            'id' => $this->id,
            'full_name' => $this->full_name,
            'inn' => $this->inn,
            'role' => $this->user_role,
            'phone' => $this->phone,
            'email' => $this->email,
            'phone_verified_at' => $this->phone_verified_at,
            'email_verified_at' => $this->email_verified_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }

    /** @return Attribute */
    public function isAdmin(): Attribute
    {
        return Attribute::make(
            fn (): bool => $this->hasRole(UserRole::ADMIN()),
        );
    }

    /** @return Attribute */
    public function userRoles(): Attribute
    {
        return Attribute::make(
            fn (): array => $this
                ->getRoleNames()
                ->toArray(),
        );
    }

    /** @return Attribute */
    public function userRole(): Attribute
    {
        return Attribute::make(
            fn (): ?string => $this
                ->getRoleNames()
                ->first(),
        );
    }

    /** @return Attribute */
    public function isTourist(): Attribute
    {
        return Attribute::make(
            fn (): bool => $this->hasRole(UserRole::TOURIST()),
        );
    }

    /** @return Attribute */
    public function isPartnerService(): Attribute
    {
        return Attribute::make(
            fn (): bool => $this->hasRole(UserRole::PARTNER_SERVICE()),
        );
    }

    /** @return Attribute */
    public function isPartnerHouse(): Attribute
    {
        return Attribute::make(
            fn (): bool => $this->hasRole(UserRole::PARTNER_HOUSE()),
        );
    }

    /** @return Attribute */
    public function fullName(): Attribute
    {
        return Attribute::make(
            fn (): string|null => match ($this->getRoleNames()->first()) {
                UserRole::TOURIST() => $this->tourist?->full_name,
                UserRole::PARTNER_HOUSE() => $this->partnerHouse->full_name,
                UserRole::PARTNER_SERVICE() => $this->partnerService->full_name,
                default => $this->email,
            },
        );
    }

    /** @return Attribute */
    public function inn(): Attribute
    {
        return Attribute::make(
            fn (): ?string => match ($this->getRoleNames()->first()) {
                UserRole::PARTNER_HOUSE() => $this->partnerHouse->inn,
                UserRole::PARTNER_SERVICE() => $this->partnerService->inn,
                default => null,
            },
        );
    }

    public function guestHouses(): HasManyThrough
    {
        return $this->through('partnerHouse')->has('guestHouses');
    }
}
