<?php

declare(strict_types=1);

namespace Domain\Users\Rules;

use Closure;
use Domain\Users\Models\User;
use Illuminate\Contracts\Validation\ValidationRule;

class InnExistsRule implements ValidationRule
{
    public function __construct(protected ?User $user, protected bool $isTourist = false)
    {
    }

    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if (!$this->isTourist && $this->user && $this->user->inn === $value) {
            $fail('Пользователь с таким ИНН уже зарегистрирован');
        }
    }
}
