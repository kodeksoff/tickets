<?php

declare(strict_types=1);

namespace Domain\Users\Rules;

use Closure;
use Domain\Users\Enums\AuthUserRole;
use Domain\Users\Models\User;
use Illuminate\Contracts\Validation\ValidationRule;

class UserLoginRule implements ValidationRule
{
    /**
     * @param User|null $user
     * @param bool $isTourist
     * @param AuthUserRole $authUserRole
     */
    public function __construct(
        protected ?User $user,
        protected bool $isTourist,
        protected AuthUserRole $authUserRole,
    ) {
    }

    /**
     * @param string $attribute
     * @param mixed $value
     * @param Closure $fail
     *
     * @return void
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if (!$this->user) {
            $fail('Пользователь с такими данными не найден');
        }

        if (!$this->isTourist && $this->user && $this->user->is_tourist) {
            $fail(
                'Пользователь с таким номером телефона зарегистрирован как турист.
            Зарегистрируйтесь с другим номером телефона для того чтобы стать партнером.'
            );
        }
    }
}
