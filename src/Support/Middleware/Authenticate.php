<?php

declare(strict_types=1);

namespace Support\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Contracts\Auth\Factory;
use Illuminate\Http\Request;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Str;

class Authenticate extends Middleware
{
    public function __construct(Factory $authFactory, private readonly UrlGenerator $urlGenerator)
    {
        parent::__construct($authFactory);
    }

    /** Get the path the user should be redirected to when they are not authenticated. */
    protected function redirectTo($request): ?string
    {
        if ($this->auth->guard('web')) {
            return $this->urlGenerator->route('filament.auth.login');
        }

        return null;
    }

    protected function isApiRequest(Request $request): bool
    {
        return Str::startsWith($request->path(), 'api/');
    }
}
