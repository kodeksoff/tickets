<?php

declare(strict_types=1);

namespace Support\Middleware;

use Carbon\Carbon;
use Closure;
use Domain\Confirmations\Exceptions\TooManyConfirmationAttempts;
use Domain\Confirmations\Models\Confirmation;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Propaganistas\LaravelPhone\Exceptions\NumberParseException;
use Support\Utils\MobilePhoneFormatter;

class ThrottleConfirmationRequests
{
    /**
     * @throws BindingResolutionException
     * @throws TooManyConfirmationAttempts
     */
    public function handle(Request $request, Closure $next)
    {
        /** @var Confirmation|null $confirmation */
        $confirmation = $request->route('confirmation');

        if (!$confirmation) {
            if ($deliverTo = $request->input('phone')) {
                try {
                    $deliverTo = resolve(MobilePhoneFormatter::class)->make($deliverTo);
                } catch (NumberParseException $exception) {
                    throw ValidationException::withMessages(['phone' => $exception->getMessage()]);
                }
            } else {
                $deliverTo = $request->input('email');
            }

            /** @var Confirmation|null $confirmation */
            $confirmation = Confirmation::query()
                ->where('deliver_to', $deliverTo)
                ->latest()
                ->first();
        }

        if (!$confirmation) {
            return $next($request);
        }

        $secondsPassed = Carbon::now()->diffInSeconds($confirmation->created_at);
        $secondsRemained = $confirmation->resend_delay_in_seconds - $secondsPassed;

        if ($secondsRemained > 0) {
            throw new TooManyConfirmationAttempts($secondsRemained);
        }

        return $next($request);
    }
}
