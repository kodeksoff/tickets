<?php

declare(strict_types=1);

namespace Support\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;
use Illuminate\Http\Request;

class UseGuard
{
    public function __construct(protected Auth $auth)
    {
    }

    public function handle(Request $request, Closure $next, string $guard): Closure
    {
        $this->auth->shouldUse($guard);

        return $next($request);
    }
}
