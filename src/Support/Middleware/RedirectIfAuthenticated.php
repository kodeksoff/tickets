<?php

declare(strict_types=1);

namespace Support\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Support\Responses\JsonResponse;

class RedirectIfAuthenticated
{
    public function __construct(private readonly \Illuminate\Auth\AuthManager $authManager, private readonly \Illuminate\Routing\Redirector $redirector)
    {
    }

    /** Handle an incoming request. */
    public function handle(Request $request, Closure $next, ?string ...$guards): Response|JsonResponse|RedirectResponse
    {
        $guards = $guards === [] ? [null] : $guards;

        foreach ($guards as $guard) {
            if ($this->authManager->guard($guard)->check()) {
                return $this->redirector->to(RouteServiceProvider::HOME);
            }
        }

        return $next($request);
    }
}
