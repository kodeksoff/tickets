<?php

declare(strict_types=1);

namespace Support\Middleware;

use Closure;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Http\Request;
use Illuminate\Routing\ResponseFactory;
use Symfony\Component\HttpFoundation\Response;

readonly class EnsureEmailIsVerified
{
    public function __construct(
        private ResponseFactory $responseFactory,
    ) {
    }

    public function handle(Request $request, Closure $next): Response
    {
        if (
            !$request->user() ||
            ($request->user() instanceof MustVerifyEmail && !$request->user()->hasVerifiedEmail())
        ) {
            return $this->responseFactory->json(
                [
                    'message' => 'Your email address is not verified.',
                ],
                409,
            );
        }

        return $next($request);
    }
}
