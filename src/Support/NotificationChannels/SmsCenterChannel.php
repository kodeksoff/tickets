<?php

declare(strict_types=1);

namespace Support\NotificationChannels;

use Domain\Integrations\SmsCenter\SmsCenterConnectorInterface;
use Illuminate\Notifications\Notification;
use ReflectionException;
use Saloon\Exceptions\InvalidResponseClassException;
use Saloon\Exceptions\PendingRequestException;

class SmsCenterChannel
{
    public function __construct(protected SmsCenterConnectorInterface $smsCenterConnector)
    {
    }

    /**
     * @throws InvalidResponseClassException
     * @throws PendingRequestException
     * @throws ReflectionException
     */
    public function send(object $notifiable, Notification $notification): void
    {
        $this
            ->smsCenterConnector
            ->sendConfirmationToken(
                $notification->confirmation->deliver_to,
                /*@phpstan-ignore-next-line */
                $notification->toSmsCenter($notifiable),
            );
    }
}
