<?php

declare(strict_types=1);

namespace Support\Interfaces;

interface Reportable
{
    public function report(): void;
}
