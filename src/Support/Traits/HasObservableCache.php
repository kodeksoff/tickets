<?php

declare(strict_types=1);

namespace Support\Traits;

use Support\Observers\CacheObserver;

trait HasObservableCache
{
    public static function bootHasObservableCache(): void
    {
        self::observe(CacheObserver::class);
    }
}
