<?php

declare(strict_types=1);

namespace Support\Responses;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Config\Repository;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\JsonResponse as Base;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Validation\ValidationException;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

use function array_key_exists;

use const JSON_PRETTY_PRINT;
use const JSON_UNESCAPED_UNICODE;

final class JsonResponse extends Base
{
    /**
     * @param  int  $httpCode
     * @param  string  $description
     * @param  mixed|null  $payload
     * @param  array  $headers
     * @param  int  $jsonOptions
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __construct(
        int $httpCode,
        string $description,
        mixed $payload = null,
        array $headers = [],
        int $jsonOptions = 0,
    ) {
        $data = [
            'description' => $description,
            'payload' => $payload instanceof Arrayable
                ? $payload->toArray()
                : $payload,
        ];

        parent::__construct($data, $httpCode, $this->headers($headers), $this->jsonOptions($jsonOptions));
    }

    /**
     * @param  mixed|null  $payload
     * @param  int  $jsonOptions
     *
     * @return self
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function success(mixed $payload = null, int $jsonOptions = 0): self
    {
        return new self(200, 'OK', $payload, [], $jsonOptions);
    }

    /**
     * @param  Throwable  $throwable
     *
     * @return self
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function exception(Throwable $throwable): self
    {
        if ($throwable instanceof ValidationException) {
            // @var ValidationException $throwable
            return self::validationException($throwable);
        }

        if ($throwable instanceof AuthenticationException) {
            // @var AuthenticationException $throwable
            return new self(401, $throwable->getMessage());
        }

        $code = 500;
        $headers = [];
        $payload = self::convertExceptionToArray($throwable);

        if (self::isHttpException($throwable)) {
            /** @var HttpException $throwable */
            $code = $throwable->getStatusCode();
            $headers = $throwable->getHeaders();
        }

        return new self($code, $payload['message'], $payload, $headers);
    }

    /**
     * @param  string  $description
     * @param  int  $code
     *
     * @return self
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function error(string $description = 'Server Error', int $code = 500): self
    {
        return self::exception(new HttpException($code, $description));
    }

    /**
     * @param  ValidationException  $validationException
     *
     * @return self
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    private static function validationException(ValidationException $validationException): self
    {
        return new self(
            $validationException->status,
            $validationException->getMessage(),
            [
                'errors' => $validationException->validator->failed(),
                'messages' => $validationException->errors(),
            ],
        );
    }

    /**
     * @param  Throwable  $throwable
     *
     * @return bool
     */
    private static function isHttpException(Throwable $throwable): bool
    {
        return $throwable instanceof HttpException;
    }

    /** @return array{message: string} */
    private static function convertExceptionToArray(Throwable $throwable): array
    {
        if (self::shouldBeVerbose()) {
            return [
                'message' => $throwable->getMessage(),
                'exception' => $throwable::class,
                'file' => $throwable->getFile(),
                'line' => $throwable->getLine(),
                'trace' => Collection::make($throwable->getTrace())
                    ->map(static fn (array $item): array => Arr::except($item, ['args']))
                    ->toArray(),
            ];
        }

        return [
            'message' => self::isHttpException($throwable)
                ? $throwable->getMessage()
                : 'Server Error',
        ];
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    private static function shouldBeVerbose(): bool
    {
        // @noinspection PhpUnhandledExceptionInspection
        return resolve(Repository::class)->get('app.debug', false) === true;
    }

    /** @return bool */
    public function isOk(): bool
    {
        return $this->statusCode >= 200 && $this->statusCode <= 299;
    }

    /**
     * @param  array<string, string>  $headers
     *
     * @return array<string, string>
     */
    private function headers(array $headers): array
    {
        if (!array_key_exists('Content-Type', $headers)) {
            // Стандартный ответ не устанавливает кодировку и она иногда получается кривой
            $headers['Content-Type'] = 'application/json; charset=utf-8';
        }

        return $headers;
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    private function jsonOptions(int $jsonOptions = 0): int
    {
        if (
            self::shouldBeVerbose() ||
            resolve(Request::class)->input('pretty', 'n') === 'y'
        ) {
            $jsonOptions = $jsonOptions | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT;
        }

        return $jsonOptions | self::DEFAULT_ENCODING_OPTIONS;
    }
}
