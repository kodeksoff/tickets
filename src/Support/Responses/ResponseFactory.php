<?php

declare(strict_types=1);

namespace Support\Responses;

use Illuminate\Routing\ResponseFactory as Base;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

class ResponseFactory extends Base
{
    /**
     * @param  mixed  $data
     * @param $status
     * @param  array  $headers
     * @param $options
     *
     * @return JsonResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function json(mixed $data = [], $status = 200, array $headers = [], $options = 0): JsonResponse
    {
        $description = match ($status) {
            200 => 'OK',
            400 => 'Client Error',
            404 => 'Not Found',
            500 => 'Server Error',
            default => (string)$status,
        };

        return new JsonResponse($status, $description, $data, $headers, $options);
    }

    /**
     * @param  mixed|null  $payload
     * @param  int  $jsonOptions
     *
     * @return JsonResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function jsonSuccess(mixed $payload = null, int $jsonOptions = 0): JsonResponse
    {
        return JsonResponse::success($payload, $jsonOptions);
    }

    /**
     * @param  Throwable  $throwable
     *
     * @return JsonResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function jsonException(Throwable $throwable): JsonResponse
    {
        return JsonResponse::exception($throwable);
    }

    /**
     * @param  string  $description
     * @param  int  $code
     *
     * @return JsonResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function jsonError(string $description = 'Server Error', int $code = 500): JsonResponse
    {
        return $this->jsonException(new HttpException($code, $description));
    }
}
