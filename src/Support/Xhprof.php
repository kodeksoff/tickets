<?php

declare(strict_types=1);

namespace Support;

use Closure;
use Illuminate\Support\Str;
use XHProfRuns_Default;

use function extension_loaded;

use const XHPROF_FLAGS_CPU;
use const XHPROF_FLAGS_MEMORY;

class Xhprof
{
    private float $startedAt = 0;

    public function profile(string $label, Closure $code): mixed
    {
        if (!$this->installed()) {
            return $code();
        }

        $this->start();

        $result = $code();

        $this->saveRun($this->makeRunName($label));

        return $result;
    }

    private function installed(): bool
    {
        return
            extension_loaded('xhprof') &&
            function_exists('xhprof_enable') &&
            function_exists('xhprof_disable') &&
            class_exists('XHProfRuns_Default');
    }

    private function start(): void
    {
        xhprof_enable(XHPROF_FLAGS_CPU + XHPROF_FLAGS_MEMORY);

        $this->startedAt = microtime(true);
    }

    private function saveRun(string $name): void
    {
        (new XHProfRuns_Default())->save_run(xhprof_disable(), $name);
    }

    private function makeRunName(string $runName): string
    {
        $runName = Str::replace(
            ['.', '/', '?', '&'],
            '-',
            $runName,
        );

        $time = Str::replace(
            '.',
            ',',
            sprintf('%.4F', microtime(true) - $this->startedAt),
        );

        return $runName . '-' . $time;
    }
}
