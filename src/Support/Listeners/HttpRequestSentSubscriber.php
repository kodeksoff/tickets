<?php

declare(strict_types=1);

namespace Support\Listeners;

use Domain\Integrations\SmsCenter\ConfirmationMessage\DataTransferObjects\SendConfirmationMessageResponseData;
use Domain\Integrations\SmsCenter\Events\SmsCenterRequestSent;
use Illuminate\Events\Dispatcher;
use Saloon\Laravel\Events\SentSaloonRequest;

class HttpRequestSentSubscriber
{
    /** @param  Dispatcher  $dispatcher */
    public function __construct(protected Dispatcher $dispatcher)
    {
    }

    /**
     * @param  SentSaloonRequest  $event
     *
     * @return void
     */
    public function handleSaloonResponse(SentSaloonRequest $event): void
    {
        $response = $event->response;

        if ($response->dto() instanceof SendConfirmationMessageResponseData) {
            $this
                ->dispatcher
                ->dispatch(
                    new SmsCenterRequestSent(
                        $response,
                        $response->dto()
                    ),
                );
        }
    }

    /**
     * @param  Dispatcher  $dispatcher
     *
     * @return string[]
     */
    public function subscribe(Dispatcher $dispatcher): array
    {
        return [
            SentSaloonRequest::class => 'handleSaloonResponse',
        ];
    }
}
