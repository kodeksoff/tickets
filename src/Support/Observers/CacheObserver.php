<?php

declare(strict_types=1);

namespace Support\Observers;

use Illuminate\Cache\CacheManager;
use Illuminate\Database\Eloquent\Model;

class CacheObserver
{
    public function __construct(protected CacheManager $cacheManager)
    {
    }

    public function created(Model $model): void
    {
        $this->clearCache($model->getTable());
    }

    public function updated(Model $model): void
    {
        $this->clearCache($model->getTable());
    }

    public function deleted(Model $model): void
    {
        $this->clearCache($model->getTable());
    }

    public function restored(Model $model): void
    {
        $this->clearCache($model->getTable());
    }

    public function forceDeleted(Model $model): void
    {
        $this->clearCache($model->getTable());
    }

    private function clearCache(string $table): void
    {
        $this
            ->cacheManager
            ->tags([$table])
            ->flush();
    }
}
