<?php

/** @noinspection TraitsPropertiesConflictsInspection */
declare(strict_types=1);

namespace Support;

use Illuminate\Console\Concerns\InteractsWithIO as Base;
use Illuminate\Console\OutputStyle;

trait InteractsWithIO
{
    use Base;

    /** @var OutputStyle|null */
    protected $output;

    /**
     * @param int|string|null $verbosity
     * @param string|null $style
     */
    public function line($string, $style = null, $verbosity = null): void
    {
        if ($this->output !== null) {
            $string = sprintf('[%s] [%s] %s', date('H:i:s d.m'), getmypid(), $string);

            $styled = $style ? "<{$style}>{$string}</{$style}>" : $string;

            $this->output->writeln($styled, $this->parseVerbosity($verbosity));
        }
    }
}
