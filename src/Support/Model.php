<?php

declare(strict_types=1);

namespace Support;

use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Eloquent\Model as Base;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use stdClass;

use function in_array;

/**
 * @mixin Eloquent
 *
 * @method static static query()
 */
class Model extends Base
{
    /** @param array<string, mixed> $attributes */
    final public function __construct(array $attributes = [])
    {
        // @see https://phpstan.org/blog/solving-phpstan-error-unsafe-usage-of-new-static
        parent::__construct($attributes);
    }

    public static function table(): string
    {
        return (new static())->getTable();
    }

    protected static function supportsSoftDelete(): bool
    {
        return in_array(
            SoftDeletes::class,
            class_uses(static::class),
            true,
        );
    }

    /**
     * Мы хотим обращаться к _значениям_ отношений через snake-case, чтобы они были аналогичны другим свойствам моделей.
     * А сами методы отношений именовать в camel-case, чтобы не оборачивать каждый из них комментариями-костылями.
     * По-умолчанию Laravel ожидает, что _значение_ отношения получается по тому же имени, что и у метода, в котором оно описано.
     * Т.е. сюда приходит snake-case, мы его конвертируем и запускаем стандартную логику.
     */
    public function getRelationValue($key): mixed
    {
        foreach ([$key, Str::camel($key)] as $possibleName) {
            // If the key already exists in the relationships array, it just means the
            // relationship has already been loaded, so we'll just return it out of
            // here because there is no need to query within the relations twice.
            if ($this->relationLoaded($possibleName)) {
                return $this->relations[$possibleName];
            }

            // If the "attribute" exists as a method on the model, we will just assume
            // it is a relationship and will load and return results from the query
            // and hydrate the relationship's value on the "relationships" array.
            if (method_exists($this, $possibleName)) {
                return $this->getRelationshipFromMethod($possibleName);
            }
        }

        return null;
    }

    public function isRelation($key): bool
    {
        return parent::isRelation($key) || parent::isRelation(Str::camel($key));
    }

    public function newEloquentBuilder($query): EloquentBuilder
    {
        return new EloquentBuilder($query);
    }

    /** @return array<int, stdClass> */
    public function getForeignKeyReferences(): array
    {
        return resolve(DatabaseManager::class)->select(
            <<<SQL
                SELECT `TABLE_NAME` as `table`, `COLUMN_NAME` as `column`
                FROM `INFORMATION_SCHEMA`.`KEY_COLUMN_USAGE`
                WHERE
                    `REFERENCED_TABLE_SCHEMA` = '{$this->getConnection()->getDatabaseName()}' AND
                    `REFERENCED_TABLE_NAME` = '{$this->getTable()}' AND
                    `REFERENCED_COLUMN_NAME` = '{$this->getKeyName()}'
                SQL
        );
    }
}
