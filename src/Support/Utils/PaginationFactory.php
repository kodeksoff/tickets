<?php

declare(strict_types=1);

namespace Support\Utils;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Pagination\CursorPaginator;
use Illuminate\Pagination\LengthAwarePaginator;

class PaginationFactory
{
    private const PAGINATION_TYPE_CURSOR = 'cursor';

    /** @param  Request  $request */
    public function __construct(private readonly Request $request)
    {
    }

    /**
     * @param  Relation|Builder  $builder
     * @param  bool  $isCursorPaginate
     * @param  int|null  $perPage
     *
     * @return CursorPaginator|LengthAwarePaginator
     */
    public function fromQueryBuilder(
        Relation|Builder $builder,
        bool $isCursorPaginate = false,
        int $perPage = null,
    ): CursorPaginator|LengthAwarePaginator {
        $perPage = $perPage ?: (int)$this->request->query('per_page', 10) ?: 10;

        if ($this->isCursorPaginate() || $isCursorPaginate) {
            return $builder
                ->cursorPaginate($perPage)
                ->withQueryString();
        }

        return $builder
            ->paginate($perPage)
            ->withQueryString();
    }

    /**
     * @param  ResourceCollection  $resourceCollection
     *
     * @return array
     */
    public function toPaginationResource(ResourceCollection $resourceCollection): array
    {
        /** @var ResourceCollection|LengthAwarePaginator $resourceCollection */
        if ($this->isCursorPaginate()) {
            return [
                'data' => $resourceCollection->collection,
                'next_page_url' => $resourceCollection->nextPageUrl(),
                'path' => $resourceCollection->path(),
                'per_page' => $resourceCollection->perPage(),
                'prev_page_url' => $resourceCollection->previousPageUrl(),

            ];
        }

        return [
            'current_page' => $resourceCollection->currentPage(),
            'data' => $resourceCollection->collection,
            'first_page_url' => $resourceCollection->url(1),
            'from' => $resourceCollection->firstItem(),
            'last_page' => $resourceCollection->lastPage(),
            'last_page_url' => $resourceCollection->url(
                $resourceCollection->lastPage(),
            ),
            'next_page_url' => $resourceCollection->nextPageUrl(),
            'path' => $resourceCollection->path(),
            'per_page' => $resourceCollection->perPage(),
            'prev_page_url' => $resourceCollection->previousPageUrl(),
            'to' => $resourceCollection->lastItem(),
            'total' => $resourceCollection->total(),
        ];
    }

    /** @return bool */
    private function isCursorPaginate(): bool
    {
        return $this->request->query('pagination_type') === self::PAGINATION_TYPE_CURSOR;
    }
}
