<?php

declare(strict_types=1);

namespace Support\Utils;

use Brick\Math\BigNumber;
use Brick\Math\Exception\NumberFormatException;
use Brick\Math\Exception\RoundingNecessaryException;
use Brick\Money\Context\AutoContext;
use Brick\Money\Exception\UnknownCurrencyException;
use Brick\Money\Money as BrickMoney;

class MoneyFactory
{
    /**
     * @throws UnknownCurrencyException
     * @throws NumberFormatException
     * @throws RoundingNecessaryException
     */
    public function ofMinor(BigNumber|int|float|string $price): BrickMoney
    {
        return BrickMoney::ofMinor(
            $price,
            'RUB',
            new AutoContext()
        );
    }

    /**
     * @throws UnknownCurrencyException
     * @throws NumberFormatException
     * @throws RoundingNecessaryException
     */
    public function of(BigNumber|int|float|string $price): BrickMoney
    {
        return BrickMoney::of(
            $price,
            'RUB',
        );
    }
}
