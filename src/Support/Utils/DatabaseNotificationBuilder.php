<?php

declare(strict_types=1);

namespace Support\Utils;

use Illuminate\Contracts\Support\Arrayable;
use Support\Enums\DatabaseNotificationStatus;

class DatabaseNotificationBuilder implements Arrayable
{
    /**
     * @param  string|null  $title
     * @param  string|null  $description
     * @param  array|null  $url
     * @param  string  $type
     */
    public function __construct(
        protected ?string $title = null,
        protected ?string $description = null,
        protected ?array $url = [],
        protected string $type = 'success',
    ) {
    }

    /**
     * @param  string  $title
     *
     * @return $this
     */
    public function title(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @param  string  $description
     *
     * @return $this
     */
    public function description(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    /** @return $this */
    public function success(): static
    {
        $this->type = DatabaseNotificationStatus::SUCCESS();

        return $this;
    }

    /** @return $this */
    public function warning(): static
    {
        $this->type = DatabaseNotificationStatus::WARNING();

        return $this;
    }

    /** @return $this */
    public function error(): static
    {
        $this->type = DatabaseNotificationStatus::ERROR();

        return $this;
    }

    /**
     * @param  string  $url
     * @param  string  $title
     *
     * @return $this
     */
    public function url(string $url, string $title): static
    {
        $this->url = [
            'url' => $url,
            'title' => $title,
        ];

        return $this;
    }

    /** @return array */
    public function toArray(): array
    {
        return [
            'title' => $this->title,
            'description' => $this->description,
            'url' => $this->url,
            'type' => $this->type,
        ];
    }
}
