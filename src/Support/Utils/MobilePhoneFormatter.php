<?php

declare(strict_types=1);

namespace Support\Utils;

use Illuminate\Http\Request;
use Propaganistas\LaravelPhone\PhoneNumber;

class MobilePhoneFormatter
{
    public function makeFromRequest(Request $request): ?string
    {
        $phone = $request->input('phone');

        return $phone ? $this->make($phone) : null;
    }

    public function make(mixed $phone): string
    {
        return (new PhoneNumber($phone, 'RU'))->formatE164();
    }
}
