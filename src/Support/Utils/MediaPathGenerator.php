<?php

declare(strict_types=1);

namespace Support\Utils;

use Illuminate\Config\Repository;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\Support\PathGenerator\DefaultPathGenerator;

class MediaPathGenerator extends DefaultPathGenerator
{
    public function __construct(protected Repository $configRepository)
    {
    }

    // Get a unique base path for the given media.
    protected function getBasePath(Media $media): string
    {
        $prefix = $this
            ->configRepository
            ->get('media-library.prefix', '');

        $modelPath = sprintf(
            '%s/%s/%s',
            $media
                ->model
                ->getMorphClass(),
            $media
                ->model
                ->id,
            $media->getKey(),
        );

        if ($prefix !== '') {
            return sprintf(
                '%s/%s',
                $prefix,
                $modelPath,
            );
        }

        return $modelPath;
    }
}
