<?php

declare(strict_types=1);

namespace Support\Utils;

use Illuminate\Cache\Repository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class CacheFactory
{
    /** @param  Repository  $cacheRepository */
    public function __construct(protected Repository $cacheRepository)
    {
    }

    /**
     * @param  Relation|Builder  $builder
     * @param  string  $key
     *
     * @return Collection
     */
    public function fromQueryBuilderToCollection(
        Relation|Builder $builder,
        string $key,
    ): Collection {
        return $this->build(
            $builder,
            $key,
            true,
        );
    }

    /**
     * @param  Relation|Builder  $builder
     * @param  string  $key
     *
     * @return Model
     */
    public function fromQueryBuilderToResource(
        Relation|Builder $builder,
        string $key,
    ): Model {
        return $this->build(
            $builder,
            $key,
        );
    }

    /**
     * @param  Relation|Builder  $builder
     * @param  string  $key
     * @param  bool  $asCollection
     *
     * @return Builder|Model
     */
    protected function build(
        Relation|Builder $builder,
        string $key,
        bool $asCollection = false,
    ): Collection|Model {
        return $this
            ->cacheRepository
            ->tags([$builder->getModel()->getTable()])
            ->remember(
                $key,
                now()->addMinutes(15),
                fn () => $builder->when(
                    $asCollection,
                    fn (Builder $builder) => $builder->get(),
                    fn (Builder $builder) => $builder->first(),
                ),
            );
    }
}
