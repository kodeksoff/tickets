<?php

declare(strict_types=1);

namespace Support\Utils;

use BadMethodCallException;
use Illuminate\Config\Repository;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Spatie\LaravelData\Attributes\MapName;
use Spatie\LaravelData\Data;
use Spatie\LaravelData\Mappers\SnakeCaseMapper;

#[MapName(SnakeCaseMapper::class)]
abstract class SettingsDataTransferObject extends Data
{
    protected static string $configKey;

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function __callStatic(string $name, array $arguments): mixed
    {
        if (!property_exists(static::class, $name)) {
            throw new BadMethodCallException(sprintf('Property %s::%s does not exist.', static::class, $name));
        }

        return static::fromConfig()->{$name};
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function fromConfig(): static
    {
        $settings = resolve(Repository::class)->get(static::$configKey);

        return parent::from($settings);
    }
}
