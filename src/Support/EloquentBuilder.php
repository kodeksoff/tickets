<?php

declare(strict_types=1);

namespace Support;

use Closure;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Connection;
use Illuminate\Database\Eloquent\Builder as Base;
use Illuminate\Database\Eloquent\Relations\Relation;
use PDO;

use function count;
use function is_array;

class EloquentBuilder extends Base
{
    /**
     * @param string $column
     * @param static|Relation|Closure|array<int|float|string, mixed> $values
     * @param string $boolean
     * @param bool $not
     */
    public function whereIn($column, $values, $boolean = 'and', $not = false): static
    {
        if ($values instanceof Arrayable) {
            $values = $values->toArray();
        }

        $largeQuerySize = 10000;

        if (is_array($values) && count($values) >= $largeQuerySize) {
            /** @var Connection $connection */
            $connection = $this->getQuery()->getConnection();

            // Иначе может возникнуть ошибка "prepared statement contains too many placeholders".
            $connection->getPdo()->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
            // TODO: Хорошо бы после выполнения запроса сбрасывать атрибут на оригинальное значение, но пока не могу придумать как это сделать.
        }

        if (empty($values)) {
            $values = [];
        }

        /**
         * @var static $base
         *
         * @noinspection PhpRedundantVariableDocTypeInspection
         */
        $base = parent::whereIn($column, $values, $boolean, $not);

        return $base;
    }
}
