<?php

declare(strict_types=1);

namespace Support;

use Illuminate\Support\Facades\Pipeline;

abstract class AbstractProcess
{
    public array $tasks;

    public function handle(object $payload): mixed
    {
        return Pipeline::send($payload)
            ->through($this->tasks)
            ->thenReturn();
    }
}
