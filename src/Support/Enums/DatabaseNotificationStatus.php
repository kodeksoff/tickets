<?php

declare(strict_types=1);

namespace Support\Enums;

use ArchTech\Enums\InvokableCases;

enum DatabaseNotificationStatus: string
{
    use InvokableCases;

    case SUCCESS = 'success';
    case WARNING = 'warning';
    case ERROR = 'error';
}
