<?php

declare(strict_types=1);

namespace Support;

use Faker\Generator;
use Faker\Provider\Base;

/**
 * @mixin Generator
 *
 * @property Generator|self $generator
 *
 * @method Generator|self unique($reset = false, $maxRetries = 10000)
 */
class FakerProvider extends Base
{
}
