<?php

declare(strict_types=1);

use Support\Xhprof;

if (!function_exists('xhprof')) {
    function xhprof(string $label, Closure $code): mixed
    {
        return resolve(Xhprof::class)->profile($label, $code);
    }
}
