<?php

declare(strict_types=1);

require_once __DIR__ . '/xhprof.php';

if (!function_exists('is_dev_env')) {
    function is_dev_env(): bool
    {
        return app()->environment(['local', 'development', 'dev', 'testing']);
    }
}
