<?php

declare(strict_types=1);

namespace Support\DataCasts;

use Brick\Math\Exception\NumberFormatException;
use Brick\Math\Exception\RoundingNecessaryException;
use Brick\Money\Exception\UnknownCurrencyException;
use Brick\Money\Money;
use Spatie\LaravelData\Casts\Cast;
use Spatie\LaravelData\Support\DataProperty;
use Support\Utils\MoneyFactory;

class MoneyCast implements Cast
{
    /**
     * @throws UnknownCurrencyException
     * @throws NumberFormatException
     * @throws RoundingNecessaryException
     */
    public function cast(DataProperty $property, mixed $value, array $context): Money
    {
        return resolve(MoneyFactory::class)->of($value);
    }
}
