<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use Rector\Php80\Rector\FunctionLike\UnionTypesRector;
use Rector\Renaming\Rector\Name\RenameClassRector;
use Rector\Set\ValueObject\LevelSetList;
use Rector\Set\ValueObject\SetList;
use Rector\Transform\Rector\StaticCall\StaticCallToMethodCallRector;
use RectorLaravel\Set\LaravelSetList;

return static function (RectorConfig $rectorConfig): void {
    $rectorConfig->bootstrapFiles([
        __DIR__ . '/../vendor/nunomaduro/larastan/bootstrap.php',
    ]);

    $rectorConfig->paths([
        __DIR__ . '/../database/seeders',
        __DIR__ . '/../src',
        __DIR__ . '/../tests',
    ]);

    $rectorConfig->phpstanConfig(__DIR__ . '/phpstan.neon');

    $rectorConfig->sets([
        SetList::CODE_QUALITY,
        SetList::NAMING,
        SetList::TYPE_DECLARATION,
        LevelSetList::UP_TO_PHP_82,
        LaravelSetList::ARRAY_STR_FUNCTIONS_TO_STATIC_CALL,
        LaravelSetList::LARAVEL_100,
        LaravelSetList::LARAVEL_ARRAY_STR_FUNCTION_TO_STATIC_CALL,
        LaravelSetList::LARAVEL_CODE_QUALITY,
        LaravelSetList::LARAVEL_LEGACY_FACTORIES_TO_CLASSES,
        LaravelSetList::LARAVEL_STATIC_TO_INJECTION,
    ]);

    $rectorConfig->skip([
        RenameClassRector::class => [
            __DIR__ . '/../src/App/Application.php',
            __DIR__ . '/../src/App/ConsoleKernel.php',
            __DIR__ . '/../src/App/HttpKernel.php',
        ],
        StaticCallToMethodCallRector::class => [
            __DIR__ . '/../src/App/Providers/AuthServiceProvider.php',
            __DIR__ . '/../src/App/Providers/RouteServiceProvider.php',
        ],
        UnionTypesRector::class => [
            __DIR__ . '/../src/Support/InteractsWithIO.php',
        ],
    ]);
};
