{
    "exclude": [
        "__data",
        "__logs",
        "_deploy",
        "_infrastructure"
    ],
    "rules": {
        "align_multiline_comment": {
            "comment_type": "all_multiline"
        },
        "array_indentation": true,
        "array_push": true,
        "array_syntax": {
            "syntax": "short"
        },
        "assign_null_coalescing_to_coalesce_equal": true,
        "backtick_to_shell_exec": true,
        "binary_operator_spaces": {
            "default": "single_space"
        },
        "blank_line_after_namespace": true,
        "blank_line_after_opening_tag": true,
        "blank_line_before_statement": {
            "statements": [
                "declare",
                "do",
                "exit",
                "for",
                "foreach",
                "include",
                "include_once",
                "phpdoc",
                "require",
                "require_once",
                "return",
                "switch",
                "throw",
                "try",
                "while",
                "yield",
                "yield_from"
            ]
        },
        "blank_line_between_import_groups": true,
        "braces": {
            "allow_single_line_anonymous_class_with_empty_body": true,
            "allow_single_line_closure": true,
            "position_after_anonymous_constructs": "same",
            "position_after_control_structures": "same",
            "position_after_functions_and_oop_constructs": "next"
        },
        "cast_spaces": {
            "space": "none"
        },
        "class_attributes_separation": {
            "elements": {
                "const": "only_if_meta",
                "method": "one",
                "property": "only_if_meta",
                "trait_import": "none",
                "case": "none"
            }
        },
        "class_definition": {
            "inline_constructor_arguments": false,
            "multi_line_extends_each_single_line": false,
            "single_item_single_line": true,
            "single_line": true,
            "space_before_parenthesis": true
        },
        "class_reference_name_casing": true,
        "clean_namespace": true,
        "combine_consecutive_issets": true,
        "combine_consecutive_unsets": true,
        "combine_nested_dirname": true,
        "comment_to_phpdoc": {
            "ignored_tags": [
                "todo",
                "see"
            ]
        },
        "compact_nullable_typehint": true,
        "concat_space": {
            "spacing": "one"
        },
        "constant_case": {
            "case": "lower"
        },
        "control_structure_braces": true,
        "control_structure_continuation_position": {
            "position": "same_line"
        },
        "curly_braces_position": {
            "allow_single_line_anonymous_functions": true,
            "allow_single_line_empty_anonymous_classes": true,
            "anonymous_classes_opening_brace": "next_line_unless_newline_at_signature_end",
            "anonymous_functions_opening_brace": "same_line",
            "classes_opening_brace": "next_line_unless_newline_at_signature_end",
            "control_structures_opening_brace": "same_line",
            "functions_opening_brace": "next_line_unless_newline_at_signature_end"
        },
        "date_time_create_from_format_call": true,
        "date_time_immutable": true,
        "declare_equal_normalize": {
            "space": "none"
        },
        "declare_parentheses": true,
        "declare_strict_types": true,
        "dir_constant": true,
        "echo_tag_syntax": {
            "format": "short",
            "long_function": "echo",
            "shorten_simple_statements_only": false
        },
        "elseif": true,
        "empty_loop_body": {
            "style": "semicolon"
        },
        "empty_loop_condition": {
            "style": "while"
        },
        "encoding": true,
        "ereg_to_preg": true,
        "error_suppression": {
            "mute_deprecation_error": false,
            "noise_remaining_usages": true,
            "noise_remaining_usages_exclude": []
        },
        "escape_implicit_backslashes": {
            "double_quoted": true,
            "heredoc_syntax": true,
            "single_quoted": false
        },
        "explicit_indirect_variable": true,
        "explicit_string_variable": true,
        "final_class": false,
        "final_internal_class": false,
        "final_public_method_for_abstract_class": false,
        "fopen_flag_order": true,
        "fopen_flags": {
            "b_mode": true
        },
        "full_opening_tag": true,
        "fully_qualified_strict_types": true,
        "function_declaration": {
            "closure_fn_spacing": "none",
            "closure_function_spacing": "one",
            "trailing_comma_single_line": false
        },
        "function_to_constant": {
            "functions": [
                "get_called_class",
                "get_class",
                "get_class_this",
                "php_sapi_name",
                "phpversion",
                "pi"
            ]
        },
        "function_typehint_space": true,
        "general_phpdoc_annotation_remove": {
            "annotations": [
                "author",
                "package",
                "version"
            ],
            "case_sensitive": false
        },
        "general_phpdoc_tag_rename": {
            "case_sensitive": false,
            "fix_annotation": true,
            "replacements": {}
        },
        "get_class_to_class_keyword": true,
        "global_namespace_import": {
            "import_classes": true,
            "import_constants": true,
            "import_functions": true
        },
        "group_import": false,
        "header_comment": false,
        "heredoc_indentation": {
            "indentation": "start_plus_one"
        },
        "heredoc_to_nowdoc": true,
        "implode_call": true,
        "include": true,
        "increment_style": {
            "style": "post"
        },
        "indentation_type": true,
        "integer_literal_case": true,
        "is_null": true,
        "lambda_not_used_import": true,
        "line_ending": true,
        "linebreak_after_opening_tag": true,
        "list_syntax": {
            "syntax": "short"
        },
        "logical_operators": true,
        "lowercase_cast": true,
        "lowercase_keywords": true,
        "lowercase_static_reference": true,
        "magic_constant_casing": true,
        "magic_method_casing": true,
        "mb_str_functions": true,
        "method_argument_space": {
            "after_heredoc": true,
            "keep_multiple_spaces_after_comma": false,
            "on_multiline": "ensure_fully_multiline"
        },
        "method_chaining_indentation": true,
        "modernize_strpos": true,
        "modernize_types_casting": true,
        "multiline_comment_opening_closing": true,
        "multiline_whitespace_before_semicolons": {
            "strategy": "no_multi_line"
        },
        "native_constant_invocation": {
            "exclude": [
                "null",
                "true",
                "false"
            ],
            "fix_built_in": true,
            "include": [],
            "scope": "all",
            "strict": true
        },
        "native_function_casing": true,
        "native_function_invocation": {
            "include": [
                "@compiler_optimized"
            ],
            "exclude": [
                "function_exists"
            ],
            "scope": "namespaced",
            "strict": true
        },
        "native_function_type_declaration_casing": true,
        "new_with_braces": {
            "anonymous_class": true,
            "named_class": true
        },
        "no_alias_functions": {
            "sets": [
                "@all"
            ]
        },
        "no_alias_language_construct_call": true,
        "no_alternative_syntax": true,
        "no_binary_string": true,
        "no_blank_lines_after_class_opening": true,
        "no_blank_lines_after_phpdoc": true,
        "no_blank_lines_before_namespace": false,
        "no_break_comment": {
            "comment_text": "intentional fall-through"
        },
        "no_closing_tag": true,
        "no_empty_comment": true,
        "no_empty_phpdoc": true,
        "no_empty_statement": true,
        "no_extra_blank_lines": {
            "tokens": [
                "extra",
                "throw",
                "curly_brace_block",
                "parenthesis_brace_block",
                "square_brace_block",
                "use",
                "use_trait"
            ]
        },
        "no_homoglyph_names": true,
        "no_leading_import_slash": true,
        "no_leading_namespace_whitespace": true,
        "no_mixed_echo_print": {
            "use": "echo"
        },
        "no_multiline_whitespace_around_double_arrow": true,
        "no_multiple_statements_per_line": true,
        "no_null_property_initialization": false,
        "no_php4_constructor": true,
        "no_short_bool_cast": true,
        "no_singleline_whitespace_before_semicolons": true,
        "no_space_around_double_colon": true,
        "no_spaces_after_function_name": true,
        "no_spaces_around_offset": {
            "positions": [
                "inside",
                "outside"
            ]
        },
        "no_spaces_inside_parenthesis": true,
        "no_superfluous_elseif": true,
        "no_superfluous_phpdoc_tags": {
            "allow_mixed": false,
            "allow_unused_params": false,
            "remove_inheritdoc": false
        },
        "no_trailing_comma_in_singleline": {
            "elements": [
                "group_import"
            ]
        },
        "no_trailing_whitespace": true,
        "no_trailing_whitespace_in_comment": true,
        "no_trailing_whitespace_in_string": true,
        "no_unneeded_control_parentheses": {
            "statements": [
                "break",
                "clone",
                "continue",
                "echo_print",
                "others",
                "return",
                "switch_case",
                "yield",
                "yield_from"
            ]
        },
        "no_unneeded_curly_braces": {
            "namespaces": false
        },
        "no_unneeded_final_method": {
            "private_methods": true
        },
        "no_unneeded_import_alias": true,
        "no_unreachable_default_argument_value": true,
        "no_unset_cast": true,
        "no_unset_on_property": true,
        "no_unused_imports": true,
        "no_useless_concat_operator": {
            "juggle_simple_strings": true
        },
        "no_useless_else": true,
        "no_useless_nullsafe_operator": true,
        "no_useless_return": true,
        "no_useless_sprintf": true,
        "no_whitespace_before_comma_in_array": {
            "after_heredoc": true
        },
        "no_whitespace_in_blank_line": true,
        "non_printable_character": {
            "use_escape_sequences_in_strings": false
        },
        "normalize_index_brace": true,
        "not_operator_with_space": false,
        "not_operator_with_successor_space": false,
        "nullable_type_declaration_for_default_null_value": true,
        "object_operator_without_whitespace": true,
        "octal_notation": true,
        "operator_linebreak": {
            "only_booleans": true,
            "position": "end"
        },
        "ordered_class_elements": {
            "sort_algorithm": "none",
            "order": [
                "use_trait",
                "case",
                "constant_public",
                "constant_protected",
                "constant_private",
                "property_static",
                "property",
                "property_public_static",
                "property_public_readonly",
                "property_public",
                "property_private_readonly",
                "property_private_static",
                "property_private",
                "property_protected_static",
                "property_protected_readonly",
                "property_protected",
                "construct",
                "destruct",
                "magic",
                "phpunit",
                "method_abstract",
                "method_static",
                "method",
                "method_public_abstract_static",
                "method_public_abstract",
                "method_protected_abstract_static",
                "method_protected_abstract",
                "method_private_abstract_static",
                "method_private_abstract",
                "method_public_static",
                "method_protected_static",
                "method_private_static",
                "method_public",
                "method_protected",
                "method_private"
            ]
        },
        "ordered_imports": {
            "sort_algorithm": "alpha",
            "imports_order": [
                "class",
                "function",
                "const"
            ]
        },
        "ordered_interfaces": {
            "direction": "ascend",
            "order": "alpha"
        },
        "ordered_traits": true,
        "php_unit_construct": {
            "assertions": [
                "assertEquals",
                "assertNotEquals",
                "assertNotSame",
                "assertSame"
            ]
        },
        "php_unit_dedicate_assert": {
            "target": "newest"
        },
        "php_unit_dedicate_assert_internal_type": {
            "target": "newest"
        },
        "php_unit_expectation": {
            "target": "newest"
        },
        "php_unit_fqcn_annotation": true,
        "php_unit_internal_class": true,
        "php_unit_method_casing": {
            "case": "camel_case"
        },
        "php_unit_mock": {
            "target": "newest"
        },
        "php_unit_mock_short_will_return": true,
        "php_unit_namespaced": {
            "target": "newest"
        },
        "php_unit_no_expectation_annotation": {
            "target": "newest"
        },
        "php_unit_set_up_tear_down_visibility": false,
        "php_unit_size_class": {
            "group": "medium"
        },
        "php_unit_strict": {
            "assertions": [
                "assertAttributeEquals",
                "assertAttributeNotEquals",
                "assertEquals",
                "assertNotEquals"
            ]
        },
        "php_unit_test_annotation": {
            "style": "prefix"
        },
        "php_unit_test_case_static_method_calls": {
            "call_type": "this",
            "methods": []
        },
        "php_unit_test_class_requires_covers": true,
        "phpdoc_add_missing_param_annotation": {
            "only_untyped": true
        },
        "phpdoc_align": {
            "align": "left",
            "tags": [
                "method",
                "param",
                "property",
                "property-read",
                "property-write",
                "return",
                "throws",
                "type",
                "var"
            ]
        },
        "phpdoc_annotation_without_dot": true,
        "phpdoc_indent": true,
        "phpdoc_inline_tag_normalizer": true,
        "phpdoc_line_span": {
            "const": "single",
            "property": "single",
            "method": "multi"
        },
        "phpdoc_no_access": true,
        "phpdoc_no_alias_tag": false,
        "phpdoc_no_empty_return": true,
        "phpdoc_no_package": true,
        "phpdoc_no_useless_inheritdoc": true,
        "phpdoc_order": {
            "order": [
                "param",
                "return",
                "throws"
            ]
        },
        "phpdoc_order_by_value": {
            "annotations": [
                "covers",
                "throws",
                "uses"
            ]
        },
        "phpdoc_return_self_reference": true,
        "phpdoc_scalar": {
            "types": [
                "boolean",
                "callback",
                "double",
                "integer",
                "real",
                "str"
            ]
        },
        "phpdoc_separation": true,
        "phpdoc_single_line_var_spacing": true,
        "phpdoc_summary": true,
        "phpdoc_tag_casing": true,
        "phpdoc_tag_type": true,
        "phpdoc_to_comment": true,
        "phpdoc_to_param_type": false,
        "phpdoc_to_property_type": false,
        "phpdoc_to_return_type": false,
        "phpdoc_trim": true,
        "phpdoc_trim_consecutive_blank_line_separation": true,
        "phpdoc_types": true,
        "phpdoc_types_order": {
            "null_adjustment": "always_last",
            "sort_algorithm": "none"
        },
        "phpdoc_var_annotation_correct_order": true,
        "phpdoc_var_without_name": true,
        "pow_to_exponentiation": true,
        "protected_to_private": true,
        "psr_autoloading": false,
        "random_api_migration": true,
        "regular_callable_call": true,
        "return_assignment": false,
        "return_type_declaration": {
            "space_before": "none"
        },
        "self_accessor": true,
        "self_static_accessor": true,
        "semicolon_after_instruction": true,
        "set_type_to_cast": true,
        "short_scalar_cast": true,
        "simple_to_complex_string_variable": true,
        "simplified_if_return": true,
        "simplified_null_return": false,
        "single_blank_line_at_eof": true,
        "single_blank_line_before_namespace": true,
        "single_class_element_per_statement": {
            "elements": [
                "const",
                "property"
            ]
        },
        "single_import_per_statement": true,
        "single_line_after_imports": true,
        "single_line_comment_spacing": true,
        "single_line_comment_style": {
            "comment_types": [
                "asterisk",
                "hash"
            ]
        },
        "single_line_throw": true,
        "single_quote": {
            "strings_containing_single_quote_chars": false
        },
        "single_space_after_construct": true,
        "single_trait_insert_per_statement": true,
        "space_after_semicolon": true,
        "standardize_increment": true,
        "standardize_not_equals": true,
        "statement_indentation": true,
        "static_lambda": false,
        "strict_comparison": true,
        "strict_param": true,
        "string_length_to_empty": true,
        "string_line_ending": true,
        "switch_case_semicolon_to_colon": true,
        "switch_case_space": true,
        "switch_continue_to_break": true,
        "ternary_operator_spaces": true,
        "ternary_to_elvis_operator": true,
        "ternary_to_null_coalescing": true,
        "trailing_comma_in_multiline": {
            "after_heredoc": false,
            "elements": [
                "arguments",
                "arrays",
                "match",
                "parameters"
            ]
        },
        "trim_array_spaces": true,
        "types_spaces": {
            "space": "none",
            "space_multiple_catch": "single"
        },
        "unary_operator_spaces": true,
        "use_arrow_functions": true,
        "visibility_required": true,
        "void_return": true,
        "whitespace_after_comma_in_array": true,
        "yoda_style": false
    }
}
